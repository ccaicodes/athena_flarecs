#include "copyright.h"
/*============================================================================*/
/*! \file fluxrope_symm.c
 *  \brief  Problem generator for Flux-rope.
 *
 * PURPOSE: flux rope.
 *
 * REFERENCE: Wang, Shen, & Lin, ApJ 2009.
 *
 */
/*============================================================================*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defs.h"
#include "athena.h"
#include "globals.h"
#include "prototypes.h"

/*==============================================================================
 * PRIVATE FUNCTION PROTOTYPES:
 * void flarecs_linetied() - sets BCs on R-x2 boundary
 *============================================================================*/
/* Functions for getting initial gas pressure and temperature */
static Real func_pini(const Real x1, const Real x2);
static Real func_teini(const Real x1, const Real x2);
static Real func_rhoini(const Real x1, const Real x2);

/* Functions for getting gravity */
static Real gsun_static(const Real x1, const Real x2, const Real x3);
static Real gsun_y(const Real x2);

/* Boundary conditions */
static void openbc_ix1(GridS *pGrid);
static void symmbc_ox1(GridS *pGrid);
static void linetiedbc_ix2(GridS *pGrid);
static Real func_pbypxini(Real x, Real y);
static void openbc_ox2(GridS *pGrid);

/* functions for compute flux */
static Real func_bphi(const Real r);
static Real func_rjphi(const Real r);
static Real func_back(const Real r);
static Real func_uphi(const Real r);

static Real func_bmx(const Real x1, const Real x2);
static Real func_bmy(const Real x1, const Real x2);

/* function to integrate initial az and pressure*/
static Real func_presini_isote(const Real x2, const Real y0,
                               const Real p0, const Real te0);
static Real func_presini_integral(Real x2);
static Real func_pini_bg(Real x2);
static Real func_bzini(const Real x1, const Real x2);

Real func_azini(Real x, Real y);
Real func_integ_py(Real y, const Real xfix);
Real func_integ_bx(Real y, const Real xfix);
Real func_integ_by(Real x, const Real yfix);
Real func_integ_pphi(Real r, const Real phi);

double adaptiveSimpsons(double (*f)(double, double), // ptr to function
                        double param,
                        double a, double b, // interval [a,b]
                        double epsilon,     // error tolerance
                        int maxRecursionDepth);
double adaptiveSimpsonsAux(double (*f)(double, double),
                           double param,
                           double a, double b,
                           double epsilon,
                           double S,
                           double fa, double fb, double fc, int bottom);

/* function to get initial beta0 */
static Real func_betaini(const Real B);
Real dx_grid, dy_grid, dz_grid;

/*============================================================================*/
/* Normalization parameters */
Real Lchar, Bchar, Nechar, Rhochar, Pchar, Timechar, Tchar, Vchar;
Real Gsun_nondim;

/* b. input parameters: height and width for the TR region */
Real Height_TR = 3.0e6, Width_TR = 1.0e6;

/* c. The height for define the pgas_ambient in corona */
Real pgas_ambient, posi_ambient, temp_ambient, rho_ambient, scale_ambient;

/* Constant parameters */
Real Kb = 1.38e-23, Mp = 1.6726e-27, Mu0 = 1.25663706144e-6, LnA = 30.0;
Real mu_tilde = 0.5;
Real R_tilde;                         // gas Constant
Real gravitational_const = 6.672e-11; /* (N M^2 kg^-2)*/
Real solarmass = 1.99e+30;            /* (kg) */
Real solarradius = 6.96e+8;           /* (m) */

/* Additional sources */
int sw_gravity, sw_cooling = 0, sw_bz = 1;
int sw_fluxrope = 1;

/* Initial conditions */
Real ***bx, ***by, ***bz;
Real ***az;

/* Flux parameters */
Real fr_bgpole; // 1 is for dipole and 2 is for quadrupole
Real fr_h, fr_d, fr_ri, fr_del, fr_rja, fr_rmom, fr_sigma;
Real fr_xc; // the center of flux rope.
Real beta_min;

/* Flux parameters to limit eta_ohm */
Real eta_ohm_origin;
Real fr_hy; // Flux rope height in y- direction
Real ***force_y;

/* Reset Output dt = dt_reset_out after time_reset_out */
Real dt_reset_out = 0.05, time_reset_out = 1.0;

/* Artificial pressure floor */
Real beta_floor;

/*=========================== PUBLIC FUNCTIONS ===============================*/
/*----------------------------------------------------------------------------*/
/* problem:  */
void problem(DomainS *pDomain)
{
  GridS *pGrid = (pDomain->Grid);
  int i, is = pGrid->is, ie = pGrid->ie;
  int j, js = pGrid->js, je = pGrid->je;
  int k, ks = pGrid->ks, ke = pGrid->ke;
  int n1 = ie - is + 1 + 2 * nghost;
  int n2 = je - js + 1 + 2 * nghost;
  int n3 = ke - ks + 1 + 2 * nghost;

  Real x1, x2, x3;
  Real x1c, x2c, x3c;
  Real x1f, x2f, x3f;
  Real x1s, x2s, x3s;
  Real r, r2;

  /* MPI info */
  int myid, nprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  /* Thermal conduction & viscosity */
  Real k_spitzer, kappa_v;
  Real nud, nu0;

  /* Magnitude of perturbations */
  Real pi;
  Real vin_turb;
  Real Lx, Ly, delta_bx, delta_by;
  Real pos_x_turb, pos_y_turb, hwd_x_turb, hwd_y_turb;

  /* Dense bottom boundary */
  Real htr, wtr, chr_rho, cor_rho;

  /* Normalizing parameters */
  R_tilde = Kb / Mp;
  Real T0 = 4.0e6; // (K)
  Real L0 = 1.0e8; // (m)
  //# If we want to Bchar = 20G, then we may set rho0 as:
  Real Btemp = par_getd("problem", "Bchar");
  Rhochar = pow(Btemp, 2) / (Gamma * R_tilde * Mu0 * T0);
  Nechar = Rhochar / Mp;
  Real cs = sqrt(Gamma * R_tilde * T0);
  Vchar = cs;
  Bchar = cs * sqrt(Rhochar * Mu0);
  Tchar = mu_tilde * Gamma * T0;
  Pchar = Gamma * R_tilde * Rhochar * T0;
  Lchar = L0;
  Timechar = Lchar / Vchar;

  /* Print initializing parameters */
  if (myid == 0)
  {
    printf("---------------\n");
    printf("Bchar=%10.3e (T)\n", Bchar);
    printf("nechar=%10.3e (cm^-3), Rhochar=%10.3e (kg/m^3)\n",
           Nechar / 1.0e6, Rhochar);
    printf("Pchar=%10.3e (Pa)\n", Pchar);
    printf("Tchar=%10.3e (K)\n", Tchar);
    printf("Vchar=%10.3e (km/s)\n", Vchar / 1.0e3);
    printf("Lchar=%10.3e (km)\n", Lchar / 1.0e3);
    printf("Timechar=%10.3e (s)\n", Timechar);
    printf("---------------\n");
  }

  /* Initialize flux-rope */
  sw_gravity = par_getd("problem", "sw_gravity");
  beta_floor = par_getd("problem", "beta_floor");
  fr_bgpole = par_getd("problem", "bgpole");
  fr_d = par_getd("problem", "d");
  fr_rja = par_getd("problem", "rja");
  fr_h = par_getd("problem", "h");
  fr_ri = par_getd("problem", "ri");
  fr_del = par_getd("problem", "del");
  if (fr_bgpole == 1)
  {
    fr_rmom = par_getd("problem", "M");
  }
  else if (fr_bgpole == 2)
  {
    fr_sigma = par_getd("problem", "sigma");
    fr_rmom = fr_d * fr_d * 125.0 / 32.0 * fr_sigma;
  }
  else
  {
    fr_rmom = 0;
  }

  /* Set ambient pressure and density */
  scale_ambient = par_getd("problem", "scale_ambient");
  posi_ambient = fr_h;
  rho_ambient = 1.0 * scale_ambient;
  pgas_ambient = 1.0 * scale_ambient / Gamma;
  temp_ambient = pgas_ambient / rho_ambient;

  // reset fr_h to the corner of cell
  k = (int)(fr_h / pGrid->dx2);
  fr_h = k * pGrid->dx2;
  fr_hy = fr_h;
  fr_xc = 0;

  if ((bx = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector bx\n");
  }
  if ((by = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector by\n");
  }
  if ((bz = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector bz\n");
  }
  if ((az = (Real ***)calloc_3d_array(n3 + 1, n2 + 1, n1 + 1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector az\n");
  }

  /* Compute initial magnetic vector potential: az */
  Real dxh = 0.5 * pGrid->dx1, dyh = 0.5 * pGrid->dx2;
  dx_grid = pGrid->dx1;
  dy_grid = pGrid->dx2;
  dz_grid = pGrid->dx3;

  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost; j <= je + nghost + 1; j++)
    {
      for (i = is - nghost; i <= ie + nghost + 1; i++)
      {
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
        x1f = x1c - 0.5 * dx_grid;
        x2f = x2c - 0.5 * dy_grid;

        if (i == ie + nghost + 1)
        {
          cc_pos(pGrid, ie + nghost, j, k, &x1c, &x2c, &x3c);
          x1f = x1c + 0.5 * dx_grid;
        }
        if (j == je + nghost + 1)
        {
          cc_pos(pGrid, i, je + nghost, k, &x1c, &x2c, &x3c);
          x2f = x2c + 0.5 * dy_grid;
        }
        az[k][j][i] = func_azini(x1f, x2f);
      }
    }
  }

  /* Set bx, by, and bz using Az */
  for (k = ks; k <= ke + 1; k++)
  {
    for (j = js; j <= je + 1; j++)
    {
      for (i = is; i <= ie + 1; i++)
      {
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
        bx[k][j][i] = (az[k][j + 1][i] - az[k][j][i]) / dy_grid;
        by[k][j][i] = -(az[k][j][i + 1] - az[k][j][i]) / dx_grid;
        bz[k][j][i] = func_bzini(x1c, x2c);
      }
    }
  }

  /* All variables */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);

        /* density */
        pGrid->U[k][j][i].d = func_rhoini(x1c, x2c);

        /* momentum */
        pGrid->U[k][j][i].M1 = 0.0;
        pGrid->U[k][j][i].M2 = 0.0;
        pGrid->U[k][j][i].M3 = 0.0;

        /* magnetic field */
        pGrid->B1i[k][j][i] = bx[k][j][i];
        pGrid->B2i[k][j][i] = by[k][j][i];
        pGrid->B3i[k][j][i] = bz[k][j][i];

        /* initial setting for magnetic field on the boundary */
        if (i == ie && ie > is)
          pGrid->B1i[k][j][i + 1] = bx[k][j][i + 1];
        if (j == je && je > js)
          pGrid->B2i[k][j + 1][i] = by[k][j + 1][i];
        if (k == ke && ke > ks)
          pGrid->B3i[k + 1][j][i] = bz[k + 1][j][i];
      }
    }
  }

  /* cell-center magnetic field */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        pGrid->U[k][j][i].B1c = 0.5 * (pGrid->B1i[k][j][i] +
                                       pGrid->B1i[k][j][i + 1]);
        pGrid->U[k][j][i].B2c = 0.5 * (pGrid->B2i[k][j][i] +
                                       pGrid->B2i[k][j + 1][i]);
        pGrid->U[k][j][i].B3c = pGrid->B3i[k][j][i];
      }
    }
  }

  /* total energy*/
  Real bsq, msq, pgas, beta_local_min;
  beta_local_min = 1.0e5;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
        bsq = 0.5 * (SQR(pGrid->U[k][j][i].B1c) + SQR(pGrid->U[k][j][i].B2c) + SQR(pGrid->U[k][j][i].B3c));
        msq = 0.5 * (SQR(pGrid->U[k][j][i].M1) + SQR(pGrid->U[k][j][i].M2) + SQR(pGrid->U[k][j][i].M3)) / pGrid->U[k][j][i].d;
        pgas = func_pini(x1c, x2c);
        pGrid->U[k][j][i].E = pgas / Gamma_1 + bsq + msq;
        beta_local_min = MIN(beta_local_min,
                             pgas / (0.5 * (SQR(pGrid->U[k][j][i].B1c) + SQR(pGrid->U[k][j][i].B2c))));
      }
    }
  }

  /* Sharing the beta_local_min and find beta_min over all parallel nodes */
  int icore;
  double *beta_min_buf = NULL;
  beta_min_buf = (double *)malloc(sizeof(double) * nprocs);

  MPI_Gather(&beta_local_min, 1, MPI_DOUBLE, beta_min_buf, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  if (myid == 0)
  {
    for (icore = 0; icore <= nprocs - 1; icore++)
    {
      if (beta_min_buf[icore] <= beta_local_min)
      {
        beta_local_min = beta_min_buf[icore];
      }
    }
    beta_min = beta_local_min;
    for (icore = 0; icore <= nprocs - 1; icore++)
    {
      beta_min_buf[icore] = beta_min;
    }
  }
  MPI_Scatter(beta_min_buf, 1, MPI_DOUBLE, &beta_min, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  free(beta_min_buf);

  // Add a safety factor
  beta_min = beta_min;

  /* Set resistivity */
#ifdef RESISTIVITY
  eta_ohm_origin = par_getd("problem", "eta_Ohm");
  eta_Ohm = eta_ohm_origin;
  Q_AD = par_getd("problem", "Q_AD");
  Q_Hall = 0.0;
  d_ind = 0.0;
#endif

  /* Set thermal conduction coefficient */
#ifdef THERMAL_CONDUCTION
  kappa_aniso = 1.0;
  kappa_iso = 0;
#endif

  /* Set viscosity 
#ifdef VISCOSITY
  // nu0 = 3.4e15 cm^2 s^-1, 
  nu_iso = 3.4e15*Timechar/(Lchar*1.0e2)/(Lchar*1.0e2);
  if (myid == 0) printf("Non-dimensional nu_iso=%10.3e\n", nu_iso);
  nu_aniso = 0.0;
#endif */

  /* Enroll gravitational potential to give acceleration in y-direction for 2D */
  if (sw_gravity == 1)
    StaticGravPot = gsun_static;

  /* Set optical thin radiation cooling and corona heating function */

  /* Set boundary value functions */
  /* (a) left-open */
  bvals_mhd_fun(pDomain, left_x1, openbc_ix1);

  /* (b) Right-symmetric */
  bvals_mhd_fun(pDomain, right_x1, symmbc_ox1);

  /* (c) Top-open */
  bvals_mhd_fun(pDomain, right_x2, openbc_ox2);

  /* (d) Bottom-line-tied */
  bvals_mhd_fun(pDomain, left_x2, linetiedbc_ix2);

  /* Print out initial parameters */
  if (myid == 0)
  {
    printf("---------------\n");
    printf("h = %f\n", fr_h);
    printf("d = %f\n", fr_d);
    printf("ri = %f\n", fr_ri);
    printf("del= %f\n", fr_del);
    printf("rmom=%f\n", fr_rmom);
    printf("rja =%f\n", fr_rja);
    printf("beta_min=%f\n", beta_min);

    printf("posi_ambient=%f, %10.3e(Mm)\n",
           posi_ambient, posi_ambient * Lchar / 1.0e6);
    printf("pgas_ambient=%f, %10.3e(Pa)\n",
           pgas_ambient, pgas_ambient * Pchar);
    printf("rho_ambient=%f, %10.3e(cm^-3)\n",
           rho_ambient, rho_ambient * Nechar * 1.0e-6);
    printf("T_ambient=%f, %10.3e(K)\n",
           temp_ambient, temp_ambient * Tchar);
    printf("---------------\n");
  }
}

/*==============================================================================
 * PROBLEM USER FUNCTIONS:
 * problem_write_restart() - writes problem-specific user data to restart files
 * problem_read_restart()  - reads problem-specific user data from restart files
 * get_usr_expr()          - sets pointer to expression for special output data
 * get_usr_out_fun()       - returns a user defined output function pointer
 * get_usr_par_prop()      - returns a user defined particle selection function
 * Userwork_in_loop        - problem specific work IN     main loop
 * Userwork_after_loop     - problem specific work AFTER  main loop
 *----------------------------------------------------------------------------*/

void problem_write_restart(MeshS *pM, FILE *fp)
{
  return;
}

void problem_read_restart(MeshS *pM, FILE *fp)
{
  return;
}

/*! \fn static Real ohmiceta(const GridS *pG, const int i, const int j, const int k)
 *  \brief return  ohmiceta*/
static Real out_ohmiceta(const GridS *pG, const int i, const int j, const int k)
{
  Real eta;
  eta = 0.0;
#ifdef RESISTIVITY
  eta = pG->eta_Ohm[k][j][i];
#endif
  return eta;
}

ConsFun_t get_usr_expr(const char *expr)
{
  if (strcmp(expr, "Ohmiceta") == 0)
    return out_ohmiceta;
  return NULL;
}

VOutFun_t get_usr_out_fun(const char *name)
{
  return NULL;
}

#ifdef RESISTIVITY

void get_eta_user(GridS *pG, int i, int j, int k,
                  Real *eta_O, Real *eta_H, Real *eta_A)
{
  /* eta */
  *eta_O = eta_ohm_origin;
  *eta_H = 0.0;
  *eta_A = 0.0;

  /* no resistivity inside the flux-rope 
  Real x1c, x2c, x3c;
  Real r;
  Real fgauss, r_fluxrope = fr_ri+3.0*fr_del, fhw=r_fluxrope;
  cc_pos(pG, i, j, k, &x1c, &x2c, &x3c);
  r = sqrt(pow(x1c - fr_xc, 2) + pow(x2c - fr_hy, 2));
  if (r <= r_fluxrope) {
    fgauss = -1.0;
  } else {
    fgauss = -exp(-pow(r-r_fluxrope, 2)/pow(fhw, 2));
  }
  //eta_O = eta_ohm_origin*(1.0+fgauss);
  */

  *eta_O = eta_ohm_origin;
  return;
}
#endif

void Userwork_in_loop(MeshS *pM)
{
  GridS *pG = pM->Domain[0][0].Grid;
  int is = pG->is, ie = pG->ie;
  int js = pG->js, je = pG->je;
  int ks = pG->ks, ke = pG->ke;
  int n1 = ie - is + 1 + 2 * nghost;
  int n2 = je - js + 1 + 2 * nghost;
  int n3 = ke - ks + 1 + 2 * nghost;
  int i, j, k;
  Real x1, x2, x3;
  Real msqr, bsqr, pres_ori, pres_new, pmag, beta_total, beta_avg;
  Real pres_cell[n3][n2][n1], beta_cell[n3][n2][n1];

  /* (1) In 2D cases, insure vz = 0 */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        pG->U[k][j][i].E = pG->U[k][j][i].E - 0.5 * SQR(pG->U[k][j][i].M3) / pG->U[k][j][i].d;
        pG->U[k][j][i].M3 = 0.0;
      }
    }
  }

  /* (2) Pressure floor for low beta case  */
  Real pres_floor = 1.0e-10;
  Real betal, betar, betau, betad;
  int ii, jj, ncell, idis, ndis = 4;
  if (beta_floor > 0) {
    // Get gas pressure and plasma beta at each cell
    for (k = ks; k <= ke; k++)
    {
      for (j = js; j <= je; j++)
      {
        for (i = is; i <= ie; i++)
        {
          msqr = SQR(pG->U[k][j][i].M1) + SQR(pG->U[k][j][i].M2) + SQR(pG->U[k][j][i].M3);
          bsqr = SQR(pG->U[k][j][i].B1c) + SQR(pG->U[k][j][i].B2c) + SQR(pG->U[k][j][i].B3c);
          pres_ori = Gamma_1 * (pG->U[k][j][i].E - 0.5 * msqr / pG->U[k][j][i].d - 0.5 * bsqr);
          pres_cell[k][j][i] = pres_ori;
          beta_cell[k][j][i] = pres_ori / (0.5 * bsqr);
        }
      }
    }

    // Check beta status, and reset pressure and density
    for (k = ks; k <= ke; k++)
    {
      for (j = js; j <= je; j++)
      {
        for (i = is; i <= ie; i++)
        {

          cc_pos(pG, i, j, k, &x1, &x2, &x3);
          if (beta_cell[k][j][i] < beta_floor)
          {
            //
            // Or: perform 2D/3D interpolation to get estimated values ...
            //
            beta_avg = 0;
            idis = 1;
            while ((idis < ndis) && (beta_avg <= 0))
            {
              betal = beta_cell[k][j][i + idis];
              betar = beta_cell[k][j][i - idis];
              betau = beta_cell[k][j + idis][i];
              betad = beta_cell[k][j - idis][i];
              if ((betal >= beta_floor) && (betar >= beta_floor))
              {
                beta_avg = 0.5 * (betal + betar);
              }
              else if ((betau >= beta_floor) && (betad >= beta_floor))
              {
                beta_avg = 0.5 * (betau + betad);
              }
              else
              {
                idis = idis + 1;
              }
            }

            if (beta_avg > 0)
            {
              pmag = 0.5 * (SQR(pG->U[k][j][i].B1c) + SQR(pG->U[k][j][i].B2c) + SQR(pG->U[k][j][i].B3c));
              pres_new = beta_avg * pmag;
            }
            else
            {
              pres_new = pres_floor;
            }

            // Update the error point
            if (pres_new >= pres_cell[k][j][i])
            {
              printf("beta=%f, beta_avg=%f, Original pres=%f, Modified pres=%f, idis=%d, at %f, %f, P=%f, %f, %f, %f\n",
                     beta_cell[k][j][i], beta_avg,
                     pres_cell[k][j][i], pres_new, idis,
                     x1, x2,
                     pres_cell[k][j + idis][i], pres_cell[k][j - idis][i],
                     pres_cell[k][j][i + idis], pres_cell[k][j][i - idis]);
              msqr = SQR(pG->U[k][j][i].M1) + SQR(pG->U[k][j][i].M2) + SQR(pG->U[k][j][i].M3);
              bsqr = SQR(pG->U[k][j][i].B1c) + SQR(pG->U[k][j][i].B2c) + SQR(pG->U[k][j][i].B3c);
              pG->U[k][j][i].E = pres_new / Gamma_1 + 0.5 * msqr / pG->U[k][j][i].d + 0.5 * bsqr;
            }
          }
        }
      }
    }
  }

  /* (3) Obtain the current fr_hy depending time 
  Real x1c, x2c, x3c;
  float fr_hy_temp;
  // (3.1) Get the fr_hy in each node including unvalid values
  fr_hy_temp = -1.0;
  if (pG->MaxX[0] == 0) {
    for (k = ks; k <= ke; k++) {
      j = je;
      while ((pG->B1i[k][j][ie+1] < 0.0) && (j >= js)) {
        j = j - 1;
      }
      if (pG->B1i[k][j][ie+1] > 0.0) {
        cc_pos(pG, ie+1, j, k, &x1c, &x2c, &x3c);
        fr_hy_temp = x2c + 0.5*pG->dx2;
      }
    }
  }
  // (3.2) fr_hy_temp and find the valid one
  int myid, nprocs, j_node;
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  float *fr_hy_buf = NULL;
  float fr_hy_valid;
  fr_hy_buf = (float *)malloc(sizeof(float) * nprocs);

  MPI_Gather(&fr_hy_temp, 1, MPI_FLOAT, fr_hy_buf, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
  if (myid == 0) {
    float fr_hy_max;
    fr_hy_max = 0.0;
    for (j_node=0; j_node<=nprocs-1; j_node++) {
      if (fr_hy_buf[j_node] >= fr_hy_max) {
        fr_hy_max = fr_hy_buf[j_node];
      }
    }
    for (j_node=0; j_node<=nprocs-1; j_node++) {
      fr_hy_buf[j_node] = fr_hy_max;
    }
  }
  MPI_Scatter(fr_hy_buf, 1, MPI_FLOAT, &fr_hy_temp, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
  free(fr_hy_buf);
  // (3.3) Update fr_hy
  fr_hy = fr_hy_temp;
  //printf("fr_hy=%f\n", fr_hy);
  */

  return;
}

void Userwork_after_loop(MeshS *pM)
{
  return;
}

/*=========================== PRIVATE FUNCTIONS ==============================*/
/*----------------------------------------------------------------------------*/
/*! \fn void linetiedbc_ix2(GridS *pGrid)
 *  \brief Sets boundary condition at the bottom.
 */
/*  ix2, line-tied, bottom */
void linetiedbc_ix2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
  Real x1c, x2c, x3c;
  Real x1f, x2f;
  Real x1_js, x2_js, x3_js;

#ifdef MHD
  int ku; /* k-upper */
#endif

  /* Set all variables in ghost zone */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i] = pGrid->U[k][js][i];
        pGrid->U[k][js - j][i].M1 = 0.0;
        pGrid->U[k][js - j][i].M2 = 0.0;
        pGrid->U[k][js - j][i].M3 = 0.0;
      }
    }
  }

#ifdef MHD
  /* B1i is not set at i=is-nghost */
  Real pbypx;
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - (nghost - 1); i <= ie + nghost; i++)
      {
        cc_pos(pGrid, i, js - j + 1, k, &x1c, &x2c, &x3c);
        x1f = x1c - 0.5 * pGrid->dx1;
        x2f = x2c - 0.5 * pGrid->dx2;
        pbypx = func_pbypxini(x1f, x2f);
        pGrid->B1i[k][js - j][i] = pGrid->B1i[k][js - j + 1][i] - pbypx * (pGrid->dx2);
      }
    }
  }
  /* B2i is not set at j=js-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B2i[k][js - j][i] = -(az[k][js - j][i + 1] - az[k][js - j][i]) / pGrid->dx1;
      }
    }
  }
  /* B3i */
  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B3i[k][js - j][i] = pGrid->B3i[k][js][i];
      }
    }
  }

  /* B1c, 2c, 3c */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - (nghost - 1); i <= ie + (nghost - 1); i++)
      {
        pGrid->U[k][js - j][i].B1c = 0.5 * (pGrid->B1i[k][js - j][i] + pGrid->B1i[k][js - j][i + 1]);
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i].B2c = 0.5 * (pGrid->B2i[k][js - j][i] + pGrid->B2i[k][js - j + 1][i]);
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i].B3c = pGrid->B3i[k][js - j][i];
      }
    }
  }

#endif /* MHD */

  /* Pressure and Total Energy */
  if (sw_gravity == 1)
  {
    int j_mj;
    Real p_js, t_js, eb_js, ek_js;
    Real p_mj, t_mj, eb_mj, ek_mj;
    for (k = ks; k <= ke; k++)
    {
      for (j = 1; j <= nghost; j++)
      {
        for (i = is - nghost; i <= ie + nghost; i++)
        {
          // Get js line
          cc_pos(pGrid, i, js, k, &x1_js, &x2_js, &x3_js);
          eb_js = 0.5 * (SQR(pGrid->U[k][js][i].B1c) + SQR(pGrid->U[k][js][i].B2c) + SQR(pGrid->U[k][js][i].B3c));
          ek_js = 0.5 * (SQR(pGrid->U[k][js][i].M1) + SQR(pGrid->U[k][js][i].M2) + SQR(pGrid->U[k][js][i].M3)) / pGrid->U[k][js][i].d;
          p_js = (pGrid->U[k][js][i].E - eb_js - ek_js) * Gamma_1;
          j_mj = js - j;
          cc_pos(pGrid, i, j_mj, k, &x1c, &x2c, &x3c);

          // fixed boundary
          p_mj = func_pini(x1c, x2c);
          pGrid->U[k][j_mj][i].d = func_rhoini(x1c, x2c);
          eb_mj = 0.5 * (SQR(pGrid->U[k][j_mj][i].B1c) + SQR(pGrid->U[k][j_mj][i].B2c) + SQR(pGrid->U[k][j_mj][i].B3c));
          pGrid->U[k][j_mj][i].E = p_mj / Gamma_1 + eb_mj;
        }
      }
    }
  }
  return;
}

static Real func_pbypxini(Real x, Real y)
{
  Real pbypx;
  pbypx = (func_bmy(x + 1.0e-7, y) - func_bmy(x - 1.0e-7, y)) / 2.0e-7;
  return pbypx;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void openbc_ox2(GridS *pGrid)
 *  \brief open boundary conditions, Outer x2 boundary (bc_ox2=2) */
/*----------------------------------------------------------------------------*/
/*  ox2, top boundary */
static void openbc_ox2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ku; /* k-upper */
#endif
  int jc;

  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][je + j][i] = pGrid->U[k][je - j + 1][i];
      }
    }
  }

#ifdef MHD
  /* B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B1i[k][je + j][i] = 2.0 * pGrid->B1i[k][je + j - 1][i] - pGrid->B1i[k][je + j - 2][i];
      }
    }
  }
  /* j=je+1 is not a boundary condition for the interface field B2i */
  for (k = ks; k <= ke; k++)
  {
    for (j = 2; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost - 1; i++)
      {
        pGrid->B2i[k][je + j][i] = -(pGrid->dx2 / pGrid->dx1) * (pGrid->B1i[k][je + j - 1][i + 1] - pGrid->B1i[k][je + j - 1][i]) + pGrid->B2i[k][je + j - 1][i];
      }
      pGrid->B2i[k][je + j][i + nghost] = pGrid->B2i[k][je + j][i + nghost - 1];
    }
  }
  /* B3i */
  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B3i[k][je + j][i] = 2.0 * pGrid->B3i[k][je + j - 1][i] - pGrid->B3i[k][je + j - 2][i];
      }
    }
  }

  /* B1c, B2c, and B3c */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost - 1; i++)
      {
        pGrid->U[k][je + j][i].B1c = 0.5 * (pGrid->B1i[k][je + j][i] + pGrid->B1i[k][je + j][i + 1]);
      }
      pGrid->U[k][je + nghost][i].B1c = pGrid->U[k][je + j][ie + nghost - 1].B1c;
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost - 1; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][je + j][i].B2c = 0.5 * (pGrid->B2i[k][je + j][i] + pGrid->B2i[k][je + j + 1][i]);
      }
    }
  }
  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][je + j][i].B3c = pGrid->B3i[k][je + j][i];
      }
    }
  }

#endif /* MHD */

  /* Pressure and energy */
  Real eb_je, ek_je;
  Real pje, Tje;
  Real pjc;
  Real x1c, x2c, x3c, yjc, yje;
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        eb_je = 0.5 * (SQR(pGrid->U[k][je][i].B1c) + SQR(pGrid->U[k][je][i].B2c) + SQR(pGrid->U[k][je][i].B3c));
        ek_je = 0.5 * (SQR(pGrid->U[k][je][i].M1) + SQR(pGrid->U[k][je][i].M2) + SQR(pGrid->U[k][je][i].M3)) / pGrid->U[k][je][i].d;
        pje = Gamma_1 * (pGrid->U[k][je][i].E - eb_je - ek_je);

        jc = je + j;
        if (sw_gravity == 1)
        {
          Tje = pje / pGrid->U[k][je][i].d;
          cc_pos(pGrid, i, je, k, &x1c, &yje, &x3c);
          cc_pos(pGrid, i, jc, k, &x1c, &yjc, &x3c);
          pjc = func_presini_isote(yjc, yje, pje, Tje);
        }
        else
        {
          pjc = pje;
        }

        pGrid->U[k][jc][i].E = pjc / (Gamma_1) + 0.5 * (SQR(pGrid->U[k][jc][i].B1c) + SQR(pGrid->U[k][jc][i].B2c) + SQR(pGrid->U[k][jc][i].B3c)) + 0.5 * (SQR(pGrid->U[k][jc][i].M1) + SQR(pGrid->U[k][jc][i].M2) + SQR(pGrid->U[k][jc][i].M3)) / pGrid->U[k][jc][i].d;
      }
    }
  }
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void openbc_ix1(GridS *pGrid)
 *  \brief open boundary condition, Inner x1 boundary (bc_ix1=2) */
/*----------------------------------------------------------------------------*/
/*  ix1, Left boundary */
static void openbc_ix1(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][is - i] = pGrid->U[k][j][is + i - 1];
      }
    }
  }

#ifdef MHD
  /* B2i: */
  if (pGrid->Nx[1] > 1)
    ju = je + 1;
  else
    ju = je;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= ju; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B2i[k][j][is - i] = 2.0 * pGrid->B2i[k][j][is - i + 1] - pGrid->B2i[k][j][is - i + 2];
      }
    }
  }

  /* B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost - 1; i++)
      {
        pGrid->B1i[k][j][is - i] = pGrid->B1i[k][j][is - i + 1] + (pGrid->dx1 / pGrid->dx2) * (pGrid->B2i[k][j + 1][is - i] - pGrid->B2i[k][j][is - i]);
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B3i[k][j][is - i] = pGrid->B3i[k][j][is + i - 1];
      }
    }
  }
#endif /* MHD */

  /* B1c, 2c, 3c */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost - 1; i++)
      {
        pGrid->U[k][j][is - i].B1c = 0.5 * (pGrid->B1i[k][j][is - i] + pGrid->B1i[k][j][is - i + 1]);
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][is - i].B2c = 0.5 * (pGrid->B2i[k][j][is - i] + pGrid->B2i[k][j + 1][is - i]);
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][is - i].B3c = pGrid->B3i[k][j][is - i];
      }
    }
  }

  /* Pressure and energy */
  int ic, im;
  Real eb_im, ek_im, pis_o_gm;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        im = is + i - 1;
        eb_im = 0.5 * (SQR(pGrid->U[k][j][im].B1c) + SQR(pGrid->U[k][j][im].B2c) + SQR(pGrid->U[k][j][im].B3c));
        ek_im = 0.5 * (SQR(pGrid->U[k][j][im].M1) + SQR(pGrid->U[k][j][im].M2) + SQR(pGrid->U[k][j][im].M3)) / pGrid->U[k][j][im].d;
        pis_o_gm = pGrid->U[k][j][im].E - eb_im - ek_im;

        ic = is - i;
        pGrid->U[k][j][ic].E = pis_o_gm + 0.5 * (SQR(pGrid->U[k][j][ic].B1c) + SQR(pGrid->U[k][j][ic].B2c) + SQR(pGrid->U[k][j][ic].B3c)) + 0.5 * (SQR(pGrid->U[k][j][ic].M1) + SQR(pGrid->U[k][j][ic].M2) + SQR(pGrid->U[k][j][ic].M3)) / pGrid->U[k][j][ic].d;
      }
    }
  }

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void symmbc_ox1(GridS *pGrid)
 *  \brief OUTFLOW boundary conditions, Outer x1 boundary (bc_ox1=2) */
static void symmbc_ox1(GridS *pGrid)
{
  int ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][ie + i] = pGrid->U[k][j][ie - i + 1];
        pGrid->U[k][j][ie + i].B2c = -pGrid->U[k][j][ie - i + 1].B2c;
        pGrid->U[k][j][ie + i].M1 = -pGrid->U[k][j][ie - i + 1].M1;
      }
    }
  }

#ifdef MHD
  /* i=ie+1 is not a boundary condition for the interface field B1i */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 2; i <= nghost; i++)
      {
        pGrid->B1i[k][j][ie + i] = pGrid->B1i[k][j][ie - i + 2];
      }
    }
  }

  if (pGrid->Nx[1] > 1)
    ju = je + 1;
  else
    ju = je;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= ju; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B2i[k][j][ie + i] = -pGrid->B2i[k][j][ie - i + 1];
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B3i[k][j][ie + i] = pGrid->B3i[k][j][ie - i + 1];
      }
    }
  }
#endif /* MHD */

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static Real gsun_static(const Real x1, const Real x2, const Real x3)
 *  \brief Gravitational potential; g = 0.1
 */
static Real gsun_static(const Real x1, const Real x2, const Real x3)
{
  Real gp, gpnondim;
  Real safety = 1.0;
  /* gravity potential (m^2/s^2) */
  gp = -(gravitational_const * solarmass) / (x2 * Lchar + solarradius);
  /* Non-dimensional gp */
  //gpnondim = gp * Rhochar / Pchar;
  gpnondim = gp / pow(Vchar, 2);
  return gpnondim;
}

/*----------------------------------------------------------------------------*/
/*! \fn static Real gsun_r(const Real x2)
 *  \brief Non-dimensional Gravity at height x2
 */
static Real gsun_y(const Real x2)
{
  Real g, gnondim;
  Real r;
  r = solarradius + x2 * Lchar;                    /* (m) */
  g = (gravitational_const * solarmass) / (r * r); /* (m s^-2)*/
  //gnondim = g * Rhochar * Lchar / Pchar;
  gnondim = g * Lchar / pow(Vchar, 2);
  return gnondim;
}

/*----------------------------------------------------------------------------*/
/*  \fn gas pressure assuming constant temperature */
static Real func_presini_isote(const Real x2, const Real y0,
                               const Real p0, const Real te0)
{
  Real pgas, gm, r, r0;
  if (sw_gravity == 1)
  {
    gm = gravitational_const * solarmass * Rhochar / (Lchar * Pchar);
    r = solarradius / Lchar + x2;
    r0 = solarradius / Lchar + y0;
    pgas = p0 * exp(gm / te0 * (1.0 / r - 1.0 / r0));
  }
  else
  {
    pgas = p0;
  }
  return pgas;
}

/*----------------------------------------------------------------------------*/
/*  \fn gas pressure performing numerical integration */
static Real func_presini_integral(Real x2)
{
  Real pgas;
  Real Iout_simp;

  if (sw_gravity == 1)
  {
    Iout_simp = adaptiveSimpsons(func_integ_py, 0, posi_ambient, x2, 1.0e-9, 100000);
    pgas = pgas_ambient * exp(Iout_simp);
  }
  else
  {
    pgas = pgas_ambient;
  }
  return pgas;
}

/*----------------------------------------------------------------------------*/
/*  \fn Initial Gas pressure */
static Real func_pini(const Real x1, const Real x2)
{
  Real p, r, pfluxrope;
  r = sqrt(pow(x1, 2) + pow(x2 - fr_h, 2));
  if (sw_bz == 0)
  {
    pfluxrope = -func_uphi(r);
  }
  else
  {
    pfluxrope = 0;
  }

  if (sw_gravity == 1)
  {
    p = func_presini_integral(x2);
  }
  else
  {
    p = pgas_ambient;
  }
  return p;
}

/*----------------------------------------------------------------------------*/
/*  \fn Initial Temperature */
static Real func_teini(const Real x1, const Real x2)
{
  /* Non-dimensional variables */
  Real techr, tecor, h, w;
  Real t1, t2, te;

  if (sw_gravity)
  {
    /* Temperature distribution in y-direction */
    /* The following calculation is in non-dimensional forms */
    techr = 0.025 * temp_ambient;
    tecor = temp_ambient;
    h = Height_TR / Lchar;
    w = Width_TR / Lchar;
    t1 = 0.5 * (tecor - techr);
    t2 = 0.5 * (tecor + techr);
    te = t1 * tanh((x2 - h) / w) + t2;
  }
  else
  {
    te = tecor;
  }
  return te;
}

/*----------------------------------------------------------------------------*/
/*  \fn Initial density */
static Real func_rhoini(const Real x1, const Real x2)
{
  Real rho, p, te;
  // Get gas pressure and temperature
  p = func_pini(x1, x2);
  te = func_teini(x1, x2);

  /* Add a dense (low temperature) fluxrope core
  Real pi = 3.14159265358979;
  Real r, r1, r2, r_edge;
  Real t_inner, t_outer;
  Real bphi2, bphi2_max, ratio;
  t_outer = te;
  t_inner = 0.01 * t_outer;
  r2 = fr_ri - 0.5 * fr_del;
  r_edge = fr_del;
  r = sqrt(x1 * x1 + pow(x2 - fr_h, 2));
  if (r <= r2)
  {
    //te = t_inner + 0.5*(t_outer-t_inner)*(1.0-cos(pi*(r-r2)/r2));
    te = t_inner;
  }
  else if (r <= r2 + r_edge)
  {
    te = t_inner + 0.5 * (t_outer - t_inner) * (1.0 - cos(pi * (r - r2) / r_edge));
  }
  else
  {
    te = te;
  }
  */

  // Get non-dimensional density
  rho = p / te;
  return rho;
}

/*----------------------------------------------------------------------------*/
/*  \fn function: initial az(x, y) */
Real func_azini(Real x, Real y)
{
  Real Ixx, Iyy, az_out;
  Real az0 = 0, x0 = 0.5, y0 = 0.5;
  // Integrate (y=y0 -> y) at x = x0 in the y-direction
  Iyy = adaptiveSimpsons(func_integ_bx,
                         x0,
                         y0, y, 1.0e-9, 1000000);

  // Integrate (x=x0 -> x) at y = y in the x-direction
  Ixx = adaptiveSimpsons(func_integ_by,
                         y,
                         x0, x, 1.0e-9, 1000000);

  az_out = Ixx + Iyy;
  return az_out;
}

/*----------------------------------------------------------------------------*/
/*  \fn Initial bz */
static Real func_bzini(const Real x1, const Real x2)
{
  Real bz, r;
  r = sqrt(pow(x1, 2) + pow(x2 - fr_h, 2));
  if (sw_bz == 1)
  {
    bz = sqrt(2.0 * (-func_uphi(r)));
  }
  else
  {
    bz = 0;
  }
  return bz;
}

/* ============================================================================
 * Functions for flux-rope
 * ===========================================================================*/
/*----------------------------------------------------------------------------*/
static Real func_bmx(const Real x, const Real y)
{
  /*c model field x-component */
  Real rs, rm, rb, r2;
  Real bmx, bmx_back, bmx_fluxrope;

  rs = sqrt(pow(x - fr_xc, 2) + pow(y - fr_h, 2));
  rm = sqrt(pow(x - fr_xc, 2) + pow(y + fr_h, 2));
  rb = sqrt(pow(x - fr_xc, 2) + pow(y + fr_d, 2));
  r2 = fr_ri + 0.5 * fr_del;

  // Get bmx_background
  if (fr_bgpole == 1)
  {
    // dipole case
    bmx_back = -func_bphi(r2) * fr_d * fr_rmom * r2 * (x * x - (y + fr_d) * (y + fr_d)) / pow(rb, 4);
  }
  else if (fr_bgpole == 2)
  {
    // quadrupole
    bmx_back = -fr_rmom * func_back(rb) * (pow(y + fr_d, 3) - 3.0 * (y + fr_d) * pow(x, 2)) / pow(rb, 3);
  }
  else
  {
    bmx_back = 0;
  }

  // Get bmx_fluxrope (and its mirror)
  if (rs > 0.0)
  {
    bmx_fluxrope = func_bphi(rs) * (y - fr_h) / rs - func_bphi(rm) * (y + fr_h) / rm;
  }
  else
  {
    bmx_fluxrope = 0.0;
  }

  bmx = bmx_back + bmx_fluxrope;
  return bmx;
}

/*----------------------------------------------------------------------------*/
static Real func_bmy(const Real x, const Real y)
{
  /*  model field y-component */
  Real rs, rm, rb, r2;
  Real bmy, bmy_fluxrope, bmy_back;

  rs = sqrt(pow(x - fr_xc, 2) + pow(y - fr_h, 2));
  rm = sqrt(pow(x - fr_xc, 2) + pow(y + fr_h, 2));
  rb = sqrt(pow(x - fr_xc, 2) + pow(y + fr_d, 2));
  r2 = fr_ri + 0.5 * fr_del;

  // Get bmy_background
  if (fr_bgpole == 1)
  {
    // dipole case
    bmy_back = -func_bphi(r2) * fr_rmom * fr_d * r2 * (2.0 * x * (y + fr_d)) / pow(rb, 4);
  }
  else if (fr_bgpole == 2)
  {
    // quadrupole case
    bmy_back = -fr_rmom * (func_back(rb)) * (pow(x, 3) - 3.0 * x * pow((y + fr_d), 2)) / pow(rb, 3);
  }
  else
  {
    bmy_back = 0;
  }

  // Get flux-rope (and its mirror)
  if (rs > 0.0)
  {
    bmy_fluxrope = -func_bphi(rs) * x / rs + func_bphi(rm) * x / rm;
  }
  else
  {
    bmy_fluxrope = 0.0;
  }

  bmy = bmy_back + bmy_fluxrope;
  return bmy;
}

/*----------------------------------------------------------------------------*/
static Real func_bphi(const Real r)
{
  /* cylindrical field function */
  Real riq, delq, piq, t1, t2, t3, bphi;
  Real pi = 3.14159265358979;
  Real ro, r1, r2;

  r1 = fr_ri - 0.5 * fr_del;
  r2 = fr_ri + 0.5 * fr_del;

  riq = fr_ri * fr_ri;
  delq = fr_del * fr_del;
  piq = pi * pi;

  if (r <= r1)
  {
    bphi = -0.5 * fr_rja * r;
  }
  else if (r <= r2)
  {
    t1 = 0.5 * r1 * r1 - delq / piq + 0.5 * r * r;
    t2 = (fr_del * r / pi) * sin((pi / fr_del) * (r - r1));
    t3 = (delq / piq) * cos((pi / fr_del) * (r - r1));
    bphi = -0.5 * fr_rja * (t1 + t2 + t3) / r;
  }
  else
  {
    bphi = -0.5 * fr_rja * (riq + 0.25 * delq - 2. * delq / piq) / r;
  }
  return bphi;
}

/*----------------------------------------------------------------------------*/
static Real func_back(const Real r)
{
  Real riq, delq, piq, rmm, back;
  Real pi = 3.14159265358979;
  riq = pow(fr_ri, 2);
  delq = pow(fr_del, 2);
  piq = pow(pi, 2);
  rmm = 0.5 * fr_rja * (riq + 0.25 * delq - 2. * delq / piq);
  back = rmm / pow(r, 3);
  return back;
}

/*----------------------------------------------------------------------------*/
static Real func_rjphi(const Real r)
{
  /*  current density */
  Real pi = 3.14159265358979;
  Real ro, r1, r2;
  Real rjphi;

  r1 = fr_ri - 0.5 * fr_del;
  r2 = fr_ri + 0.5 * fr_del;

  if (r <= r1)
  {
    rjphi = 1.0 * fr_rja;
  }
  else if (r <= r2)
  {
    rjphi = 0.5 * fr_rja * (cos((pi / fr_del) * (r - r1)) + 1.);
  }
  else
  {
    rjphi = 0.;
  }
  return rjphi;
}

static Real func_uphi(const Real r)
{
  Real uphi;
  Real rend = fr_ri + fr_del;
  if (r < rend)
  {
    uphi = adaptiveSimpsons(func_integ_pphi,
                            0,
                            r, rend, 1.0e-9, 1000000);
  }
  else
  {
    uphi = 0;
  }
  return uphi;
}

static Real func_betaini(const Real B)
{
  Real r, pmag, pgas;
  Real riq, delq, piq;
  Real pi = 3.14159265358979;

  // Get r from B
  riq = fr_ri * fr_ri;
  delq = fr_del * fr_del;
  piq = pi * pi;
  r = fabs(0.5 * fr_rja * (riq + 0.25 * delq - 2. * delq / piq) / B);
  r = MAX(fr_ri + 0.5 * fr_del, r);

  // Get pmag and pgas
  pmag = 0.5 * pow(func_bphi(r), 2);
  pgas = -func_uphi(r) + pgas_ambient;

  return pgas / pmag;
};

//==============================================================================
// function: integrations
//==============================================================================
Real func_integ_bx(Real y, const Real xfix)
{
  return func_bmx(xfix, y);
}

Real func_integ_by(Real x, const Real yfix)
{
  return -func_bmy(x, yfix);
}

Real func_integ_pphi(Real r, const Real phi)
{
  return func_rjphi(r) * func_bphi(r);
}

Real func_integ_py(Real y, const Real xfix)
{
  return -gsun_y(y) / func_teini(0.0, y);
}

//==============================================================================
// Adaptive Simpson's Rule
//==============================================================================
double adaptiveSimpsons(double (*f)(double, double), // ptr to function
                        double param,
                        double a, double b, // interval [a,b]
                        double epsilon,     // error tolerance
                        int maxRecursionDepth)
{ // recursion cap
  double c = (a + b) / 2, h = b - a;
  double fa = f(a, param), fb = f(b, param), fc = f(c, param);
  double S = (h / 6) * (fa + 4 * fc + fb);
  return adaptiveSimpsonsAux(f,
                             param,
                             a, b, epsilon, S, fa, fb, fc, maxRecursionDepth);
}

//==============================================================================
// Recursive auxiliary function for adaptiveSimpsons() function below
//==============================================================================
double adaptiveSimpsonsAux(double (*f)(double, double),
                           double param,
                           double a, double b,
                           double epsilon,
                           double S,
                           double fa, double fb, double fc, int bottom)
{

  double c = (a + b) / 2, h = b - a;
  double d = (a + c) / 2, e = (c + b) / 2;
  double fd = f(d, param), fe = f(e, param);
  double Sleft = (h / 12) * (fa + 4 * fd + fc);
  double Sright = (h / 12) * (fc + 4 * fe + fb);
  double S2 = Sleft + Sright;
  if (bottom <= 0 || fabs(S2 - S) <= 15 * epsilon) // magic 15 comes
                                                   // from error analysis
    return S2 + (S2 - S) / 15;
  return adaptiveSimpsonsAux(f,
                             param,
                             a, c, epsilon / 2, Sleft, fa, fc, fd, bottom - 1) +
         adaptiveSimpsonsAux(f,
                             param,
                             c, b, epsilon / 2, Sright, fc, fb, fe, bottom - 1);
}