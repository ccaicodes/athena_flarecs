#include "copyright.h"
/*============================================================================*/
/*! \file flarecs.c
 *  \brief Problem generator for the current sheet in solar flare.
 *
 * PURPOSE: Problem generator for the magnetic reconnection in flare CS.
 *
 * REFERENCE: Forbes & Malherbe SoPh 1991; Shen, Lin & Murphy ApJ 2011.
 *
 * Update:
 *  20140822_2300:
 *  Left and Right boundary.
 *  20150227
 *  Set the first line in ghost zones.
 *  2015-0228
 *  Add linetiedbc_ix2_test.
 *  2015-0604-0605
 *  Line-tied boundary conditions. Jz =/= 0 at the bottome boundary.
 *  The guide field Bz =/= 0.
 *  Increase resistivity at the open boundary.
 *  Density compensation at the line-tied boundary.
 *  2015-11-26
 *  2016-02-15
 *  Change input parameters
 *  2016-03-16.
 *  flarecs_1.0.
 *  2016-03-24
 *  flarecs_1.3.
 *  Modified top, left and symmetry boundaries.
 *  Modified line-tied conditions, based on _20160321k_2153_islington.
 */

/*============================================================================*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defs.h"
#include "athena.h"
#include "globals.h"
#include "prototypes.h"

/*==============================================================================
 * PRIVATE FUNCTION PROTOTYPES:
 * void flarecs_linetied() - sets BCs on R-x2 boundary
 *============================================================================*/
static Real byini(const Real x1);
static Real bzini(const Real x1);
static Real pgas_bg(const Real x1);
static Real func_pbypx(const Real x1);
static Real func_extrp_2rd(const Real y1, const Real y2,
    const Real f1, const Real f2,
    const Real y0, const Real deriv,
    const Real y);
static Real Qt(const Real T);
Real cool_heat_corona(const Real dens, const Real Press, const Real dt);

static void openbc_ox2(GridS *pGrid);
static void openbc_ix1(GridS *pGrid);
static void openbc_ox1(GridS *pGrid);
static void symmbc_ox1(GridS *pGrid);
static void linetiedbc_ix2_mode_dev(GridS *pGrid);
static void linetiedbc_ix2_mode_1(GridS *pGrid);
static void linetiedbc_ix2_mode_2(GridS *pGrid);

/*=========================== PUBLIC FUNCTIONS ===============================*/
/*----------------------------------------------------------------------------*/

/* problem:  */

void problem(DomainS *pDomain)
{
  GridS *pGrid = (pDomain->Grid);
  int i, is = pGrid->is, ie = pGrid->ie;
  int j, js = pGrid->js, je = pGrid->je;
  int k, ks = pGrid->ks, ke = pGrid->ke;
  Real x1, x2, x3;
  Real gamma, ini_width, beta0;
  int linetied_mode;
  Real pgas;
  Real lchar;

  /* Magnitude of perturbations */
  Real vin_turb;
  Real pos_x_turb, pos_y_turb, hwd_x_turb, hwd_y_turb;

  /* Read input parameters */
  gamma    = par_getd("problem", "gamma");
  beta0    = par_getd("problem", "beta0");
  ini_width= par_getd("problem", "ini_width");
  linetied_mode = par_getd("problem", "linetied_mode");

  lchar = par_getd("problem", "Lchar");

  vin_turb = par_getd("problem", "vin_turb"); 
  pos_x_turb = par_getd("problem", "pos_x_turb");
  pos_y_turb = par_getd("problem", "pos_y_turb");
  hwd_x_turb = par_getd("problem", "hwd_x_turb");
  hwd_y_turb = par_getd("problem", "hwd_y_turb");


  for(k = ks; k <= ke; k++) {
    for(j = js; j <= je; j++) {
      for(i = is; i <= ie; i++) {
        /* axis */
        cc_pos(pGrid, i, j, k, &x1, &x2, &x3);

        /* Pressure */
        pgas = pgas_bg(x1);

        /* density */
        pGrid->U[k][j][i].d = pgas/(beta0/2.0);

        /* monentum */
        pGrid->U[k][j][i].M1 = 0.0;
        pGrid->U[k][j][i].M2 = 0.0;
        pGrid->U[k][j][i].M3 = 0.0;

        /* magnetic field */
        pGrid->B1i[k][j][i] = 0.0;
        pGrid->B2i[k][j][i] = byini(x1);
        pGrid->B3i[k][j][i] = bzini(x1);

        /* initial setting for magnetic field on the boundary */
        if(i == ie && ie > is) pGrid->B1i[k][j][i + 1] = 0.0;
        if(j == je && je > js) pGrid->B2i[k][j + 1][i] = byini(x1);
        if(k == ke && ke > ks) pGrid->B3i[k + 1][j][i] = bzini(x1);

        /* monmentum perturbations */
        /*pGrid->U[k][j][i].M1 = -vin_turb*x1
         *exp(-SQR(x2-pos_y_turb)/SQR(hwd_y_turb))*pGrid->U[k][j][i].d;*/
        if(x2 <= 1.0){
          pGrid->U[k][j][i].M1 = -x1*vin_turb*x2;
        }
      }
    }
  }

  /* cell-center magnetic field */
  for(k = ks; k <= ke; k++) {
    for(j = js; j <= je; j++) {
      for(i = is; i <= ie; i++) {
        pGrid->U[k][j][i].B1c = 0.5*(pGrid->B1i[k][j][i  ] +
            pGrid->B1i[k][j][i+1]);
        pGrid->U[k][j][i].B2c = 0.5*(pGrid->B2i[k][j  ][i] +
            pGrid->B2i[k][j+1][i]);
        pGrid->U[k][j][i].B3c =      pGrid->B3i[k][j][i];
      }
    }
  }

  /* total energy*/
  for(k = ks; k <= ke; k++) {
    for(j = js; j <= je; j++) {
      for(i = is; i <= ie; i++) {
        cc_pos(pGrid, i, j, k, &x1, &x2, &x3);
        pgas = pgas_bg(x1);

        pGrid->U[k][j][i].E = pgas/(gamma - 1.0)
          + 0.5*(SQR(pGrid->U[k][j][i].B1c)
              + SQR(pGrid->U[k][j][i].B2c)
              + SQR(pGrid->U[k][j][i].B3c))
          + 0.5*(SQR(pGrid->U[k][j][i].M1)
              + SQR(pGrid->U[k][j][i].M2)
              + SQR(pGrid->U[k][j][i].M3))/pGrid->U[k][j][i].d;
      }
    }
  }

  /* Set resistivity */
#ifdef RESISTIVITY
  eta_Ohm = par_getd("problem", "eta_Ohm");
  Q_AD = par_getd("problem", "Q_AD");
  Q_Hall = 0.0;
  d_ind = 0.0;
#endif

  /* Set thermel conduction*/
#ifdef THERMAL_CONDUCTION
  kappa_aniso = par_getd_def("problem","kappa_aniso",0.0);
  kappa_iso = par_getd_def("problem","kappa_iso",0.0);
#endif

  /* Viscosity */
#ifdef VISCOSITY
  nu_iso = par_getd_def("problem", "nu_iso", 1.0e-06);
#endif

  /* Set optical thin radiation cooling and corona heating function */
  CoolingFunc = cool_heat_corona;

  /* Set boundary value functions */
  /* (a) Left */
  bvals_mhd_fun(pDomain, left_x1,  openbc_ix1);

  /* (b) Right */
  /*bvals_mhd_fun(pDomain, right_x1, openbc_ox1);*/
  bvals_mhd_fun(pDomain, right_x1, symmbc_ox1);

  /* (c) Top */
  bvals_mhd_fun(pDomain, right_x2, openbc_ox2);

  /* (d) Bottom */
  if (linetied_mode == 0) {
    bvals_mhd_fun(pDomain, left_x2,  linetiedbc_ix2_mode_dev);
  }
  if (linetied_mode == 1) {
    bvals_mhd_fun(pDomain, left_x2,  linetiedbc_ix2_mode_1);
  }
  if (linetied_mode == 2) {
    bvals_mhd_fun(pDomain, left_x2,  linetiedbc_ix2_mode_2);
  }

}

/*==============================================================================
 * PROBLEM USER FUNCTIONS:
 * problem_write_restart() - writes problem-specific user data to restart files
 * problem_read_restart()  - reads problem-specific user data from restart files
 * get_usr_expr()          - sets pointer to expression for special output data
 * get_usr_out_fun()       - returns a user defined output function pointer
 * get_usr_par_prop()      - returns a user defined particle selection function
 * Userwork_in_loop        - problem specific work IN     main loop
 * Userwork_after_loop     - problem specific work AFTER  main loop
 *----------------------------------------------------------------------------*/

void problem_write_restart(MeshS *pM, FILE *fp)
{
  /* gamma and beta0 are used in line-tied boundary */
  Real gamma, beta0, ini_width;
  gamma = par_getd("problem", "gamma");
  beta0 = par_getd("problem", "beta0");
  /* Set resistivity */
  /* Set thermel conduction*/
#ifdef THERMAL_CONDUCTION
  kappa_aniso = par_getd_def("problem","kappa_aniso",0.0);
  kappa_iso = par_getd_def("problem","kappa_iso",0.0);
#endif

  return;
}

void problem_read_restart(MeshS *pM, FILE *fp)
{
  return;
}

#ifdef MHD
/*! \fn static Real current(const GridS *pG, const int i, const int j, const
 *			   int k)
 *  \brief computes x3-component of current
 */
static Real current(const GridS *pG, const int i, const int j, const int k)
{
  return ((pG->B2i[k][j][i]-pG->B2i[k][j][i-1])/pG->dx1 -
      (pG->B1i[k][j][i]-pG->B1i[k][j-1][i])/pG->dx2);
}

/*! \fn static Real divB(const GridS *pG, const int i, const int j, const int k)
 *  \brief  calculates div(B) */
static Real divB(const GridS *pG, const int i, const int j, const int k)
{
  Real qa;
  if (pG->Nx[2] > 1) {
    qa = (pG->B1i[k][j][i+1]-pG->B1i[k][j][i])/pG->dx1 +
      (pG->B2i[k][j+1][i]-pG->B2i[k][j][i])/pG->dx2 +
      (pG->B3i[k+1][j][i]-pG->B3i[k][j][i])/pG->dx3;
  } else {
    qa = (pG->B1i[k][j][i+1]-pG->B1i[k][j][i])/pG->dx1 +
      (pG->B2i[k][j+1][i]-pG->B2i[k][j][i])/pG->dx2;
  }
  return qa;
}

/*! \fn static Real bz(const GridS *pG, const int i, const int j, const int k)
 *  \brief return bz in 2.5D simulations*/
static Real bz(const GridS *pG, const int i, const int j, const int k)
{
  return pG->U[k][j][i].B3c;
}

/*! \fn static Real ohmiceta(const GridS *pG, const int i, const int j, const int k)
 *  \brief return  ohmiceta*/
static Real ohmiceta(const GridS *pG, const int i, const int j, const int k)
{
  return pG->eta_Ohm[k][j][i];
}

#endif

ConsFun_t get_usr_expr(const char *expr)
{
#ifdef MHD
  if(strcmp(expr,"J3")==0) return current;
  else if(strcmp(expr,"DivB")==0) return divB;
  else if(strcmp(expr,"B3")==0) return bz;
  else if(strcmp(expr,"Ohmiceta")==0) return ohmiceta;
#endif
  return NULL;
}

VOutFun_t get_usr_out_fun(const char *name)
{
  return NULL;
}

#ifdef RESISTIVITY

void get_eta_user(GridS *pG, int i, int j, int k,
    Real *eta_O, Real *eta_H, Real *eta_A)
{
  Real x1, x2, x3;
  Real r, r_eta = 0.1;
  Real eta_input, eta_plus;

  /* Magnitude of perturbations */
  Real eta_turb, tim_turb;
  Real pos_x_turb, pos_y_turb, hwd_x_turb, hwd_y_turb;
  Real err=1.0e-03, hwd_y_low;

  /* Read input parameters */

  eta_turb = par_getd("problem", "eta_turb");
  tim_turb = par_getd("problem", "tim_turb");
  pos_x_turb = par_getd("problem", "pos_x_turb");
  pos_y_turb = par_getd("problem", "pos_y_turb");
  hwd_x_turb = par_getd("problem", "hwd_x_turb");
  hwd_y_turb = par_getd("problem", "hwd_y_turb");

  *eta_O = par_getd("problem", "eta_Ohm");
  *eta_H = 0.0;
  *eta_A = par_getd("problem", "Q_AD");

  /* initial enhanced resistivity diffusion */
  cc_pos(pG, i, j, k, &x1, &x2, &x3);

  /* case 2, line */
  eta_input = par_getd("problem", "eta_Ohm");
  hwd_y_low = pow(-SQR(pos_y_turb)/(log(err)), 0.5);
  if(pG->time <= tim_turb){
    cc_pos(pG, i, j, k, &x1, &x2, &x3);
    if(x2 >= pos_y_turb){
      *eta_O = eta_turb
        *exp(-SQR(x2-pos_y_turb)/SQR(hwd_y_turb))
        *exp(-SQR(x1-pos_x_turb)/SQR(hwd_x_turb))
        + eta_input;
    }else{
      *eta_O = eta_turb
        *exp(-SQR(x2-pos_y_turb)/SQR(hwd_y_low))
        *exp(-SQR(x1-pos_x_turb)/SQR(hwd_x_turb))
        + eta_input;
    }
  }else{
    if(x2 >= pos_y_turb){
      *eta_O = eta_turb
        *exp(-(pG->time-tim_turb)/tim_turb)
        *exp(-SQR(x2-pos_y_turb)/SQR(hwd_y_turb))
        *exp(-SQR(x1-pos_x_turb)/SQR(hwd_x_turb))
        + eta_input;
    }else{
      *eta_O = eta_turb
        *exp(-(pG->time-tim_turb)/tim_turb)
        *exp(-SQR(x2-pos_y_turb)/SQR(hwd_y_low))
        *exp(-SQR(x1-pos_x_turb)/SQR(hwd_x_turb))
        + eta_input;
    }
  }

  /* at bottom: decreaseing eta_ohmic
     eta_input = par_getd("problem", "eta_Ohm");
     cc_pos(pG, i, j, k, &x1, &x2, &x3);
     if(pG->time >= tim_turb){
     if (x2 <= 0.1) {
   *eta_O = eta_input/(0.1-(pG->dx2))*(x2-(pG->dx2));
   }
   if (x2 <= (pG->dx2)) {
   *eta_O = 0;
   }
   }*/
  return;
}
#endif

void Userwork_in_loop(MeshS *pM)
{
  GridS *pG=pM->Domain[0][0].Grid;
  return;
}

void Userwork_after_loop(MeshS *pM)
{
  return;

}

/*=========================== PRIVATE FUNCTIONS ==============================*/
/*----------------------------------------------------------------------------*/
/*  \funtion of By for initial conditions */
static Real byini(const Real x1)
{
  Real by0;
  Real ini_width;
  int mode_cs;

  /* initialize parameters */
  ini_width = par_getd("problem", "ini_width");
  mode_cs = par_getd("problem", "cs_mode");

  /* case 1: Sin function */
  if(mode_cs == 0) {
    if(x1 >  ini_width) by0 = 1.0;
    if(x1 <= ini_width && x1 >= -ini_width) {
      by0 = sin(PI*x1/(2.0*ini_width));
    }
    if(x1 < -ini_width) by0 = -1.0;
  }

  /* case 2: Harris sheet */
  if(mode_cs == 1) {
    by0 = tanh(x1/ini_width);
  }

  /* case 3: Ideal sheet */
  if(mode_cs == 2){
    if(x1 <= 0.0){
      by0 = -1.0;
    }else{
      by0 = 1.0;
    }
  }

  return by0;
}

/*----------------------------------------------------------------------------*/
/*  \funtion of pBypx */
static Real func_pbypx(const Real x1)
{
  Real pbpx;
  Real ini_width;
  int mode_cs;
  Real cosh_temp;

  /* initialize parameters */
  ini_width = par_getd("problem", "ini_width");
  mode_cs = par_getd("problem", "cs_mode");

  /* case 1: Sin function (not continue) */
  if(mode_cs == 0) {
    if(x1 >  ini_width) pbpx = 0;
    if(x1 <= ini_width && x1 >= -ini_width){
      pbpx = (PI/2.0/ini_width)*cos(PI*x1/(2.0*ini_width));
    }
    if(x1 < -ini_width) pbpx = 0;
  }

  /* case 2: Harris sheet */
  if(mode_cs == 1) {
    /*by0 = tanh(x1/ini_width);*/
    cosh_temp = cosh(x1/ini_width);
    pbpx = (1./ini_width)*(1./(cosh_temp*cosh_temp));
  }

  /* case 3: Ideal sheet */
  if(mode_cs == 2){
    pbpx=0;
  }

  return pbpx;
}

/*----------------------------------------------------------------------------*/
/*  \funtion of func_extrp_2rd,
    a extrapolation function with 2rd */
static Real func_extrp_2rd(const Real y1, const Real y2,
    const Real f1, const Real f2,
    const Real y0, const Real deriv,
    const Real y)
{
  Real a, b, c, fy;
  a = ((f2 - f1) - deriv*(y2-y1))/((y2*y2 - y1*y1) - 2.0*y0*(y2-y1));
  b = deriv - 2.0*a*y0;
  c = f1 - a*y1*y1 - b*y1;
  fy = a*y*y + b*y + c;
  return fy;
}

/*---------------------------------------------------------------------------*/
/*  \funtion of Bz for initial conditions */
static Real bzini(const Real x1)
{
  Real by0, bz;
  by0 = byini(x1);
  bz = sqrt(1.0-by0*by0);
  bz = 0.0;
  return bz;
}

/*---------------------------------------------------------------------------*/
/*  \function pgas_bg */
static Real pgas_bg(const Real x1)
{
  Real beta0, p0, pB;
  beta0 = par_getd("problem", "beta0");
  pB = 0.5*(byini(x1)*byini(x1) + bzini(x1)*bzini(x1));
  p0 = (beta0*0.5 + 0.5) - pB;
  return p0;
}

/*----------------------------------------------------------------------------*/
/*! \fn void linetiedbc_ix2_mode_dev(GridS *pGrid)
 *  \brief Sets boundary condition at the bottom.
 */
/*  ix2, line-tied (mode develop), bottom */
void linetiedbc_ix2_mode_dev(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
  int kl, ku;
  Real x1, x2, x3;
  Real gamma, beta0, ini_width;
  int jc, j2;
  Real pjc;
  Real pbypx;
  Real bxc, byc, bzc;

  gamma = par_getd("problem", "gamma");
  beta0 = par_getd("problem", "beta0");
  ini_width = par_getd("problem", "ini_width");

  if (pGrid->Nx[2] > 1) {
    kl = pGrid->ks - nghost;
    ku = pGrid->ke + nghost;
  } else {
    kl = pGrid->ks;
    ku = pGrid->ke;
  }

  /* All variables */
  for (k=ks; k<=ke; k++) {
    for (j=1; j<=nghost; j++) {
      for (i=is-nghost; i<=ie+nghost; i++) {
        pGrid->U[k][js-j][i] = pGrid->U[k][js][i];
      }
    }
  }

  /* Momentum */
  for (k=ks; k<=ke; k++) {
    for (j=1; j<=nghost; j++) {
      for (i=is-nghost; i<=ie+nghost; i++) {
        pGrid->U[k][js-j][i].M1 = 0;
        pGrid->U[k][js-j][i].M2 = 0;
        pGrid->U[k][js-j][i].M3 = 0;

        /* set js line */
        pGrid->U[k][js][i].E = pGrid->U[k][js][i].E
          - 0.5*(SQR(pGrid->U[k][js][i].M1)
              + SQR(pGrid->U[k][js][i].M2)
              + SQR(pGrid->U[k][js][i].M3))
          /pGrid->U[k][js][i].d;
        pGrid->U[k][js][i].M1 = 0;
        pGrid->U[k][js][i].M2 = 0;
        pGrid->U[k][js][i].M3 = 0;
      }
    }
  }


  /* Bottom: B2i is not set at j=js-nghost */
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= (nghost-1); j++) {
      for (i = is-nghost; i <= ie + nghost; i++) {
        cc_pos(pGrid, i, js, k, &x1, &x2, &x3);
        pGrid->B2i[k][js-j][i] = byini(x1);
      }
    }
  }

  /* Bottom: B1i is not set at i=is-nghost
     for (k = ks; k <= ke; k++) {
     for (j = 1; j <= nghost; j++) {
     for (i = is - nghost; i <= ie + nghost; i++) {
     jc = js - j;
     cc_pos(pGrid, i, jc, k, &x1, &x2, &x3);

     y2 = x2 + 2.*pGrid->dx2;
     bx2 = pGrid->B1i[k][jc+2][i];

     y1 = x2 + 1.*pGrid->dx2;
     bx1 = pGrid->B1i[k][jc+1][i];

     y0 = x2 - 0.5*pGrid->dx2;
     pbypx = func_pbypx(x1-0.5*pGrid->dx1);

     yc = x2;
     bxjsm1 = func_extrp_2rd(y1, y2, bx1, bx2, y0, pbypx, yc);

     pGrid->B1i[k][js-j][i] = bxjsm1;
     }
     }
     }*/

  /* Bottom: B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is-(nghost-1); i <= ie + nghost; i++) {
        jc = js-j;
        cc_pos(pGrid, i, jc, k, &x1, &x2, &x3);

        /* unsatable: pbypx = (pGrid->B2i[k][js][i]
           - pGrid->B2i[k][js][i-1])/pGrid->dx1;*/

        pbypx = func_pbypx(x1 - 0.5*pGrid->dx1);
        pGrid->B1i[k][js-j][i] = pGrid->B1i[k][js-j+1][i] - pbypx*(pGrid->dx2);
      }
    }
  }

  /* Bottom: B3i */
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is-nghost; i <= ie + nghost; i++) {
        pGrid->B3i[k][js-j][i] = pGrid->B3i[k][js][i];
      }
    }
  }

  /* Cell-center magnetic field */
  /* B1c [is-nghost+1: ie+nhgost-1], others are obtianed from extrapolation  */
  for (k = ks; k <= ke; k++){
    for (j = 1; j <= nghost; j++){
      for (i = is-(nghost-1); i <= ie+(nghost-1); i++){
        jc = js-j;
        pGrid->U[k][jc][i].B1c = 0.5*(pGrid->B1i[k][jc][i] +
            pGrid->B1i[k][jc][i+1]);
      }
    }
  }
  /* B2c [js-nghost+1: js-1], others are obtianed from extrapolation */
  for (k = ks; k <= ke; k++){
    for (j = 1; j <= (nghost-1); j++){
      for (i = is-nghost; i <= ie+nghost; i++){
        jc = js -j;
        pGrid->U[k][jc][i].B2c = 0.5*(pGrid->B2i[k][jc][i] +
            pGrid->B2i[k][jc+1][i]);
      }
    }
  }
  /* B3c */
  for (k = ks; k <= ke; k++){
    for (j = 1; j <= (nghost-1); j++){
      for (i = is-nghost; i <= ie+nghost; i++){
        jc = js-j;
        pGrid->U[k][jc][i].B3c = pGrid->B3i[k][jc][i];
      }
    }
  }


  /* Total energy and Density */
  for (k=ks; k<=ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i=is-nghost; i<=ie+nghost; i++) {
        cc_pos(pGrid, i, js, k, &x1, &x2, &x3);

        jc = js - j;
        bxc = pGrid->U[k][jc][i].B1c;
        byc = pGrid->U[k][jc][i].B2c;
        bzc = pGrid->U[k][jc][i].B3c;

        j2 = js+1;
        pjc = (gamma - 1.0)*(pGrid->U[k][j2][i].E
            - 0.5*(SQR(pGrid->U[k][j2][i].B1c)
              + SQR(pGrid->U[k][j2][i].B2c)
              + SQR(pGrid->U[k][j2][i].B3c))
            - 0.5*(SQR(pGrid->U[k][j2][i].M1)
              + SQR(pGrid->U[k][j2][i].M2)
              + SQR(pGrid->U[k][j2][i].M3))
            /pGrid->U[k][j2][i].d);


        pGrid->U[k][jc][i].d = pjc/(beta0*0.5);
        pGrid->U[k][js][i].d = pjc/(beta0*0.5);
        pGrid->U[k][js+1][i].d = pjc/(beta0*0.5);

        pGrid->U[k][jc][i].E = pjc/(gamma - 1.0)
          + 0.5*(SQR(bxc) + SQR(byc) + SQR(bzc)); /* Becasue M1,2,3 == 0*/

      }
    }
  }

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void linetiedbc_ix2_mode_1(GridS *pGrid)
 *  \brief Sets boundary condition at the bottom.
 */
/*  ix2, line-tied (mode 1), bottom */
void linetiedbc_ix2_mode_1(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
  int kl, ku;
  Real x1, x2, x3;
  Real gamma, beta0, ini_width;
  int jc, j2;
  Real pjc;
  Real pbypx;

  gamma = par_getd("problem", "gamma");
  beta0 = par_getd("problem", "beta0");
  ini_width = par_getd("problem", "ini_width");

  if (pGrid->Nx[2] > 1) {
    kl = pGrid->ks - nghost;
    ku = pGrid->ke + nghost;
  } else {
    kl = pGrid->ks;
    ku = pGrid->ke;
  }

  /* All variables */
  for (k=ks; k<=ke; k++) {
    for (j=1; j<=nghost; j++) {
      for (i=is-nghost; i<=ie+nghost; i++) {
        pGrid->U[k][js-j][i] = pGrid->U[k][js][i];
      }
    }
  }

  /* Momentum */
  for (k=ks; k<=ke; k++) {
    for (j=1; j<=nghost; j++) {
      for (i=is-nghost; i<=ie+nghost; i++) {
        pGrid->U[k][js-j][i].M1 = 0;
        pGrid->U[k][js-j][i].M2 = 0;
        pGrid->U[k][js-j][i].M3 = 0;
        /* set js line */
        pGrid->U[k][js][i].E = pGrid->U[k][js][i].E
          - 0.5*(SQR(pGrid->U[k][js][i].M1)
              + SQR(pGrid->U[k][js][i].M2)
              + SQR(pGrid->U[k][js][i].M3))
          /pGrid->U[k][js][i].d;
        pGrid->U[k][js][i].M1 = 0;
        pGrid->U[k][js][i].M2 = 0;
        pGrid->U[k][js][i].M3 = 0;
      }
    }
  }

  /* Bottom: B2i is not set at j=js-nghost */
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= (nghost-1); j++) {
      for (i = is-nghost; i<=ie+nghost; i++) {
        cc_pos(pGrid, i, js, k, &x1, &x2, &x3);
        pGrid->B2i[k][js-j][i] = byini(x1);
      }
    }
  }

  /* Bottom: B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is-(nghost-1); i <= ie + nghost; i++) {
        jc = js-j;
        cc_pos(pGrid, i, jc, k, &x1, &x2, &x3);

        pbypx = func_pbypx(x1 - 0.5*pGrid->dx1);
        pGrid->B1i[k][js-j][i] = pGrid->B1i[k][js-j+1][i] - pbypx*(pGrid->dx2);
      }
    }
  }

  /* Bottom: B3i */
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is-nghost; i <= ie + nghost; i++) {
        pGrid->B3i[k][js-j][i] = pGrid->B3i[k][js][i];
      }
    }
  }

  /* Cell-center magnetic field */
  /* B1c [is-nghost+1: ie+nhgost-1], others are obtianed from extrapolation  */
  for (k = ks; k <= ke; k++){
    for (j = 1; j <= nghost; j++){
      for (i = is-(nghost-1); i <= ie+(nghost-1); i++){
        jc = js-j;
        pGrid->U[k][jc][i].B1c = 0.5*(pGrid->B1i[k][jc][i] +
            pGrid->B1i[k][jc][i+1]);
      }
    }
  }
  /* B2c [js-nghost+1: js-1], others are obtianed from extrapolation */
  for (k = ks; k <= ke; k++){
    for (j = 1; j <= (nghost-1); j++){
      for (i = is-nghost; i <= ie+nghost; i++){
        jc = js -j;
        pGrid->U[k][jc][i].B2c = 0.5*(pGrid->B2i[k][jc][i] +
            pGrid->B2i[k][jc+1][i]);
      }
    }
  }
  /* B3c */
  for (k = ks; k <= ke; k++){
    for (j = 1; j <= (nghost-1); j++){
      for (i = is-nghost; i <= ie+nghost; i++){
        jc = js-j;
        pGrid->U[k][jc][i].B3c = pGrid->B3i[k][jc][i];
      }
    }
  }

  /* Total energy and Density */
  for (k=ks; k<=ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i=is-nghost; i<=ie+nghost; i++) {

        cc_pos(pGrid, i, js, k, &x1, &x2, &x3);
        jc = js - j;

        j2 = js+1;
        pjc = (gamma - 1.0)*(pGrid->U[k][j2][i].E
            - 0.5*(SQR(pGrid->U[k][j2][i].B1c)
              + SQR(pGrid->U[k][j2][i].B2c)
              + SQR(pGrid->U[k][j2][i].B3c))
            - 0.5*(SQR(pGrid->U[k][j2][i].M1)
              + SQR(pGrid->U[k][j2][i].M2)
              + SQR(pGrid->U[k][j2][i].M3))
            /pGrid->U[k][j2][i].d);


        pGrid->U[k][jc][i].d = pjc/(beta0*0.5);
        pGrid->U[k][js][i].d = pjc/(beta0*0.5);
        pGrid->U[k][js+1][i].d = pjc/(beta0*0.5);

        pGrid->U[k][jc][i].E = pjc/(gamma - 1.0)
          + 0.5*(SQR(pGrid->U[k][jc][i].B1c)
              + SQR(pGrid->U[k][jc][i].B2c)
              + SQR(pGrid->U[k][jc][i].B3c)); /* M1,2,3 ==0 */

      }
    }
  }

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void linetiedbc_ix2_mode_2(GridS *pGrid)
 *  \brief Sets boundary condition at the bottom.
 */
/*  ix2, line-tied (mode 2), bottom */
void linetiedbc_ix2_mode_2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
  int kl, ku;
  Real x1, x2, x3;
  Real gamma, beta0, ini_width;
  int jc, j2;
  Real y1, y2, yc, y0;
  Real p2, pjc;
  Real bx1, bx2, bxjsm1, pbypx;
  Real g2, mot2;

  gamma = par_getd("problem", "gamma");
  beta0 = par_getd("problem", "beta0");
  ini_width = par_getd("problem", "ini_width");

  if (pGrid->Nx[2] > 1) {
    kl = pGrid->ks - nghost;
    ku = pGrid->ke + nghost;
  } else {
    kl = pGrid->ks;
    ku = pGrid->ke;
  }

  /* All variables */
  for (k=ks; k<=ke; k++) {
    for (j=1; j<=nghost; j++) {
      for (i=is-nghost; i<=ie+nghost; i++) {
        pGrid->U[k][js-j][i] = pGrid->U[k][js][i];
      }
    }
  }

  /* Momentum */
  for (k=ks; k<=ke; k++) {
    for (j=1; j<=nghost; j++) {
      for (i=is-nghost; i<=ie+nghost; i++) {
        pGrid->U[k][js-j][i].M1 = 0;
        pGrid->U[k][js-j][i].M2 = 0;
        pGrid->U[k][js-j][i].M3 = 0;
      }
    }
  }

  /* Bottom: B2i is not set at j=js-nghost */
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= (nghost-1); j++) {
      for (i = is - nghost; i <= ie + nghost; i++) {
        cc_pos(pGrid, i, js, k, &x1, &x2, &x3);
        pGrid->B2i[k][js-j][i] = byini(x1);
      }
    }
  }

  /* Bottom: B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is-(nghost-1); i <= ie + nghost; i++) {
        jc = js - j;
        cc_pos(pGrid, i, jc, k, &x1, &x2, &x3);

        y2 = x2 + 2.*pGrid->dx2;
        bx2 = pGrid->B1i[k][jc+2][i];

        y1 = x2 + 1.*pGrid->dx2;
        bx1 = pGrid->B1i[k][jc+1][i];

        y0 = x2 - 1.*pGrid->dx2;
        pbypx = func_pbypx(x1-0.5*pGrid->dx1);

        yc = x2;
        bxjsm1 = func_extrp_2rd(y1, y2, bx1, bx2, y0, pbypx, yc);

        pGrid->B1i[k][js-j][i] = bxjsm1;
      }
    }
  }

  /* Bottom: B3i */
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is-nghost; i <= ie + nghost; i++) {
        pGrid->B3i[k][js-j][i] = pGrid->B3i[k][js][i];
      }
    }
  }

  /* Cell-center magnetic field */
  /* B1c [is-nghost+1: ie+nhgost-1], others are obtianed from extrapolation  */
  for (k = ks; k <= ke; k++){
    for (j = 1; j <= nghost; j++){
      for (i = is-(nghost-1); i <= ie+(nghost-1); i++){
        jc = js-j;
        pGrid->U[k][jc][i].B1c = 0.5*(pGrid->B1i[k][jc][i] +
            pGrid->B1i[k][jc][i+1]);
      }
    }
  }
  /* B2c [js-nghost+1: js-1], others are obtianed from extrapolation */
  for (k = ks; k <= ke; k++){
    for (j = 1; j <= (nghost-1); j++){
      for (i = is-nghost; i <= ie+nghost; i++){
        jc = js -j;
        pGrid->U[k][jc][i].B2c = 0.5*(pGrid->B2i[k][jc][i] +
            pGrid->B2i[k][jc+1][i]);
      }
    }
  }
  /* B3c */
  for (k = ks; k <= ke; k++){
    for (j = 1; j <= (nghost-1); j++){
      for (i = is-nghost; i <= ie+nghost; i++){
        jc = js-j;
        pGrid->U[k][jc][i].B3c = pGrid->B3i[k][jc][i];
      }
    }
  }

  /* Total energy and Density */
  for (k=ks; k<=ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i=is-nghost; i<=ie+nghost; i++) {

        /* G at js+1 line */
        j2 = js+1;
        p2 = (gamma - 1.0)*(pGrid->U[k][j2][i].E
            - 0.5*(SQR(pGrid->U[k][j2][i].B1c)
              + SQR(pGrid->U[k][j2][i].B2c)
              + SQR(pGrid->U[k][j2][i].B3c))
            - 0.5*(SQR(pGrid->U[k][j2][i].M1)
              + SQR(pGrid->U[k][j2][i].M2)
              + SQR(pGrid->U[k][j2][i].M3))
            /pGrid->U[k][j2][i].d);

        g2 = SQR(pGrid->U[k][j2][i].M2)/pGrid->U[k][j2][i].d
          + p2
          + 0.5*(SQR(pGrid->U[k][j2][i].B1c)
              + SQR(pGrid->U[k][j2][i].B2c)
              + SQR(pGrid->U[k][j2][i].B3c))
          - SQR(pGrid->U[k][j2][i].B2c);

        /* m2 at js line*/
        mot2 = (2.*pGrid->dx2/pGrid->dt)*pGrid->U[k][j2][i].M2;

        jc = js - j;
        cc_pos(pGrid, i, jc, k, &x1, &x2, &x3);

        /* Estimate pressure from momentum equation */
        pjc = g2 - mot2
          - 0.0
          - (0.5*(SQR(pGrid->U[k][jc][i].B1c)
                + SQR(pGrid->U[k][jc][i].B2c)
                + SQR(pGrid->U[k][jc][i].B3c))
              - SQR(pGrid->U[k][jc][i].B2c));

        /* Estimate density from js+1 line */
        pGrid->U[k][jc][i].d = pGrid->U[k][js+1][i].d;

        /* Energy density */
        pGrid->U[k][jc][i].E = pjc/(gamma - 1.0)
          + 0.5*(SQR(pGrid->U[k][jc][i].B1c)
              + SQR(pGrid->U[k][jc][i].B2c)
              + SQR(pGrid->U[k][jc][i].B3c))
          + 0;
      }
    }
  }



  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void openbc_ox2(GridS *pGrid)
 *  \brief open boundary conditions, Outer x2 boundary (bc_ox2=2) */
/*----------------------------------------------------------------------------*/
/*  ox2, top boundary */

void openbc_ox2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i,j,k;
#ifdef MHD
  int ku; /* k-upper */
#endif
  Real gamma;

  gamma = par_getd("problem", "gamma");

  for(k=ks; k<=ke; k++) {
    for(j=1; j<=nghost; j++) {
      for(i=is-nghost; i<=ie+nghost; i++) {
        pGrid->U[k][je+j][i] = pGrid->U[k][je-1][i];

        /* Restric extra inflow */
        if (pGrid->U[k][je][i].M2 <= 0.0){
          pGrid->U[k][je+j][i].E = pGrid->U[k][je+j][i].E
            -0.5*SQR(pGrid->U[k][je+j][i].M2)
            /pGrid->U[k][je+j][i].d;
          pGrid->U[k][je+j][i].M2 = 0;
        }
      }
    }
  }

#ifdef MHD
  /* B1i is not set at i=is-nghost */
  for(k=ks; k<=ke; k++) {
    for(j=1; j<=nghost; j++) {
      for(i=is-(nghost-1); i<=ie+nghost; i++) {
        pGrid->B1i[k][je+j][i] = pGrid->B1i[k][je-1][i];
      }
    }
  }

  /* j=je+1 is not a boundary condition for the interface field B2i */
  for(k=ks; k<=ke; k++) {
    for(j=2; j<=nghost; j++) {
      for(i=is-nghost; i<=ie+nghost; i++) {
        pGrid->B2i[k][je+j][i] = pGrid->B2i[k][je-1][i];
      }
    }
  }

  if(pGrid->Nx[2] > 1) ku=ke+1;
  else ku=ke;
  for(k=ks; k<=ku; k++) {
    for(j=1; j<=nghost; j++) {
      for(i=is-nghost; i<=ie+nghost; i++) {
        pGrid->B3i[k][je+j][i] = pGrid->B3i[k][je-1][i];
      }
    }
  }

#endif /* MHD */
  return;
}


/*----------------------------------------------------------------------------*/
/*! \fn void openbc_ix1(GridS *pGrid)
 *  \brief open boundary condition, Inner x1 boundary (bc_ix1=2) */
/*----------------------------------------------------------------------------*/
/*  ix1, Left boundary */

void openbc_ix1(GridS *pGrid)
{
  int is = pGrid->is;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i,j,k;
  Real gamma;

  gamma = par_getd("problem", "gamma");

#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  for(k=ks; k<=ke; k++) {
    for(j=js; j<=je; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->U[k][j][is-i] = pGrid->U[k][j][is+1];
      }
    }
  }

  for(k=ks; k<=ke; k++) {
    for(j=js; j<=je; j++) {
      for(i=1; i<=nghost; i++) {
        /* Restrict extra inflow */
        if(pGrid->U[k][j][is].M1 >= 0.0) {
          pGrid->U[k][j][is-i].E = pGrid->U[k][j][is-i].E
            -0.5*(SQR(pGrid->U[k][j][is-i].M1))
            /pGrid->U[k][j][is-i].d;
          pGrid->U[k][j][is-i].M1 = 0.0;
        }
      }
    }
  }

#ifdef MHD
  /* B2i */
  if(pGrid->Nx[1] > 1) ju=je+1;
  else ju=je;
  for(k=ks; k<=ke; k++) {
    for(j=js; j<=ju; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->B2i[k][j][is-i] = pGrid->B2i[k][j][is+1];
      }
    }
  }
  /* B1i is not set at i=is-nghost */
  for(k=ks; k<=ke; k++) {
    for(j=js; j<=je; j++) {
      for(i=1; i<=nghost-1; i++) {
        pGrid->B1i[k][j][is-i] = pGrid->B1i[k][j][is+1];
      }
    }
  }

  if(pGrid->Nx[2] > 1) ku=ke+1;
  else ku=ke;
  for(k=ks; k<=ku; k++) {
    for(j=js; j<=je; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->B3i[k][j][is-i] = pGrid->B3i[k][j][is+1];
      }
    }
  }
#endif /* MHD */
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void openbc_ox1(GridS *pGrid)
 *  \brief openbc boundary conditions, Outer x1 boundary (bc_ox1=2) */
/*----------------------------------------------------------------------------*/
/* Right boundary */

void openbc_ox1(GridS *pGrid)
{
  int ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i,j,k;
  Real gamma;

  gamma = par_getd("problem", "gamma");

#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  for(k=ks; k<=ke; k++) {
    for(j=js; j<=je; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->U[k][j][ie+i] = pGrid->U[k][j][ie];
      }
    }
  }

  for(k=ks; k<=ke; k++) {
    for(j=js; j<=je; j++) {
      for(i=1; i<=nghost; i++) {
        /* Restric extra inflow */
        if(pGrid->U[k][j][ie].M1 < 0.0) {
          pGrid->U[k][j][ie+i].M1 = -pGrid->U[k][j][ie].M1;
        }
      }
    }
  }

#ifdef MHD
  /* B2i */
  if(pGrid->Nx[1] > 1) ju=je+1;
  else ju=je;
  for(k=ks; k<=ke; k++) {
    for(j=js; j<=ju; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->B2i[k][j][ie+i] = pGrid->B2i[k][j][ie];
      }
    }
  }

  /* i=ie+1 is not a boundary condition for the interface field B1i */
  for(k=ks; k<=ke; k++) {
    for(j=js; j<=je; j++) {
      for(i=2; i<=nghost; i++) {
        pGrid->B1i[k][j][ie+i] = pGrid->B1i[k][j][ie];
      }
    }
  }

  if(pGrid->Nx[2] > 1) ku=ke+1;
  else ku=ke;
  for(k=ks; k<=ku; k++) {
    for(j=js; j<=je; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->B3i[k][j][ie+i] = pGrid->B3i[k][j][ie];
      }
    }
  }
#endif /* MHD */
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void symmbc_ox1(GridS *pGrid)
 *  \brief openbc boundary conditions, Outer x1 boundary (bc_ox1=2) */
/*----------------------------------------------------------------------------*/
/* Right boundary at y = 0, symmetry condition */

void symmbc_ox1(GridS *pGrid)
{
  int ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i,j,k;

#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  for(k=ks; k<=ke; k++) {
    for(j=js; j<=je; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->U[k][j][ie+i] = pGrid->U[k][j][ie-i+1];
        /* monmentum */
        pGrid->U[k][j][ie+i].M1 = -pGrid->U[k][j][ie-i+1].M1;
        /* Magnetic */
        pGrid->U[k][j][ie+i].B2c = -pGrid->U[k][j][ie-i+1].B2c;
      }
    }
  }

#ifdef MHD
  /* B2i */
  if(pGrid->Nx[1] > 1) ju=je+1;
  else ju=je;
  for(k=ks; k<=ke; k++) {
    for(j=js; j<=ju; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->B2i[k][j][ie+i] = -pGrid->B2i[k][j][ie-i+1];
      }
    }
  }

  /* i=ie+1 is not a boundary condition for the interface field B1i */
  for(k=ks; k<=ke; k++) {
    for(j=js; j<=je; j++) {
      for(i=2; i<=nghost; i++) {
        pGrid->B1i[k][j][ie+i] = pGrid->B1i[k][j][ie-i+2];
      }
    }
  }

  if(pGrid->Nx[2] > 1) ku=ke+1;
  else ku=ke;
  for(k=ks; k<=ku; k++) {
    for(j=js; j<=je; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->B3i[k][j][ie+i] = pGrid->B3i[k][j][ie-i+1];
      }
    }
  }
#endif /* MHD */
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn Real cool_heat_corona(const Real dens, const Real Press, const Real dt)
 *  \A more accurate piecewise linear approximation (Klimchuk et al. 2008).
 Returns rate in non-dimensional value. The other local variables appear
 in SI unit.
 dE'/dt' ~ - rho'*[rho'Q'], where Q(T)' = tau_A/P_0*(n0^2)*Q(T).
 H' = rho'*[Q'(T=10^6K)].
 Therefor, the routine returns rho'*Q(T)' - H'.
 */

#ifndef BAROTROPIC
Real cool_heat_corona(const Real dens, const Real Press, const Real dt)
{
  Real coolrate=0.0;
  Real T, ne, nH, coolratepp;
  Real Teq, Tmin;

  Real Lchar;
  Real Tchar;
  Real Bchar;
  Real beta0;

  Real kb, mu0;
  Real nchar, pchar, timechar, pmag, pgas, rhochar, va;

  /* Get character variables */
  Lchar = par_getd("problem", "Lchar");
  Tchar = par_getd("problem", "Tchar");
  Bchar = par_getd("problem", "Bchar");
  beta0 = par_getd("problem", "beta0");

  /* basic parameters */
  mu0 = 4.0*3.141592653589793*1.0e-07;
  kb = 1.381e-23; /* (J K^-1) */
  Tmin = 10.0; 

  pchar = Bchar*Bchar/mu0;
  pmag = 0.5*Bchar*Bchar/mu0;
  pgas = beta0*pmag;
  nchar = pgas/(kb*Tchar);
  rhochar = nchar*1.66057e-27; /* (kg m^-3) */
  va = Bchar/pow(mu0*rhochar, 0.5);
  timechar = Lchar/va;

  /* Compute number density for character density */
  T = Tchar*(Press/dens/(beta0/2.0));
  ne = dens*rhochar/1.66057e-27;
  nH = ne;

  /* Compute the minimun Temperature*/
  Teq = Tmin;

  /* Assuming thermal equilibrium at T = 1.0e06, then
     coolratepp = n_e*n_H*Qt(T) - n_e*const, where const = nchar*Qt(1.0e6);*/
  coolratepp = ne*nH*Qt(T) - ne*(nchar*Qt(Tchar));

  /* return result */
  return coolrate;
}
#endif /* BAROTROPIC */

/*----------------------------------------------------------------------------*/
/*  \brief Calculate Qt
 *  piecewise lineat approximation (Klimcuk et al. 2008)*/
/* T: dimensional variable, SI unit */
static Real Qt(const Real T)
{
  Real q;
  /* first in cgs: ergs sec^-1 cm^3 */

  if(T <= pow(10.0, 4.97))
  {
    q = 1.09e-31*(pow(T, 2));
  }else if (T <= pow(10.0, 5.67))
  {
    q = 8.87e-17*(pow(T, -1.0));
  }else if (T <= pow(10.0, 6.18))
  {
    q = 1.90e-22;
  }else if (T <= pow(10.0, 6.55))
  {
    q = 3.54e-13*(pow(T, -3./2.));
  }else if (T <= pow(10.0, 6.90))
  {
    q = 3.46e-25*(pow(T, 1./3.));
  }else if (T <= pow(10.0, 7.63))
  {
    q = 5.49e-16*(pow(T, -1.0));
  }else{
    q = 1.96e-27*(pow(T, 0.5));
  }
  /* to SI unit: W m^3 */
  q = q * 1.0e-13;

  return q;
}

