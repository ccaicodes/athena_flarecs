#include "copyright.h"
/*============================================================================*/
/*! \file fluxrope_it.c
 *  \brief  Problem generator for Fluxrope.
 *
 * PURPOSE: Test the initial fluxrope.
 *
 * REFERENCE: Wang, Shen, & Lin, ApJ 2009.
 *
 * Update:
 *  2018-04-04
 *    Starting version.
 *  2019-01-16
 *    Add fluxrope and linetied BC.
 */

/*============================================================================*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defs.h"
#include "athena.h"
#include "globals.h"
#include "prototypes.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_integration.h>
#include "./prob/simpson_integ.c"

/*==============================================================================
 * PRIVATE FUNCTION PROTOTYPES:
 * void flarecs_linetied() - sets BCs on R-x2 boundary
 *============================================================================*/
static Real presini(const Real x1, const Real x2);
static Real densini(const Real x1, const Real x2);
static Real teini(const Real x1, const Real x2);
static Real func_te_rel(const Real x, const Real y);

/* functions for compute flux */
static Real func_bphi(const Real r);
static Real func_rjphi(const Real r);
static Real func_uphi(const Real r);
static Real func_back(const Real r);

static Real func_bmx(const Real x, const Real y);
static Real func_bmy(const Real x, const Real y);
static Real func_uphi_xy(const Real x, const Real y);

Real func_integ_bx(Real y, void *params);
Real func_integ_by(Real x, void *params);

Real func_integ_current(Real r, void *params);
Real func_integ_pphi_v2(Real r, void *params);
Real func_integ_pphi_v3(Real x, void *params);
Real my_integ_pphi(Real r);
Real func_pres_avg(Real const xc, Real const yc, Real const dx, Real const dy);
Real func_integ_px_yc(Real x, Real *y_current);
Real func_integ_py(Real y, void *p);
struct struct_params_2
{
  Real a;
  Real b;
};
Real x_current, y_current, dx_grid, dy_grid, dz_grid;

/* boundary */
static void openbc_ix1(GridS *pGrid);
static void openbc_ox1(GridS *pGrid);
static void symmbc_ox1(GridS *pGrid);
static void openbc_ix2(GridS *pGrid);
static void openbc_ox2(GridS *pGrid);
static void linetiedbc_ix2(GridS *pGrid);
static Real func_pbypxini(Real x, Real y);

/*============================================================================*/
#define PI 3.14159265358979323846

/* Normalization parameters */
Real Lchar, Bchar, Nechar, Rhochar, Pchar, Timechar, Tchar, Vchar;
Real Gsun_nondim;

/* a. input parameters: pressure and temperature in chromosphere and corona */
Real Te_corona = 2.0e6, Te_photos = 1.0e4;

/* b. input parameters: height and width for the TR region */
Real Height_TR = 3.0e6, Width_TR = 1.0e6;

/* c. The height for define the pgas_c in corona */
Real pgas_c = 0.05, posi_c = 1.0;

/* Constant parameters */
Real Kb = 1.38e-23, Mp = 1.6726e-27, Mu0 = 1.25663706144e-6, LnA = 30.0;
Real gravitational_const = 6.672e-11; /* (N M^2 kg^-2)*/
Real solarmass = 1.99e+30;            /* (kg) */
Real solarradius = 6.96e+8;           /* (m) */

/* Initial conditions */
Real ***pgasini, ***rhoini, ***tempini, ***az, ***jz;
Real ***bx, ***by, ***bz;

/* Flux rope parameters */
Real fr_h, fr_d, fr_ri, fr_del, fr_rja, fr_sigma, fr_rmom;
Real fr_case, fr_b0;
Real fr_xc;   // the center of flux rope.
Real I_total; // Total current
Real scale_ambient;
Real gauss_c1, gauss_c2; // Two parameters for setting guassian J(r)
Real rja_bgscale; // The relative strength of background Jz

/* Define the temperature range in computing kappa(T) */
Real temp_low, temp_high;

/* Artificialy reset cooling and conduction in lower temperature region */
Real Tc_low;
int sw_lowt_cool; // use for decreasing the cooling and increacing thermal
                  // conduction once tempreture is lower than Tc_low to keep
                  // Kappa(T)*Q(T) constant. (Reeves et al.2010 ApJ)
int sw_explicit_conduct_dt;

/* Reset Output dt = dt_reset_out after time_reset_out */
Real dt_reset_out = 0.05, time_reset_out = 10.0;

Real pres_floor;

/*=========================== PUBLIC FUNCTIONS ===============================*/
/*----------------------------------------------------------------------------*/
/* problem:  */

void problem(DomainS *pDomain)
{
  GridS *pGrid = (pDomain->Grid);
  int i, is = pGrid->is, ie = pGrid->ie;
  int j, js = pGrid->js, je = pGrid->je;
  int k, ks = pGrid->ks, ke = pGrid->ke;
  int n1 = ie - is + 1 + 2 * nghost;
  int n2 = je - js + 1 + 2 * nghost;
  int n3 = ke - ks + 1 + 2 * nghost;

  Real x1, x2, x3;
  Real x1c, x2c, x3c;
  Real x1f, x2f, x3f;
  Real x1s, x2s, x3s;
  Real r;
  Real ***pc;
  dx_grid = pGrid->dx1;
  dy_grid = pGrid->dx2;
  dz_grid = pGrid->dx3;

  /* Inilize pgasini, rhoini, and tempini */
  if ((pgasini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for pgasini \n");
  }
  if ((rhoini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for rhoini \n");
  }
  if ((tempini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for tempini \n");
  }

  if ((bx = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector bx\n");
  }
  if ((by = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector by\n");
  }
  if ((bz = (Real ***)calloc_3d_array(n3, n2 + 2, n1 + 2, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector bz\n");
  }
  if ((az = (Real ***)calloc_3d_array(n3 + 1, n2 + 1, n1 + 1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector az\n");
  }
  if ((jz = (Real ***)calloc_3d_array(n3 + 1, n2 + 1, n1 + 1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector jz\n");
  }
  if ((pc = (Real ***)calloc_3d_array(n3 + 1, n2 + 1, n1 + 1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector pc\n");
  }

  /* Termal conduction & viscosity */
  Real k_spitzer, kappa_v;
  Real nud, nu0;

  /* Magnitude of perturbations */
  Real pi;
  Real vin_turb;
  Real Lx, Ly, delta_bx, delta_by;
  Real pos_x_turb, pos_y_turb, hwd_x_turb, hwd_y_turb;

  /* Dence bottom boundary */
  Real htr, wtr, chr_rho, cor_rho;

  /* Read input parameters */
  Lchar = par_getd("problem", "Lchar");
  Bchar = par_getd("problem", "Bchar");
  Nechar = par_getd("problem", "Nechar");

  Pchar = Bchar * Bchar / Mu0;
  Rhochar = Nechar * Mp;
  Tchar = Pchar / (Nechar * 2. * Kb); /*total number is 2*ne for full ionization 
                                  plasma */
  Vchar = Bchar / sqrt(Mu0 * Rhochar);
  Timechar = Lchar / Vchar;

  /* Print*/
  int myid;
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  if (myid == 0)
  {
    printf("nechar=%10.3e (cm^-3), Rhochar=%10.3e (kg/m^3)\n",
           Nechar / 1.0e6, Rhochar);
    printf("Pchar=%10.3e (Pa)\n", Pchar);
    printf("Tchar=%10.3e (K)\n", Tchar);
    printf("Vchar=%10.3e (km/s)\n", Vchar / 1.0e3);
    printf("Lchar=%10.3e (km)\n", Lchar / 1.0e3);
    printf("Bchar=%10.3e (T)\n", Bchar);
    printf("Timechar=%10.3e (s)\n", Timechar);
  }

  /* Read temperature floor for kappa calculations */
  temp_low = par_getd("problem", "Temperature_low");
  temp_high = par_getd("problem", "Temperature_high");

  /* Read parameters for cooling */
  Tc_low = par_getd("problem", "Tc_low");
  sw_lowt_cool = par_getd("problem", "sw_lowt_cool");
  sw_explicit_conduct_dt = par_getd("problem", "sw_explicit_conduct_dt");

  /* Initialize fluxrope */
  fr_case = par_getd("problem", "bgpole");
  fr_h = par_getd("problem", "h");
  fr_d = par_getd("problem", "d");
  fr_ri = par_getd("problem", "ri");
  fr_del = par_getd("problem", "del");
  fr_rja = par_getd("problem", "rja");
  if (fr_case == 2) {
    fr_sigma = par_getd("problem", "sigma");
    fr_rmom = fr_d * fr_d * 125.0 / 32.0 * fr_sigma;
  } 
  if (fr_case == 1) {
    fr_rmom = par_getd("problem", "M");
  }
  scale_ambient = par_getd("problem", "scale_ambient");
  Real scale_riod = par_getd("problem", "scale_riod");
  gauss_c1 = scale_riod*fr_d/2.35482;
  gauss_c2 = par_getd("problem", "ri_bg")/2.35482;
  rja_bgscale = par_getd("problem", "rja_bgscale");
  pres_floor = par_getd("problem", "pres_floor");

  // Reset the center of the fluxrope at the grid
  int j_center;
  j_center = (int)(fr_h / pGrid->dx2);
  fr_h = (j_center)*pGrid->dx2;
  fr_xc = 0.0;
  cc_pos(pGrid, is, js, k, &x1c, &x2c, &x3c);
  //printf("xis = %.14f, yjs = %.14f\n", x1c, x2c);
  //printf("fr_xc=%.14f, fr_h=%.14f\n", fr_xc, fr_h);

  /* Total current inside the fluxrope */
  gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000000);
  Real result, error;
  Real alpha = 1.0;
  gsl_function F2;
  F2.function = &func_integ_current;
  F2.params = &alpha;
  gsl_integration_qags(&F2, 0, 10.0, 0, 1e-10, 1000000,
                       w, &result, &error);
  gsl_integration_workspace_free(w);
  I_total = result;

  Real I1 = fr_rja*2.0*PI*SQR(gauss_c1)
    * (1.0 - exp(-0.5*SQR(10.0/gauss_c1)));

  printf("I_num=%.4e, I1=%.4e\n", I_total, I1);

  /* Calculate bx, by */
  for (k = ks; k <= ke + 1; k++)
  {
    for (j = js - nghost; j <= je + nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
        bx[k][j][i] = func_bmx(x1c - 0.5 * dx_grid, x2c);
        by[k][j][i] = func_bmy(x1c, x2c - 0.5 * dy_grid);
        bz[k][j][i] = 0;
      }
    }
  }
  // Reset By <-- jz == 0, fix Bx
  // jz[k][j][i] = (by[k][j][i]-by[k][j][i-1])/dx_grid
  //             - (bx[k][j][i]-bx[k][j-1][i])/dy_grid;
  /*
  for (k = ks; k <= ke+1; k++) {
    for (j = js-nghost+1; j <= j_center; j++) {
      for (i = is-nghost+1; i <= ie+nghost; i++) {
	cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
	x1f = x1c - 0.5*dx_grid;
	x2f = x2c - 0.5*dy_grid;
	r = sqrt(pow(x1f-fr_xc, 2) + pow(x2f-fr_h, 2));
	jz[k][j][i] = (by[k][j][i]-by[k][j][i-1])/dx_grid
             - (bx[k][j][i]-bx[k][j-1][i])/dy_grid;
	
	//printf("+r=%f, jz(r)=%10.3e, jz[ij]=%10.3e\n", r, func_rjphi(r), jz[k][j][i]);
	if (r > (fr_ri + 0.5*fr_del)) {
	  by[k][j][i] = 0.0
                    + by[k][j][i-1]
                    + dx_grid*(bx[k][j][i]-bx[k][j-1][i])/dy_grid;
	}
      }
    }
    for (j = je+nghost; j > j_center ; j--) {
      for (i = is-nghost+1; i <= ie+nghost; i++) {
	cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
	x1f = x1c - 0.5*dx_grid;
	x2f = x2c - 0.5*dy_grid;
	r = sqrt(pow(x1f-fr_xc, 2) + pow(x2f-fr_h, 2));
	jz[k][j][i] = (by[k][j][i]-by[k][j][i-1])/dx_grid
             - (bx[k][j][i]-bx[k][j-1][i])/dy_grid;
	
	//printf("-r=%f, jz(r)=%10.3e, jz[ij]=%10.3e\n", r, func_rjphi(r), jz[k][j][i]);
	if (r > (fr_ri + 0.5*fr_del)) {
	  by[k][j][i] = 0.0
                    + by[k][j][i-1]
                    + dx_grid*(bx[k][j][i]-bx[k][j-1][i])/dy_grid;
	}
      }
    }
  }
  */

  /* Compute initial magnetic vector potential 
  Real azax, az0 = 0.0, x00 = fr_xc, y00 = fr_h;
  
  for (k = ks; k <= ke; k++) {
    for (j = js - nghost; j <= je + nghost + 1; j++) {
      for (i = is - nghost; i <= ie + nghost + 1; i++) {
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
        x1f = x1c - 0.5 * dx_grid;
        x2f = x2c - 0.5 * dy_grid;
        
        if (i == ie + nghost + 1) {
          cc_pos(pGrid, ie + nghost, j, k, &x1c, &x2c, &x3c);
          x1f = x1c + 0.5 * dx_grid;
        }
        if (j == je + nghost + 1) {
          cc_pos(pGrid, i, je + nghost, k, &x1c, &x2c, &x3c);
          x2f = x2c + 0.5 * dy_grid;
        }
        az[k][j][i] = func_azini(x1f, x2f);
      }
    }
  }
  */

  /* Recalculate Az */
  for (k = ks; k <= ke; k++)
  {
    az[k][js - nghost][is - nghost] = 0;
    for (j = js - nghost + 1; j <= je + nghost + 1; j++)
    {
      az[k][j][is - nghost] = az[k][j - 1][is - nghost] + bx[k][j - 1][is - nghost] * dy_grid;
    }
    for (j = js - nghost; j <= je + nghost + 1; j++)
    {
      for (i = is - nghost + 1; i <= ie + nghost + 1; i++)
      {
        az[k][j][i] = az[k][j][i - 1] - by[k][j][i - 1] * dx_grid;
      }
    }
  }

  /* Calculate bxi, byi from Az */
  for (k = ks; k <= ke + 1; k++)
  {
    for (j = js - nghost; j <= je + nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        bx[k][j][i] = (az[k][j + 1][i] - az[k][j][i]) / dy_grid;
        by[k][j][i] = -(az[k][j][i + 1] - az[k][j][i]) / dx_grid;
        bz[k][j][i] = 0;
      }
    }
  }

  /* Compute initial pressure, temperature and density */
  // (a) Jz
  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost + 1; j <= je + nghost; j++)
    {
      for (i = is - nghost + 1; i <= ie + nghost; i++)
      {
        jz[k][j][i] = (by[k][j][i] - by[k][j][i - 1]) / dx_grid - (bx[k][j][i] - bx[k][j - 1][i]) / dy_grid;
      }
    }
  }
  // (b) Pc
  Real byavg;
  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost + 1; j <= je + nghost; j++)
    {
      pc[k][j][is - nghost] = 0;
      pc[k][j][is - nghost + 1] = 0;
      for (i = is - nghost + 2; i <= ie + nghost; i++)
      {
        byavg = 0.5 * (by[k][j][i - 2] + by[k][j][i - 1]);
        cc_pos(pGrid, i - 1, j, k, &x1c, &x2c, &x3c);
        r = sqrt(pow(x1c - 0.5 * dx_grid - fr_xc, 2) + pow(x2c - 0.5 * dy_grid - fr_h, 2));
        pc[k][j][i] = pc[k][j][i - 2] - func_rjphi(r) * byavg * (2.0 * dx_grid);
      }
    }
  }
  // (c) average pressure
  Real peq_avg;
  Real dxh = 0.5 * pGrid->dx1, dyh = 0.5 * pGrid->dx2;
  Real rho0 = 1.0 * scale_ambient, t0 = Te_corona / Tchar, pgas0;
  Real beta_pre, pmag;
  pgas0 = rho0 * t0;
  printf("rho0 = %10.3e, pgas0 = %10.3e, t0 = %10.3e\n", rho0, pgas0, t0);
  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost; j <= je + nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
        peq_avg = 0.25 * (pc[k][j][i] + pc[k][j][i+1] + pc[k][j+1][i] + pc[k][j+1][i+1]);
 
        // Update Bz or Gas pressure ;
        //bz[k][j][i] = sqrt(2.0 * fabs(peq_avg));
        pgasini[k][j][i] = pgas0 + fabs(peq_avg);
        tempini[k][j][i] = t0*func_te_rel(x1c, x2c);
        //rhoini[k][j][i] = pgasini[k][j][i] / tempini[k][j][i];
        rhoini[k][j][i] = rho0*pow(pgasini[k][j][i]/pgas0, 3./5.)/func_te_rel(x1c, x2c);
        
        // Reset Bz and pgas
        pgasini[k][j][i] = tempini[k][j][i]*rhoini[k][j][i];
        Real pbz = MAX(pgas0 + fabs(peq_avg) - pgasini[k][j][i], 0.0);
        bz[k][j][i] = sqrt(2.0 * pbz);
      }
    }
  }

  /* All variables */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        /* density */
        pGrid->U[k][j][i].d = rhoini[k][j][i];

        /* monentum */
        pGrid->U[k][j][i].M1 = 0.0;
        pGrid->U[k][j][i].M2 = 0.0;
        pGrid->U[k][j][i].M3 = 0.0;

        /* magnetic field */
        pGrid->B1i[k][j][i] = bx[k][j][i];
        pGrid->B2i[k][j][i] = by[k][j][i];
        pGrid->B3i[k][j][i] = bz[k][j][i];

        /* initial setting for magnetic field on the boundary */
        if (i == ie && ie > is)
          pGrid->B1i[k][j][i + 1] = bx[k][j][i + 1];
        if (j == je && je > js)
          pGrid->B2i[k][j + 1][i] = by[k][j + 1][i];
        if (k == ke && ke > ks)
          pGrid->B3i[k + 1][j][i] = bz[k + 1][j][i];
      }
    }
  }

  /* Cell-center magnetic field */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        pGrid->U[k][j][i].B1c = 0.5 * (pGrid->B1i[k][j][i] +
                                       pGrid->B1i[k][j][i + 1]);
        pGrid->U[k][j][i].B2c = 0.5 * (pGrid->B2i[k][j][i] +
                                       pGrid->B2i[k][j + 1][i]);
        pGrid->U[k][j][i].B3c = pGrid->B3i[k][j][i];
      }
    }
  }

  /* total energy*/
  Real p_uniform = 0.5;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        pGrid->U[k][j][i].E = pgasini[k][j][i] / (Gamma_1) + 0.5 * (SQR(pGrid->U[k][j][i].B1c) + SQR(pGrid->U[k][j][i].B2c) + SQR(pGrid->U[k][j][i].B3c)) + 0.5 * (SQR(pGrid->U[k][j][i].M1) + SQR(pGrid->U[k][j][i].M2) + SQR(pGrid->U[k][j][i].M3)) / pGrid->U[k][j][i].d;
      }
    }
  }

  /* Set resistivity */
#ifdef RESISTIVITY
  eta_Ohm = par_getd("problem", "eta_Ohm");
  Q_AD = par_getd("problem", "Q_AD");
  Q_Hall = 0.0;
  d_ind = 0.0;
#endif

  /* Set thermel conduction coefficient */
#ifdef THERMAL_CONDUCTION
  kappa_aniso = 1.0;
  kappa_iso = 0;
#endif

  /* Set viscosity */
#ifdef VISCOSITY
  // nu0 = 3.4e15 cm^2 s^-1,
  //nu_iso = 3.4e15*Timechar/(Lchar*1.0e2)/(Lchar*1.0e2);
  //printf("Non-dimensional nu_iso=%10.3e\n", nu_iso);
  //nu_aniso = 0.0;
#endif

  /* Enroll gravitational potential to give acceleration in y-direction for 2D
  StaticGravPot = gsun_static; */

  /* Set optical thin radiation cooling and corona heating function */

  /* Set boundary value functions: Order for updating boundary conditions must
      always be x1-x2-x3 in order to
      fill the corner cells properly*/

  /* (a) left-open */
  bvals_mhd_fun(pDomain, left_x1, openbc_ix1);

  /* (b) Right-open */
  //bvals_mhd_fun(pDomain, right_x1, openbc_ox1);
  bvals_mhd_fun(pDomain, right_x1, symmbc_ox1);

  /* (c) Botom-open */
  //bvals_mhd_fun(pDomain, left_x2, openbc_ix2);
  bvals_mhd_fun(pDomain, left_x2, linetiedbc_ix2);

  /* (d) Top-open */
  bvals_mhd_fun(pDomain, right_x2, openbc_ox2);
}

/*==============================================================================
 * PROBLEM USER FUNCTIONS:
 * problem_write_restart() - writes problem-specific user data to restart files
 * problem_read_restart()  - reads problem-specific user data from restart files
 * get_usr_expr()          - sets pointer to expression for special output data
 * get_usr_out_fun()       - returns a user defined output function pointer
 * get_usr_par_prop()      - returns a user defined particle selection function
 * Userwork_in_loop        - problem specific work IN     main loop
 * Userwork_after_loop     - problem specific work AFTER  main loop
 *----------------------------------------------------------------------------*/

void problem_write_restart(MeshS *pM, FILE *fp)
{
  return;
}

void problem_read_restart(MeshS *pM, FILE *fp)
{
  return;
}

static Real out_azini(const GridS *pG, const int i, const int j, const int k)
{
  return az[k][j][i];
}

static Real out_jz(const GridS *pG, const int i, const int j, const int k)
{
  return ((pG->B2i[k][j][i] - pG->B2i[k][j][i - 1]) / pG->dx1 -
          (pG->B1i[k][j][i] - pG->B1i[k][j - 1][i]) / pG->dx2);
}

static Real out_accy(const GridS *pG, const int i, const int j, const int k)
{
  Real jz = ((pG->B2i[k][j][i] - pG->B2i[k][j][i - 1]) / pG->dx1 -
             (pG->B1i[k][j][i] - pG->B1i[k][j - 1][i]) / pG->dx2);
  Real bx_avg = 0.5*(pG->B1i[k][j][i] + pG->B1i[k][j - 1][i]);
  Real rho_avg = 0.25*(pG->U[k][j][i].d
                      +pG->U[k][j][i-1].d
                      +pG->U[k][j-1][i].d
                       +pG->U[k][j-1][i-1].d);
  return jz*bx_avg/rho_avg;
}

ConsFun_t get_usr_expr(const char *expr)
{
  if (strcmp(expr, "azini") == 0)
    return out_azini;
  if (strcmp(expr, "jz") == 0)
    return out_jz;
  if (strcmp(expr, "accy") == 0)
    return out_accy;
  return NULL;
}

VOutFun_t get_usr_out_fun(const char *name)
{
  return NULL;
}

#ifdef RESISTIVITY

void get_eta_user(GridS *pG, int i, int j, int k,
                  Real *eta_O, Real *eta_H, Real *eta_A)
{
  return;
}
#endif

void Userwork_in_loop(MeshS *pM)
{
  GridS *pG = pM->Domain[0][0].Grid;
  int is = pG->is, ie = pG->ie;
  int js = pG->js, je = pG->je;
  int ks = pG->ks, ke = pG->ke;
  int i, j, k;
  int ic, jc;
  int jj;
  int ista, iend, jsta, jend, n1 = ie-is+1+2*nghost;
  Real x1, x2, x3;
  Real rc;
  Real pres_arr[n1], vx_arr[n1], vy_arr[n1];

  /* Reset vz == 0 */
  Real ek_z;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        pG->U[k][j][i].E = pG->U[k][j][i].E - 0.5 * SQR(pG->U[k][j][i].M3) / pG->U[k][j][i].d;
        pG->U[k][j][i].M3 = 0.0;
      }
    }
  }
  
  return;
}

void Userwork_after_loop(MeshS *pM)
{
  return;
}

/*=========================== PRIVATE FUNCTIONS ==============================*/
/* ============================================================================
 * Functions for fluxrope
 * ===========================================================================*/
/*----------------------------------------------------------------------------*/
static Real func_bmx(const Real x, const Real y)
{
  /*c model field x-component */
  Real rs, rm, rd;
  Real bmx, bmx_back;
  rs = sqrt(pow(x, 2) + pow(y - fr_h, 2));
  rm = sqrt(pow(x, 2) + pow(y + fr_h, 2));
  rd = sqrt(pow(x, 2) + pow(y + fr_d, 2));
  
  // flux rope (and its mirros)
  bmx = 0.0;
  if (rs > 0.0) {
    bmx = +func_bphi(rs)*(y-fr_h)/rs
    -func_bphi(rm)*(y+fr_h)/rm;
  } else {
    bmx = 0.0;
  }
  
  // background
  if (fr_case == 1) {
    /* dipole case */
    Real I1 = fr_rja*2.0*PI*SQR(gauss_c1)
      * (1.0 - exp(-0.5*SQR(10.0/gauss_c1))); // Gaussion Jz(r)
    bmx_back = I1/(2.0*PI)*fr_rmom*fr_d*(pow(x,2)-pow(y+fr_d, 2))/pow(rd,4);
  } else if (fr_case == 2) {
    /* quadrupole */
    bmx_back = -fr_rmom*func_back(rd)*(pow(y+fr_d,3)-3.0*(y+fr_d)*pow(x,2))/pow(rd,3);
  } else {
    bmx_back = 0.0;
  }
  
  return bmx+bmx_back;
}

/*----------------------------------------------------------------------------*/
static Real func_bmy(const Real x, const Real y)
{
  /*  model field y-component */
  Real rs, rm, rd;
  Real bmy, bmy_back;
  rs = sqrt(pow(x, 2) + pow(y - fr_h, 2));
  rm = sqrt(pow(x, 2) + pow(y + fr_h, 2));
  rd = sqrt(pow(x, 2) + pow(y + fr_d, 2));
  
  // fluxrope
  bmy = 0;
  if (rs > 0.0) {
    bmy = -func_bphi(rs)*x/rs
    +func_bphi(rm)*x/rm;
  } else {
    bmy = 0;
  }
  
  // background
  if (fr_case == 1) {
    /* dipole case */
    Real I1 = fr_rja*2.0*PI*SQR(gauss_c1)
      * (1.0 - exp(-0.5*SQR(10.0/gauss_c1))); // Gaussion Jz(r)
    bmy_back = I1/(2.0*PI)*fr_rmom*fr_d*(2.0*x*(y+fr_d))/pow(rd,4);
  } else if (fr_case == 2) {
    /* quadrupole */
    bmy_back = -fr_rmom*(func_back(rd))*(pow(x,3) - 3.0*x*pow((y+fr_d),2))/pow(rd,3);
  } else {
    bmy_back = 0.0;
  }
  
  return bmy+bmy_back;
}

/*----------------------------------------------------------------------------*/
static Real func_bphi(const Real r)
{
  /* cylindrical field function */
  Real riq, delq, piq, t1, t2, t3, bphi;
  Real pi = 3.14159265358979;
  Real ro, r1, r2;
  Real I;
  
  /*
  r1 = fr_ri - 0.5 * fr_del;
  r2 = fr_ri + 0.5 * fr_del;

  riq = fr_ri * fr_ri;
  delq = fr_del * fr_del;
  piq = pi * pi;

  if (r <= r1)
  {
    bphi = -0.5 * fr_rja * r;
  }
  else if (r <= r2)
  {
    t1 = 0.5 * r1 * r1 - delq / piq + 0.5 * r * r;
    t2 = (fr_del * r / pi) * sin((pi / fr_del) * (r - r1));
    t3 = (delq / piq) * cos((pi / fr_del) * (r - r1));
    bphi = -0.5 * fr_rja * (t1 + t2 + t3) / r;
  }
  else
  {
    bphi = -0.5 * fr_rja * (riq + 0.25 * delq - 2. * delq / piq) / r;
  }
  */

  /* Numerical Integration
  gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000000);

  Real result, error;
  Real alpha = 1.0;
  gsl_function F2;
  F2.function = &func_integ_current;
  F2.params = &alpha;
  gsl_integration_qags(&F2, 0, r, 0, 1e-10, 1000000,
                       w, &result, &error);
  gsl_integration_workspace_free(w);
  I = result;
  */

  /* Gaussian distribtion */
  Real I1 = fr_rja*2.0*PI*SQR(gauss_c1)
     *(1.0 - exp(-0.5*SQR(r/gauss_c1)));

  Real Ibg = rja_bgscale*fr_rja*2.0*PI*SQR(gauss_c2)
     *(1.0 - exp(-0.5*SQR(r/gauss_c2)));
  I = I1 + Ibg;

  if (r > 0.0)
  {
    bphi = -I / (2.0 * pi * r);
  }
  else
  {
    bphi = 0.0;
  }

  return bphi;
}

/*----------------------------------------------------------------------------*/
static Real func_rjphi(const Real r)
{
  /*  current density */
  Real pi = 3.14159265358979;
  Real ro, r1, r2;
  Real rjphi;

  /*
  r1 = fr_ri - 0.5 * fr_del;
  r2 = fr_ri + 0.5 * fr_del;

  if (r <= r1)
  {
    rjphi = 1.0 * fr_rja;
  }
  else if (r <= r2)
  {
    rjphi = 0.5 * fr_rja * (cos((pi / fr_del) * (r - r1)) + 1.);
  }
  else
  {
    rjphi = 0.0;
  }
  */

  /* Gaussion distribution */
  rjphi = fr_rja*exp(-0.5*pow((r/gauss_c1), 2));
  
  // Add a background jz
  rjphi = rjphi + rja_bgscale*fr_rja*exp(-0.5*pow((r/gauss_c2), 2));

  return rjphi;
}

static Real func_te_rel(const Real x, const Real y)
{
  /* Temperature inside the fluxrope */
  Real pi = 3.14159265358979;
  Real r = sqrt(pow(x - fr_xc, 2) + pow(y - fr_h, 2));
  Real Te, t1, t2;

  /* Temperature distribution in y-direction: in non-dimensional forms */
  Real h = 3.5e6 / Lchar;
  Real w = 1.0e6 / Lchar;
  Real tecor = 1.0, techr = 1.0e-4 * tecor;
  t1 = 0.5 * (tecor - techr);
  t2 = 0.5 * (tecor + techr);
  Te = t1 * tanh((y - h) / w) + t2;
  
  return Te;
}

/*----------------------------------------------------------------------------*/
static Real func_uphi_xy(const Real x, const Real y)
{
  //Real r = sqrt(pow(x - fr_xc, 2) + pow(y - fr_h, 2));
  //return func_uphi(r);

  // Along x- direction
  Real uphi, xend;
  Real yc;
  yc = y;
  gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000000);
  Real error;
  gsl_function F;
  F.function = &func_integ_pphi_v3;
  F.params = &yc;
  if (x <= 0)
  {
    xend = -10.0;
  }
  else
  {
    xend = 10.0;
  }
  gsl_integration_qags(&F, x, xend, 0, 1e-10, 1000000,
                       w, &uphi, &error);
  gsl_integration_workspace_free(w);
  return uphi;
}

/*----------------------------------------------------------------------------*/
static Real func_uphi(const Real r)
{
  Real rend = 10.0;
  Real uphi;
  //uphi = adaptiveSimpsons(my_integ_pphi, r, rend, 1.0e-7, 100000);

  // Use GSL Lib
  gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000000);
  Real error;
  gsl_function F;
  F.function = &func_integ_pphi_v2;
  gsl_integration_qags(&F, r, rend, 0, 1e-10, 1000000,
                       w, &uphi, &error);
  gsl_integration_workspace_free(w);
  return uphi;
}

static Real func_back(const Real r)
{
  Real riq, delq, piq, rmm, back;
  riq = pow(fr_ri, 2);
  delq = pow(fr_del, 2);
  piq = pow(PI, 2);
  //rmm = 0.5 * fr_rja * (riq + 0.25 * delq - 2. * delq / piq);
  Real I1 = fr_rja*2.0*PI*SQR(gauss_c1)
  * (1.0 - exp(-0.5*SQR(10.0/gauss_c1)));
  rmm = I1/(2.0*PI);
  back = rmm / pow(r, 3);
  return back;
}

/* ----------------------------------------------------------------------------
 * For integration
 * ---------------------------------------------------------------------------*/
Real func_integ_bx(Real y, void *params)
{
  Real xfix = *(Real *)params;
  return func_bmx(xfix, y);
}

Real func_integ_by(Real x, void *params)
{
  Real yfix = *(Real *)params;
  return -func_bmy(x, yfix);
}

Real my_integ_pphi(Real r)
{
  return func_rjphi(r) * func_bphi(r);
}

Real func_integ_pphi_v2(Real r, void *params)
{
  return func_rjphi(r) * func_bphi(r);
}

Real func_integ_pphi_v3(Real x, void *params)
{
  Real yc = *(Real *)params;
  Real r = sqrt(pow(x - fr_xc, 2) + pow(yc - fr_h, 2));
  return func_rjphi(r) * func_bmy(x, yc);
}

Real func_integ_current(Real r, void *params)
{
  Real func_integ_current = func_rjphi(r)*PI*r*2.0;
  return func_integ_current;
}

Real func_integ_px_yc(Real x, Real *y_current)
{
  Real yc = *(Real *)y_current;
  Real func_integ_px_yc = -func_uphi_xy(x, yc);
  return func_integ_px_yc;
}

Real func_integ_py(Real y, void *p)
{
  struct struct_params_2 *params = (struct struct_params_2 *)p;
  Real xsta = (params->a);
  Real xend = (params->b);

  gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000000);
  Real result, error;
  gsl_function F;
  F.function = &func_integ_px_yc;
  F.params = &y;
  gsl_integration_qags(&F, xsta, xend, 0, 1e-10, 1000000,
                       w, &result, &error);
  gsl_integration_workspace_free(w);
  return result;
}

Real func_pres_avg(Real const xc, Real const yc, Real const dx, Real const dy)
{
  Real pavg, ptotal;
  Real xsta = xc - 0.5 * dx, xend = xc + 0.5 * dx;
  Real ysta = yc - 0.5 * dy, yend = yc + 0.5 * dy;
  Real r_lim = sqrt(pow(xc - fr_xc, 2) + pow(yc - fr_h, 2)) - sqrt(pow(0.5 * dx, 2) + pow(0.5 * dy, 2));
  Real error;

  if (func_rjphi(r_lim) == 0)
  {
    pavg = 0.0;
  }
  else
  {
    gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000000);
    gsl_function F;
    struct struct_params_2 xrange = {xsta, xend};
    F.function = &func_integ_py;
    F.params = &xrange;
    gsl_integration_qags(&F, ysta, yend, 0, 1e-10, 1000000,
                         w, &ptotal, &error);
    gsl_integration_workspace_free(w);
    pavg = ptotal / (dx * dy);
  }
  return pavg;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void openbc_ix1(GridS *pGrid)
 *  \brief open boundary condition, Inner x1 boundary (bc_ix1=2) */
static void openbc_ix1(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][is - i] = pGrid->U[k][j][is];
      }
    }
  }

#ifdef MHD
  /* B2i: */
  if (pGrid->Nx[1] > 1)
    ju = je + 1;
  else
    ju = je;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= ju; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B2i[k][j][is - i] = 2.0 * pGrid->B2i[k][j][is - i + 1] - pGrid->B2i[k][j][is - i + 2];
      }
    }
  }

  /* B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost - 1; i++)
      {
        pGrid->B1i[k][j][is - i] = pGrid->B1i[k][j][is - i + 1] + (pGrid->dx1 / pGrid->dx2) * (pGrid->B2i[k][j + 1][is - i] - pGrid->B2i[k][j][is - i]);
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B3i[k][j][is - i] = pGrid->B3i[k][j][is];
      }
    }
  }
#endif /* MHD */

  /* B1c, 2c, 3c */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost - 1; i++)
      {
        pGrid->U[k][j][is - i].B1c = 0.5 * (pGrid->B1i[k][j][is - i] + pGrid->B1i[k][j][is - i + 1]);
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][is - i].B2c = 0.5 * (pGrid->B2i[k][j][is - i] + pGrid->B2i[k][j + 1][is - i]);
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][is - i].B3c = pGrid->B3i[k][j][is - i];
      }
    }
  }

  /* Pressure and energy */
  int ic;
  Real eb_is, ek_is, pis_o_gm;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        eb_is = 0.5 * (SQR(pGrid->U[k][j][is].B1c) + SQR(pGrid->U[k][j][is].B2c) + SQR(pGrid->U[k][j][is].B3c));
        ek_is = 0.5 * (SQR(pGrid->U[k][j][is].M1) + SQR(pGrid->U[k][j][is].M2) + SQR(pGrid->U[k][j][is].M3)) / pGrid->U[k][j][is].d;
        pis_o_gm = pGrid->U[k][j][is].E - eb_is - ek_is;

        ic = is - i;
        pGrid->U[k][j][ic].E = pis_o_gm + 0.5 * (SQR(pGrid->U[k][j][ic].B1c) + SQR(pGrid->U[k][j][ic].B2c) + SQR(pGrid->U[k][j][ic].B3c)) + 0.5 * (SQR(pGrid->U[k][j][ic].M1) + SQR(pGrid->U[k][j][ic].M2) + SQR(pGrid->U[k][j][ic].M3)) / pGrid->U[k][j][ic].d;
      }
    }
  }

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void openbc_ix2(GridS *pGrid)
 *  \brief openbc boundary conditions, Inner x2 boundary (bc_ix2=2) */
static void openbc_ix2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ku; /* k-upper */
#endif
  int jc;

  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i] = pGrid->U[k][js][i];
      }
    }
  }

#ifdef MHD
  /* B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      jc = js - j;
      for (i = is - (nghost - 1); i <= ie + nghost; i++)
      {
        pGrid->B1i[k][js - j][i] = 2.0 * pGrid->B1i[k][js - j + 1][i] - pGrid->B1i[k][js - j + 2][i];
      }
    }
  }

  /* B2i is not set at j=js-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost - 1; j++)
    {
      jc = js - j;
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B2i[k][js - j][i] = pGrid->B2i[k][js - j + 1][i] + (pGrid->dx2 / pGrid->dx1) * (pGrid->B1i[k][js - j][i + 1] - pGrid->B1i[k][js - j][i]);
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B3i[k][js - j][i] = pGrid->B3i[k][js][i];
      }
    }
  }

  /* B1c, B2c */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - (nghost - 1); i <= ie + (nghost - 1); i++)
      {
        pGrid->U[k][js - j][i].B1c = 0.5 * (pGrid->B1i[k][js - j][i] + pGrid->B1i[k][js - j][i + 1]);
      }
    }
  }

  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost - 1; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i].B2c = 0.5 * (pGrid->B2i[k][js - j][i] + pGrid->B2i[k][js - j + 1][i]);
      }
    }
  }
#endif /* MHD */

  /* Pressure and energy */
  Real eb_js, ek_js, pjs;
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        eb_js = 0.5 * (SQR(pGrid->U[k][js][i].B1c) + SQR(pGrid->U[k][js][i].B2c) + SQR(pGrid->U[k][js][i].B3c));
        ek_js = 0.5 * (SQR(pGrid->U[k][js][i].M1) + SQR(pGrid->U[k][js][i].M2) + SQR(pGrid->U[k][js][i].M3)) / pGrid->U[k][js][i].d;
        pjs = Gamma_1 * (pGrid->U[k][js][i].E - eb_js - ek_js);

        jc = js - j;
        pGrid->U[k][jc][i].E = pjs / (Gamma_1) + 0.5 * (SQR(pGrid->U[k][jc][i].B1c) + SQR(pGrid->U[k][jc][i].B2c) + SQR(pGrid->U[k][jc][i].B3c)) + 0.5 * (SQR(pGrid->U[k][jc][i].M1) + SQR(pGrid->U[k][jc][i].M2) + SQR(pGrid->U[k][jc][i].M3)) / pGrid->U[k][jc][i].d;
      }
    }
  }
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void linetiedbc_ix2(GridS *pGrid)
 *  \brief Sets boundary condition at the bottom.
 */
/*  ix2, line-tied, bottom */
void linetiedbc_ix2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
  Real x1c, x2c, x3c;
  Real x1f, x2f;
  Real x1_js, x2_js, x3_js;

#ifdef MHD
  int ku; /* k-upper */
#endif

  /* Set all variables in ghost zone */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i] = pGrid->U[k][js][i];
        pGrid->U[k][js - j][i].d = rhoini[k][j - j][i];
        pGrid->U[k][js - j][i].M1 = 0.0;
        pGrid->U[k][js - j][i].M2 = 0.0;
        pGrid->U[k][js - j][i].M3 = 0.0;
      }
    }
  }

#ifdef MHD
  /* B1i is not set at i=is-nghost */
  Real pbypx;
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - (nghost - 1); i <= ie + nghost; i++)
      {
        //cc_pos(pGrid, i, js - j + 1, k, &x1c, &x2c, &x3c);
        //x1f = x1c - 0.5 * pGrid->dx1;
        //x2f = x2c - 0.5 * pGrid->dx2;
        //pbypx = func_pbypxini(x1f, x2f);
        pbypx = (by[k][js - j + 1][i] - by[k][js - j + 1][i - 1]) / pGrid->dx1;
        pGrid->B1i[k][js - j][i] = pGrid->B1i[k][js - j + 1][i] - pbypx * (pGrid->dx2);
      }
    }
  }
  /* B2i is not set at j=js-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B2i[k][js - j][i] = by[k][js - j][i];
      }
    }
  }
  /* B3i */
  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B3i[k][js - j][i] = pGrid->B3i[k][js][i];
      }
    }
  }
#endif /* MHD */

  /* Pressure and energy */
  Real eb_js, ek_js, pjs;
  int jc;
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        eb_js = 0.5 * (SQR(pGrid->U[k][js][i].B1c) + SQR(pGrid->U[k][js][i].B2c) + SQR(pGrid->U[k][js][i].B3c));
        ek_js = 0.5 * (SQR(pGrid->U[k][js][i].M1) + SQR(pGrid->U[k][js][i].M2) + SQR(pGrid->U[k][js][i].M3)) / pGrid->U[k][js][i].d;
        pjs = Gamma_1 * (pGrid->U[k][js][i].E - eb_js - ek_js);

        jc = js - j;
        pGrid->U[k][jc][i].E = pjs / (Gamma_1) + 0.5 * (SQR(pGrid->U[k][jc][i].B1c) + SQR(pGrid->U[k][jc][i].B2c) + SQR(pGrid->U[k][jc][i].B3c));
      }
    }
  }
  return;
}

static Real func_pbypxini(Real x, Real y)
{
  Real pbypx;
  pbypx = (func_bmy(x + 1.0e-9, y) - func_bmy(x - 1.0e-9, y)) / 2.0e-9;
  return pbypx;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void openbc_ox1(GridS *pGrid)
 *  \brief open boundary conditions, Outer x1 boundary (bc_ox1=2) */

static void openbc_ox1(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][ie + i] = pGrid->U[k][j][ie];
      }
    }
  }

#ifdef MHD
  /* B2i */
  if (pGrid->Nx[1] > 1)
    ju = je + 1;
  else
    ju = je;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= ju; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B2i[k][j][ie + i] = 2.0 * pGrid->B2i[k][j][ie + i - 1] - pGrid->B2i[k][j][ie + i - 2];
      }
    }
  }

  /* B1i: i=ie+1 is not a boundary condition for the interface field B1i */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 2; i <= nghost; i++)
      {
        pGrid->B1i[k][j][ie + i] = -(pGrid->dx1 / pGrid->dx2) * (pGrid->B2i[k][j + 1][ie + i - 1] - pGrid->B2i[k][j][ie + i - 1]) + pGrid->B1i[k][j][ie + i - 1];
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B3i[k][j][ie + i] = pGrid->B3i[k][j][ie];
      }
    }
  }
#endif /* MHD */

  /* B1c, 2c, 3c */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost - 1; i++)
      {
        pGrid->U[k][j][ie + i].B1c = 0.5 * (pGrid->B1i[k][j][ie + i] + pGrid->B1i[k][j][ie + i + 1]);
      }
    }
  }

  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][ie + i].B2c = 0.5 * (pGrid->B2i[k][j][ie + i] + pGrid->B2i[k][j + 1][ie + i]);
      }
    }
  }

  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][ie + i].B3c = pGrid->U[k][j][ie].B3c;
      }
    }
  }

  /* Pressure and energy */
  int ic;
  Real eb_ic, ek_ic, pic;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {

        ic = ie;
        eb_ic = 0.5 * (SQR(pGrid->U[k][j][ic].B1c) + SQR(pGrid->U[k][j][ic].B2c) + SQR(pGrid->U[k][j][ic].B3c));
        ek_ic = 0.5 * (SQR(pGrid->U[k][j][ic].M1) + SQR(pGrid->U[k][j][ic].M2) + SQR(pGrid->U[k][j][ic].M3)) / pGrid->U[k][j][ic].d;
        pic = Gamma_1 * (pGrid->U[k][j][ic].E - eb_ic - ek_ic);

        ic = ie + i;
        pGrid->U[k][j][ic].E = pic / (Gamma_1) + 0.5 * (SQR(pGrid->U[k][j][ic].B1c) + SQR(pGrid->U[k][j][ic].B2c) + SQR(pGrid->U[k][j][ic].B3c)) + 0.5 * (SQR(pGrid->U[k][j][ic].M1) + SQR(pGrid->U[k][j][ic].M2) + SQR(pGrid->U[k][j][ic].M3)) / pGrid->U[k][j][ic].d;
      }
    }
  }

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void symmbc_ox1(GridS *pGrid)
 *  \brief OUTFLOW boundary conditions, Outer x1 boundary (bc_ox1=2) */
static void symmbc_ox1(GridS *pGrid)
{
  int ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][ie + i] = pGrid->U[k][j][ie - i + 1];
        pGrid->U[k][j][ie + i].B2c = -pGrid->U[k][j][ie - i + 1].B2c;
        pGrid->U[k][j][ie + i].M1 = -pGrid->U[k][j][ie - i + 1].M1;
      }
    }
  }

#ifdef MHD
  /* i=ie+1 is not a boundary condition for the interface field B1i */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 2; i <= nghost; i++)
      {
        pGrid->B1i[k][j][ie + i] = pGrid->B1i[k][j][ie - i + 2];
      }
    }
  }

  if (pGrid->Nx[1] > 1)
    ju = je + 1;
  else
    ju = je;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= ju; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B2i[k][j][ie + i] = -pGrid->B2i[k][j][ie - i + 1];
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B3i[k][j][ie + i] = pGrid->B3i[k][j][ie - i + 1];
      }
    }
  }
#endif /* MHD */

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void outflow_ox2(GridS *pGrid)
 *  \brief OpenBC_ox2 boundary conditions, Outer x2 boundary (bc_ox2=2) */
static void openbc_ox2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ku; /* k-upper */
#endif
  int jc;

  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][je + j][i] = pGrid->U[k][je][i];
      }
    }
  }

#ifdef MHD
  /* B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - (nghost - 1); i <= ie + nghost; i++)
      {
        pGrid->B1i[k][je + j][i] = 2.0 * pGrid->B1i[k][je + j - 1][i] - pGrid->B1i[k][je + j - 2][i];
      }
    }
  }

  /* j=je+1 is not a boundary condition for the interface field B2i */
  for (k = ks; k <= ke; k++)
  {
    for (j = 2; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B2i[k][je + j][i] = -(pGrid->dx2 / pGrid->dx1) * (pGrid->B1i[k][je + j - 1][i + 1] - pGrid->B1i[k][je + j - 1][i]) + pGrid->B2i[k][je + j - 1][i];
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B3i[k][je + j][i] = pGrid->B3i[k][je][i];
      }
    }
  }

  /* B1c, B2c */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - (nghost - 1); i <= ie + (nghost - 1); i++)
      {
        pGrid->U[k][je + j][i].B1c = 0.5 * (pGrid->B1i[k][je + j][i] + pGrid->B1i[k][je + j][i + 1]);
      }
    }
  }

  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost - 1; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][je + j][i].B2c = 0.5 * (pGrid->B2i[k][je + j][i] + pGrid->B2i[k][je + j + 1][i]);
      }
    }
  }

#endif /* MHD */

  /* Pressure and energy */
  Real eb_je, ek_je, pje;
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        eb_je = 0.5 * (SQR(pGrid->U[k][je][i].B1c) + SQR(pGrid->U[k][je][i].B2c) + SQR(pGrid->U[k][je][i].B3c));
        ek_je = 0.5 * (SQR(pGrid->U[k][je][i].M1) + SQR(pGrid->U[k][je][i].M2) + SQR(pGrid->U[k][je][i].M3)) / pGrid->U[k][je][i].d;
        pje = Gamma_1 * (pGrid->U[k][je][i].E - eb_je - ek_je);

        jc = je + j;
        pGrid->U[k][je + j][i].E = pje / (Gamma_1) + 0.5 * (SQR(pGrid->U[k][jc][i].B1c) + SQR(pGrid->U[k][jc][i].B2c) + SQR(pGrid->U[k][jc][i].B3c)) + 0.5 * (SQR(pGrid->U[k][jc][i].M1) + SQR(pGrid->U[k][jc][i].M2) + SQR(pGrid->U[k][jc][i].M3)) / pGrid->U[k][jc][i].d;
      }
    }
  }
  return;
}
