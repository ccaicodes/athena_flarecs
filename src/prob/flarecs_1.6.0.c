#include "copyright.h"
/*============================================================================*/
/*! \file flarecs_1.6.0.c
 *  \brief  Problem generator for Fluxrope.
 *
 * PURPOSE: Flare CurrentSheet.
 *
 * REFERENCE: Shen, Lin, & Murphy, ApJ 2011.
 *
 */

/*============================================================================*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defs.h"
#include "athena.h"
#include "globals.h"
#include "prototypes.h"
#include "./prob/simpson_integ.c"
#include "./microphysics/nei_calc.c"

/*==============================================================================
 * PRIVATE FUNCTION PROTOTYPES:
 * void flarecs_linetied() - sets BCs on R-x2 boundary
 *============================================================================*/
static Real func_pcs(const Real x1, const Real x2);
static Real func_teini(const Real x1, const Real x2);

/* functions for compute flux */
static Real func_brini(const Real r, const Real phi);
static Real func_bxini(const Real x1, const Real x2);
static Real func_byini(const Real x1, const Real x2);
static Real func_bzini(const Real x1, const Real x2);

/* boundary */
static void openbc_ix1(GridS *pGrid);
static void symmbc_ox1(GridS *pGrid);
static void openbc_ox2(GridS *pGrid);
static void linetiedbc_ix2(GridS *pGrid);
static Real func_pbypxini(Real x, Real y);

/* Gravity */
static Real gsun_static(const Real x1, const Real x2, const Real x3);
static Real gsun_y(const Real x2);
static Real func_presini_integral(Real x2, 
                                  const Real pgas_sta,
                                  const Real posi_sta);
static Real func_presini_isote(const Real x2, const Real y0,
                               const Real p0, const Real te0);
Real func_integ_py(Real y);

/*============================================================================*/
#define PI 3.14159265358979323846

/* Normalization parameters */
Real Lchar, Bchar, Nechar, Rhochar, Pchar, Timechar, Tchar, Vchar;
Real Gsun_nondim;

/* c. The height for define the pgas_c in corona */
Real dens_c_dimension = 5.0e15; /* (m^-3) ~ 5.0e9 (cm^-3)*/
Real dens_c;
Real pgas_c, posi_c;

/* Grid size */
Real dx_grid, dy_grid, dz_grid;

/* Constant parameters */
Real Kb = 1.38e-23, Mp = 1.6726e-27, Mu0 = 1.25663706144e-6, LnA = 30.0;
Real gravitational_const = 6.672e-11; /* (N M^2 kg^-2)*/
Real solarmass = 1.99e+30;            /* (kg) */
Real solarradius = 6.96e+8;           /* (m) */

/* Initial conditions */
Real ***pgasini, ***rhoini, ***tempini;
Real ***bx, ***by, ***bz;
Real cs_mode, cs_width;
Real Te_corona, Te_photos;
Real Height_TR, Width_TR;
Real sw_bz;
Real sw_etajz, jz_eta0;

/* Coronal heating rate H */
Real ***H_rate, ***dens_pre;

/* Define the temperature range in computing kappa(T) */
Real temp_low, temp_high;

/* Artificialy reset cooling and conduction in lower temperature region */
Real Tc_low;
int sw_lowt_cool; // use for decreasing the cooling and increacing thermal
                  // conduction once tempreture is lower than Tc_low to keep
                  // Kappa(T)*Q(T) constant. (Reeves et al.2010 ApJ)
int sw_explicit_conduct_dt;

/* Reset Output dt = dt_reset_out after time_reset_out */
Real dt_reset_out = 0.05, time_reset_out = 100.0;

/* Initial NEI parameters */
int sw_nei;

/*=========================== PUBLIC FUNCTIONS ===============================*/
/*----------------------------------------------------------------------------*/
/* problem:  */
void problem(DomainS *pDomain)
{
  GridS *pGrid = (pDomain->Grid);
  int i, is = pGrid->is, ie = pGrid->ie;
  int j, js = pGrid->js, je = pGrid->je;
  int k, ks = pGrid->ks, ke = pGrid->ke;
  int n1 = ie - is + 1 + 2 * nghost;
  int n2 = je - js + 1 + 2 * nghost;
  int n3 = ke - ks + 1 + 2 * nghost;

  Real x1, x2, x3;
  Real x1c, x2c, x3c;
  Real x1f, x2f, x3f;
  Real x1s, x2s, x3s;
  Real r;
  Real ***az;
  dx_grid = pGrid->dx1;
  dy_grid = pGrid->dx2;
  dz_grid = pGrid->dx3;

  /* Inilize pgasini, rhoini, and tempini */
  if ((pgasini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for pgasini \n");
  }
  if ((rhoini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for rhoini \n");
  }
  if ((tempini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for tempini \n");
  }

  if ((bx = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector bx\n");
  }
  if ((by = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector by\n");
  }
  if ((bz = (Real ***)calloc_3d_array(n3, n2 + 2, n1 + 2, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector bz\n");
  }
  if ((az = (Real ***)calloc_3d_array(n3 + 1, n2 + 1, n1 + 1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector az\n");
  }

  /* Termal conduction & viscosity */
  Real k_spitzer, kappa_v;
  Real nud, nu0;

  /* Magnitude of perturbations */
  Real pi;
  Real vin_turb;
  Real Lx, Ly, delta_bx, delta_by;
  Real pos_x_turb, pos_y_turb, hwd_x_turb, hwd_y_turb;

  /* Dence bottom boundary */
  Real htr, wtr, chr_rho, cor_rho;

  /* Read input parameters */
  Lchar = par_getd("problem", "Lchar");
  Bchar = par_getd("problem", "Bchar");
  Nechar = par_getd("problem", "Nechar");

  Pchar = Bchar * Bchar / Mu0;
  Rhochar = Nechar * Mp;
  Tchar = Pchar / (Nechar * 2. * Kb); /*total number is 2*ne for full ionization 
                                  plasma */
  Vchar = Bchar / sqrt(Mu0 * Rhochar);
  Timechar = Lchar / Vchar;

  /* Print*/
  int myid;
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  if (myid == 0)
  {
    printf("nechar=%10.3e (cm^-3), Rhochar=%10.3e (kg/m^3)\n",
           Nechar / 1.0e6, Rhochar);
    printf("Pchar=%10.3e (Pa)\n", Pchar);
    printf("Tchar=%10.3e (K)\n", Tchar);
    printf("Vchar=%10.3e (km/s)\n", Vchar / 1.0e3);
    printf("Lchar=%10.3e (km)\n", Lchar / 1.0e3);
    printf("Bchar=%10.3e (T)\n", Bchar);
    printf("Timechar=%10.3e (s)\n", Timechar);
  }

  /* CS condition */
  sw_bz = par_getd("problem", "sw_bz");
  cs_mode  = par_getd("problem", "cs_mode");
  cs_width = par_getd("problem", "cs_width");
  Te_corona = par_getd("problem", "tecor");
  Te_photos = par_getd("problem", "tepho");
  Height_TR = par_getd("problem", "htr");
  Width_TR  = par_getd("problem", "wtr");

  /* Resistivity */
  sw_etajz = par_getd("problem", "sw_etajz");
  jz_eta0  = par_getd("problem", "jz_eta0");
  
  /* Read temperature floor for kappa calculations */
  temp_low = par_getd("problem", "Temperature_low");
  temp_high = par_getd("problem", "Temperature_high");

  /* Read parameters for cooling */
  Tc_low = par_getd("problem", "Tc_low");
  sw_lowt_cool = par_getd("problem", "sw_lowt_cool");
  sw_explicit_conduct_dt = par_getd("problem", "sw_explicit_conduct_dt");
  Real scale_ambient = par_getd("problem", "scale_ambient");
  
  /* NEI */
  sw_nei = par_getd("NEI", "sw_nei");

  /* Calculate bx, by */
  for (k = ks; k <= ke + 1; k++)
  {
    for (j = js - nghost; j <= je + nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
        bx[k][j][i] = func_bxini(x1c - 0.5 * dx_grid, x2c);
        by[k][j][i] = func_byini(x1c, x2c - 0.5 * dy_grid);
        bz[k][j][i] = func_bzini(x1c, x2c);
      }
    }
  }

  /* Recalculate Az */
  for (k = ks; k <= ke; k++)
  {
    az[k][js - nghost][is - nghost] = 0;
    for (j = js - nghost + 1; j <= je + nghost + 1; j++)
    {
      az[k][j][is - nghost] = az[k][j - 1][is - nghost] + bx[k][j - 1][is - nghost] * dy_grid;
    }
    for (j = js - nghost; j <= je + nghost + 1; j++)
    {
      for (i = is - nghost + 1; i <= ie + nghost + 1; i++)
      {
        az[k][j][i] = az[k][j][i - 1] - by[k][j][i - 1] * dx_grid;
      }
    }
  }

  /* Calculate bx, by from Az */
  for (k = ks; k <= ke + 1; k++)
  {
    for (j = js - nghost; j <= je + nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        bx[k][j][i] = (az[k][j + 1][i] - az[k][j][i]) / dy_grid;
        by[k][j][i] = -(az[k][j][i + 1] - az[k][j][i]) / dx_grid;
      }
    }
  }

  /* Compute initial pressure, temperature and density */
  Real rho0 = 1.0 * scale_ambient, t0 = Te_corona/Tchar, pgas0;
  pgas0 = rho0 * t0;
  if (myid == 0)
  {
    printf("rho0 = %10.3e, pgas0 = %10.3e, t0 = %10.3e\n", rho0, pgas0, t0);
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost; j <= je + nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
        pgasini[k][j][i] = func_presini_integral(x2c, pgas0, 1.0); // rho = 1
        if (sw_bz == 0) {
          pgasini[k][j][i] = pgasini[k][j][i] + func_pcs(x1c, x2c);
        }
        tempini[k][j][i] = func_teini(x1c, x2c);
        rhoini[k][j][i] = pgasini[k][j][i] / tempini[k][j][i];
      }
    }
  }

  /* All variables */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {

        /* density */
        pGrid->U[k][j][i].d = rhoini[k][j][i];

        /* monentum */
        pGrid->U[k][j][i].M1 = 0.0;
        pGrid->U[k][j][i].M2 = 0.0;
        pGrid->U[k][j][i].M3 = 0.0;

        /* magnetic field */
        pGrid->B1i[k][j][i] = bx[k][j][i];
        pGrid->B2i[k][j][i] = by[k][j][i];
        pGrid->B3i[k][j][i] = bz[k][j][i];

        /* initial setting for magnetic field on the boundary */
        if (i == ie && ie > is)
          pGrid->B1i[k][j][i + 1] = bx[k][j][i + 1];
        if (j == je && je > js)
          pGrid->B2i[k][j + 1][i] = by[k][j + 1][i];
        if (k == ke && ke > ks)
          pGrid->B3i[k + 1][j][i] = bz[k + 1][j][i];
      }
    }
  }

  /* cell-center magnetic field */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        pGrid->U[k][j][i].B1c = 0.5 * (pGrid->B1i[k][j][i] +
                                       pGrid->B1i[k][j][i + 1]);
        pGrid->U[k][j][i].B2c = 0.5 * (pGrid->B2i[k][j][i] +
                                       pGrid->B2i[k][j + 1][i]);
        pGrid->U[k][j][i].B3c = pGrid->B3i[k][j][i];
      }
    }
  }

  /* total energy*/
  Real p_uniform = 0.5;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        pGrid->U[k][j][i].E = pgasini[k][j][i] / (Gamma_1) + 0.5 * (SQR(pGrid->U[k][j][i].B1c) + SQR(pGrid->U[k][j][i].B2c) + SQR(pGrid->U[k][j][i].B3c)) + 0.5 * (SQR(pGrid->U[k][j][i].M1) + SQR(pGrid->U[k][j][i].M2) + SQR(pGrid->U[k][j][i].M3)) / pGrid->U[k][j][i].d;
      }
    }
  }

  /* Set resistivity */
#ifdef RESISTIVITY
  eta_Ohm = par_getd("problem", "eta_Ohm");
  Q_AD = par_getd("problem", "Q_AD");
  Q_Hall = 0.0;
  d_ind = 0.0;
#endif

  /* Set thermel conduction coefficient */
#ifdef THERMAL_CONDUCTION
  kappa_aniso = 1.0;
  kappa_iso = 0;
#endif

  /* Set viscosity */
#ifdef VISCOSITY
  // nu0 = 3.4e15 cm^2 s^-1,
  //nu_iso = 3.4e15*Timechar/(Lchar*1.0e2)/(Lchar*1.0e2);
  //printf("Non-dimensional nu_iso=%10.3e\n", nu_iso);
  //nu_aniso = 0.0;
#endif

  /* Enroll gravitational potential to give acceleration in y-direction for 2D*/
  StaticGravPot = gsun_static;

  /* Set optical thin radiation cooling and corona heating function */

  /* Set boundary value functions: Order for updating boundary conditions must
      always be x1-x2-x3 in order to
      fill the corner cells properly*/

  /* (a) left-open */
  bvals_mhd_fun(pDomain, left_x1, openbc_ix1);

  /* (b) Right-open */
  bvals_mhd_fun(pDomain, right_x1, symmbc_ox1);

  /* (c) Botom-open */
  bvals_mhd_fun(pDomain, left_x2, linetiedbc_ix2);

  /* (d) Top-open */
  bvals_mhd_fun(pDomain, right_x2, openbc_ox2);
  
  /* ion_nei */
  /* Check if conce structure has been allocated */
  if (sw_nei != 0)
  {
    printf("initialize ion_mesh\n");
    nei_init(pGrid);
  }
}

/*==============================================================================
 * PROBLEM USER FUNCTIONS:
 * problem_write_restart() - writes problem-specific user data to restart files
 * problem_read_restart()  - reads problem-specific user data from restart files
 * get_usr_expr()          - sets pointer to expression for special output data
 * get_usr_out_fun()       - returns a user defined output function pointer
 * get_usr_par_prop()      - returns a user defined particle selection function
 * Userwork_in_loop        - problem specific work IN     main loop
 * Userwork_after_loop     - problem specific work AFTER  main loop
 *----------------------------------------------------------------------------*/

void problem_write_restart(MeshS *pM, FILE *fp)
{
  return;
}

void problem_read_restart(MeshS *pM, FILE *fp)
{
  return;
}

static Real out_jz(const GridS *pG, const int i, const int j, const int k)
{
  Real jz;
  int is = pG->is, ie = pG->ie;
  int js = pG->js, je = pG->je;
  if ((i > is-nghost+1) && (i < ie + nghost -1)
   && (j > js-nghost+1) && (j < je + nghost -1)) {
    jz = ((pG->B2i[k][j][i] - pG->B2i[k][j][i - 1]) / pG->dx1 -
          (pG->B1i[k][j][i] - pG->B1i[k][j - 1][i]) / pG->dx2);
  } else {
    jz = 0;
  }
  return jz;
}
static Real out_b1i(const GridS *pG, const int i, const int j, const int k)
{
  return pG->B1i[k][j][i];
}
static Real out_b2i(const GridS *pG, const int i, const int j, const int k)
{
  return pG->B2i[k][j][i];
}
static Real out_b3i(const GridS *pG, const int i, const int j, const int k)
{
  return pG->B3i[k][j][i];
}
static Real out_ohmiceta(const GridS *pG, const int i, const int j, const int k)
{
  Real eta;
  eta = 0.0;
#ifdef RESISTIVITY
  eta = pG->eta_Ohm[k][j][i];
#endif
  return eta;
}
ConsFun_t get_usr_expr(const char *expr)
{
  if (strcmp(expr, "jz") == 0)
    return out_jz;
  if (strcmp(expr, "b1i") == 0)
    return out_b1i;
  if (strcmp(expr, "b2i") == 0)
    return out_b2i;
  if (strcmp(expr, "b3i") == 0)
    return out_b3i;
  if (strcmp(expr, "Ohmiceta") == 0)
    return out_ohmiceta;
  return NULL;
}

VOutFun_t get_usr_out_fun(const char *name)
{
  return NULL;
}

#ifdef RESISTIVITY

void get_eta_user(GridS *pG, int i, int j, int k,
                  Real *eta_O, Real *eta_H, Real *eta_A)
{
  int is = pG->is, ie = pG->ie;
  int js = pG->js, je = pG->je;
  int ks = pG->ks, ke = pG->ke;
  
  /* Case 1: Uniform eta */
  *eta_O = eta_Ohm;

  /* Case 2: Jz  dependent eta */
  if (sw_etajz == 1) {
  Real jz;
  if ((i >= is-nghost+2) && (i <= ie+nghost-2)
   && (j >= js-nghost+2) && (j <= je+nghost-2)) { 
    jz = (pG->B2i[k][j][i] - pG->B2i[k][j][i - 1])/pG->dx1
       - (pG->B1i[k][j][i] - pG->B1i[k][j - 1][i])/pG->dx2;
  } else {
    jz = 0;
  }
  Real jzsq = SQR(jz);
  Real j0sq = SQR(jz_eta0);
  Real anomalous_scale = MAX(0.0, (jzsq-j0sq)/j0sq);
  *eta_O = eta_Ohm*(anomalous_scale + 1.0);
  }

  return;
}
#endif

void Userwork_in_loop(MeshS *pM)
{
  GridS *pG = pM->Domain[0][0].Grid;
  int is = pG->is, ie = pG->ie;
  int js = pG->js, je = pG->je;
  int ks = pG->ks, ke = pG->ke;
  int i, j, k;
  int ic, jc;
  int jj;
  int ista, iend, jsta, jend, n1 = ie-is+1+2*nghost;
  Real x1, x2, x3;
  Real rc;
  Real pres_arr[n1], vx_arr[n1], vy_arr[n1];

  if (sw_bz == 1) {
    /* Reset vz == 0 */
    for (k = ks; k <= ke; k++) {
      for (j = js; j <= je; j++) {
        for (i = is; i <= ie; i++) {
          //pG->U[k][j][i].E = pG->U[k][j][i].E - 0.5 * SQR(pG->U[k][j][i].M3) / pG->U[k][j][i].d;
          pG->U[k][j][i].M3 = 0.0;
        }
      }
    }
  }

  /* NEI_two_step */
  if (sw_nei != 0)
  {
    nei_two_step(pG);
  }
  
  return;
}

void Userwork_after_loop(MeshS *pM)
{
  return;
}

/*=========================== PRIVATE FUNCTIONS ==============================*/
/*----------------------------------------------------------------------------*/
/*  \fn Initial Pressure inside the CS */
static Real func_pcs(const Real x1, const Real x2)
{
  Real pcs;
  Real r, rs, phi;
  Real br, br_ambient;
  rs = solarradius / Lchar;
  r = sqrt(x1 * x1 + (x2 + rs) * (x2 + rs));
  phi = atan(x1 / (x2 + rs));
  br = func_brini(r, phi);
  br_ambient = func_brini(r, 1.0);
  pcs = 0.5 * (SQR(br_ambient) - SQR(br));
  return pcs;
}

/*----------------------------------------------------------------------------*/
/* \function: te_rel */
static Real func_teini(const Real x, const Real y)
{
  /* Temperature inside the fluxrope */
  Real Te, t1, t2;
  Real tecor = Te_corona/Tchar, tepho = Te_photos/Tchar;
  /* Temperature distribution in y-direction: in non-dimensional forms */
  Real h = Height_TR / Lchar;
  Real w = Width_TR / Lchar;
  t1 = 0.5 * (tecor - tepho);
  t2 = 0.5 * (tecor + tepho);
  Te = t1 * tanh((y - h) / w) + t2;
  return Te;
}

/*----------------------------------------------------------------------------*/
/*  \funtion of Br for initial conditions */
static Real func_brini(const Real r, const Real phi)
{
  Real br, b0 = 1.0;
  Real w_phi, rs;
  w_phi = cs_width;
  rs = solarradius / Lchar;
  
  /* cs_mode 1: sin type */
  if (cs_mode == 1) {
    if (phi >= w_phi) {
      br = b0 * (rs / r);
    } else if (phi <= -w_phi) {
      br = -b0 * (rs / r); 
    } else {
      br = b0 * (rs / r) * sin(phi * PI * 0.5 / w_phi);
    }
  } else if (cs_mode == 2) {
    /* cs_mode 2: harris sheet */
    br = b0 * (rs / r)*tanh(phi * PI * 0.5 / w_phi);
  } else {
    /* ideal */
    br = -b0 * (rs / r);
  }
  return br;
}

/*----------------------------------------------------------------------------*/
/*  \funtion of Bx for initial conditions */
static Real func_bxini(const Real x1, const Real x2)
{
  Real bx0, br0;
  Real r, rs, phi;
  rs = solarradius / Lchar;
  r = sqrt(x1 * x1 + (x2 + rs) * (x2 + rs));
  phi = atan(x1 / (x2 + rs));
  
  br0 = func_brini(r, phi);
  bx0 = br0 * sin(phi);
  return bx0;
}

/*----------------------------------------------------------------------------*/
/*  \funtion of By for initial conditions */
static Real func_byini(const Real x1, const Real x2)
{
  Real by0, br0;
  Real r, rs, phi;
  rs = solarradius / Lchar;
  r = sqrt(x1 * x1 + (x2 + rs) * (x2 + rs));
  phi = atan(x1 / (x2 + rs));
  
  br0 = func_brini(r, phi);
  by0 = br0 * cos(phi);
  return by0;
}

/*---------------------------------------------------------------------------*/
/*  \funtion of Bz for initial conditions */
static Real func_bzini(const Real x1, const Real x2)
{
  Real bz0;
  if (sw_bz == 1) {
    bz0 = sqrt(2.0 * func_pcs(x1, x2));
  } else {
    bz0 = 0;
  }

  return bz0;
}

/*----------------------------------------------------------------------------*/
/*! \fn static Real gsun_static(const Real x1, const Real x2, const Real x3)
 *  \brief Gravitational potential; g = 0.1
 */
static Real gsun_static(const Real x1, const Real x2, const Real x3)
{
  Real gp, gpnondim;
  Real safety = 1.0;
  /* gravity potential (m^2/s^2) */
  gp = -(gravitational_const * solarmass) / (x2 * Lchar + solarradius);
  /* Non-dimensional gp */
  gpnondim = gp * Rhochar / Pchar;
  //gpnondim = gp / pow(Vchar, 2);
  return gpnondim;
}

/*----------------------------------------------------------------------------*/
/*! \fn static Real gsun_r(const Real x2)
 *  \brief Non-dimensional Gravity at height x2
 */
static Real gsun_y(const Real x2)
{
  Real g, gnondim;
  Real r;
  r = solarradius + x2 * Lchar;                    /* (m) */
  g = (gravitational_const * solarmass) / (r * r); /* (m s^-2)*/
  gnondim = g * Rhochar * Lchar / Pchar;
  //gnondim = g * Lchar / pow(Vchar, 2);
  return gnondim;
}

/*----------------------------------------------------------------------------*/
/*  \fn gas pressure performing numerical integration */
static Real func_presini_integral(Real x2, 
  const Real pgas_sta, const Real posi_sta)
{
  Real pgas;
  Real Iout_simp;

  Iout_simp = adaptiveSimpsons(func_integ_py, posi_sta, x2, 1.0e-9, 1000000);
  pgas = pgas_sta * exp(Iout_simp);

  return pgas;
}

Real func_integ_py(Real y)
{
  return -gsun_y(y) / func_teini(0.0, y);
}

/*----------------------------------------------------------------------------*/
/*  \fn gas pressure assuming constant temperature */
static Real func_presini_isote(const Real x2, const Real y0,
                               const Real p0, const Real te0)
{
  Real pgas, gm, r, r0;
  
  gm = gravitational_const * solarmass * Rhochar / (Lchar * Pchar);
  r = solarradius / Lchar + x2;
  r0 = solarradius / Lchar + y0;
  pgas = p0 * exp(gm / te0 * (1.0 / r - 1.0 / r0));

  return pgas;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void openbc_ix1(GridS *pGrid)
 *  \brief open boundary condition, Inner x1 boundary (bc_ix1=2) */
static void openbc_ix1(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  for (k = ks; k <= ke; k++) {
    for (j = js; j <= je; j++) {
      for (i = 1; i <= nghost; i++) {
        pGrid->U[k][j][is - i] = pGrid->U[k][j][is + i - 1];
      }
    }
  }

#ifdef MHD
  /* B2i: */
  if (pGrid->Nx[1] > 1)
    ju = je + 1;
  else
    ju = je;
  for (k = ks; k <= ke; k++) {
    for (j = js; j <= ju; j++) {
      for (i = 1; i <= nghost; i++) {
        pGrid->B2i[k][j][is - i] = 2.0 * pGrid->B2i[k][j][is - i + 1]
          - pGrid->B2i[k][j][is - i + 2];
      }
    }
  }

  /* B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++) {
    for (j = js; j <= je; j++) {
      for (i = 1; i <= nghost - 1; i++) {
        pGrid->B1i[k][j][is - i] = pGrid->B1i[k][j][is - i + 1]
          + (pGrid->dx1 / pGrid->dx2)
          * (pGrid->B2i[k][j + 1][is - i] - pGrid->B2i[k][j][is - i]);
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++) {
    for (j = js; j <= je; j++) {
      for (i = 1; i <= nghost; i++) {
        pGrid->B3i[k][j][is - i] = pGrid->B3i[k][j][is + i - 1];
      }
    }
  }
#endif /* MHD */

  /* B1c, 2c, 3c */
  for (k = ks; k <= ke; k++) {
    for (j = js; j <= je; j++) {
      for (i = 1; i <= nghost - 1; i++) {
        pGrid->U[k][j][is - i].B1c = 0.5 * (pGrid->B1i[k][j][is - i] + pGrid->B1i[k][j][is - i + 1]);
      }
    }
  }
  for (k = ks; k <= ke; k++) {
    for (j = js; j <= je; j++) {
      for (i = 1; i <= nghost; i++) {
        pGrid->U[k][j][is - i].B2c = 0.5 * (pGrid->B2i[k][j][is - i] + pGrid->B2i[k][j + 1][is - i]);
      }
    }
  }
  for (k = ks; k <= ke; k++) {
    for (j = js; j <= je; j++) {
      for (i = 1; i <= nghost; i++) {
        pGrid->U[k][j][is - i].B3c = pGrid->B3i[k][j][is + i -1];
      }
    }
  }

  /* Pressure and energy  */
  int isp, ic;
  Real eb_isp, ek_isp, pis_o_gm;
  for (k = ks; k <= ke; k++) {
    for (j = js; j <= je; j++) {
      for (i = 1; i <= nghost; i++) {
        isp = is + i - 1;
        eb_isp = 0.5 * (SQR(pGrid->U[k][j][isp].B1c)
                      + SQR(pGrid->U[k][j][isp].B2c)
                      + SQR(pGrid->U[k][j][isp].B3c));
        ek_isp = 0.5 * (SQR(pGrid->U[k][j][isp].M1) 
                      + SQR(pGrid->U[k][j][isp].M2)
                      + SQR(pGrid->U[k][j][isp].M3)) / pGrid->U[k][j][isp].d;
        pis_o_gm = pGrid->U[k][j][isp].E - eb_isp - ek_isp;

        ic = is - i;
        pGrid->U[k][j][ic].E = pis_o_gm + 0.5 * (SQR(pGrid->U[k][j][ic].B1c) + SQR(pGrid->U[k][j][ic].B2c) + SQR(pGrid->U[k][j][ic].B3c)) + 0.5 * (SQR(pGrid->U[k][j][ic].M1) + SQR(pGrid->U[k][j][ic].M2) + SQR(pGrid->U[k][j][ic].M3)) / pGrid->U[k][j][ic].d;
      }
    }
  }

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void symmbc_ox1(GridS *pGrid)
 *  \brief OUTFLOW boundary conditions, Outer x1 boundary (bc_ox1=2) */
static void symmbc_ox1(GridS *pGrid)
{
  int ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][ie + i] = pGrid->U[k][j][ie - i + 1];
        pGrid->U[k][j][ie + i].B2c = -pGrid->U[k][j][ie - i + 1].B2c;
        pGrid->U[k][j][ie + i].M1 = -pGrid->U[k][j][ie - i + 1].M1;
      }
    }
  }

#ifdef MHD
  /* i=ie+1 is not a boundary condition for the interface field B1i */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 2; i <= nghost; i++)
      {
        pGrid->B1i[k][j][ie + i] = pGrid->B1i[k][j][ie - i + 2];
      }
    }
  }

  if (pGrid->Nx[1] > 1)
    ju = je + 1;
  else
    ju = je;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= ju; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B2i[k][j][ie + i] = -pGrid->B2i[k][j][ie - i + 1];
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B3i[k][j][ie + i] = pGrid->B3i[k][j][ie - i + 1];
      }
    }
  }
#endif /* MHD */

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void linetiedbc_ix2(GridS *pGrid)
 *  \brief Sets boundary condition at the bottom.
 */
/*  ix2, line-tied, bottom */
void linetiedbc_ix2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
  Real x1c, x2c, x3c;
  Real x1f, x2f;
  Real x1_js, x2_js, x3_js;

#ifdef MHD
  int ku; /* k-upper */
#endif

  /* Set all variables in ghost zone */
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is - nghost; i <= ie + nghost; i++) {
        pGrid->U[k][js - j][i] = pGrid->U[k][js][i];
        pGrid->U[k][js - j][i].M1 = 0.0;
        pGrid->U[k][js - j][i].M2 = 0.0;
        pGrid->U[k][js - j][i].M3 = 0.0;
      }
    }
  }

#ifdef MHD
  /* B2i is not set at j=js-nghost */
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is - nghost; i <= ie + nghost; i++) {
        pGrid->B2i[k][js-j][i] = by[k][js - j][i];
      }
    }
  }
  /* B1i is not set at i=is-nghost  */
  Real pbypx;
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is - (nghost - 1); i <= ie + nghost; i++) {
        cc_pos(pGrid, i, js-j+1, k, &x1c, &x2c, &x3c);
        x1f = x1c - 0.5 * pGrid->dx1;
        x2f = x2c - 0.5 * pGrid->dx2;
        pbypx = func_pbypxini(x1f, x2f);
        pGrid->B1i[k][js-j][i] = pGrid->B1i[k][js-j+1][i]-(pbypx)*(pGrid->dx2);
      }
    }
  }
  /* B3i */
  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is - nghost; i <= ie + nghost; i++) {
        pGrid->B3i[k][js - j][i] = pGrid->B3i[k][js][i];
      }
    }
  }

  /* B1c, 2c, 3c */
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is - nghost; i <= ie + nghost-1; i++) {
        pGrid->U[k][js-j][i].B1c = 0.5*(pGrid->B1i[k][js-j][i]
                                      + pGrid->B1i[k][js-j][i+1]);
      }
    }
  }
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is - nghost; i <= ie + nghost; i++) {
        pGrid->U[k][js-j][i].B2c = 0.5*(pGrid->B2i[k][js-j][i]
                                      + pGrid->B2i[k][js-j+1][i]);
      }
    }
  }
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is - nghost; i <= ie + nghost; i++) {
        pGrid->U[k][js-j][i].B3c = pGrid->B3i[k][js-j][i];
      }
    }
  }
#endif /* MHD */

  /* Pressure and energy */
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is - nghost; i <= ie + nghost; i++) {
        // fixed boundary with gravity
        int j_mj = js - j;
        //Real p_mj = pgasini[k][j_mj][i];
        cc_pos(pGrid, is, js, ks, &x1c, &x2c, &x3c);
        Real y_js = x2c;
        Real eb_js = 0.5 * (SQR(pGrid->U[k][js][i].B1c)
                          + SQR(pGrid->U[k][js][i].B2c)
                          + SQR(pGrid->U[k][js][i].B3c));
        Real ek_js = 0.5 * (SQR(pGrid->U[k][js][i].M1)
                          + SQR(pGrid->U[k][js][i].M2)
                          + SQR(pGrid->U[k][js][i].M3)) / pGrid->U[k][js][i].d;
        Real p_js = (pGrid->U[k][js][i].E - eb_js - ek_js) * Gamma_1;

        // P, extrapolation
        cc_pos(pGrid, is, j_mj, k, &x1c, &x2c, &x3c);
        Real y_mj = x2c;
        Real te_js = p_js / pGrid->U[k][js][i].d;
        Real te_mj = te_js;
        Real p_mj = func_presini_isote(y_mj, y_js, p_js, te_mj);
        pGrid->U[k][j_mj][i].d = p_mj / te_mj;
        Real eb_mj = 0.5*(SQR(pGrid->U[k][j_mj][i].B1c)
                        + SQR(pGrid->U[k][j_mj][i].B2c)
                        + SQR(pGrid->U[k][j_mj][i].B3c));
        Real ek_mj = 0.5*(SQR(pGrid->U[k][j_mj][i].M1)
                        + SQR(pGrid->U[k][j_mj][i].M2)
                        + SQR(pGrid->U[k][j_mj][i].M3))/pGrid->U[k][j_mj][i].d;      
        pGrid->U[k][j_mj][i].E = p_mj / Gamma_1 + eb_mj + ek_mj;
      }
    }
  }
  return;
}

static Real func_pbypxini(Real x, Real y)
{
  Real pbypx;
  pbypx = (func_byini(x + 1.0e-9, y) - func_byini(x - 1.0e-9, y)) / 2.0e-9;
  return pbypx;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void outflow_ox2(GridS *pGrid)
 *  \brief OpenBC_ox2 boundary conditions, Outer x2 boundary (bc_ox2=2) */
static void openbc_ox2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ku; /* k-upper */
#endif
  int jc;

  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is - nghost; i <= ie + nghost; i++) {
        pGrid->U[k][je+j][i] = pGrid->U[k][je-j+1][i];
        pGrid->U[k][je+j][i].B1c = -pGrid->U[k][je-j+1][i].B1c;
      }
    }
  }

#ifdef MHD
  /* B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is - (nghost - 1); i <= ie + nghost; i++) {
        pGrid->B1i[k][je+j][i] = -pGrid->B1i[k][je-j+1][i];
      }
    }
  }

  /* j=je+1 is not a boundary condition for the interface field B2i */
  for (k = ks; k <= ke; k++) {
    for (j = 2; j <= nghost; j++) {
      for (i = is - nghost; i <= ie + nghost; i++) {
        pGrid->B2i[k][je+j][i] = pGrid->B2i[k][je-j+2][i];
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is - nghost; i <= ie + nghost; i++) {
        pGrid->B3i[k][je+j][i] = pGrid->B3i[k][je-j+1][i];
      }
    }
  }
  
#endif /* MHD */

  /* Pressure and energy */
  Real eb_je, ek_je, pje;
  Real x1c, yje, yjc, x3c;
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is - nghost; i <= ie + nghost; i++) {
        eb_je = 0.5 * (SQR(pGrid->U[k][je][i].B1c) + SQR(pGrid->U[k][je][i].B2c) + SQR(pGrid->U[k][je][i].B3c));
        ek_je = 0.5 * (SQR(pGrid->U[k][je][i].M1) + SQR(pGrid->U[k][je][i].M2) + SQR(pGrid->U[k][je][i].M3)) / pGrid->U[k][je][i].d;
        pje = Gamma_1 * (pGrid->U[k][je][i].E - eb_je - ek_je);

        jc = je + j;
        Real Tje = pje / pGrid->U[k][je][i].d;
        cc_pos(pGrid, i, je, k, &x1c, &yje, &x3c);
        cc_pos(pGrid, i, jc, k, &x1c, &yjc, &x3c);
        Real pjc = func_presini_isote(yjc, yje, pje, Tje);
        pGrid->U[k][jc][i].d = pjc/Tje;
        pGrid->U[k][jc][i].E = pjc / (Gamma_1)
          + 0.5 *(SQR(pGrid->U[k][jc][i].B1c)
                + SQR(pGrid->U[k][jc][i].B2c)
                + SQR(pGrid->U[k][jc][i].B3c))
          + 0.5 *(SQR(pGrid->U[k][jc][i].M1)
                + SQR(pGrid->U[k][jc][i].M2)
                + SQR(pGrid->U[k][jc][i].M3)) / pGrid->U[k][jc][i].d;
      }
    }
  }
  return;
}
