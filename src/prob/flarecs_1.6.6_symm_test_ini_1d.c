#include "copyright.h"
/*============================================================================*/
/*! \file fluxrope_symm.c
 *  \brief  Problem generator for falrecs_1.6.6_symm.
 *
 * PURPOSE: Flare CurrentSheet.
 *
 * REFERENCE: Wang, Shen, & Lin, ApJ 2009.
 */
/*============================================================================*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defs.h"
#include "athena.h"
#include "globals.h"
#include "prototypes.h"
#include "./prob/simpson_integ.c"

/*==============================================================================
 * PRIVATE FUNCTION PROTOTYPES:
 * void flarecs_linetied() - sets BCs on R-x2 boundary
 *============================================================================*/
static Real func_bxini(const Real x1, const Real x2);
static Real func_byini(const Real x1, const Real x2);
static Real func_bzini(const Real x1, const Real x2);
static Real func_brini(const Real r, const Real phi);

static Real func_teini(const Real x2);
static Real func_presini_isote(const Real x2, const Real y0,
                               const Real p0, const Real te0);
static Real func_pcs(const Real x1, const Real x2);
Real func_presini_integral(Real x2);
Real func_my_integ_py(Real y);

static Real func_gsun_static(const Real x1, const Real x2, const Real x3);
static Real func_gsun_y(const Real x2);

/* Boundary conditions */
static void linetiedbc_ix2(GridS *pGrid);
static void openbc_ox2(GridS *pGrid);

/* Radiative loss function */
static Real func_Qt(const Real T);
Real func_opticalthinloss_corona(const Real dens, const Real Press, const Real dt);

/*============================================================================*/
/* Normalization parameters */
Real Lchar, Bchar, Nechar, Rhochar, Pchar, Timechar, Tchar, Vchar;
Real Gsun_nondim;

/* a. input parameters: pressure and temperature in chromosphere and corona */
Real Te_corona = 1.5e6, Te_photos = 1.5e6;

/* b. input parameters: height and width for the TR region */
Real Height_TR = 3.5e6, Width_TR = 1.0e6;

/* c. The height for define the pgas_c in corona */
Real dens_c_dimension = 5.0e15; /* (m^-3) ~ 5.0e9 (cm^-3)*/
Real dens_c;
Real pgas_c, posi_c;

/* Constant parameters */
Real Kb = 1.38e-23, Mp = 1.6726e-27, Mu0 = 1.25663706144e-6, LnA = 30.0;
Real gravitational_const = 6.672e-11; /* (N M^2 kg^-2)*/
Real solarmass = 1.99e+30;            /* (kg) */
Real solarradius = 6.96e+8;           /* (m) */

/* Initial conditions */
Real ***pgasini, ***pbackini, ***rhoini, ***tempini;
Real ***bx, ***by, ***bz;
Real ***de_conduction, ***E_ini, ***te_prestep, ***rho_prestep;

/* Thermal conduction parameters: */
/* Define the temperature range in computing kappa(T) */
Real temp_low, temp_high;

/* Artificialy reset cooling and conduction in lower temperature region */
Real Tc_low;
int sw_lowt_cool; // use for decreasing the cooling and increacing thermal
                  // conduction once tempreture is lower than Tc_low to keep
                  // Kappa(T)*Q(T) constant. (Reeves et al.2010 ApJ)
int sw_explicit_conduct_dt;

/*=========================== PUBLIC FUNCTIONS ===============================*/
/*----------------------------------------------------------------------------*/
/* problem:  */

void problem(DomainS *pDomain)
{
  GridS *pGrid = (pDomain->Grid);
  int i, is = pGrid->is, ie = pGrid->ie;
  int j, js = pGrid->js, je = pGrid->je;
  int k, ks = pGrid->ks, ke = pGrid->ke;
  int n1 = ie - is + 1 + 2 * nghost;
  int n2 = je - js + 1 + 2 * nghost;
  int n3 = ke - ks + 1 + 2 * nghost;

  Real x1, x2, x3;
  Real x1c, x2c, x3c;
  Real x1f, x2f, x3f;
  Real x1s, x2s, x3s;
  Real r;

  /* Inilize pgasini, rhoini, and tempini */
  if ((pgasini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for pgasini \n");
  }
  if ((pbackini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for pbackini \n");
  }
  if ((rhoini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for rhoini \n");
  }
  if ((tempini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for tempini \n");
  }

  if ((bx = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector bx\n");
  }
  if ((by = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector by\n");
  }
  if ((bz = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector bz\n");
  }
  if ((de_conduction = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector de_c\n");
  }
  if ((E_ini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector E_ini\n");
  }
  if ((te_prestep = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector te_prestep\n");
  }
  if ((rho_prestep = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector rho_prestep\n");
  }

  /* Termal conduction & viscosity */
  Real k_spitzer, kappa_v;
  Real nud, nu0;

  /* Magnitude of perturbations */
  Real pi;
  Real vin_turb;
  Real Lx, Ly, delta_bx, delta_by;
  Real pos_x_turb, pos_y_turb, hwd_x_turb, hwd_y_turb;

  /* Read input parameters */
  Lchar = par_getd("problem", "Lchar");
  Bchar = par_getd("problem", "Bchar");
  Nechar = par_getd("problem", "Nechar");

  Pchar = Bchar * Bchar / Mu0;
  Rhochar = Nechar * Mp;
  Tchar = Pchar / (Nechar * 2. * Kb); /*total number is 2*ne for full ionization plasma */
  Vchar = Bchar / sqrt(Mu0 * Rhochar);
  Timechar = Lchar / Vchar;

  /* Print*/
  int myid;
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  if (myid == 0)
  {
    printf("Bchar=%10.3e (T)\n", Bchar);
    printf("nechar=%10.3e (cm^-3), Rhochar=%10.3e (kg/m^3)\n",
           Nechar / 1.0e6, Rhochar);
    printf("Pchar=%10.3e (Pa)\n", Pchar);
    printf("Tchar=%10.3e (K)\n", Tchar);
    printf("Vchar=%10.3e (km/s)\n", Vchar / 1.0e3);
    printf("Lchar=%10.3e (km)\n", Lchar / 1.0e3);
    printf("Timechar=%10.3e (s)\n", Timechar);
  }

  /* Set the density (and pressure) base */
  dens_c = dens_c_dimension / Nechar;
  pgas_c = dens_c * (Te_corona / Tchar);
  posi_c = (Height_TR + Width_TR) / Lchar;

  /* Read temperature floor for kappa calculations */
  temp_low = par_getd("problem", "Temperature_low");
  temp_high = par_getd("problem", "Temperature_high");

  /* Read parameters for cooling */
  Tc_low = par_getd("problem", "Tc_low");
  sw_lowt_cool = par_getd("problem", "sw_lowt_cool");
  // if it equals to 1, then new_diff_dt will calculate updated time-step
  // according to the explicit diffusion schema.
  sw_explicit_conduct_dt = par_getd("problem", "sw_explicit_conduct_dt");

  /* Compute initial magnetic field: b and az */
  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost; j <= je + nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
        x1f = x1c - 0.5 * pGrid->dx1;
        x2f = x2c - 0.5 * pGrid->dx2;
        bx[k][j][i] = func_bxini(x1f, x2c);
        by[k][j][i] = 1.0;
        bz[k][j][i] = func_bzini(x1c, x2c);
      }
    }
  }

  /* coronal gas pressure: pback*/
  Real pgas0;
  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost; j <= je + nghost; j++)
    {
      cc_pos(pGrid, is, j, k, &x1, &x2, &x3);
      pgas0 = func_presini_integral(x2);
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pbackini[k][j][i] = pgas0;
      }
    }
  }

  /* Pressure =  pback + pcs */
  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost; j <= je + nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
        pgasini[k][j][i] = pbackini[k][j][i];
        tempini[k][j][i] = func_teini(x2c);
        rhoini[k][j][i] = pgasini[k][j][i] / tempini[k][j][i];
      }
    }
  }

  /* All variables */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        /* density */
        pGrid->U[k][j][i].d = rhoini[k][j][i];

        /* monentum */
        pGrid->U[k][j][i].M1 = 0.0;
        pGrid->U[k][j][i].M2 = 0.0;
        pGrid->U[k][j][i].M3 = 0.0;

        /* magnetic field */
        pGrid->B1i[k][j][i] = bx[k][j][i];
        pGrid->B2i[k][j][i] = by[k][j][i];
        pGrid->B3i[k][j][i] = bz[k][j][i];

        /* initial setting for magnetic field on the boundary */
        if (i == ie && ie > is)
          pGrid->B1i[k][j][i + 1] = bx[k][j][i + 1];
        if (j == je && je > js)
          pGrid->B2i[k][j + 1][i] = by[k][j + 1][i];
        if (k == ke && ke > ks)
          pGrid->B3i[k + 1][j][i] = bz[k + 1][j][i];
      }
    }
  }

  /* cell-center magnetic field */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        pGrid->U[k][j][i].B1c = 0.5 * (pGrid->B1i[k][j][i] +
                                       pGrid->B1i[k][j][i + 1]);
        pGrid->U[k][j][i].B2c = 0.5 * (pGrid->B2i[k][j][i] +
                                       pGrid->B2i[k][j + 1][i]);
        pGrid->U[k][j][i].B3c = pGrid->B3i[k][j][i];
      }
    }
  }

  /* total energy*/
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        pGrid->U[k][j][i].E = pgasini[k][j][i] / (Gamma_1) + 0.5 * (SQR(pGrid->U[k][j][i].B1c) + SQR(pGrid->U[k][j][i].B2c) + SQR(pGrid->U[k][j][i].B3c)) + 0.5 * (SQR(pGrid->U[k][j][i].M1) + SQR(pGrid->U[k][j][i].M2) + SQR(pGrid->U[k][j][i].M3)) / pGrid->U[k][j][i].d;
      }
    }
  }

  /* Save variables for computing heating rate */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        E_ini[k][j][i] = pGrid->U[k][j][i].E;
        te_prestep[k][j][i] = tempini[k][j][i];
        rho_prestep[k][j][i] = rhoini[k][j][i];
      }
    }
  }

  /* Set resistivity */
#ifdef RESISTIVITY
  eta_Ohm = par_getd("problem", "eta_Ohm");
  Q_AD = par_getd("problem", "Q_AD");
  Q_Hall = 0.0;
  d_ind = 0.0;
#endif

  /* Set thermel conduction coefficient */
#ifdef THERMAL_CONDUCTION
  kappa_aniso = 1.0;
  kappa_iso = 0;
#endif
  /* Set viscosity */

  /* Enroll gravitational potential to give acceleration in y-direction for 2D */
  StaticGravPot = func_gsun_static;

  /* Set optical thin radiation cooling and corona heating function */
  CoolingFunc = func_opticalthinloss_corona;

  /* Set boundary value functions */
  /* (a) (b) left and right are outflow BCs for 1d tests */

  /* (c) Top-open */
  bvals_mhd_fun(pDomain, right_x2, openbc_ox2);

  /* (d) Bottom-line-tied */
  bvals_mhd_fun(pDomain, left_x2, linetiedbc_ix2);
}

/*==============================================================================
 * PROBLEM USER FUNCTIONS:
 * problem_write_restart() - writes problem-specific user data to restart files
 * problem_read_restart()  - reads problem-specific user data from restart files
 * get_usr_expr()          - sets pointer to expression for special output data
 * get_usr_out_fun()       - returns a user defined output function pointer
 * get_usr_par_prop()      - returns a user defined particle selection function
 * Userwork_in_loop        - problem specific work IN     main loop
 * Userwork_after_loop     - problem specific work AFTER  main loop
 *----------------------------------------------------------------------------*/

void problem_write_restart(MeshS *pM, FILE *fp)
{
  return;
}

void problem_read_restart(MeshS *pM, FILE *fp)
{
  return;
}

ConsFun_t get_usr_expr(const char *expr)
{
  return NULL;
}

VOutFun_t get_usr_out_fun(const char *name)
{
  return NULL;
}

#ifdef RESISTIVITY

void get_eta_user(GridS *pG, int i, int j, int k,
                  Real *eta_O, Real *eta_H, Real *eta_A)
{
  *eta_O = eta_Ohm;
  *eta_H = Q_Hall;
  *eta_A = Q_AD;
  //if (pG->time <= 2.0)
  //{
  //  *eta_O = eta_Ohm + 1.0e-3*(2.0-pG->time)/2.0;
  //}
  return;
}
#endif

void Userwork_in_loop(MeshS *pM)
{
  GridS *pG = pM->Domain[0][0].Grid;
  int is = pG->is, ie = pG->ie;
  int js = pG->js, je = pG->je;
  int ks = pG->ks, ke = pG->ke;
  int i, j, k;
  Real x1, x2, x3;

  /*  Pressure floor 
  Real pres_floor = 1.0e-4;
  Real pres_c, dpres;
  Real msqr, bsqr;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        msqr = SQR(pG->U[k][j][i].M1) + SQR(pG->U[k][j][i].M2) + SQR(pG->U[k][j][i].M3);
        bsqr = SQR(pG->U[k][j][i].B1c) + SQR(pG->U[k][j][i].B2c) + SQR(pG->U[k][j][i].B3c);
        pres_c = Gamma_1 * (pG->U[k][j][i].E - 0.5 * msqr / pG->U[k][j][i].d - 0.5 * bsqr);
        pres_c = MAX(pres_floor, pres_c);
        pG->U[k][j][i].E = pres_c / Gamma_1 + 0.5 * msqr / pG->U[k][j][i].d - 0.5 * bsqr;
      }
    }
  } */

  /* coronal heating term: rho*H. H = rho_ini*Q(T_ini) */
  Real de_cooling_rate, de_coronal_heating_rate, de_heating_rate;
  Real rho_avg, H_rate;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        rho_avg = 0.5 * (rho_prestep[k][j][i] + pG->U[k][j][i].d);
        H_rate = rhoini[k][j][i] * Nechar * func_Qt(Te_corona);
        pG->U[k][j][i].E = pG->U[k][j][i].E + rho_avg * Nechar * H_rate * (Timechar / Pchar) * pG->dt;
      }
    }
  }

  /* At the first one-step: estimate loss rate due to thermal conduction */
  Real eb_jc, ek_jc, pjc, Tjc;
  Real de_onestep;
  if (pG->time <= pG->dt)
  {
    for (k = ks; k <= ke; k++)
    {
      for (j = js; j <= je; j++)
      {
        for (i = is; i <= ie; i++)
        {
          eb_jc = 0.5 * (SQR(pG->U[k][j][i].B1c) + SQR(pG->U[k][j][i].B2c) + SQR(pG->U[k][j][i].B3c));
          ek_jc = 0.5 * (SQR(pG->U[k][j][i].M1) + SQR(pG->U[k][j][i].M2) + SQR(pG->U[k][j][i].M3)) / pG->U[k][j][i].d;
          pjc = Gamma_1 * (pG->U[k][j][i].E - eb_jc - ek_jc);

          de_onestep = (pjc - rho_prestep[k][j][i] * te_prestep[k][j][i]) / Gamma_1;

          de_conduction[k][j][i] = de_onestep / pG->dt;
        }
      }
    }
  }

  /* Update var_prestep */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        eb_jc = 0.5 * (SQR(pG->U[k][j][i].B1c) + SQR(pG->U[k][j][i].B2c) + SQR(pG->U[k][j][i].B3c));
        ek_jc = 0.5 * (SQR(pG->U[k][j][i].M1) + SQR(pG->U[k][j][i].M2) + SQR(pG->U[k][j][i].M3)) / pG->U[k][j][i].d;
        pjc = Gamma_1 * (pG->U[k][j][i].E - eb_jc - ek_jc);
        Tjc = pjc / pG->U[k][j][i].d;
        te_prestep[k][j][i] = Tjc;
        rho_prestep[k][j][i] = pG->U[k][j][i].d;
      }
    }
  }
  return;
}

void Userwork_after_loop(MeshS *pM)
{
  return;
}

/*=========================== PRIVATE FUNCTIONS ==============================*/
/*----------------------------------------------------------------------------*/
/*  \funtion of Br for initial conditions */
static Real func_brini(const Real r, const Real phi)
{
  Real br, b0 = 1.0;
  Real w_phi, rs;
  w_phi = 0.01;
  rs = solarradius / Lchar;

  /* cs_mode 1: sin type */
  if (phi >= w_phi)
  {
    br = b0 * (rs / r);
  }
  else if (phi <= -w_phi)
  {
    br = -b0 * (rs / r);
  }
  else
  {
    br = b0 * (rs / r) * sin(phi * 3.1415925359 * 0.5 / w_phi);
  }

  return br;
}

/*----------------------------------------------------------------------------*/
/*  \funtion of Bx for initial conditions */
static Real func_bxini(const Real x1, const Real x2)
{
  Real bx0;
  bx0 = 0;
  return bx0;
}

/*----------------------------------------------------------------------------*/
/*  \funtion of By for initial conditions */
static Real func_byini(const Real x1, const Real x2)
{
  Real by0, br0;
  by0 = 1.0;
  return by0;
}

/*---------------------------------------------------------------------------*/
/*  \funtion of Bz for initial conditions */
static Real func_bzini(const Real x1, const Real x2)
{
  Real bz0;
  bz0 = 0;
  return bz0;
}

/*----------------------------------------------------------------------------*/
/*  \fn Initial Pressure inside the CS */
static Real func_pcs(const Real x1, const Real x2)
{
  Real pcs;
  Real r, rs, phi;
  Real br, br_ambient;
  rs = solarradius / Lchar;
  r = sqrt(x1 * x1 + (x2 + rs) * (x2 + rs));
  phi = atan(x1 / (x2 + rs));
  br = func_brini(r, phi);
  br_ambient = func_brini(r, 1.0);
  pcs = 0.5 * (SQR(br_ambient) - SQR(br));
  return pcs;
}

/*----------------------------------------------------------------------------*/
/*  \fn Initial Temperature */
static Real func_teini(const Real x2)
{
  /* Non-dimensional variables */
  Real techr, tecor, h, w;
  Real t1, t2, te;

  /* The following calculation is in non-dimensional forms */
  techr = Te_photos / Tchar;
  tecor = Te_corona / Tchar;
  h = Height_TR / Lchar;
  w = Width_TR / Lchar;

  /* Temperature */
  t1 = 0.5 * (tecor - techr);
  t2 = 0.5 * (tecor + techr);
  te = t1 * tanh((x2 - h) / w) + t2;

  return te;
}

/*----------------------------------------------------------------------------*/
/*! \fn static Real func_gsun_static(const Real x1, const Real x2, const Real x3)
 *  \brief Gravitational potential; g = 0.1
 */
static Real func_gsun_static(const Real x1, const Real x2, const Real x3)
{
  Real gp;
  Real safety = 1.0;
  /* gravity potential */
  gp = -(gravitational_const * solarmass) / (x2 * Lchar + solarradius);
  /* Nondimensional gp */
  gp = gp * Rhochar / Pchar;
  return gp;
}

/*----------------------------------------------------------------------------*/
/*! \fn static Real gsun_r(const Real x2)
 *  \brief Nondimensional Gravitaty at height x2
 */
static Real func_gsun_y(const Real x2)
{
  Real g, gnondim;
  Real r;
  r = solarradius + x2 * Lchar;                    /* (m) */
  g = (gravitational_const * solarmass) / (r * r); /* (m s^-2)*/
  gnondim = g * Rhochar * Lchar / Pchar;
  return gnondim;
}

/*----------------------------------------------------------------------------*/
/*  \fn gas pressure assuming constant temperature */
static Real func_presini_isote(const Real x2, const Real y0,
                               const Real p0, const Real te0)
{
  Real pgas, gm, r, r0;
  gm = gravitational_const * solarmass * Rhochar / (Lchar * Pchar);
  r = solarradius / Lchar + x2;
  r0 = solarradius / Lchar + y0;
  pgas = p0 * exp(gm / te0 * (1.0 / r - 1.0 / r0));
  return pgas;
}

/*----------------------------------------------------------------------------*/
/*  \fn gas pressure performing numerical integration */
Real func_presini_integral(Real x2)
{
  Real pgas;
  Real Iout_simp;

  /* Simpson's rule */
  Iout_simp = -adaptiveSimpsons(func_my_integ_py, posi_c, x2, 1.0e-9, 100000);
  pgas = pgas_c * exp(Iout_simp);

  return pgas;
}

/*-----------------------------------------------------------------------------
 * \fn Real func_my_integ_py                                  
 * \brief Integration function to get integ(gsun(y)/T(y))dy
 * ---------------------------------------------------------------------------*/
Real func_my_integ_py(Real y)
{
  return func_gsun_y(y) / func_teini(y);
}

/*----------------------------------------------------------------------------*/
/*! \fn Real func_opticalthinloss_corona(const Real dens, const Real Press, 
  const Real dt) 
  Returns rate in non-dimensional value. The other local variables appear
  in SI unit.
  */
#ifndef BAROTROPIC
Real func_opticalthinloss_corona(const Real dens, const Real Press, const Real dt)
{
  Real coolrate;
  Real ne, nH;
  Real T, Teqi, Tmin;

  /* Checking */
  Real dE, dTemp, Tnext;

  /* Compute dimensional T, ne, nH */
  T = Tchar * (Press / dens);
  ne = dens * Nechar;
  nH = ne;

  /* Set the minimum Temperature to prevent negative Te */
  Tmin = 5500.0;

  /* only cooling rate */
  coolrate = ne * nH * func_Qt(T);

  /* Plus coronal heating term: rho*H */
  //Real H;
  //H = nH * func_Qt(Te_corona);
  //coolrate = coolrate - ne * H;

  /* To non-dimensional value */
  coolrate = coolrate * Timechar / Pchar;

  /* Check after time=dt, T is still larger than Tmin */
  dE = coolrate * dt;
  dTemp = (Gamma_1 * dE) / dens;
  Tnext = T - dTemp * Tchar; /* in unit: K*/

  if (Tnext <= Tmin)
  {
    dTemp = (T - Tmin) / Tchar;
    dE = dTemp * dens / Gamma_1;
    coolrate = dE / dt;
    if (coolrate >= 0.0)
      coolrate = 0.0;
  }

  /* return result */
  return coolrate;
}
#endif /* BAROTROPIC */

/*----------------------------------------------------------------------------*/
/*  \brief Calculate Qt
 *  piecewise linear approximation (Klimcuk et al. 2008)*/
/* T: dimensional variable, SI unit */
static Real func_Qt(const Real T)
{
  Real q;
  Real factor;
  Real logt = log10(T);

  /* first in cgs: ergs sec^-1 cm^3 */
  if (logt <= 4.97)
  {
    q = 1.09e-31 * (pow(T, 2));
  }
  else if (logt <= 5.67)
  {
    q = 8.87e-17 * (pow(T, -1.0));
  }
  else if (logt <= 6.18)
  {
    q = 1.90e-22;
  }
  else if (logt <= 6.55)
  {
    q = 3.54e-13 * (pow(T, -3. / 2.));
  }
  else if (logt <= 6.90)
  {
    q = 3.46e-25 * (pow(T, 1. / 3.));
  }
  else if (logt <= 7.63)
  {
    q = 5.49e-16 * (pow(T, -1.0));
  }
  else
  {
    q = 1.96e-27 * (pow(T, 0.5));
  }

  /* Decrease Q(T) if T<500,000K */
  if ((T <= Tc_low) && (sw_lowt_cool == 1))
  {
    factor = (Tc_low / T) * (Tc_low / T) * (Tc_low / T);
    q = q / factor;
  }

  /* to SI unit: W m^3 */
  q = q * 1.0e-13;

  return q;
}

/*----------------------------------------------------------------------------*/
/*! \fn void linetiedbc_ix2(GridS *pGrid)
 *  \brief Sets boundary condition at the bottom.
 */
/*  ix2, line-tied, bottom 
    In 1D test, the Bx == 0, and By == 1. */
void linetiedbc_ix2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ku; /* k-upper */
#endif

  /* Set all variables in ghost zone */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i] = pGrid->U[k][js][i];
        pGrid->U[k][js - j][i].M1 = 0.0;
        pGrid->U[k][js - j][i].M2 = 0.0;
        pGrid->U[k][js - j][i].M3 = 0.0;
      }
    }
  }

#ifdef MHD
  /* B2i*/
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B2i[k][js - j][i] = 1.0;
      }
    }
  }
  /* Bottom: B1i is not set at i=is-nghost */
  Real pbypx;
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - (nghost - 1); i <= ie + nghost; i++)
      {
        pGrid->B1i[k][js - j][i] = 0.0;
      }
    }
  }
  /* Bottom: B3i */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B3i[k][js - j][i] = 0.0;
      }
    }
  }

  /* B1c, B2c, B3c */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost + 1; i <= ie + nghost - 1; i++)
      {
        pGrid->U[k][js - j][i].B1c = 0.0;
      }
    }
  }

  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i].B2c = 1.0;
      }
    }
  }

  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i].B3c = 0.0;
      }
    }
  }

#endif /* MHD */

  /* Pressure and Total Energy */
  int j_mj;
  Real y_js, y_mj;
  Real p_js, eb_js, ek_js, te_js;
  Real p_mj, eb_mj, ek_mj, te_mj;
  Real x1, x2, x3;
  cc_pos(pGrid, is, js, ks, &x1, &x2, &x3);
  y_js = x2;

  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      j_mj = js - j;
      cc_pos(pGrid, is, j_mj, k, &x1, &x2, &x3);
      y_mj = x2;

      for (i = is - nghost; i <= ie + nghost; i++)
      {
        /* Case 1: extrapolation */
        eb_js = 0.5 * (SQR(pGrid->U[k][js][i].B1c) + SQR(pGrid->U[k][js][i].B2c) + SQR(pGrid->U[k][js][i].B3c));
        ek_js = 0.5 * (SQR(pGrid->U[k][js][i].M1) + SQR(pGrid->U[k][js][i].M2) + SQR(pGrid->U[k][js][i].M3)) / pGrid->U[k][js][i].d;
        p_js = (pGrid->U[k][js][i].E - eb_js - ek_js) * Gamma_1;
        te_mj = p_js / pGrid->U[k][js][i].d;
        p_mj = func_presini_isote(y_mj, y_js, p_js, te_mj);

        /* Case 2: fixed rho, p 
        p_mj = pgasini[k][j][i];
        te_mj = tempini[k][j][i]; */

        pGrid->U[k][j_mj][i].d = p_mj / te_mj;

        eb_mj = 0.5 * (SQR(pGrid->U[k][j_mj][i].B1c) + SQR(pGrid->U[k][j_mj][i].B2c) + SQR(pGrid->U[k][j_mj][i].B3c));
        ek_mj = 0.5 * (SQR(pGrid->U[k][j_mj][i].M1) + SQR(pGrid->U[k][j_mj][i].M2) + SQR(pGrid->U[k][j_mj][i].M3)) / pGrid->U[k][j_mj][i].d;

        pGrid->U[k][j_mj][i].E = p_mj / Gamma_1 + eb_mj + ek_mj;
      }
    }
  }
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void openbc_ox2(GridS *pGrid)
 *  \brief open boundary conditions, Outer x2 boundary (bc_ox2=2) */
/*----------------------------------------------------------------------------*/
/*  ox2, top boundary */
static void openbc_ox2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ku; /* k-upper */
#endif
  Real x1, x2, x3;

  /* Non-reflection condition */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][je + j][i] = pGrid->U[k][je][i];
        if (pGrid->U[k][je + j][i].M2 < 0)
        {
          pGrid->U[k][je + j][i].M2 = 0;
        }
      }
    }
  }

#ifdef MHD
  /* B1i: is not set at i=is-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - (nghost - 1); i <= ie + nghost; i++)
      {
        pGrid->B1i[k][je + j][i] = 0;
      }
    }
  }

  /* B2i: j=je+1 is not a boundary condition for the interface field B2i */
  for (k = ks; k <= ke; k++)
  {
    for (j = 2; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B2i[k][je + j][i] = 1.0;
      }
    }
  }

  /* B3i: */
  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B3i[k][je + j][i] = 0;
      }
    }
  }

  /* B1c, B2c, B3c */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - (nghost - 1); i <= ie + (nghost - 1); i++)
      {
        pGrid->U[k][je + j][i].B1c = 0.5 * (pGrid->B1i[k][je + j][i] + pGrid->B1i[k][je + j][i + 1]);
      }
    }
  }

  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost - 1; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][je + j][i].B2c = 0.5 * (pGrid->B2i[k][je + j][i] + pGrid->B2i[k][je + j + 1][i]);
      }
    }
  }

  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost - 1; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][je + j][i].B3c = pGrid->B3i[k][je + j][i];
      }
    }
  }

  /* Pressure and energy */
  Real eb_je, ek_je;
  Real pje, Tje;
  Real pjc;
  Real x1c, x2c, x3c, yjc, yje;
  int jc;
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      cc_pos(pGrid, is, je, k, &x1c, &yje, &x3c);

      for (i = is - nghost; i <= ie + nghost; i++)
      {
        eb_je = 0.5 * (SQR(pGrid->U[k][je][i].B1c) + SQR(pGrid->U[k][je][i].B2c) + SQR(pGrid->U[k][je][i].B3c));
        ek_je = 0.5 * (SQR(pGrid->U[k][je][i].M1) + SQR(pGrid->U[k][je][i].M2) + SQR(pGrid->U[k][je][i].M3)) / pGrid->U[k][je][i].d;
        pje = Gamma_1 * (pGrid->U[k][je][i].E - eb_je - ek_je);
        Tje = pje / pGrid->U[k][je][i].d;

        jc = je + j;
        cc_pos(pGrid, i, jc, k, &x1c, &yjc, &x3c);
        pjc = func_presini_isote(yjc, yje, pje, Tje);
        pGrid->U[k][jc][i].d = pjc / Tje;

        pGrid->U[k][jc][i].E = pjc / (Gamma_1) + 0.5 * (SQR(pGrid->U[k][jc][i].B1c) + SQR(pGrid->U[k][jc][i].B2c) + SQR(pGrid->U[k][jc][i].B3c)) + 0.5 * (SQR(pGrid->U[k][jc][i].M1) + SQR(pGrid->U[k][jc][i].M2) + SQR(pGrid->U[k][jc][i].M3)) / pGrid->U[k][jc][i].d;
      }
    }
  }

#endif
  return;
}
