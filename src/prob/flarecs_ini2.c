#include "copyright.h"
/*============================================================================*/
/*! \file flarecs.c
 *  \brief Problem generator for the current sheet in solar flare.
 *
 * PURPOSE: Problem generator for the magnetic reconnection in flare CS.
 *
 * REFERENCE: Forbes & Malherbe SoPh 1991; Shen, Lin & Murphy ApJ 2011.
 *
 * Update:
 *  20140822_2300:
 *  Left and Right boundary.
 *  20150227
 *  Set the first line in ghost zones.
 *  2015-0228
 *  Add linetiedbc_ix2_test.
 *  2015-0604-0605
 *  Line-tied boundary conditions. Jz =/= 0 at the bottome boundary.
 *  The guide field Bz =/= 0.
 *  Increase resistivity at the open boundary.
 *  Density compensation at the line-tied boundary.
 *  2015-11-26
 *  Gravity problem: initial reconnection takes place at the top boundary.
 *  Add a initial ohmic resistivity perturbation.
 *  2015-12-20
 *  Initial conditions: bz(y) does not changes with y.
 */

/*============================================================================*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defs.h"
#include "athena.h"
#include "globals.h"
#include "prototypes.h"

/*==============================================================================
 * PRIVATE FUNCTION PROTOTYPES:
 * void flarecs_linetied() - sets BCs on R-x2 boundary
 *============================================================================*/
static Real byini(const Real x1);
static Real bzini(const Real x1, const Real x2);
static Real pgas_bg(const Real x2);
static Real Qt(const Real T);
static Real grav_sun(const Real x1, const Real x2, const Real x3);
static Real grav_pot2(const Real x1, const Real x2, const Real x3);
static Real te_bg(const Real x1, const Real x2);
static Real rho_bg(const Real x1, const Real x2);
static void linetiedbc_ix2_test(GridS *pGrid);
static void openbc_ox2(GridS *pGrid);
static void openbc_ix1(GridS *pGrid);
static void openbc_ox1(GridS *pGrid);
static void symmetrybc_ix1(GridS *pGrid);
static void openmode2bc_ox2(GridS *pGrid);

/*=========================== PUBLIC FUNCTIONS ===============================*/
/*----------------------------------------------------------------------------*/

/* problem:  */

void problem(DomainS *pDomain)
{
  GridS *pGrid = (pDomain->Grid);
  Prim1DS W;
  Cons1DS U1d;
  int i, is = pGrid->is, ie = pGrid->ie;
  int j, js = pGrid->js, je = pGrid->je;
  int k, ks = pGrid->ks, ke = pGrid->ke;
  Real pressure, drat, prat, rad, pa, da, x1, x2, x3, xf, yf, zf;
  Real gamma, ini_width, beta0;
  Real p0, pgas, bz0;
  Real bxturb, byturb, lx, ly;
  int jj;
  int icplus;
  Real G=6.672e-11, Msun=1.989e30, Rsun=6.693e8; /*in unit m^3kg^-1s^-2*/
  Real KB = 1.38e-23, mp = 1.6726e-27; /*J K^-1, kg m^-3*/
  Real GM2;
  Real lchar, vchar;
  Real height, height0;
  Real height_bot = 2.5e6; /* 2.5Mm */

  /* Magnitude of perturbations */
  Real vin_turb;
  Real pos_x_turb, pos_y_turb, hwd_x_turb, hwd_y_turb;
  
  /* Read input parameters */
  gamma    = par_getd("problem", "gamma");
  beta0    = par_getd("problem", "beta0");
  ini_width= par_getd("problem", "ini_width");
  lchar = par_getd("problem", "Lchar");
  vchar = par_getd("problem", "vchar");

  vin_turb = par_getd("problem", "vin_turb"); 
  pos_x_turb = par_getd("problem", "pos_x_turb");
  pos_y_turb = par_getd("problem", "pos_y_turb");
  hwd_x_turb = par_getd("problem", "hwd_x_turb");
  hwd_y_turb = par_getd("problem", "hwd_y_turb");

  /* for initial gravity */
  GM2 = G*Msun/(vchar*vchar*lchar)/(beta0/2.0);

  for(k = ks; k <= ke; k++) {
    for(j = js; j <= je; j++) {
      for(i = is; i <= ie; i++) {
        /* axis */
        cc_pos(pGrid, i, j, k, &x1, &x2, &x3);
        /* Pressure background */
        /*case1: */
        pgas = pgas_bg(x2);
        /*case2: 
        pgas = pgas_ini(x1, x2);*/

        /* density */
        pGrid->U[k][j][i].d = pgas/(beta0/2.0);
        /* monentum */
        pGrid->U[k][j][i].M1 = 0.0;
        pGrid->U[k][j][i].M2 = 0.0;
        pGrid->U[k][j][i].M3 = 0.0;
        /* magnetic field */
        pGrid->B1i[k][j][i] = 0.0;
        pGrid->B2i[k][j][i] = byini(x1);
        /* case 1: 
        bz0 = bzini(x1, x2); */
        /* case 2:
        bz0 = sqrt(2.0*(pgas_bg(x2) + 0.5 - pgas - 0.5*byini(x1)*byini(x1)));*/
        bz0 = 0.0;
        pGrid->B3i[k][j][i] = bz0;

        /* initial setting for magnetic field on the boundary */
        if(i == ie && ie > is) pGrid->B1i[k][j][i + 1] = 0.0;
        if(j == je && je > js) pGrid->B2i[k][j + 1][i] = byini(x1);
        if(k == ke && ke > ks) pGrid->B3i[k + 1][j][i] = bz0;

        /* monmentum perturbations */
        pGrid->U[k][j][i].M1 = -vin_turb*x1
          *exp(-SQR(x2-pos_y_turb)/SQR(hwd_y_turb))*pGrid->U[k][j][i].d;
      }
    }
  }

  /* cell-center magnetic field */
  for(k = ks; k <= ke; k++) {
    for(j = js; j <= je; j++) {
      for(i = is; i <= ie; i++) {
        pGrid->U[k][j][i].B1c = 0.5*(pGrid->B1i[k][j][i  ] +
            pGrid->B1i[k][j][i+1]);
        pGrid->U[k][j][i].B2c = 0.5*(pGrid->B2i[k][j  ][i] +
            pGrid->B2i[k][j+1][i]);
        pGrid->U[k][j][i].B3c =      pGrid->B3i[k][j][i];
      }
    }
  }

  /* total energy*/
  for(k = ks; k <= ke; k++) {
    for(j = js; j <= je; j++) {
      for(i = is; i <= ie; i++) {
        cc_pos(pGrid, i, j, k, &x1, &x2, &x3);
        /*case1: */
        pgas = pgas_bg(x2);

        pGrid->U[k][j][i].E = pgas/(gamma - 1.0)
          + 0.5*(SQR(pGrid->U[k][j][i].B1c)
              + SQR(pGrid->U[k][j][i].B2c)
              + SQR(pGrid->U[k][j][i].B3c))
          + 0.5*(SQR(pGrid->U[k][j][i].M1)
              + SQR(pGrid->U[k][j][i].M2)
              + SQR(pGrid->U[k][j][i].M3))/pGrid->U[k][j][i].d;
      }
    }
  }

  /* Set resistivity */
#ifdef RESISTIVITY
  eta_Ohm = par_getd("problem", "eta_Ohm");
  Q_AD = par_getd("problem", "Q_AD");
  Q_Hall = 0.0;
  d_ind = 0.0;
#endif

  /* Set thermel conduction*/
#ifdef THERMAL_CONDUCTION
  kappa_aniso = par_getd_def("problem","kappa_aniso",0.0);
  kappa_iso = par_getd_def("problem","kappa_iso",0.0);
#endif

  /* Set optical thin radiation cooling and corona heating function*/
  CoolingFunc = cool_heat_corona;

  /* Enroll gravitational potential to give acceleration in y-direction for 2D
  */ 
  StaticGravPot = grav_pot2;

  /* Set boundary value functions */
  bvals_mhd_fun(pDomain, left_x2,  linetiedbc_ix2_test);
  bvals_mhd_fun(pDomain, right_x2, openbc_ox2);

  bvals_mhd_fun(pDomain, left_x1,  openbc_ix1);
  bvals_mhd_fun(pDomain, right_x1, openbc_ox1);

}

/*==============================================================================
 * PROBLEM USER FUNCTIONS:
 * problem_write_restart() - writes problem-specific user data to restart files
 * problem_read_restart()  - reads problem-specific user data from restart files
 * get_usr_expr()          - sets pointer to expression for special output data
 * get_usr_out_fun()       - returns a user defined output function pointer
 * get_usr_par_prop()      - returns a user defined particle selection function
 * Userwork_in_loop        - problem specific work IN     main loop
 * Userwork_after_loop     - problem specific work AFTER  main loop
 *----------------------------------------------------------------------------*/

void problem_write_restart(MeshS *pM, FILE *fp)
{
  /* gamma and beta0 are used in line-tied boundary */
  Real gamma, beta0, ini_width;
  gamma = par_getd("problem", "gamma");
  beta0 = par_getd("problem", "beta0");
  /* Set resistivity */
  /* Set thermel conduction*/
#ifdef THERMAL_CONDUCTION
  kappa_aniso = par_getd_def("problem","kappa_aniso",0.0);
  kappa_iso = par_getd_def("problem","kappa_iso",0.0);
#endif

  return;
}

void problem_read_restart(MeshS *pM, FILE *fp)
{
  int nl, nd;
  /* gamma and beta0 are used in line-tied boundary */
  Real gamma, beta0, ini_width;
  gamma = par_getd("problem", "gamma");
  beta0 = par_getd("problem", "beta0");
  /* Set resistivity */
  /* Set thermel conduction*/
#ifdef THERMAL_CONDUCTION
  kappa_aniso = par_getd_def("problem","kappa_aniso",0.0);
  kappa_iso = par_getd_def("problem","kappa_iso",0.0);
#endif

  /* Set optical thin radiation cooling and corona heating function*/
  CoolingFunc = cool_heat_corona;

  /* Set boundary value functions */
  for (nl=0; nl<(pM->NLevels); nl++){
    for (nd=0; nd<(pM->DomainsPerLevel[nl]); nd++){

      bvals_mhd_fun(&(pM->Domain[nl][nd]), left_x2,  linetiedbc_ix2_test);
      bvals_mhd_fun(&(pM->Domain[nl][nd]), right_x2, openbc_ox2);

      bvals_mhd_fun(&(pM->Domain[nl][nd]), left_x1,  openbc_ix1);
      bvals_mhd_fun(&(pM->Domain[nl][nd]), right_x1, openbc_ox1);
    }
  }
  return;
}

#ifdef MHD
/*! \fn static Real current(const GridS *pG, const int i, const int j, const
 *			   int k)
 *  \brief computes x3-component of current
 */
static Real current(const GridS *pG, const int i, const int j, const int k)
{
  return ((pG->B2i[k][j][i]-pG->B2i[k][j][i-1])/pG->dx1 -
      (pG->B1i[k][j][i]-pG->B1i[k][j-1][i])/pG->dx2);
}

/*! \fn static Real divB(const GridS *pG, const int i, const int j, const int k)
 *  \brief  calculates div(B) */
static Real divB(const GridS *pG, const int i, const int j, const int k)
{
  Real qa;
  if (pG->Nx[2] > 1) {
    qa = (pG->B1i[k][j][i+1]-pG->B1i[k][j][i])/pG->dx1 +
      (pG->B2i[k][j+1][i]-pG->B2i[k][j][i])/pG->dx2 +
      (pG->B3i[k+1][j][i]-pG->B3i[k][j][i])/pG->dx3;
  } else {
    qa = (pG->B1i[k][j][i+1]-pG->B1i[k][j][i])/pG->dx1 +
      (pG->B2i[k][j+1][i]-pG->B2i[k][j][i])/pG->dx2;
  }
  return qa;
}

/*! \fn static Real bz(const GridS *pG, const int i, const int j, const int k)
 *  \brief return bz in 2.5D simulations*/
static Real bz(const GridS *pG, const int i, const int j, const int k)
{
  return pG->U[k][j][i].B3c;
}

/*! \fn static Real ohmiceta(const GridS *pG, const int i, const int j, const int k)
 *  \brief return  ohmiceta*/
static Real ohmiceta(const GridS *pG, const int i, const int j, const int k)
{
  return pG->eta_Ohm[k][j][i];
}


/* \fn static Real Az(const GridS *pG, const int i, const int j, const int k)
 *  \brief  calculates Az */
static Real az(const GridS *pG, const int i, const int j, const int k)
{
  Real a;
  int is, ie, js, je, ks, ke;
  int mx, mz;

  is = pG->is; ie = pG->ie;
  js = pG->js; je = pG->je;
  ks = pG->ks; ke = pG->ke;

  a = 0.0;
  for (mz=je-1; mz>=j; mz--){
    a = a - 0.5*(pG->U[ks][mz][ie].B1c+pG->U[ks][mz+1][ie].B1c)*(pG->dx2);
  }
  for (mx=ie-1; mx>=i; mx--){
    a = a + 0.5*(pG->U[ks][j][mx-1].B2c +pG->U[ks][j][mx].B2c)*(pG->dx1);
  }
  return a;
}
#endif

ConsFun_t get_usr_expr(const char *expr)
{
#ifdef MHD
  if(strcmp(expr,"J3")==0) return current;
  else if(strcmp(expr,"DivB")==0) return divB;
  else if(strcmp(expr,"A3")==0) return az;
  else if(strcmp(expr,"B3")==0) return bz;
  else if(strcmp(expr,"Ohmiceta")==0) return ohmiceta;
#endif
  return NULL;
}

VOutFun_t get_usr_out_fun(const char *name)
{
  return NULL;
}

#ifdef RESISTIVITY

void get_eta_user(GridS *pG, int i, int j, int k,
    Real *eta_O, Real *eta_H, Real *eta_A)
{
  Real x1, x2, x3;
  Real r, r_eta = 0.1;
  Real eta_input;
  
  /* Magnitude of perturbations */
  Real eta_turb, tim_turb;
  Real pos_x_turb, pos_y_turb, hwd_x_turb, hwd_y_turb;
  Real err=1.0e-03, hwd_y_low;

  /* Read input parameters */

  eta_turb = par_getd("problem", "eta_turb");
  tim_turb = par_getd("problem", "tim_turb");
  pos_x_turb = par_getd("problem", "pos_x_turb");
  pos_y_turb = par_getd("problem", "pos_y_turb");
  hwd_x_turb = par_getd("problem", "hwd_x_turb");
  hwd_y_turb = par_getd("problem", "hwd_y_turb");

  *eta_O = par_getd("problem", "eta_Ohm");
  *eta_H = 0.0;
  *eta_A = par_getd("problem", "Q_AD");

  /* initial enhanced resistivity diffusion */
  /* case 1, local points
  cc_pos(pG, i, j, k, &x1, &x2, &x3);
     r = sqrt(x1*x1 + (x2-0.5)*(x2-0.5));
     if((pG->time <= 2.0) && (r <= r_eta)){
   *eta_O = *eta_O + eta_ini*(2.0*pow(r/r_eta,3.0)-3.0*pow(r/r_eta,2.0) + 1.);
   }*/

  /* case 2, inside of the CS 
  cc_pos(pG, i, j, k, &x1, &x2, &x3);
  eta_input = par_getd("problem", "eta_Ohm");
  hwd_y_low = pow(-SQR(pos_y_turb)/(log(err)), 0.5);
  if(pG->time <= tim_turb){
    cc_pos(pG, i, j, k, &x1, &x2, &x3);
    if(x2 >= pos_y_turb){
      *eta_O = eta_turb
        *exp(-SQR(x2-pos_y_turb)/SQR(hwd_y_turb))
        *exp(-SQR(x1-pos_x_turb)/SQR(hwd_x_turb));
    }else{
      *eta_O = eta_turb
        *exp(-SQR(x2-pos_y_turb)/SQR(hwd_y_low))
        *exp(-SQR(x1-pos_x_turb)/SQR(hwd_x_turb));
    }
  }else{
    if(x2 >= pos_y_turb){
      *eta_O = eta_turb
        *exp(-(pG->time-tim_turb)/tim_turb)
        *exp(-SQR(x2-pos_y_turb)/SQR(hwd_y_turb))
        *exp(-SQR(x1-pos_x_turb)/SQR(hwd_x_turb))
        + eta_input*(1.0 - exp(-(pG->time-tim_turb)/tim_turb));
    }else{
      *eta_O = eta_turb
        *exp(-(pG->time-tim_turb)/tim_turb)
        *exp(-SQR(x2-pos_y_turb)/SQR(hwd_y_low))
        *exp(-SQR(x1-pos_x_turb)/SQR(hwd_x_turb))
        + eta_input*(1.0 - exp(-(pG->time-tim_turb)/tim_turb));
    }
  }*/
    
  /* case 3, diffustion for the ideal CS */
  if (pG->time <= tim_turb) {
    *eta_O = 0.005;
  }

  /* Bottom boundary */
  eta_input = par_getd("problem", "eta_Ohm");
  cc_pos(pG, i, j, k, &x1, &x2, &x3);
  if(x2 <= 0.1){
    *eta_O = eta_input*exp(-SQR(x2-0.1)/SQR(0.03));
  }
  
  return;
}
#endif

void Userwork_in_loop(MeshS *pM)
{
  GridS *pG=pM->Domain[0][0].Grid;
  int i,j,k,is,ie,js,je,ks,ke;
  Real maxvout=0.0, time_reduc;
  Real x1, x2, x3;

  is = pG->is; ie = pG->ie;
  js = pG->js; je = pG->je;
  ks = pG->ks; ke = pG->ke;

  /* get the maximum outflow velocity
     for (k=ks; k<=ke; k++) {
     for (j=js; j<=je; j++) {
     for (i=is; i<=ie; i++) {
     maxvout = MAX(maxvout,pG->U[k][j][i].M2/pG->U[k][j][i].d);
     }
     }
     }*/

  /* decrease guild field Bz 
     time_reduc = 1.0/maxvout;

     for (k=ks; k<=ke; k++) {
     for (j=js; j<=je; j++) {
     for (i=is; i<=ie; i++) {
     cc_pos(pG, i, j, k, &x1, &x2, &x3);
     pG->B3i[k][j][i] = ...; 
     if(pG->B3i[k][j][i] <= 1.0e-2){
     pG->B3i[k][j][i] = 0.0;
     }
     }
     }
     }*/

  /* boundary diffusion */

  return;
}

void Userwork_after_loop(MeshS *pM)
{
  return;

}

/*=========================== PRIVATE FUNCTIONS ==============================*/
/*----------------------------------------------------------------------------*/
/*  \funtion of By for initial conditions */
static Real byini(const Real x1)
{
  Real by0;
  Real b0;
  Real ini_width;
  int mode_cs;

  /* initialize parameters */
  ini_width = par_getd("problem", "ini_width");
  mode_cs = par_getd("problem", "mode_cs");
  b0 = 1.0;

  /* case 1: Sin function */
  if(mode_cs == 0) {
    if(x1 >  ini_width) by0 = b0;
    if(x1 <= ini_width && x1 >= -ini_width) by0 =
      b0 * sin(PI * x1 / (2.0 * ini_width));
    if(x1 < -ini_width) by0 = -b0;
  }

  /* case 2: Harris sheet */
  if(mode_cs == 1) {
    by0 = b0*tanh(x1/ini_width);
  }

  /* case 3: Ideal sheet */
  if(mode_cs == 2){
    if(x1 <= 0.0){
      by0 = -1.0;
    }else{
      by0 = 1.0;
    }
  }

  return by0;
}

/*---------------------------------------------------------------------------*/
/*  \funtion of Bz for initial conditions */
static Real bzini(const Real x1, const Real x2)
{
  Real beta0;
  Real by0, bzsqr_half, bz;
  Real ptol_amb;
  beta0 = par_getd("problem", "beta0");

  by0 = byini(x1);
  bz = sqrt(1.0-by0*by0);
  return bz;
}

/*---------------------------------------------------------------------------*/
/*  \function pgas_bg */
static Real pgas_bg(const Real x2)
{
  Real G=6.672e-11, Msun=1.989e30, Rsun=6.693e8; /*in unit m^3kg^-1s^-2*/
  Real KB = 1.38e-23, mp = 1.6726e-27; /*J K^-1, kg m^-3*/
  Real GM2;
  Real beta0, lchar, vchar, Tchar, pchar;
  Real height, height1;
  Real height_bot = 2.5e6; /* 2.5Mm */
  Real p_top;
  Real p0;

  beta0 = par_getd("problem", "beta0");
  lchar = par_getd("problem", "Lchar");
  vchar = par_getd("problem", "vchar");

  GM2 = G*Msun/(vchar*vchar*lchar)/(beta0/2.0);
  p_top = 0.5*beta0;
  height = x2 + height_bot/lchar + Rsun/lchar;
  height1 = 2.0 + height_bot/lchar + Rsun/lchar;
  p0 = p_top*exp(GM2/height - GM2/height1);

  return p0;
}


/*----------------------------------------------------------------------------*/
/*! \fn void linetiedbc_ix2_test(GridS *pGrid)
 *  \brief Sets boundary condition at the bottom.
 */
/*  ix2, line-tied-test, bottom */
void linetiedbc_ix2_test(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
  int kl, ku;
  Real x1, x2, x3;
  Real gamma, beta0, ini_width, lchar, vchar;
  Real ca, cb, cc, x1_left, yc, y1, y2, pbypx, bx1, bx2;
  Real vxc, vyc, vzc, vx1, vx2, vy1, vy2, vz1, vz2;
  Real bxup, byl, byr;
  Real bxc, byc, bzc, pc, bxc1, byc1, bzc1, p1, bxc2, byc2, bzc2, p2;
  Real gsun;
  int jc;
  Real pjc, pjc_up, djc_up, pjsplus;
  Real pjs, GM2, height, height0, height1;
  Real G=6.672e-11, Msun=1.989e30, Rsun=6.693e8; /*in unit m^3kg^-1s^-2*/
  Real KB = 1.38e-23, mp = 1.6726e-27; /*J K^-1, kg m^-3*/
  Real height_bot = 2.5e6; /* 2.5Mm */

  /* perturbation */
  Real tim_turb;

  gamma = par_getd("problem", "gamma");
  beta0 = par_getd("problem", "beta0");
  ini_width = par_getd("problem", "ini_width");

  tim_turb = par_getd("problem", "tim_turb");

  lchar = par_getd("problem", "Lchar");
  vchar = par_getd("problem", "vchar");
  GM2 = G*Msun/(vchar*vchar*lchar)/(beta0/2.0);
  height0 = height_bot/lchar+Rsun/lchar;

  if (pGrid->Nx[2] > 1) {
    kl = pGrid->ks - nghost;
    ku = pGrid->ke + nghost;
  } else {
    kl = pGrid->ks;
    ku = pGrid->ke;
  }

  /* all variables */
  for (k=ks; k<=ke; k++) {
    for (j=1; j<=nghost; j++) {
      for (i=is-nghost; i<=ie+nghost; i++) {
        pGrid->U[k][js-j][i] = pGrid->U[k][js][i];
      }
    }
  }

  /* density will be resetted after SETTING total energy */

  /* momentum */
  if (pGrid->time >= tim_turb) {
    for (k=ks; k<=ke; k++) {
      for (j=1; j<=nghost; j++) {
        for (i=is-nghost; i<=ie+nghost; i++) {
          pGrid->U[k][js-j][i].M1 = 0.0;       
          pGrid->U[k][js-j][i].M2 = 0.0;
          pGrid->U[k][js-j][i].M3 = 0.0;
        }
      }
    }
  }

  /* Bottom: B2i is not set at j=js-nghost */
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is - nghost; i <= ie + nghost; i++) {
        cc_pos(pGrid, i, j, k, &x1, &x2, &x3);
        pGrid->B2i[k][js-j][i] = byini(x1);
        /* case 2: */
        pGrid->B2i[k][js-j][i] = pGrid->B2i[k][js][i];
      }
    }
  }

  /* Bottom: B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is - nghost; i <= ie + nghost; i++) {
        /*pGrid->B1i[k][js-j][i] = pGrid->B1i[k][js][i];*/
        /* i and js */
        cc_pos(pGrid, i, js, k, &x1, &x2, &x3);
        x1_left = x1 - 0.5*pGrid->dx1;
        if (x1_left>ini_width || x1_left<-ini_width) 
        {
          pbypx = 0;
        }
        else
        {
          pbypx=(PI/2.0/ini_width)*cos(PI*x1_left / (2.0 * ini_width));
        }
        bx2 = pGrid->B1i[k][js+1][i];
        bx1 = pGrid->B1i[k][js][i];
        y2 = x2 + 0.5*pGrid->dx2;
        y1 = x2;
        ca = (bx2 - bx1)/(y2*y2 - y1*y1);
        cb = pbypx;
        cc = bx1 - cb*y1 - ca*y1*y1;

        /* set the js - j row */
        cc_pos(pGrid, i, js-j, k, &x1, &x2, &x3);
        yc = x2;
        pGrid->B1i[k][js-j][i] = ca*yc*yc + cb*yc + cc;

        /* new set bx == 0 for fixed BCs*/
        /* run_1: 
           pGrid->B1i[k][js-j][i] = 0.0;*/
        /* run_2: */
        pGrid->B1i[k][js-j][i] = pGrid->B1i[k][js][i];
      }
    }
  }

  /* Bottom: B3i */
  for (k = ks; k <= ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i = is-nghost; i <= ie + nghost; i++) {
        cc_pos(pGrid, i, j, k, &x1, &x2, &x3);
        pGrid->B3i[k][js-j][i] = pGrid->B3i[k][js][i];
      }
    }
  }

  /* cell-center magnetic field
     for (k = ks; k <= ke; k++){
     for (j = js-nghost; j <= js; j++){
     for (i = is; i <= ie; i++){
     pGrid->U[k][j][i].B1c = 0.5*(pGrid->B1i[k][j][i  ] +
     pGrid->B1i[k][j][i+1]);
     pGrid->U[k][j][i].B2c = 0.5*(pGrid->B2i[k][j  ][i] +
     pGrid->B2i[k][j+1][i]);
     pGrid->U[k][j][i].B3c = pGrid->B3i[k][j][i];
     }
     for (i = is-nghost; i <= is-1; i++){
     pGrid->U[k][j][i].B1c = pGrid->U[k][j][is].B1c;
     pGrid->U[k][j][i].B2c = pGrid->U[k][j][is].B2c;
     pGrid->U[k][j][i].B3c = pGrid->U[k][j][is].B3c;
     }
     for (i = ie; i <= ie+nghost; i++){
     pGrid->U[k][j][i].B1c = pGrid->U[k][j][ie].B1c;
     pGrid->U[k][j][i].B2c = pGrid->U[k][j][ie].B2c;
     pGrid->U[k][j][i].B3c = pGrid->U[k][j][ie].B3c;
     }
     }
     } */

  /*total energy at the last line, assuming pressure equilirium*/
  for (k=ks; k<=ke; k++) {
    for (j = 1; j <= nghost; j++) {
      for (i=is-nghost; i<=ie+nghost; i++) {
        cc_pos(pGrid, i, js, k, &x1, &x2, &x3);
        height1 = (pGrid->dx2) + x2 + height_bot/lchar + Rsun/lchar;
        pjsplus = (gamma - 1.0)*(pGrid->U[k][js+1][i].E
            - 0.5*(SQR(pGrid->U[k][js+1][i].B1c)
              + SQR(pGrid->U[k][js+1][i].B2c)
              + SQR(pGrid->U[k][js+1][i].B3c))
            - 0.5*(SQR(pGrid->U[k][js+1][i].M1)
              + SQR(pGrid->U[k][js+1][i].M2)
              + SQR(pGrid->U[k][js+1][i].M3))/pGrid->U[k][js+1][i].d);
        jc = js - j;
        height = x2 - j*(pGrid->dx2) + height_bot/lchar + Rsun/lchar;
        pjc = pjsplus*exp(GM2/height - GM2/height1);
        pGrid->U[k][jc][i].d = pjc/(beta0/2.0);
        pGrid->U[k][jc][i].E = pjc/(gamma - 1.0)
          + 0.5*(SQR(pGrid->U[k][jc][i].B1c)
              + SQR(pGrid->U[k][jc][i].B2c)
              + SQR(pGrid->U[k][jc][i].B3c))
          + 0.5*(SQR(pGrid->U[k][jc][i].M1)
              + SQR(pGrid->U[k][jc][i].M2)
              + SQR(pGrid->U[k][jc][i].M3))/pGrid->U[k][jc][i].d;
      }
    }
  }
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void openbc_ox2(GridS *pGrid)
 *  \brief open boundary conditions, Outer x2 boundary (bc_ox2=2) */
/*----------------------------------------------------------------------------*/
/*  ox2, top boundary */

void openbc_ox2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i,j,k;
#ifdef MHD
  int ku; /* k-upper */
#endif
  Real dx1, dx2;
  Real x1, x2, x3;
  Real gamma;
  Real p0, pjc, b10, b20, b30, b11, b21, b31;
  Real gsun, beta0, lchar, vchar, djeplus, pje, pjeplus;
  Real GM2, height, height0, height1;
  Real G=6.672e-11, Msun=1.989e30, Rsun=6.693e8; /*in unit m^3kg^-1s^-2*/
  Real KB = 1.38e-23, mp = 1.6726e-27; /*J K^-1, kg m^-3*/
  Real height_bot = 2.5e6; /* 2.5Mm */
  int jc;
  Real pjcm1;

  gamma = par_getd("problem", "gamma");
  beta0 = par_getd("problem", "beta0");
  lchar = par_getd("problem", "Lchar");
  vchar = par_getd("problem", "vchar");

  GM2 = G*Msun/(vchar*vchar*lchar)/(beta0/2.0);
  height0 = height_bot/lchar+Rsun/lchar;

  for(k=ks; k<=ke; k++) {
    for(j=1; j<=nghost; j++) {
      for(i=is-nghost; i<=ie+nghost; i++) {
        pGrid->U[k][je+j][i] = pGrid->U[k][je][i];
        if (pGrid->U[k][je][i].M2 <= 0.0) {
          pGrid->U[k][je+j][i].M2 = -pGrid->U[k][je-j+1][i].M2;
        }
        /* limit inflow */
        if (pGrid->U[k][je][i].M2 <= 0.0){
          /* pGrid->U[k][je+j][i].M2 = -pGrid->U[k][je-j+1][i].M2;*/
          pGrid->U[k][je+j][i].M2 = 0.0;
        }
        /* limit tension force */
        if (pGrid->U[k][je][i].B1c <= 0.0){
          /*pGrid->U[k][je+j][i].B1c = -pGrid->U[k][je][i].B1c;*/
          pGrid->U[k][je+j][i].B1c = 0.0;
        }
      }
    }
  }

  /* new set for gravity at the top boundary, which do not depends on 
   * Temperature */
  for(k=ks; k<=ke; k++) {
    for(j=1; j<=nghost; j++) {
      for(i=is-nghost; i<=ie+nghost; i++) {
        jc = je + j;

        pjcm1 = (gamma - 1.0)*(pGrid->U[k][jc-1][i].E
            - 0.5*(SQR(pGrid->U[k][jc-1][i].B1c)
              + SQR(pGrid->U[k][jc-1][i].B2c)
              + SQR(pGrid->U[k][jc-1][i].B3c))
            - 0.5*(SQR(pGrid->U[k][jc-1][i].M1)
              + SQR(pGrid->U[k][jc-1][i].M2)
              + SQR(pGrid->U[k][jc-1][i].M3))/pGrid->U[k][jc-1][i].d);

        cc_pos(pGrid, i, je, k, &x1, &x2, &x3);

        pjc = pjcm1 - (pGrid->dx2)*pGrid->U[k][jc-1][i].d
          *grav_sun(x1, (x2 + j*(pGrid->dx2)), x3);

        pGrid->U[k][jc][i].E = pjc/(gamma - 1.0)
          + 0.5*(SQR(pGrid->U[k][jc][i].B1c)
              + SQR(pGrid->U[k][jc][i].B2c)
              + SQR(pGrid->U[k][jc][i].B3c))
          + 0.5*(SQR(pGrid->U[k][jc][i].M1)
              + SQR(pGrid->U[k][jc][i].M2)
              + SQR(pGrid->U[k][jc][i].M3))/pGrid->U[k][jc][i].d;
      }
    }
  }

#ifdef MHD
  /* B1i is not set at i=is-nghost */
  for(k=ks; k<=ke; k++) {
    for(j=1; j<=nghost; j++) {
      for(i=is-(nghost-1); i<=ie+nghost; i++) {
        pGrid->B1i[k][je+j][i] = pGrid->B1i[k][je][i];
      }
    }
  }

  /* j=je+1 is not a boundary condition for the interface field B2i */
  for(k=ks; k<=ke; k++) {
    for(j=2; j<=nghost; j++) {
      for(i=is-nghost; i<=ie+nghost; i++) {
        pGrid->B2i[k][je+j][i] = pGrid->B2i[k][je][i];
      }
    }
  }

  if(pGrid->Nx[2] > 1) ku=ke+1;
  else ku=ke;
  for(k=ks; k<=ku; k++) {
    for(j=1; j<=nghost; j++) {
      for(i=is-nghost; i<=ie+nghost; i++) {
        pGrid->B3i[k][je+j][i] = pGrid->B3i[k][je][i];
      }
    }
  }
#endif /* MHD */
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void symmetrybc_ix1(GridS *pGrid)
 *  \brief open boundary condition, Inner x1 boundary (bc_ix1=2) */
/*----------------------------------------------------------------------------*/
/*  ix1, Left, symmetry conditions */

void symmetrybc_ix1(GridS *pGrid)
{
  int is = pGrid->is;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i,j,k;

#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  /* (1) density */
  for(k=ks; k<=ke; k++) {
    for(j=js-nghost; j<=je+nghost; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->U[k][j][is-i].d = pGrid->U[k][j][is+i-1].d;
      }
    }
  }

  /* (2-4) momentum */
  for(k=ks; k<=ke; k++) {
    for(j=js-nghost; j<=je+nghost; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->U[k][j][is-i].M1 = -1.0*pGrid->U[k][j][is+i-1].M1;
        pGrid->U[k][j][is-i].M2 = pGrid->U[k][j][is+i-1].M2;
        pGrid->U[k][j][is-i].M3 = pGrid->U[k][j][is+i-1].M3;
      }
    }
  }

  /* (5) energy density*/
  for(k=ks; k<=ke; k++) {
    for(j=js-nghost; j<=je+nghost; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->U[k][j][is-i].E = pGrid->U[k][j][is+i-1].E;
      }
    }
  }


#ifdef MHD
  /* B2i */
  if(pGrid->Nx[1] > 1) ju=je+1;
  else ju=je;
  for(k=ks; k<=ke; k++) {
    for(j=js-nghost; j<=je+nghost; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->B2i[k][j][is-i] = -1.0*pGrid->B2i[k][j][is+i-1];
      }
    }
  }
  /* B1i is not set at i=is-nghost */
  for(k=ks; k<=ke; k++) {
    for(j=js-nghost; j<=je+nghost; j++) {
      for(i=1; i<=nghost-1; i++) {
        pGrid->B1i[k][j][is-i] = pGrid->B1i[k][j][is+i];
      }
    }
  }

  if(pGrid->Nx[2] > 1) ku=ke+1;
  else ku=ke;
  for(k=ks; k<=ku; k++) {
    for(j=js-nghost; j<=je+nghost; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->B3i[k][j][is-i] = pGrid->B3i[k][j][is+i-1];
      }
    }
  }

  /* (6) B1c */
  for(k=ks; k<=ke; k++) {
    for(j=js-nghost; j<=je+nghost; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->U[k][j][is-i].B1c = pGrid->U[k][j][is+i-1].B1c;
      }
    }
  }

  /* (7) B2c */
  for(k=ks; k<=ke; k++) {
    for(j=js-nghost; j<=je+nghost; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->U[k][j][is-i].B2c = -1.0*pGrid->U[k][j][is+i-1].B2c;
      }
    }
  }

  /* (8) B3c */
  for(k=ks; k<=ke; k++) {
    for(j=js-nghost; j<=je+nghost; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->U[k][j][is-i].B3c = pGrid->U[k][j][is+i-1].B3c;
      }
    }
  }

#endif /* MHD */
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void openbc_ix1(GridS *pGrid)
 *  \brief open boundary condition, Inner x1 boundary (bc_ix1=2) */
/*----------------------------------------------------------------------------*/
/*  ix1, Left boundary */

void openbc_ix1(GridS *pGrid)
{
  int is = pGrid->is;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i,j,k;
  int j0, j1;
  Real x1, x2, x3;
  Real bxup, byl, byr;
  Real bxc0, byc0, bzc0, p0, bxc1, byc1, bzc1, p1;

  /* Magnitude of perturbations */
  Real vin_turb, tim_turb;
  Real pos_y_turb, hwd_y_turb;

  /* Read input parameters */
  vin_turb = par_getd("problem", "vin_turb"); 
  tim_turb = par_getd("problem", "tim_turb");
  pos_y_turb = par_getd("problem", "pos_y_turb");
  hwd_y_turb = par_getd("problem", "hwd_y_turb");

#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  for(k=ks; k<=ke; k++) {
    for(j=js; j<=je; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->U[k][j][is-i] = pGrid->U[k][j][is];
        if(pGrid->U[k][j][is].M1 >= 0.0) {
          pGrid->U[k][j][is-i].M1 = -pGrid->U[k][j][is+i-1].M1;
        }
        /* Inflow pertubations */
        if(pGrid->time <= tim_turb){
          cc_pos(pGrid, i, j, k, &x1, &x2, &x3);
          pGrid->U[k][j][is-i].M1 = vin_turb
            *exp(-SQR(x2-pos_y_turb)/SQR(hwd_y_turb))*pGrid->U[k][j][is-i].d;
        }
      }
    }
  }

#ifdef MHD
  /* B2i */
  if(pGrid->Nx[1] > 1) ju=je+1;
  else ju=je;
  for(k=ks; k<=ke; k++) {
    for(j=js; j<=ju; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->B2i[k][j][is-i] = pGrid->B2i[k][j][is];
      }
    }
  }
  /* B1i is not set at i=is-nghost */
  for(k=ks; k<=ke; k++) {
    for(j=js; j<=je; j++) {
      for(i=1; i<=nghost-1; i++) {
        pGrid->B1i[k][j][is-i] = pGrid->B1i[k][j][is];
      }
    }
  }

  if(pGrid->Nx[2] > 1) ku=ke+1;
  else ku=ke;
  for(k=ks; k<=ku; k++) {
    for(j=js; j<=je; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->B3i[k][j][is-i] = pGrid->B3i[k][j][is];
      }
    }
  }
#endif /* MHD */
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void openbc_ox1(GridS *pGrid)
 *  \brief openbc boundary conditions, Outer x1 boundary (bc_ox1=2) */
/*----------------------------------------------------------------------------*/
/* Right boundary */

void openbc_ox1(GridS *pGrid)
{
  int ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i,j,k;
  int j0, j1;
  Real x1, x2, x3;
  Real bxup, byl, byr;
  Real bxc0, byc0, bzc0, p0, bxc1, byc1, bzc1, p1;

  /* Magnitude of perturbations */
  Real vin_turb, tim_turb;
  Real pos_y_turb, hwd_y_turb;

  /* Read input parameters */
  vin_turb = par_getd("problem", "vin_turb"); 
  tim_turb = par_getd("problem", "tim_turb");
  pos_y_turb = par_getd("problem", "pos_y_turb");
  hwd_y_turb = par_getd("problem", "hwd_y_turb");

#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  for(k=ks; k<=ke; k++) {
    for(j=js; j<=je; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->U[k][j][ie+i] = pGrid->U[k][j][ie];
        if(pGrid->U[k][j][ie].M1 <= 0.0) {
          pGrid->U[k][j][ie+i].M1 = -pGrid->U[k][j][ie-i+1].M1;
        }
        /* Inflow pertubations */
        if(pGrid->time <= tim_turb){
          cc_pos(pGrid, i, j, k, &x1, &x2, &x3);
          pGrid->U[k][j][ie+i].M1 = -vin_turb
            *exp(-SQR(x2-pos_y_turb)/SQR(hwd_y_turb))*pGrid->U[k][j][ie+i].d;
        }
      }
    }
  }
#ifdef MHD
  /* B2i */
  if(pGrid->Nx[1] > 1) ju=je+1;
  else ju=je;
  for(k=ks; k<=ke; k++) {
    for(j=js; j<=ju; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->B2i[k][j][ie+i] = pGrid->B2i[k][j][ie];
      }
    }
  }

  /* i=ie+1 is not a boundary condition for the interface field B1i */
  for(k=ks; k<=ke; k++) {
    for(j=js; j<=je; j++) {
      for(i=2; i<=nghost; i++) {
        pGrid->B1i[k][j][ie+i] = pGrid->B1i[k][j][ie];
      }
    }
  }

  if(pGrid->Nx[2] > 1) ku=ke+1;
  else ku=ke;
  for(k=ks; k<=ku; k++) {
    for(j=js; j<=je; j++) {
      for(i=1; i<=nghost; i++) {
        pGrid->B3i[k][j][ie+i] = pGrid->B3i[k][j][ie];
      }
    }
  }
#endif /* MHD */
  return;
}


/*----------------------------------------------------------------------------*/
/*! \fn Real cool_heat_corona(const Real dens, const Real Press, const Real dt)
 *  \A more accurate piecewise linear approximation (Klimchuk et al. 2008).
 Returns rate in non-dimensional value. The other local variables appear
 in SI unit.
 dE'/dt' ~ - rho'*[rho'Q'], where Q(T)' = tau_A/P_0*(n0^2)*Q(T).
 H' = rho'*[Q'(T=10^6K)].
 Therefor, the routine returns rho'*Q(T)' - H'.
 */

#ifndef BAROTROPIC
Real cool_heat_corona(const Real dens, const Real Press, const Real dt)
{
  Real coolrate=0.0;
  Real T,coolratepp,MaxdT,dT;
  Real Teq, logn, lognT;
  Real nchar, pchar, Tchar, timechar, beta0;
  Real var_char;
  Real dTemp, deltaE, deltap, deltaT, maxcool, Tmin;

  /* Get character variables
     nchar = par_gerd("problem", "nchar");
     pchar = par_gerd("problem", "pchar");
     Tchar = par_gerd("problem", "Tchar");
     timechar = par_gerd("problem", "timechar");
     beta0 = par_gerd("problem", "beta0"); */
  nchar           =    1.15330e+09;
  Tchar           =    1000000.0000000000;
  pchar           =    0.0159155;
  timechar        =    77.8469;
  beta0           =    0.01;
  Tmin            =    100.0;

  /* Compute number density for character density */
  T = Tchar*(Press/dens/(beta0/2.0));

  /* Compute the minimun Temperature*/
  Teq = Tmin;

  /* cooling rate per particle, including heating term (rho*H) */
  var_char = timechar*nchar*nchar/pchar;
  /*coolratepp = dens*var_char*(dens*Qt(T) - Qt(1.0e6));*/
  coolratepp = dens*var_char*(dens*Qt(T));

  /* Expected dT by cooling rate */
  dTemp = abs(T - Teq);
  if(T <= Teq){
    dTemp = 0.0;
  }
  deltaT = (dTemp/Tchar)*(beta0/2.0);
  deltap = deltaT*dens;
  deltaE = deltap/(5./3. - 1.0);
  maxcool = deltaE/dt;

  /* Compute maximum change in T allowed to keep T positive, and limit cooling
   * rate to this value */
  coolrate = MIN(coolratepp, maxcool);

  /* return result */
  return coolrate;
}
#endif /* BAROTROPIC */

/*----------------------------------------------------------------------------*/
/*  \brief Calculate Qt
 *  piecewise lineat approximation (Klimcuk et al. 2008)*/
/* T: dimensional variable, SI unit */
Real Qt(const Real T)
{
  Real q;
  /* first in cgs: ergs sec^-1 cm^3 */

  if(T <= pow(10.0, 4.97))
  {
    q = 1.09e-31*(pow(T, 2));
  }else if (T <= pow(10.0, 5.67))
  {
    q = 8.87e-17*(pow(T, -1.0));
  }else if (T <= pow(10.0, 6.18))
  {
    q = 1.90e-22;
  }else if (T <= pow(10.0, 6.55))
  {
    q = 3.54e-13*(pow(T, -3./2.));
  }else if (T <= pow(10.0, 6.90))
  {
    q = 3.46e-25*(pow(T, 1./3.));
  }else if (T <= pow(10.0, 7.63))
  {
    q = 5.49e-16*(pow(T, -1.0));
  }else{
    q = 1.96e-27*(pow(T, 0.5));
  }
  /* to SI unit: W m^3 */
  q = q * 1.0e-13;

  return q;
}

/*----------------------------------------------------------------------------*/
/*! \fn static Real grav_pot2(const Real x1, const Real x2, const Real x3)
 *  \brief Gravitational potential; g = 0.1
 */

static Real grav_pot2(const Real x1, const Real x2, const Real x3)
{
  Real G=6.672e-11, Msun=1.989e30, Rsun=6.693e8; /*in unit m^3kg^-1s^-2*/
  Real KB = 1.38e-23, mp = 1.6726e-27; /*J K^-1, kg m^-3*/
  Real height_bot = 2.5e6; /* 2500.0 km */
  Real GM;
  Real beta0, lchar, vchar, Tchar;
  Real height;
  Real gp;

  beta0 = par_getd("problem", "beta0");
  lchar = par_getd("problem", "Lchar");
  vchar = par_getd("problem", "vchar");
  Tchar = par_getd("problem", "Tchar");

  /* for test 
     Real g0 = 274.0;
     gp = g0*(lchar/vchar/vchar)*x2;*/

  /* continue change ..*/
  height= x2 + height_bot/lchar + Rsun/lchar;
  GM = G*Msun/(vchar*vchar*lchar);
  gp=-GM/height;

  return gp;
}

static Real grav_sun(const Real x1, const Real x2, const Real x3)
{
  Real G=6.672e-11, Msun=1.989e30, Rsun=6.693e8; /*in unit m^3kg^-1s^-2*/
  Real KB = 1.38e-23, mp = 1.6726e-27; /*J K^-1, kg m^-3*/
  Real height_bot = 2.5e6; /* 2500.0 km */
  Real GM;
  Real beta0, lchar, vchar, Tchar;
  Real height;
  Real gsun;

  beta0 = par_getd("problem", "beta0");
  lchar = par_getd("problem", "Lchar");
  vchar = par_getd("problem", "vchar");
  Tchar = par_getd("problem", "Tchar");

  /* continue change ..*/
  height= x2 + height_bot/lchar + Rsun/lchar;
  GM = G*Msun/(vchar*vchar*lchar);
  gsun=GM/height/height;

  return gsun;
}
