#include "copyright.h"
/*============================================================================*/
/*! \file flarecs.c
*  \brief Problem generator for the current sheet in solar flare.
*
* PURPOSE: Problem generator for the magnetic reconnection in flare CS.
*
* REFERENCE: Forbes & Malherbe SoPh 1991; Shen, Lin & Murphy ApJ 2011.
*
* Update:
*  20140822_2300:
*  Left and Right boundary.
*  In the left-bottom corner and right-bottom corner, equilibrium of p
*  in both x- and z- direction is required.
*/

/*============================================================================*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "defs.h"
#include "athena.h"
#include "globals.h"
#include "prototypes.h"

/*==============================================================================
* PRIVATE FUNCTION PROTOTYPES:
* void flarecs_linetied() - sets BCs on R-x2 boundary
*============================================================================*/
static Real byini(const Real x1);
void linetiedbc_ix2(GridS *pGrid);
void openbc_ox2(GridS *pGrid);
void openbc_ix1(GridS *pGrid);
void openbc_ox1(GridS *pGrid);

/*=========================== PUBLIC FUNCTIONS ===============================*/
/*----------------------------------------------------------------------------*/

/* problem:  */

void problem(DomainS *pDomain)
{
	GridS *pGrid = (pDomain->Grid);
	Prim1DS W;
	Cons1DS U1d;
	int i, is = pGrid->is, ie = pGrid->ie;
	int j, js = pGrid->js, je = pGrid->je;
	int k, ks = pGrid->ks, ke = pGrid->ke;
	Real pressure, drat, prat, rad, pa, da, x1, x2, x3, xf, yf, zf;
	Real b0 = 0.0, Bx = 0.0, by0, by;
	Real gamma, ini_width, beta0, p0, d0;
	Real bxturb, byturb, lx, ly;

	/* Magnitude of perturbations */
	Real mturb;
	/* The center of perturbations in x2 direction */
	Real x2_turb = 0.5;
	double theta;

	gamma = par_getd("problem", "gamma");
	beta0 = par_getd("problem", "beta0");
	ini_width = par_getd("problem", "ini_width");
	mturb = par_getd("problem", "mturb");

#ifdef MHD
	b0 = 1.0;
#endif

	/* setup uniform ambient medium with spherical over-pressured region */

	W.Vx = 0.0;
	W.Vy = 0.0;
	W.Vz = 0.0;
#ifdef MHD
	Bx   = 0.0;
	W.By = 0.0;
	W.Bz = 0.0;
#endif

	for (k = ks; k <= ke; k++)
	{
		for (j = js; j <= je; j++)
		{
			for (i = is; i <= ie; i++)
			{
				/* axis */
				cc_pos(pGrid, i, j, k, &x1, &x2, &x3);

				/* By */
				W.By = byini(x1);
				by0 = byini(x1);

				/* Pressure */
				W.P = 0.5 + beta0 / 2.0 - 0.5 * (W.By)*(W.By);
				p0 = 0.5 + beta0 / 2.0 - 0.5 * (W.By)*(W.By);
				/* Density */
				W.d = W.P/(beta0/2.0);
				d0 = p0/(beta0/2.0);

				/* transfer to cons structure */
				U1d = Prim1D_to_Cons1D(&(W), &Bx);

				pGrid->U[k][j][i].d = d0;
				pGrid->U[k][j][i].M1 = 0.0;
				pGrid->U[k][j][i].M2 = 0.0;
				pGrid->U[k][j][i].M3 = 0.0;

				pGrid->B1i[k][j][i] = Bx;
				pGrid->B2i[k][j][i] = by0;
				pGrid->B3i[k][j][i] = 0.0;

				/* initial setting for magnetic field on the boundary */
				if (i == ie && ie > is) pGrid->B1i[k][j][i + 1] = Bx;
				if (j == je && je > js) pGrid->B2i[k][j + 1][i] = by0;
				if (k == ke && ke > ks) pGrid->B3i[k + 1][j][i] = 0.0;

				/* magnetic perturbations
				lx = 2.0;
				ly = 2.0;
				if (x1 >= -ini_width && x1 <= ini_width) {
				byturb = -mturb*PI/ly*cos((2.0*PI*x1)/lx)*sin((PI*x2)/ly);
				bxturb =mturb*2.0*PI/lx*sin((2.0*PI*x1)/lx)*cos((PI*x2)/ly);

				pGrid->B1i[k][j][i] = pGrid->B1i[k][j][i] + bxturb;
				pGrid->B2i[k][j][i] = pGrid->B2i[k][j][i] + byturb;
				}
				*/
				/* monmentum perturbations */
				if (x2 >= 1.0)
				{
					pGrid->U[k][j][i].M1 = -mturb*x1;
				}
				else
				{
					pGrid->U[k][j][i].M1 = -mturb*x1*x2;
				}
			}
		}
	}
	/* cell-center magnetic field */
	for (k = ks; k <= ke; k++)
	{
		for (j = js; j <= je; j++)
		{
			for (i = is; i <= ie; i++)
			{
				pGrid->U[k][j][i].B1c = 0.5*(pGrid->B1i[k][j][i  ] +
					pGrid->B1i[k][j][i+1]);
				pGrid->U[k][j][i].B2c = 0.5*(pGrid->B2i[k][j  ][i] +
					pGrid->B2i[k][j+1][i]);
				pGrid->U[k][j][i].B3c = 0.0;
			}
		}
	}
	/* total energy*/
	for (k = ks; k <= ke; k++)
	{
		for (j = js; j <= je; j++)
		{
			for (i = is; i <= ie; i++)
			{
				p0 = 0.5 + beta0 / 2.0
					- 0.5 * (SQR(pGrid->U[k][j][i].B1c)
					+ SQR(pGrid->U[k][j][i].B2c)
					+ SQR(pGrid->U[k][j][i].B3c));

				pGrid->U[k][j][i].E = p0/(gamma - 1.0)
					+ 0.5*(SQR(pGrid->U[k][j][i].B1c)
					+ SQR(pGrid->U[k][j][i].B2c)
					+ SQR(pGrid->U[k][j][i].B3c))
					+ 0.5*(SQR(pGrid->U[k][j][i].M1)
					+ SQR(pGrid->U[k][j][i].M2)
					+ SQR(pGrid->U[k][j][i].M3))/pGrid->U[k][j][i].d;
			}
		}
	}
	/* Set resistivity */
#ifdef RESISTIVITY
	eta_Ohm = par_getd("problem", "eta_Ohm");
	Q_AD = par_getd("problem", "Q_AD");
	Q_Hall = 0.0;
	d_ind = 0.0;
#endif

	/* Set thermel conduction*/
#ifdef THERMAL_CONDUCTION    
	kappa_aniso = par_getd_def("problem","kappa_aniso",0.0);
	kappa_iso = par_getd_def("problem","kappa_iso",0.0);
#endif

	/* Set optical thin radiation cooling and corona heating function */
	/*CoolingFunc = rcool_cheat;*/


	/* det optical thin radiation cooling and corona heating function */
	/*CoolingFunc = rcool_cheat;*/

	/* Set boundary value function pointers for linetied boundary*/
	bvals_mhd_fun(pDomain, left_x1,  openbc_ix1);
	bvals_mhd_fun(pDomain, right_x1, openbc_ox1);

	bvals_mhd_fun(pDomain, left_x2,  linetiedbc_ix2);
	bvals_mhd_fun(pDomain, right_x2, openbc_ox2);


}

/*==============================================================================
* PROBLEM USER FUNCTIONS:
* problem_write_restart() - writes problem-specific user data to restart files
* problem_read_restart()  - reads problem-specific user data from restart files
* get_usr_expr()          - sets pointer to expression for special output data
* get_usr_out_fun()       - returns a user defined output function pointer
* get_usr_par_prop()      - returns a user defined particle selection function
* Userwork_in_loop        - problem specific work IN     main loop
* Userwork_after_loop     - problem specific work AFTER  main loop
*----------------------------------------------------------------------------*/

void problem_write_restart(MeshS *pM, FILE *fp)
{
	return;
}

void problem_read_restart(MeshS *pM, FILE *fp)
{
    int nl, nd;
    /* gamma and beta0 are used in line-tied boundary */
    Real gamma, beta0, ini_width;
    gamma = par_getd("problem", "gamma");
    beta0 = par_getd("problem", "beta0");
    /* Set resistivity */
#ifdef RESISTIVITY
    eta_Ohm = par_getd("problem", "eta_Ohm");
    Q_AD = par_getd("problem", "Q_AD");
    Q_Hall = 0.0;
    d_ind = 0.0;
#endif
    /* Set thermel conduction*/
#ifdef THERMAL_CONDUCTION
    kappa_aniso = par_getd_def("problem","kappa_aniso",0.0);
    kappa_iso = par_getd_def("problem","kappa_iso",0.0);
#endif
    /* Set boundary value functions */
    for (nl=0; nl<(pM->NLevels); nl++){
        for (nd=0; nd<(pM->DomainsPerLevel[nl]); nd++){
            bvals_mhd_fun(&(pM->Domain[nl][nd]), left_x1,  openbc_ix1);
            bvals_mhd_fun(&(pM->Domain[nl][nd]), right_x1, openbc_ox1);
            bvals_mhd_fun(&(pM->Domain[nl][nd]), left_x2,  linetiedbc_ix2);
            bvals_mhd_fun(&(pM->Domain[nl][nd]), right_x2, openbc_ox2);
        }
    }
	return;
}

ConsFun_t get_usr_expr(const char *expr)
{
	return NULL;
}

VOutFun_t get_usr_out_fun(const char *name)
{
	return NULL;
}

#ifdef RESISTIVITY

void get_eta_user(GridS *pG, int i, int j, int k,
	Real *eta_O, Real *eta_H, Real *eta_A)
{

	*eta_O = 0.0;
	*eta_H = 0.0;
	*eta_A = 0.0;

	return;
}
#endif

void Userwork_in_loop(MeshS *pM)
{
}

void Userwork_after_loop(MeshS *pM)
{
}

/*=========================== PRIVATE FUNCTIONS ==============================*/
/*----------------------------------------------------------------------------*/
/*  \funtion of By for initial conditions */
static Real byini(const Real x1)
{
	Real by0;
	Real b0;
	Real ini_width;
	int mode_cs;

	/* initialize parameters */
	ini_width = par_getd("problem", "ini_width");
	mode_cs = par_getd("problem", "mode_cs");
	b0 = 1.0;

	/* case 1: Sin function */
	if (mode_cs == 0)
	{
		if (x1 >  ini_width) by0 = b0;
		if (x1 <= ini_width && x1 >= -ini_width) by0 =
			b0 * sin(PI * x1 / (2.0 * ini_width));
		if (x1 < -ini_width) by0 = -b0;
	}

	/* case 2: Harris sheet */
	if (mode_cs == 1)
	{
		by0 = b0*tanh(x1/ini_width);
	}

	return by0;
}

/*----------------------------------------------------------------------------*/

/*! \fn void linetiedbc_ix2(GridS *pGrid)
*  \brief Sets boundary condition at the bottom.
*/
/*  ix2, line-tied, bottom */
void linetiedbc_ix2(GridS *pGrid)
{
    int is = pGrid->is, ie = pGrid->ie;
    int js = pGrid->js;
    int ks = pGrid->ks, ke = pGrid->ke;
    int i, j, k;
    int kl, ku;
    Real x1, x2, x3, xf, yf, zf;
    Real bxup, byl, byr, bxji, byji, bzji, byjs, byjnghost;
    Real pjsi, pjci;
    Real pini, dini;
    Real gamma, beta0, ini_width;
    
    gamma = par_getd("problem", "gamma");
    beta0 = par_getd("problem", "beta0");
    ini_width = par_getd("problem", "ini_width");
    
    if (pGrid->Nx[2] > 1)
    {
        kl = pGrid->ks - nghost;
        ku = pGrid->ke + nghost;
    }
    else
    {
        kl = pGrid->ks;
        ku = pGrid->ke;
    }
    
    /* (0) set all */
    for (k=ks; k<=ke; k++){
        for (j=1; j<=nghost; j++){
            for (i=is-nghost; i<=ie+nghost; i++){
                pGrid->U[k][js-j][i] = pGrid->U[k][js][i];
            }
        }
    }
    
    /* (1) density */
    for (k=ks; k<=ke; k++){
        for (j=1; j<=nghost; j++){
            for (i=is-nghost; i<=ie+nghost; i++){
                /*pGrid->U[k][js-j][i].d = pGrid->U[k][js][i].d;*/
                pjsi = (gamma - 1.0)
                *(pGrid->U[k][js][i].E
                  - 0.5*(SQR(pGrid->U[k][js][i].B1c)
                         + SQR(pGrid->U[k][js][i].B2c)
                         + SQR(pGrid->U[k][js][i].B3c))
                  - 0.5*(SQR(pGrid->U[k][js][i].M1)
                         + SQR(pGrid->U[k][js][i].M2)
                         + SQR(pGrid->U[k][js][i].M3))/pGrid->U[k][js][i].d);
                
                
				/* Initial Pressure and density */
                cc_pos(pGrid, i, js-j, k, &x1, &x2, &x3);
				pini = 0.5 + beta0 / 2.0 - 0.5 * byini(x1)*byini(x1);
                dini = pini/(beta0/2.0);
                /*pGrid->U[k][js-j][i].d = pow(pjsi, 3./5.);*/
                pGrid->U[k][js-j][i].d = dini*(pow(pjsi/pini, 3./5.));
            }
        }
    }
    
    /* (2-4) momentum density in 1,2,3 directions*/
    for (k=ks; k<=ke; k++){
        for (j=1; j<=nghost; j++){
            for (i=is-nghost; i<=ie+nghost; i++){
                pGrid->U[k][js-j][i].M1 = 0.0;
                pGrid->U[k][js-j][i].M2 = 0.0;
                pGrid->U[k][js-j][i].M3 = 0.0;
            }
        }
    }
    
    /* (5) total energy density.*/
    for (k=ks; k<=ke; k++){
        for (j=1; j<=nghost; j++){
            for (i=is-nghost; i<=ie+nghost; i++){
                pjsi = (gamma - 1.0)
                *(pGrid->U[k][js][i].E
                  - 0.5*(SQR(pGrid->U[k][js][i].B1c)
                         + SQR(pGrid->U[k][js][i].B2c)
                         + SQR(pGrid->U[k][js][i].B3c))
                  - 0.5*(SQR(pGrid->U[k][js][i].M1)
                         + SQR(pGrid->U[k][js][i].M2)
                         + SQR(pGrid->U[k][js][i].M3))/pGrid->U[k][js][i].d);
                
                bxji = pGrid->U[k][js-j][i].B1c;
                
                byji = pGrid->U[k][js-j][i].B2c;
                
                bzji = pGrid->U[k][js-j][i].B3c;
                
                /* get gas pressure */
                pjci = pjsi;
                
                /*pjci = (beta0/2.0)*pGrid->U[k][js-j][i].d;*/
                
                /* initial values
                cc_pos(pGrid, i, j, k, &x1, &x2, &x3);
                pjci = 0.5+(beta0/2.0)-0.5*byini(x1)*byini(x1);*/
                
                pGrid->U[k][js-j][i].E = pjci/(gamma - 1.0)
                + 0.5*(SQR(bxji) + SQR(byji) +SQR(bzji))
                + 0.5*(SQR(pGrid->U[k][js-j][i].M1)
                     + SQR(pGrid->U[k][js-j][i].M2)
                     + SQR(pGrid->U[k][js-j][i].M3))/pGrid->U[k][js-j][i].d;
            }
        }
    }
    
    /* (0.1) B1i (Bxi)
     B1i can not be defined at is-nghost based on Dleta curl B == 0.
     B1i is not set at i = is - nghost */
    for (k = ks; k <= ke; k++){
        for (j = 1; j <= nghost; j++){
            for (i = is-(nghost-1); i <= ie + nghost; i++){
                bxup   = pGrid->B1i[k][js-j+1][i];
                cc_pos(pGrid, i, js-j, k, &x1, &x2, &x3);
                byr    = byini(x1);
                cc_pos(pGrid, i-1, js-j, k, &x1, &x2, &x3);
                byl    = byini(x1);
                
                pGrid->B1i[k][js-j][i] = bxup-(pGrid->dx2/pGrid->dx1)*(byr-byl);
            }
        }
        /* js-2 -> js-nghost */
        for (j = 2; j <= nghost; j++){
            for (i = is-(nghost-1); i <= ie + nghost; i++){
                pGrid->B1i[k][js-j][i] = pGrid->B1i[k][js-1][i];
            }
        }
    }
    /* (0.2) B2i (Byi) is not set at j = js - nghost */
    for (k = ks; k <= ke; k++){
        for (j = 1; j <= (nghost-1); j++){
            for (i = is-nghost; i <= ie + nghost; i++){
                cc_pos(pGrid, i, js-j, k, &x1, &x2, &x3);
                pGrid->B2i[k][js-j][i] = byini(x1);
            }
        }
    }
    /* (0.3) B3i */
    for (k = ks; k <= ke; k++){
        for (j = 1; j <= nghost; j++){
            for (i = is-nghost; i <= ie+nghost; i++){
                pGrid->B3i[k][js-j][i] = 0.0;
            }
        }
    }
    
    return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void openbc_ox2(GridS *pGrid)
*  \brief open boundary conditions, Outer x2 boundary (bc_ox2=2) */
/*----------------------------------------------------------------------------*/
/*  ox2, top boundary */

void openbc_ox2(GridS *pGrid)
{
	int is = pGrid->is, ie = pGrid->ie;
	int je = pGrid->je;
	int ks = pGrid->ks, ke = pGrid->ke;
	int i,j,k;
#ifdef MHD
	int ku; /* k-upper */
#endif
	Real dx1, dx2;
	Real gamma;
	Real p0, b10, b20, b30, b11, b21, b31;

	gamma = par_getd("problem", "gamma");

	for (k=ks; k<=ke; k++)
	{
		for (j=1; j<=nghost; j++)
		{
			for (i=is-nghost; i<=ie+nghost; i++)
			{
				pGrid->U[k][je+j][i] = pGrid->U[k][je][i];
                /* limit inflow */
				if (pGrid->U[k][je][i].M2 <= 0.0)
				{
					/*pGrid->U[k][je+j][i].M2 = -pGrid->U[k][je-j+1][i].M2;*/
                    pGrid->U[k][je+j][i].M2 = 0.0;
				}
                /* limit tension force */
                if (pGrid->U[k][je][i].B1c <= 0.0)
                {
                    /*pGrid->U[k][je+j][i].B1c = -pGrid->U[k][je][i].B1c;*/
                    pGrid->U[k][je+j][i].B1c = 0.0;
                }
                /* limit b1c
                pGrid->U[k][je+j][i].B1c = 0.0;*/
			}
		}
	}
#ifdef MHD
	/* B1i is not set at i=is-nghost */
	for (k=ks; k<=ke; k++)
	{
		for (j=1; j<=nghost; j++)
		{
			for (i=is-(nghost-1); i<=ie+nghost; i++)
			{
				pGrid->B1i[k][je+j][i] = pGrid->B1i[k][je][i];
                /* limit tension force */
                if (pGrid->B1i[k][je][i] <= 0.0)
                {
                    /*pGrid->B1i[k][je+j][i] = -pGrid->B1i[k][je][i];*/
                    pGrid->B1i[k][je+j][i] = 0.0;
                }
			}
		}
	}

	/* j=je+1 is not a boundary condition for the interface field B2i */
	for (k=ks; k<=ke; k++)
	{
		for (j=2; j<=nghost; j++)
		{
			for (i=is-nghost+1; i<=ie+nghost-1; i++)
			{
				/*pGrid->B2i[k][je+j][i] = pGrid->B2i[k][je][i];*/
                pGrid->B2i[k][je+j][i] = pGrid->B2i[k][je+j-1][i] - (pGrid->dx2/pGrid->dx1)
                *(pGrid->B1i[k][je+j-1][i+1]-pGrid->B1i[k][je+j-1][i]);
			}
            /* BCs at is-nghost & ie+nghost */
            pGrid->B2i[k][je+j][is-nghost] = pGrid->B2i[k][je][is-nghost];
            pGrid->B2i[k][je+j][ie+nghost] = pGrid->B2i[k][je][ie+nghost];
		}
	}

	if (pGrid->Nx[2] > 1) ku=ke+1;
	else ku=ke;
	for (k=ks; k<=ku; k++)
	{
		for (j=1; j<=nghost; j++)
		{
			for (i=is-nghost; i<=ie+nghost; i++)
			{
				pGrid->B3i[k][je+j][i] = pGrid->B3i[k][je][i];
			}
		}
	}
#endif /* MHD */
	return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void openbc_ix1(GridS *pGrid)
*  \brief open boundary condition, Inner x1 boundary (bc_ix1=2) */
/*----------------------------------------------------------------------------*/
/*  ix1, Left boundary */

void openbc_ix1(GridS *pGrid)
{
	int is = pGrid->is;
	int js = pGrid->js, je = pGrid->je;
	int ks = pGrid->ks, ke = pGrid->ke;
	int i,j,k;
	int j0, j1;
	Real x1, x2, x3;
	Real bxup, byl, byr;
	Real bxc0, byc0, bzc0, p0, bxc1, byc1, bzc1, p1;
	Real gamma, ini_width;

	gamma = par_getd("problem", "gamma");
	ini_width = par_getd("problem", "ini_width");

#ifdef MHD
	int ju, ku; /* j-upper, k-upper */
#endif

	for (k=ks; k<=ke; k++)
	{
		for (j=js; j<=je; j++)
		{
			for (i=1; i<=nghost; i++)
			{
				pGrid->U[k][j][is-i] = pGrid->U[k][j][is];
				if (pGrid->U[k][j][is].M1 >= 0.0)
				{
					/*pGrid->U[k][j][is-i].M1 = -pGrid->U[k][j][is+i-1].M1;*/
                    pGrid->U[k][j][is-i].M1 = 0.0;
				}
			}
		}
	}

#ifdef MHD
	/* B2i */
	if (pGrid->Nx[1] > 1) ju=je+1;
	else ju=je;
	for (k=ks; k<=ke; k++)
	{
		for (j=js; j<=ju; j++)
		{
			for (i=1; i<=nghost; i++)
			{
				pGrid->B2i[k][j][is-i] = pGrid->B2i[k][j][is];
			}
		}
	}
	/* B1i is not set at i=is-nghost */
	for (k=ks; k<=ke; k++)
	{
		for (j=js; j<=je; j++)
		{
			for (i=1; i<=nghost-1; i++)
			{
				/*pGrid->B1i[k][j][is-i] = pGrid->B1i[k][j][is];*/
                pGrid->B1i[k][j][is-i] = pGrid->B1i[k][j][is-i+1]
                +(pGrid->dx1/pGrid->dx2)
                *(pGrid->B2i[k][j+1][is-i] - pGrid->B2i[k][j][is-i]);
			}
		}
	}

	if (pGrid->Nx[2] > 1) ku=ke+1;
	else ku=ke;
	for (k=ks; k<=ku; k++)
	{
		for (j=js; j<=je; j++)
		{
			for (i=1; i<=nghost; i++)
			{
				pGrid->B3i[k][j][is-i] = pGrid->B3i[k][j][is];
			}
		}
	}
#endif /* MHD */
	return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void openbc_ox1(GridS *pGrid)
*  \brief openbc boundary conditions, Outer x1 boundary (bc_ox1=2) */
/*----------------------------------------------------------------------------*/
/* Right boundary */

void openbc_ox1(GridS *pGrid)
{
	int ie = pGrid->ie;
	int js = pGrid->js, je = pGrid->je;
	int ks = pGrid->ks, ke = pGrid->ke;
	int i,j,k;
	int j0, j1;
	Real x1, x2, x3;
	Real bxup, byl, byr;
	Real bxc0, byc0, bzc0, p0, bxc1, byc1, bzc1, p1;
	Real gamma, ini_width;

	gamma = par_getd("problem", "gamma");
	ini_width = par_getd("problem", "ini_width");
#ifdef MHD
	int ju, ku; /* j-upper, k-upper */
#endif

	for (k=ks; k<=ke; k++)
	{
		for (j=js; j<=je; j++)
		{
			for (i=1; i<=nghost; i++)
			{
				pGrid->U[k][j][ie+i] = pGrid->U[k][j][ie];
				if (pGrid->U[k][j][ie].M1 <= 0.0)
				{
					/*pGrid->U[k][j][ie+i].M1 = -pGrid->U[k][j][ie-i+1].M1;*/
                    pGrid->U[k][j][ie+i].M1 = 0.0;
				}
			}
		}
	}
#ifdef MHD
	/* B2i */
	if (pGrid->Nx[1] > 1) ju=je+1;
	else ju=je;
	for (k=ks; k<=ke; k++)
	{
		for (j=js; j<=ju; j++)
		{
			for (i=1; i<=nghost; i++)
			{
				pGrid->B2i[k][j][ie+i] = pGrid->B2i[k][j][ie];
			}
		}
	}

	/* i=ie+1 is not a boundary condition for the interface field B1i */
	for (k=ks; k<=ke; k++)
	{
		for (j=js; j<=je; j++)
		{
			for (i=2; i<=nghost; i++)
			{
				/*pGrid->B1i[k][j][ie+i] = pGrid->B1i[k][j][ie];*/
                pGrid->B1i[k][j][ie+i] = pGrid->B1i[k][j][ie+i-1]
                -(pGrid->dx1/pGrid->dx2)
                *(pGrid->B2i[k][j+1][ie+i-1] - pGrid->B2i[k][j][ie+i-1]);
			}
		}
	}

	if (pGrid->Nx[2] > 1) ku=ke+1;
	else ku=ke;
	for (k=ks; k<=ku; k++)
	{
		for (j=js; j<=je; j++)
		{
			for (i=1; i<=nghost; i++)
			{
				pGrid->B3i[k][j][ie+i] = pGrid->B3i[k][j][ie];
			}
		}
	}
#endif /* MHD */
	return;
}
