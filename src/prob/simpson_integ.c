//
//  Name:
//    simpson_integ.c
//  Purpose:
//    Apply Simpson's rule and perform integration in 1D.
//  Created by:
//    Chengcai on 3/30/18.
//

/* ----------------------------------------------------------------------------
 * Including functions: 
 * --------------------------------------------------------------------------*/
Real adaptiveSimpsons(Real (*f)(Real),   // ptr to function
                        Real a, Real b, // interval [a,b]
                        Real epsilon,  // error tolerance
                        int maxRecursionDepth);
Real adaptiveSimpsonsAux(Real (*f)(Real), Real a, Real b,
                           Real epsilon,
                           Real S, Real fa, Real fb, Real fc, int bottom);

/* ----------------------------------------------------------------------------
 * Adaptive Simpson's Rule
 * --------------------------------------------------------------------------*/
Real adaptiveSimpsons(Real (*f)(Real),   // ptr to function
    Real a, Real b, // interval [a,b]
    Real epsilon,  // error tolerance
    int maxRecursionDepth) {   // recursion cap
  Real c = (a + b)/2, h = b - a;
  Real fa = f(a), fb = f(b), fc = f(c);
  Real S = (h/6)*(fa + 4*fc + fb);
  return adaptiveSimpsonsAux(f, a, b, epsilon, S, fa, fb, fc, maxRecursionDepth);
}

/* ----------------------------------------------------------------------------
 * Recursive auxiliary function for adaptiveSimpsons() function below
 * --------------------------------------------------------------------------*/
Real adaptiveSimpsonsAux(Real (*f)(Real), Real a, Real b,
    Real epsilon,
    Real S, Real fa, Real fb, Real fc, int bottom) {
  Real c = (a + b)/2, h = b - a;
  Real d = (a + c)/2, e = (c + b)/2;
  Real fd = f(d), fe = f(e);
  Real Sleft = (h/12)*(fa + 4*fd + fc);
  Real Sright = (h/12)*(fc + 4*fe + fb);
  Real S2 = Sleft + Sright;
  if (bottom <= 0 || fabs(S2 - S) <= 15*epsilon)   // magic 15 comes from error analysis
    return S2 + (S2 - S)/15;
  return adaptiveSimpsonsAux(f, a, c, epsilon/2, Sleft,  fa, fc, fd, bottom-1) +
    adaptiveSimpsonsAux(f, c, b, epsilon/2, Sright, fc, fb, fe, bottom-1);
}
