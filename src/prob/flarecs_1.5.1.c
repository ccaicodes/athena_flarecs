#include "copyright.h"
/*============================================================================*/
/*! \file flarecs.c
 *  \brief Problem generator for the current sheet in solar flare.
 *
 * PURPOSE: Problem generator for the magnetic reconnection in flare CS.
 *
 * REFERENCE: Forbes & Malherbe SoPh 1991; Shen, Lin & Murphy ApJ 2011.
 *
 */

/*============================================================================*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defs.h"
#include "athena.h"
#include "globals.h"
#include "prototypes.h"
#include "./prob/simpson_integ.c"

/*==============================================================================
 * PRIVATE FUNCTION PROTOTYPES:
 * void flarecs_linetied() - sets BCs on R-x2 boundary
 *============================================================================*/
/* Magnetic field*/
static Real bxini(const Real x1, const Real x2);
static Real byini(const Real x1, const Real x2);
static Real bzini(const Real x1, const Real x2);
static Real azini(const Real x1, const Real x2);

/* Pressure*/
static Real pres_blance_cs(const Real x1);
static Real presini_isote(const Real x2, const Real y0,
                          const Real p0, const Real te0);
static Real presini_integral(const Real x2, const Real y0, const Real p0);
Real my_integ_py(Real y);

/* Density and Temperature */
static Real densini(const Real x2);
static Real teini(const Real x2);

/* Radiative loss function */
static Real Qt(const Real T);
Real opticalthinloss_corona(const Real dens, const Real Press, const Real dt);

/* Gravity */
static Real gsun_potential(const Real x1, const Real x2, const Real x3);
static Real gsun_y(const Real x2);

/* Boundary conditions*/
static void openbc_ox2(GridS *pGrid);
static void openbc_ix1(GridS *pGrid);
static void openbc_ox1(GridS *pGrid);
static void symmbc_ox1(GridS *pGrid);
static void linetiedbc_ix2(GridS *pGrid);

/*============================================================================*/
/* Normalization parameters */
Real Lchar, Bchar, Nechar, Rhochar, Pchar, Timechar, Tchar, Vchar;

/* Initial current sheet */
int cs_mode;
Real cs_width;
Real xc_symm;

/* a. input parameters: pressure and temperature in chromosphere and corona */
Real Te_corona, Te_photos;

/* b. input parameters: height and width for the TR region */
Real Height_TR, Width_TR;

/* c. The height of the pgas_c in corona */
Real pgas_c, posi_c;

/* d. 3d array to store initial pressure, density and temperature */
Real ***pgasini, ***rhoini, ***tempini;

/* e. Grid size*/
Real pgddx, pgddy;

/* Constant parameters */
Real Kb = 1.38e-23, Mp = 1.6726e-27, Mu0 = 1.25663706144e-6, LnA = 30.0;
Real gravitational_const = 6.672e-11; /* (N M^2 kg^-2)*/
Real solarmass = 1.99e+30;            /* (kg) */
Real solarradius = 6.96e+8;           /* (m) */

/* PSI perturbation */
Real psi_pert, lx_psipert, ly_psipert, yc_psipert, b0 = 1.0, pi = 3.141592653589793;

/* Define the temperature range in computing kappa(T) */
Real temp_low, temp_high;

/* Artificialy reset cooling and conduction in lower temperature region */
Real Tc_low;
int sw_lowt_cool; // use for decreasing the cooling and increacing thermal
                  // conduction once tempreture is lower than Tc_low to keep
                  // Kappa(T)*Q(T) constant. (Reeves et al.2010 ApJ)
int sw_explicit_conduct_dt;

/*=========================== PUBLIC FUNCTIONS ===============================*/
/*----------------------------------------------------------------------------*/

/* problem:  */

void problem(DomainS *pDomain)
{
  GridS *pGrid = (pDomain->Grid);
  int i, is = pGrid->is, ie = pGrid->ie;
  int j, js = pGrid->js, je = pGrid->je;
  int k, ks = pGrid->ks, ke = pGrid->ke;
  int n1 = ie - is + 1 + 2 * nghost;
  int n2 = je - js + 1 + 2 * nghost;
  int n3 = ke - ks + 1 + 2 * nghost;

  Real x1, x2, x3;
  Real pgas;

  /* Inilize pgasini, rhoini, and tempini */
  if ((pgasini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for pgasini \n");
  }
  if ((rhoini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for rhoini \n");
  }
  if ((tempini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for tempini \n");
  }

  /* Termal conduction & viscosity */
  Real k_spitzer, kappa_v;
  Real nud, nu0;

  /* Magnitude of perturbations */
  Real pi;
  Real vin_turb;
  Real Lx, Ly, delta_bx, delta_by;
  Real pos_x_turb, pos_y_turb, hwd_x_turb, hwd_y_turb;

  /* Dence bottom boundary */
  Real htr, wtr, chr_rho, cor_rho;

  /* Read input parameters */
  cs_width = par_getd("problem", "cs_width");
  cs_mode = par_getd("problem", "cs_mode");

  Lchar = par_getd("problem", "Lchar");
  Bchar = par_getd("problem", "Bchar");
  Nechar = par_getd("problem", "Nechar");

  Pchar = Bchar * Bchar / Mu0;
  Rhochar = Nechar * Mp;
  Tchar = Pchar / (Nechar * 2. * Kb); /*total number is 2*ne for full ionization plasma */
  Vchar = Bchar / sqrt(Mu0 * Rhochar);
  Timechar = Lchar / Vchar;

  /* Print*/
  int myid;
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  if (myid == 0)
  {
    printf("Bchar=%10.3e (T)\n", Bchar);
    printf("nechar=%10.3e (cm^-3), Rhochar=%10.3e (kg/m^3)\n",
           Nechar / 1.0e6, Rhochar);
    printf("Pchar=%10.3e (Pa)\n", Pchar);
    printf("Tchar=%10.3e (K)\n", Tchar);
    printf("Vchar=%10.3e (km/s)\n", Vchar / 1.0e3);
    printf("Lchar=%10.3e (km)\n", Lchar / 1.0e3);
    printf("Timechar=%10.3e (s)\n", Timechar);
  }

  /* Read perturbation inputs*/
  vin_turb = par_getd("problem", "vin_turb");
  pos_x_turb = par_getd("problem", "pos_x_turb");
  pos_y_turb = par_getd("problem", "pos_y_turb");
  hwd_x_turb = par_getd("problem", "hwd_x_turb");
  hwd_y_turb = par_getd("problem", "hwd_y_turb");

  psi_pert = par_getd("problem", "psi_pert");
  lx_psipert = par_getd("problem", "lx_psi");
  ly_psipert = par_getd("problem", "ly_psi");
  yc_psipert = par_getd("problem", "yc_psi");

  /* Read input arguments for initial pressure */
  pgas_c = par_getd("problem", "pgas_corona"); /* in non-dimensional */
  posi_c = par_getd("problem", "posi_corona"); /* in non-dimensional */
  Te_corona = par_getd("problem", "Te_corona");
  Te_photos = par_getd("problem", "Te_photos");
  Height_TR = par_getd("problem", "Height_TR");
  Width_TR = par_getd("problem", "Width_TR");
  htr = Height_TR / Lchar;
  wtr = Width_TR / Lchar;

  /* Read temperature floor for kappa calculations */
  temp_low = par_getd("problem", "Temperature_low");
  temp_high = par_getd("problem", "Temperature_high");

  /* Read parameters for cooling */
  Tc_low = par_getd("problem", "Tc_low");
  sw_lowt_cool = par_getd("problem", "sw_lowt_cool");
  sw_explicit_conduct_dt = par_getd("problem", "sw_explicit_conduct_dt"); // if it equals to 1, then new_diff_dt will calculate updated time-step according to the explicit diffusion schema.

  /* For right symmetry cases: set the symmetry center at the cell center */
  xc_symm = 0.5*pGrid->dx1;

  /* Grid size */
  pgddx = pGrid->dx1;
  pgddy = pGrid->dx2;

  /* Compute initial pressure, temperature and density */
  Real pgas_backuniform, te_backuniform;
  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost; j <= je + nghost; j++)
    {

      // Compute pgas on the background
      cc_pos(pGrid, is, j, k, &x1, &x2, &x3);
      te_backuniform = teini(x2);
      pgas_backuniform = presini_integral(x2, posi_c, pgas_c);

      // Set P, rho, and Te including the initial current sheet
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        cc_pos(pGrid, i, j, k, &x1, &x2, &x3);
        pgasini[k][j][i] = pgas_backuniform; // P is uniform
        //pgasini[k][j][i] = pgas_backuniform + pres_blance_cs(x1); //P is not uniform
        rhoini[k][j][i] = pgas_backuniform / te_backuniform;   // rho is uniform
        tempini[k][j][i] = pgasini[k][j][i] / rhoini[k][j][i]; //
      }
    }
  }

  /* All variables */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        /* axis */
        cc_pos(pGrid, i, j, k, &x1, &x2, &x3);

        /* Pressure */
        pgas = pgasini[k][j][i];

        /* density */
        pGrid->U[k][j][i].d = rhoini[k][j][i];

        /* monentum */
        pGrid->U[k][j][i].M1 = 0.0;
        pGrid->U[k][j][i].M2 = 0.0;
        pGrid->U[k][j][i].M3 = 0.0;

        /* magnetic field */
        pGrid->B1i[k][j][i] = bxini(x1 - 0.5 * pGrid->dx1, x2);
        pGrid->B2i[k][j][i] = byini(x1, x2 - 0.5 * pGrid->dx2);
        pGrid->B3i[k][j][i] = bzini(x1, x2);

        /* initial setting for magnetic field on the boundary */
        if (i == ie && ie > is)
        {
          cc_pos(pGrid, i + 1, j, k, &x1, &x2, &x3);
          pGrid->B1i[k][j][i + 1] = bxini(x1 - 0.5 * pGrid->dx1, x2);
        }
        if (j == je && je > js)
        {
          cc_pos(pGrid, i, j + 1, k, &x1, &x2, &x3);
          pGrid->B2i[k][j + 1][i] = byini(x1, x2 - 0.5 * pGrid->dx2);
        }
        if (k == ke && ke > ks)
        {
          cc_pos(pGrid, i, j, k + 1, &x1, &x2, &x3);
          pGrid->B3i[k + 1][j][i] = bzini(x1, x2);
        }

        /* monmentum perturbations
           pGrid->U[k][j][i].M1 = -vin_turb*x1
         *exp(-SQR(x2-pos_y_turb)/SQR(hwd_y_turb))*pGrid->U[k][j][i].d;*/
        if (x2 <= 1.0)
        {
          pGrid->U[k][j][i].M1 = -(x1 - xc_symm) * vin_turb * x2 * pGrid->U[k][j][i].d;
        }
        else
        {
          pGrid->U[k][j][i].M1 = -(x1 - xc_symm) * vin_turb * pGrid->U[k][j][i].d;
        }
      }
    }
  }

  /* cell-center magnetic field */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        pGrid->U[k][j][i].B1c = 0.5 * (pGrid->B1i[k][j][i] +
                                       pGrid->B1i[k][j][i + 1]);
        pGrid->U[k][j][i].B2c = 0.5 * (pGrid->B2i[k][j][i] +
                                       pGrid->B2i[k][j + 1][i]);
        pGrid->U[k][j][i].B3c = pGrid->B3i[k][j][i];
      }
    }
  }

  /* total energy*/
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        cc_pos(pGrid, i, j, k, &x1, &x2, &x3);
        pgas = pgasini[k][j][i];

        pGrid->U[k][j][i].E = pgas / (Gamma_1) + 0.5 * (SQR(pGrid->U[k][j][i].B1c) + SQR(pGrid->U[k][j][i].B2c) + SQR(pGrid->U[k][j][i].B3c)) + 0.5 * (SQR(pGrid->U[k][j][i].M1) + SQR(pGrid->U[k][j][i].M2) + SQR(pGrid->U[k][j][i].M3)) / pGrid->U[k][j][i].d;
      }
    }
  }

  /* Set resistivity */
#ifdef RESISTIVITY
  eta_Ohm = par_getd("problem", "eta_Ohm");
  Q_AD = par_getd("problem", "Q_AD");
  Q_Hall = 0.0;
  d_ind = 0.0;
#endif

  /* Set thermel conduction coefficient */
#ifdef THERMAL_CONDUCTION
  kappa_aniso = 1.0;
  kappa_iso = 0;
#endif

  /* Set viscosity */
#ifdef VISCOSITY
  nud = 2.21 * 1.0e-16 * (pow(Tchar, 2.5) / LnA); /*kg m^-1 s^-1*/
  nu0 = nud / Rhochar;                            /*[m^2 s^-1]*/
  nu_iso = nu0 / (Lchar * Vchar);
#endif

  /* Enroll gravitational potential to give acceleration in y-direction for 2D */
  StaticGravPot = gsun_potential;

  /* Set optical thin radiation cooling and corona heating function */
  CoolingFunc = opticalthinloss_corona;

  /* Set boundary value functions */
  /* (a) left-open */
  bvals_mhd_fun(pDomain, left_x1, openbc_ix1);

  /* (b) Right-open */
  bvals_mhd_fun(pDomain, right_x1, openbc_ox1);
  //bvals_mhd_fun(pDomain, right_x1, symmbc_ox1);

  /* (c) Top-open */
  bvals_mhd_fun(pDomain, right_x2, openbc_ox2);

  /* (d) Bottom-line-tied */
  bvals_mhd_fun(pDomain, left_x2, linetiedbc_ix2);
}

/*==============================================================================
 * PROBLEM USER FUNCTIONS:
 * problem_write_restart() - writes problem-specific user data to restart files
 * problem_read_restart()  - reads problem-specific user data from restart files
 * get_usr_expr()          - sets pointer to expression for special output data
 * get_usr_out_fun()       - returns a user defined output function pointer
 * get_usr_par_prop()      - returns a user defined particle selection function
 * Userwork_in_loop        - problem specific work IN     main loop
 * Userwork_after_loop     - problem specific work AFTER  main loop
 *----------------------------------------------------------------------------*/

void problem_write_restart(MeshS *pM, FILE *fp)
{
  /* Write parameters for gravity */
  Real cs_buf[3], pte_buf[6], char_buf[8];
  int cs_mode_buf[2];

  cs_buf[0] = cs_width;
  cs_buf[1] = xc_symm;
  cs_buf[2] = yc_psipert;

  cs_mode_buf[0] = cs_mode;

  pte_buf[0] = Te_corona;
  pte_buf[1] = Te_photos;
  pte_buf[2] = Height_TR;
  pte_buf[3] = Width_TR;
  pte_buf[4] = pgas_c;
  pte_buf[5] = posi_c;

  char_buf[0] = Lchar;
  char_buf[1] = Bchar;
  char_buf[2] = Nechar;
  char_buf[3] = Rhochar;
  char_buf[4] = Pchar;
  char_buf[5] = Timechar;
  char_buf[6] = Tchar;
  char_buf[7] = Vchar;

  fwrite(cs_buf, sizeof(cs_buf), 1, fp);
  fwrite(cs_mode_buf, sizeof(cs_mode_buf), 1, fp);
  fwrite(pte_buf, sizeof(pte_buf), 1, fp);
  fwrite(char_buf, sizeof(char_buf), 1, fp);

  return;
}

void problem_read_restart(MeshS *pM, FILE *fp)
{
  /* Constant parameters */
  Real Kb = 1.38e-23, Mp = 1.6726e-27, Mu0 = 1.25663706144e-6, LnA = 30.0;
  Real gravitational_const = 6.672e-11; /* (N M^2 kg^-2)*/
  Real solarmass = 1.99e+30;            /* (kg) */
  Real solarradius = 6.96e+8;           /* (m) */

  /* Read saved parameters */
  Real cs_buf[3], pte_buf[6], char_buf[8];
  int cs_mode_buf[2];

  fread(cs_buf, sizeof(cs_buf), 1, fp);
  fread(cs_mode_buf, sizeof(cs_mode_buf), 1, fp);
  fread(pte_buf, sizeof(pte_buf), 1, fp);
  fread(char_buf, sizeof(char_buf), 1, fp);

  cs_width = cs_buf[0];
  xc_symm = cs_buf[1];
  yc_psipert = cs_buf[2];

  cs_mode = cs_mode_buf[0];

  Te_corona = pte_buf[0];
  Te_photos = pte_buf[1];
  Height_TR = pte_buf[2];
  Width_TR = pte_buf[3];
  pgas_c = pte_buf[4];
  posi_c = pte_buf[5];

  Lchar = char_buf[0];
  Bchar = char_buf[1];
  Nechar = char_buf[2];
  Rhochar = char_buf[3];
  Pchar = char_buf[4];
  Timechar = char_buf[5];
  Tchar = char_buf[6];
  Vchar = char_buf[7];

  /* Enroll gravitational potential */
  StaticGravPot = gsun_potential;

  /* cooling */
  CoolingFunc = opticalthinloss_corona;

  /* Set boundary value functions */
  int nl, nd;
  if (pM->Nx[2] == 1)
  {
    for (nl = 0; nl < (pM->NLevels); nl++)
    {
      for (nd = 0; nd < (pM->DomainsPerLevel[nl]); nd++)
      {
        bvals_mhd_fun(&(pM->Domain[nl][nd]), left_x1, openbc_ix1);
        bvals_mhd_fun(&(pM->Domain[nl][nd]), right_x1, openbc_ox1);
        bvals_mhd_fun(&(pM->Domain[nl][nd]), right_x2, openbc_ox2);
        bvals_mhd_fun(&(pM->Domain[nl][nd]), left_x2, linetiedbc_ix2);
      }
    }
  }

  /* Set resistivity */
#ifdef RESISTIVITY
  eta_Ohm = par_getd("problem", "eta_Ohm");
  Q_AD = par_getd("problem", "Q_AD");
  Q_Hall = 0.0;
  d_ind = 0.0;
#endif

  /* Set thermel conduction coefficient */
#ifdef THERMAL_CONDUCTION
  kappa_aniso = (1.84e-10 / LnA * (pow(Tchar, 2.5))) * Tchar / (Pchar * Lchar * Vchar);
  kappa_iso = (1.84e-10 / LnA * (pow(Tchar, 2.5))) * Tchar / (Pchar * Lchar * Vchar);
#endif

  /* Set viscosity */
#ifdef VISCOSITY
  nud = 2.21 * 1.0e-16 * (pow(Tchar, 2.5) / LnA); /*kg m^-1 s^-1*/
  nu0 = nud / Rhochar;                            /*[m^2 s^-1]*/
  nu_iso = nu0 / (Lchar * Vchar);
#endif

  return;
}

#ifdef MHD
/*! \fn static Real current(const GridS *pG, const int i, const int j, const
 *			   int k)
 *  \brief computes x3-component of current
 */
static Real current(const GridS *pG, const int i, const int j, const int k)
{
  return ((pG->B2i[k][j][i] - pG->B2i[k][j][i - 1]) / pG->dx1 -
          (pG->B1i[k][j][i] - pG->B1i[k][j - 1][i]) / pG->dx2);
}

/*! \fn static Real divB(const GridS *pG, const int i, const int j, const int k)
 *  \brief  calculates div(B) */
static Real divB(const GridS *pG, const int i, const int j, const int k)
{
  Real qa;
  if (pG->Nx[2] > 1)
  {
    qa = (pG->B1i[k][j][i + 1] - pG->B1i[k][j][i]) / pG->dx1 +
         (pG->B2i[k][j + 1][i] - pG->B2i[k][j][i]) / pG->dx2 +
         (pG->B3i[k + 1][j][i] - pG->B3i[k][j][i]) / pG->dx3;
  }
  else
  {
    qa = (pG->B1i[k][j][i + 1] - pG->B1i[k][j][i]) / pG->dx1 +
         (pG->B2i[k][j + 1][i] - pG->B2i[k][j][i]) / pG->dx2;
  }
  return qa;
}

/*! \fn static Real bz(const GridS *pG, const int i, const int j, const int k)
 *  \brief return bz in 2.5D simulations*/
static Real bz(const GridS *pG, const int i, const int j, const int k)
{
  return pG->U[k][j][i].B3c;
}

/*! \fn static Real ohmiceta(const GridS *pG, const int i, const int j, const int k)
 *  \brief return  ohmiceta*/
static Real ohmiceta(const GridS *pG, const int i, const int j, const int k)
{
  return pG->eta_Ohm[k][j][i];
}

/*! \fn static Real b1i(const GridS *pG, const int i, const int j, const int k)
 *  \brief return  ohmiceta*/
static Real b1i(const GridS *pG, const int i, const int j, const int k)
{
  return pG->B1i[k][j][i];
}

/*! \fn static Real b2i(const GridS *pG, const int i, const int j, const int k)
 *  \brief return  ohmiceta*/
static Real b2i(const GridS *pG, const int i, const int j, const int k)
{
  return pG->B2i[k][j][i];
}

/*! \fn static Real b3i(const GridS *pG, const int i, const int j, const int k)
 *  \brief return  ohmiceta*/
static Real b3i(const GridS *pG, const int i, const int j, const int k)
{
  return pG->B3i[k][j][i];
}

#endif

ConsFun_t get_usr_expr(const char *expr)
{
#ifdef MHD
  if (strcmp(expr, "J3") == 0)
    return current;
  else if (strcmp(expr, "DivB") == 0)
    return divB;
  else if (strcmp(expr, "B3") == 0)
    return bz;
  else if (strcmp(expr, "Ohmiceta") == 0)
    return ohmiceta;
  else if (strcmp(expr, "B1i") == 0)
    return b1i;
  else if (strcmp(expr, "B2i") == 0)
    return b2i;
  else if (strcmp(expr, "B3i") == 0)
    return b3i;
#endif
  return NULL;
}

VOutFun_t get_usr_out_fun(const char *name)
{
  return NULL;
}

#ifdef RESISTIVITY

void get_eta_user(GridS *pG, int i, int j, int k,
                  Real *eta_O, Real *eta_H, Real *eta_A)
{
  Real x1, x2, x3;
  Real r, r_eta = 0.1;
  Real eta_input, eta_plus;
  /* Artificial resistivity at the bottom boundary */
  Real height_eta_bot;

  /* Magnitude of perturbations */
  Real eta_turb, time_turb;
  Real pos_x_turb, pos_y_turb, hwd_x_turb, hwd_y_turb;
  Real err = 1.0e-03, hwd_y_low;

  /* Read input parameters */
  eta_turb = par_getd("problem", "eta_turb");
  time_turb = par_getd("problem", "time_turb");
  pos_x_turb = par_getd("problem", "pos_x_turb");
  pos_y_turb = par_getd("problem", "pos_y_turb");
  hwd_x_turb = par_getd("problem", "hwd_x_turb");
  hwd_y_turb = par_getd("problem", "hwd_y_turb");

  *eta_O = par_getd("problem", "eta_Ohm");
  *eta_H = 0.0;
  *eta_A = par_getd("problem", "Q_AD");

  /* Initial increased eta  = eta_Ohm + eta_turb*time
     eta_input = par_getd("problem", "eta_Ohm");
     if (pG->time <= time_turb) {
   *eta_O = eta_input + eta_turb*((time_turb-pG->time)/time_turb);
   }*/

  return;
}
#endif

void Userwork_in_loop(MeshS *pM)
{
  GridS *pG = pM->Domain[0][0].Grid;
  /* Calculate eta_x and eta_y for checking the numerical diffusion */
  int is = pG->is, ie = pG->ie;
  int js = pG->js, je = pG->je;
  int ks = pG->ks, ke = pG->ke;
  int i, j, k;
  int ic, jc;
  Real x1, x2, x3;
  /* (1) Pressure floor */
  Real dens_floor = 1.0e-10;
  Real pres_floor = 1.0e-7;
  Real pres_c, dpres;
  Real msqr, bsqr;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        msqr = SQR(pG->U[k][j][i].M1) + SQR(pG->U[k][j][i].M2) + SQR(pG->U[k][j][i].M3);
        bsqr = SQR(pG->U[k][j][i].B1c) + SQR(pG->U[k][j][i].B2c) + SQR(pG->U[k][j][i].B3c);
        pres_c = Gamma_1 * (pG->U[k][j][i].E - 0.5 * msqr / pG->U[k][j][i].d - 0.5 * bsqr);

        pG->U[k][j][i].d = MAX(pG->U[k][j][i].d, dens_floor);
        pres_c = MAX(pres_c, pres_floor);

        pG->U[k][j][i].E = pres_c / Gamma_1 + 0.5 * msqr / pG->U[k][j][i].d + 0.5 * bsqr;
      }
    }
  }

  /* (2) Check time-step */
  if (pG->dt <= 1.0e-7)
  {
    ath_error("[Stop]: Time step is less than 10^-7. \n");
  }

  /* (3) In pure 2D cases, Vz == 0 */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        pG->U[k][j][i].E = pG->U[k][j][i].E - 0.5 * SQR(pG->U[k][j][i].M3) / pG->U[k][j][i].d;
        pG->U[k][j][i].M3 = 0.0;
      }
    }
  }

  /* (4) Constant background Heating term: heatrate*dt */
  Real eff_constant, hrate, H;
  Real eb_jc, ek_jc, pjc, Tjc;
  Real e_new, de_cooling, de_heating, de_heating_expect, de_heating_const;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {

        eb_jc = 0.5 * (SQR(pG->U[k][j][i].B1c) + SQR(pG->U[k][j][i].B2c) + SQR(pG->U[k][j][i].B3c));
        ek_jc = 0.5 * (SQR(pG->U[k][j][i].M1) + SQR(pG->U[k][j][i].M2) + SQR(pG->U[k][j][i].M3)) / pG->U[k][j][i].d;
        pjc = Gamma_1 * (pG->U[k][j][i].E - eb_jc - ek_jc);
        Tjc = pjc / pG->U[k][j][i].d;

        e_new = (pG->U[k][j][i].d * tempini[k][j][i]) / Gamma_1 + eb_jc + ek_jc;
        de_cooling = (pow(pG->U[k][j][i].d * Nechar, 2) * Qt(Tjc * Tchar)) * (Timechar / Pchar) * pG->dt;
        de_heating_expect = e_new - pG->U[k][j][i].E + de_cooling;

        // heating limits:
        if (de_heating_expect < 0.0)
        {
          de_heating_expect = 0.0;
        }

        // Reset the heating term
        de_heating = de_heating_expect;

        pG->U[k][j][i].E = pG->U[k][j][i].E + de_heating;
      }
    }
  }
  return;
}

void Userwork_after_loop(MeshS *pM)
{
  return;
}

/*=========================== PRIVATE FUNCTIONS ==============================*/
/*----------------------------------------------------------------------------*/
/*  \funtion of Bx for initial conditions */
static Real bxini(const Real x1, const Real x2)
{
  Real bx0;
  bx0 = 0;

  /* Add perturbation */
  Real pix, piy, bx1;

  /* Symmetry center: xc_symm */
  pix = PI * (x1 - xc_symm) / lx_psipert;
  piy = 2.0 * pi * (x2 - yc_psipert) / ly_psipert;
  bx1 = (2.0 * pi / ly_psipert) * psi_pert * cos(pix) * sin(piy);
  bx0 = bx0 + bx1;
  return bx0;
}

/*-------------------------------------------------------------------------- -*/
/*  \funtion of By for initial conditions */
static Real byini(const Real x1, const Real x2)
{
  Real by0;
  Real x1p = x1 - xc_symm;
  if (x1p < -cs_width)
  {
    by0 = -1.0;
  }
  else if (fabs(x1p) <= cs_width)
  {
    by0 = sin(PI * x1p / 2.0 / cs_width);
  }
  else
  {
    by0 = 1.0;
  }

  /* Add perturbation */
  Real pix, piy, by1;

  pix = PI * (x1 - xc_symm) / lx_psipert;
  piy = 2.0 * pi * (x2 - yc_psipert) / ly_psipert;
  by1 = (-PI / lx_psipert) * psi_pert * b0 * sin(pix) * cos(piy);
  by0 = by0 + by1;
  return by0;
}

/*------------------------------------------------------------------------- --*/
/*  \funtion of Bz for initial conditions */
static Real bzini(const Real x1, const Real x2)
{
  Real bx0, by0, bz0, bzsq;
  Real pbz;
  //pbz = 0.5 - 0.5 * byini(x1, x2) * byini(x1, x2);
  //bz0 = sqrt(2.0 * pbz);
  Real pmag_max;
  pmag_max = 0.5 * b0 * b0 * (1.0 + psi_pert * pi / lx_psipert) * (1.0 + psi_pert * pi / lx_psipert);
  bx0 = bxini(x1, x2);
  by0 = byini(x1, x2);
  bzsq = 2.0 * pmag_max - bx0 * bx0 - by0 * by0;
  bz0 = sqrt(bzsq);
  return bz0;
}

/*----------------------------------------------------------------------------*/
/*  \fn gas pressure assuming constant temperature */
static Real presini_isote(const Real x2, const Real y0,
                          const Real p0, const Real te0)
{
  Real pgas, gm, r, r0;
  gm = gravitational_const * solarmass * Rhochar / (Lchar * Pchar);
  r = solarradius / Lchar + x2;
  r0 = solarradius / Lchar + y0;
  pgas = p0 * exp(gm / te0 * (1.0 / r - 1.0 / r0));
  return pgas;
}

/*----------------------------------------------------------------------------*/
/*  \fn gas pressure performing numerical integration */
static Real presini_integral(const Real x2, const Real y0, const Real p0)
{
  Real pgas;
  Real Iout_simp;

  /* Simpson's rule */
  Iout_simp = -adaptiveSimpsons(my_integ_py, posi_c, x2, 1.0e-10, 10000);

  pgas = pgas_c * exp(Iout_simp);
  return pgas;
}

/*-----------------------------------------------------------------------------
 * \fn Real my_integ_py                                  
 * \brief Integration function to get integ(gsun(y)/T(y))dy
 * ---------------------------------------------------------------------------*/
Real my_integ_py(Real y)
{
  return gsun_y(y) / teini(y);
}

/*----------------------------------------------------------------------------*/
/*  \fn Initial Temperature */
static Real teini(const Real x2)
{
  /* Non-dimensional variables */
  Real techr, tecor, h, w;
  Real t1, t2, te;

  /* The following calculation is in non-dimensional forms */
  techr = Te_photos / Tchar;
  tecor = Te_corona / Tchar;
  h = Height_TR / Lchar;
  w = Width_TR / Lchar;

  /* Temperature */
  t1 = 0.5 * (tecor - techr);
  t2 = 0.5 * (tecor + techr);
  te = t1 * tanh((x2 - h) / w) + t2;

  return te;
}

/*----------------------------------------------------------------------------*/
/*! \fn void linetiedbc_ix2(GridS *pGrid)
 *  \brief Sets boundary condition at the bottom.
 */
/*  ix2, line-tied, bottom */
/* Update:
 *   2018-03-06:
 *   Check the reflection on the bottom: m2 = m2 in ghost zone.*/
void linetiedbc_ix2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ku; /* k-upper */
#endif

  /* Local variables */
  Real x1, x2, x3;
  Real pjs, pjsmj, Tjsmj, yjs, yjsmj;
  Real pbypx, pgas;

  Real te_js, p_js, eb_js, ek_js;
  Real te_mj, p_mj, eb_mj, ek_mj;
  Real y_js, y_mj;
  int j_mj;

  /*  There are two cases for setting Bx and P, rho, and T in ghost zone. 
      One is for jz == 0 and other is for fixed BC.
      */
  int case_index = 1;

  /* Set all variables in ghost zone */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i] = pGrid->U[k][js][i];
        pGrid->U[k][js - j][i].M1 = 0.0;
        pGrid->U[k][js - j][i].M2 = 0.0;
        pGrid->U[k][js - j][i].M3 = 0.0;
      }
    }
  }

#ifdef MHD
  /* B2i*/
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        cc_pos(pGrid, i, js - j, k, &x1, &x2, &x3);
        pGrid->B2i[k][js - j][i] = byini(x1, x2 - 0.5 * pGrid->dx2);
      }
    }
  }

  /* Bottom: B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - (nghost - 1); i <= ie + nghost; i++)
      {
        cc_pos(pGrid, i, js - j, k, &x1, &x2, &x3);

        /* Case 1: Jz == 0 */
        if (case_index == 1)
        {
          pbypx = (byini(x1, x2 - 0.5 * pGrid->dx2) - byini(x1 - pGrid->dx1, x2 - 0.5 * pGrid->dx2)) / pGrid->dx1;
          pGrid->B1i[k][js - j][i] = pGrid->B1i[k][js - j + 1][i] - pbypx * (pGrid->dx2);
        }

        /* Case 2: fixed Bx*/
        if (case_index == 2)
        {
          pGrid->B1i[k][js - j][i] = bxini(x1 - 0.5 * pGrid->dx1, x2);
        }
      }
    }
  }
  /* B2i: */
  if (cs_mode == 0)
  {
    for (k = ks; k <= ke; k++)
    {
      for (j = 1; j <= nghost; j++)
      {
        for (i = is - (nghost - 1); i <= ie + nghost; i++)
        {
          pGrid->B1i[k][js - j][i] = pGrid->B1i[k][js][i];
        }
      }
    }
  }
  /* B3i: */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        cc_pos(pGrid, i, js - j, k, &x1, &x2, &x3);
        pGrid->B3i[k][js - j][i] = bzini(x1, x2);
      }
    }
  }

  /* Compute B1c */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - (nghost - 1); i <= ie + nghost - 1; i++)
      {
        pGrid->U[k][js - j][i].B1c = 0.5 * (pGrid->B1i[k][js - j][i] + pGrid->B1i[k][js - j][i + 1]);
      }
    }
  }
  /* Compute B2c */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i].B2c = 0.5 * (pGrid->B2i[k][js - j][i] + pGrid->B2i[k][js - j + 1][i]);
      }
    }
  }
  /* Compute B3c */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i].B3c = pGrid->B3i[k][js - j][i];
        ;
      }
    }
  }
#endif /* MHD */

  /* Pressure and Total Energy */
  cc_pos(pGrid, is, js, ks, &x1, &x2, &x3);
  y_js = x2;

  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      j_mj = js - j;
      cc_pos(pGrid, is, j_mj, k, &x1, &x2, &x3);
      y_mj = x2;
      for (i = is - nghost; i <= ie + nghost; i++)
      {

        /* Case 1 */
        if (case_index == 1)
        {
          eb_js = 0.5 * (SQR(pGrid->U[k][js][i].B1c) + SQR(pGrid->U[k][js][i].B2c) + SQR(pGrid->U[k][js][i].B3c));
          ek_js = 0.5 * (SQR(pGrid->U[k][js][i].M1) + SQR(pGrid->U[k][js][i].M2) + SQR(pGrid->U[k][js][i].M3)) / pGrid->U[k][js][i].d;
          p_js = (pGrid->U[k][js][i].E - eb_js - ek_js) * Gamma_1;
          te_js = p_js / pGrid->U[k][js][i].d;
          p_mj = presini_isote(y_mj, y_js, p_js, te_js);
          te_mj = te_js;
          pGrid->U[k][j_mj][i].d = p_mj / te_mj;
        }

        /* Case 2 */
        if (case_index == 2)
        {
          p_mj = pgasini[k][j_mj][i];
          pGrid->U[k][j_mj][i].d = rhoini[k][j_mj][i];
        }

        eb_mj = 0.5 * (SQR(pGrid->U[k][j_mj][i].B1c) + SQR(pGrid->U[k][j_mj][i].B2c) + SQR(pGrid->U[k][j_mj][i].B3c));
        ek_mj = 0.5 * (SQR(pGrid->U[k][j_mj][i].M1) + SQR(pGrid->U[k][j_mj][i].M2) + SQR(pGrid->U[k][j_mj][i].M3)) / pGrid->U[k][j_mj][i].d;

        pGrid->U[k][j_mj][i].E = p_mj / Gamma_1 + eb_mj + ek_mj;
      }
    }
  }
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void openbc_ox2(GridS *pGrid)
 *  \brief open boundary conditions, Outer x2 boundary (bc_ox2=2) */
/*----------------------------------------------------------------------------*/
/*  ox2, top boundary */
void openbc_ox2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ku; /* k-upper */
#endif
  Real x1, x2, x3;

  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][je+j][i] = pGrid->U[k][je-j+1][i];
        pGrid->U[k][je+j][i].B1c = -pGrid->U[k][je-j+1][i].B1c;
      }
    }
  }

#ifdef MHD
  /* B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - (nghost - 1); i <= ie + nghost; i++)
      {
        pGrid->B1i[k][je+j][i] = -pGrid->B1i[k][je-j+1][i];
      }
    }
  }

  /* B2i: j=je+1 is not a boundary condition for the interface field B2i */
  for (k = ks; k <= ke; k++)
  {
    for (j = 2; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B2i[k][je+j][i] = pGrid->B2i[k][je-j+2][i];
      }
    }
  }

  /* B3i: */
  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B3i[k][je+j][i] = pGrid->B3i[k][je-j+1][i];
      }
    }
  }
#endif
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void openbc_ix1(GridS *pGrid)
 *  \brief open boundary condition, Inner x1 boundary (bc_ix1=2) */
/*----------------------------------------------------------------------------*/
/*  ix1, Left boundary */
static void openbc_ix1(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][is - i] = pGrid->U[k][j][is];
        if (pGrid->U[k][j][is - i].M1 > 0.0)
        {
          pGrid->U[k][j][is - i].M1 = -pGrid->U[k][j][is - i].M1;
        }
      }
    }
  }

#ifdef MHD
  /* B1i*/
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B1i[k][j][is - i] = pGrid->B1i[k][j][is];
      }
    }
  }

  /* B2i */
  if (pGrid->Nx[1] > 1)
    ju = je + 1;
  else
    ju = je;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= ju; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B2i[k][j][is - i] = pGrid->B2i[k][j][is];
      }
    }
  }

  /* B3i */
  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B3i[k][j][is - i] = pGrid->B3i[k][j][is];
      }
    }
  }
#endif /* MHD */

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void openbc_ox1(GridS *pGrid)
 *  \brief openbc boundary conditions, Outer x1 boundary (bc_ox1=2) */
/*----------------------------------------------------------------------------*/
/* Right boundary */
static void openbc_ox1(GridS *pGrid)
{
  int ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif
  /* Centered Variables */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][ie + i] = pGrid->U[k][j][ie];
        if (pGrid->U[k][j][ie + i].M1 < 0.0)
        {
          pGrid->U[k][j][ie + i].M1 = -pGrid->U[k][j][ie + i].M1;
        }
      }
    }
  }

#ifdef MHD
  /* B2i */
  if (pGrid->Nx[1] > 1)
    ju = je + 1;
  else
    ju = je;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= ju; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B2i[k][j][ie + i] = pGrid->B2i[k][j][ie];
      }
    }
  }

  /* i=ie+1 is not a boundary condition for the interface field B1i */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 2; i <= nghost; i++)
      {
        pGrid->B1i[k][j][ie + i] = pGrid->B1i[k][j][ie + 1];
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B3i[k][j][ie + i] = pGrid->B3i[k][j][ie];
      }
    }
  }
#endif /* MHD */

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn Real opticalthinloss_corona(const Real dens, const Real Press, 
  const Real dt) 
  Returns rate in non-dimensional value. The other local variables appear
  in SI unit.
  */
#ifndef BAROTROPIC
Real opticalthinloss_corona(const Real dens, const Real Press, const Real dt)
{
  Real coolrate, hrate, hrate2;
  Real ne, nH;
  Real T, Teqi, Tmin;

  /* Checking */
  Real dE, dTemp, Tnext;

  /* Compute dimensional T, ne, nH */
  T = Tchar * (Press / dens);
  ne = dens * Nechar;
  nH = ne;

  /* Set the minimum Temperature to prevent nagtive Te */
  Tmin = 5500.0;

  /* only cooling rate */
  coolrate = ne * nH * Qt(T);

  /* To non-dimensional value */
  coolrate = coolrate * Timechar / Pchar;

  /* Check after time=dt, T is still larger than Tmin */
  dE = coolrate * dt;
  dTemp = (Gamma_1 * dE) / dens;
  Tnext = T - dTemp * Tchar; /* in unit: K*/

  if (Tnext <= Tmin)
  {
    dTemp = (T - Tmin) / Tchar;
    dE = dTemp * dens / Gamma_1;
    coolrate = dE / dt;
    if (coolrate >= 0.0)
      coolrate = 0.0;
  }

  /* return result */
  return coolrate;
}
#endif /* BAROTROPIC */

/*----------------------------------------------------------------------------*/
/*  \brief Calculate Qt
 *  piecewise linear approximation (Klimcuk et al. 2008)*/
/* T: dimensional variable, SI unit */
static Real Qt(const Real T)
{
  Real q;
  Real factor;
  Real logt = log10(T);

  /* first in cgs: ergs sec^-1 cm^3 */
  if (logt <= 4.97)
  {
    q = 1.09e-31 * (pow(T, 2));
  }
  else if (logt <= 5.67)
  {
    q = 8.87e-17 * (pow(T, -1.0));
  }
  else if (logt <= 6.18)
  {
    q = 1.90e-22;
  }
  else if (logt <= 6.55)
  {
    q = 3.54e-13 * (pow(T, -3. / 2.));
  }
  else if (logt <= 6.90)
  {
    q = 3.46e-25 * (pow(T, 1. / 3.));
  }
  else if (logt <= 7.63)
  {
    q = 5.49e-16 * (pow(T, -1.0));
  }
  else
  {
    q = 1.96e-27 * (pow(T, 0.5));
  }

  /* Decrease Q(T) if T<500,000K */
  if ((T <= Tc_low) && (sw_lowt_cool == 1))
  {
    factor = (Tc_low / T) * (Tc_low / T) * (Tc_low / T);
    q = q / factor;
  }

  /* to SI unit: W m^3 */
  q = q * 1.0e-13;

  return q;
}

/*----------------------------------------------------------------------------*/
/*! \fn static Real gsun_static(const Real x1, const Real x2, const Real x3)
 *  \brief Gravitational potential; g = 0.1
 */
static Real gsun_potential(const Real x1, const Real x2, const Real x3)
{
  Real gp;
  /* gravity potential */
  gp = -(gravitational_const * solarmass) / (x2 * Lchar + solarradius);
  /* Nondimensional gp */
  gp = gp * Rhochar / Pchar;
  return gp;
}

/*----------------------------------------------------------------------------*/
/*! \fn static Real gsun_y(const Real x2)
 *  \brief Nondimensional Gravitaty at height x2
 */
static Real gsun_y(const Real x2)
{
  Real g, gnondim;
  Real r;
  r = solarradius + x2 * Lchar;                    /* (m) */
  g = (gravitational_const * solarmass) / (r * r); /* (m s^-2)*/
  gnondim = g * Rhochar * Lchar / Pchar;
  return gnondim;
}
