#include "copyright.h"
/*============================================================================*/
/*! \file flarecs.c
 *  \brief Problem generator for the current sheet in solar flare.
 *
 * PURPOSE: Problem generator for the magnetic reconnection in flare CS.
 *
 * REFERENCE: Forbes & Malherbe SoPh 1991; Shen, Lin & Murphy ApJ 2011.
 *
 * Update:
 *  2016-10-28: flarecs_1.4
 *    Calculate characteristic Values in problem routine.
 *    Developing: based on 20161116 version.
 *  2017-04-05:
 *    Boundary mode 3.
 *    flarecs_1.4.1.
 *  2017-04-19
 *    flarecs_1.4.2.
 *    Add artificial diffusion terms on momentum after each step.
 *  2017-04-20
 *    Add a flow restriction on the bottom BC in lineited_bc_mode1;
 *    Add strong inflow restriction in openbc_ix1, openbc_ox1
 *    and openbc_ox2;
 *    Using divergence free condition in open BCs.
 *    No-restriction for Bx on top BC.
 *  2017-04-26
 *    Output dBx/dt and dBy/dt and estimated eta.
 *    Output dBx/dt and dBy/dt.
 *  2017-04-28
 *    Add magnetic perturbation: psi(x,y) = psi0*B0*cos(PI*x/Lx)*cos(2PI*y/Ly).
 *  2017-05-04
 *    Bug fixed in function of psi(x, y) perturbation.
 *    Clean up parameters in input file.
 *  2017-05-10 
 *    No-density average at the Line-tied BC.
 *  2017-05-13
 *    Openbc_ox2.
 *  2017-05-16
 *    Bug fixed: Rhochar.
 *  2018-08-22
 *    Follows 2017-05-22
 *    Add constant Bz.
 */

/*============================================================================*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defs.h"
#include "athena.h"
#include "globals.h"
#include "prototypes.h"

/*==============================================================================
 * PRIVATE FUNCTION PROTOTYPES:
 * void flarecs_linetied() - sets BCs on R-x2 boundary
 *============================================================================*/
static Real bxini(const Real x1, const Real x2);
static Real byini(const Real x1, const Real x2);
static Real bzini(const Real x1, const Real x2);
static Real presini(const Real x1, const Real x2);

static void openbc_ix1(GridS *pGrid);
static void openbc_ox1(GridS *pGrid);
static void openbc_ox2(GridS *pGrid);

static void symmbc_ix1(GridS *pGrid);
static void symmbc_ox1(GridS *pGrid);

static void linetiedbc_ix2(GridS *pGrid);

/* Constant parameters */
Real Kb = 1.38e-23, Mp = 1.6726e-27, Mu0 = 1.25663706144e-6, LnA = 30.0;

/* Basic characteristic Vcharlues except beta0 */
Real Lchar, Bchar, Tchar;

/* Dependent characteristic Vcharlues */
Real Pchar, Rhochar, Nechar, Vchar;
Real Timechar;

/* Parameters in initial configurations */
int cs_mode;
Real cs_width, beta0;
Real fact_bz, bz_const;

/* The artificial dissipation, numerical dissipation */
Real height_numdiss, factor_numdiss;
Real ***d_new, ***m1_new, ***m2_new, ***m3_new;

/* Save the previous Bx, By */
Real ***B1pre, ***B2pre;
Real ***pB1pt, ***pB2pt;

/* PSI perturbation */
Real psi_pert, lx_psipert, ly_psipert, yc_psipert, b0 = 1.0, pi = 3.141592653589793;

/* eta perturbation */
Real eta_pert, eta_pert_time, eta_ohm_origin;

/* Define the temperature range in computing kappa(T) */
Real temp_low, temp_high;

/* Artificially reset cooling and conduction in lower temperature region */
Real Tc_low;
int sw_lowt_cool; // use for decreasing the cooling and increasing thermal
                  // conduction once temperature is lower than Tc_low to keep
                  // Kappa(T)*Q(T) constant. (Reeves et al.2010 ApJ)
int sw_explicit_conduct_dt;

/* Reset Output dt = dt_reset_out after time_reset_out */
Real dt_reset_out = 0.05, time_reset_out = 1.0;

/*=========================== PUBLIC FUNCTIONS ===============================*/
/*----------------------------------------------------------------------------*/

/* problem:  */
void problem(DomainS *pDomain)
{
  GridS *pGrid = (pDomain->Grid);
  int i, is = pGrid->is, ie = pGrid->ie;
  int j, js = pGrid->js, je = pGrid->je;
  int k, ks = pGrid->ks, ke = pGrid->ke;
  Real x1, x2, x3;

  Real pgas;

  /* Viscosity */
  Real nud, nu0;

  /* Magnitude of perturbations */
  Real vin_pert;

  /* Read input parameters */
  beta0 = par_getd("problem", "beta0");
  cs_mode = par_getd("problem", "cs_mode");
  cs_width = par_getd("problem", "cs_width");
  fact_bz = par_getd("problem", "fact_bz");
  bz_const = par_getd("problem", "bz_const");

  Lchar = par_getd("problem", "Lchar");
  Bchar = par_getd("problem", "Bchar");
  Tchar = par_getd("problem", "Tchar");

  Pchar = Bchar * Bchar / Mu0;
  Rhochar = 0.5 * (beta0 * 0.5 * Bchar * Bchar / Mu0) / ((Kb / Mp) * Tchar);
  Nechar = Rhochar / Mp;
  Vchar = Bchar / sqrt(Mu0 * Rhochar);
  Timechar = Lchar / Vchar;

  printf("Rhochar=%10.3e (kg/m^3)\n", Rhochar);
  printf("Pchar=%10.3e (Pa)\n", Pchar);
  printf("Tchar=%10.3e (K)\n", Tchar);
  printf("vchar=%10.3e (m/s)\n", Vchar);

  /* Perturbation and dissipation */
  vin_pert = par_getd("problem", "vin_pert");
  height_numdiss = par_getd("problem", "height_numdiss");
  factor_numdiss = par_getd("problem", "factor_numdiss");

  psi_pert = par_getd("problem", "psi_pert");
  lx_psipert = par_getd("problem", "lx_psi");
  ly_psipert = par_getd("problem", "ly_psi");
  yc_psipert = par_getd("problem", "yc_psi");

  eta_pert = par_getd("problem", "eta_pert");
  eta_pert_time = par_getd("problem", "eta_pert_time");
  eta_ohm_origin = par_getd("problem", "eta_Ohm");

  /* Read temperature floor for kappa calculations */
  temp_low = par_getd("problem", "Temperature_low");
  temp_high = par_getd("problem", "Temperature_high");

  /* Read parameters for cooling */
  Tc_low = par_getd("problem", "Tc_low");
  sw_lowt_cool = par_getd("problem", "sw_lowt_cool");
  /* if it equals to 1, then new_diff_dt will calculate updated time-step 
  according to the explicit diffusion schema. */
  sw_explicit_conduct_dt = par_getd("problem", "sw_explicit_conduct_dt");

  /* Set all Vcharriables */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        /* axis */
        cc_pos(pGrid, i, j, k, &x1, &x2, &x3);

        /* Pressure */
        pgas = presini(x1, x2);

        /* density */
        pGrid->U[k][j][i].d = pgas / (beta0 / 2.0);

        /* monentum */
        pGrid->U[k][j][i].M1 = 0.0;
        pGrid->U[k][j][i].M2 = 0.0;
        pGrid->U[k][j][i].M3 = 0.0;

        /* magnetic field */
        pGrid->B1i[k][j][i] = bxini(x1, x2);
        pGrid->B2i[k][j][i] = byini(x1, x2);
        pGrid->B3i[k][j][i] = bzini(x1, x2) + bz_const;

        /* initial setting for magnetic field on the boundary */
        if (i == ie && ie > is)
        {
          cc_pos(pGrid, i + 1, j, k, &x1, &x2, &x3);
          pGrid->B1i[k][j][i + 1] = bxini(x1, x2);
        }
        if (j == je && je > js)
        {
          cc_pos(pGrid, i, j + 1, k, &x1, &x2, &x3);
          pGrid->B2i[k][j + 1][i] = byini(x1, x2);
        }
        if (k == ke && ke > ks)
        {
          cc_pos(pGrid, i, j, k + 1, &x1, &x2, &x3);
          pGrid->B3i[k + 1][j][i] = bzini(x1, x2) + bz_const;
        }

        /* monmentum perturbation */
        if (x2 <= 1.0)
        {
          pGrid->U[k][j][i].M1 = -x1 * vin_pert * x2;
        }
        else
        {
          pGrid->U[k][j][i].M1 = -x1 * vin_pert;
        }
      }
    }
  }

  /* cell-center magnetic field */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        pGrid->U[k][j][i].B1c = 0.5 * (pGrid->B1i[k][j][i] +
                                       pGrid->B1i[k][j][i + 1]);
        pGrid->U[k][j][i].B2c = 0.5 * (pGrid->B2i[k][j][i] +
                                       pGrid->B2i[k][j + 1][i]);
        pGrid->U[k][j][i].B3c = pGrid->B3i[k][j][i];
      }
    }
  }

  /* total energy*/
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        cc_pos(pGrid, i, j, k, &x1, &x2, &x3);
        pgas = presini(x1, x2);

        pGrid->U[k][j][i].E = pgas / (Gamma_1) + 0.5 * (SQR(pGrid->U[k][j][i].B1c) + SQR(pGrid->U[k][j][i].B2c) + SQR(pGrid->U[k][j][i].B3c)) + 0.5 * (SQR(pGrid->U[k][j][i].M1) + SQR(pGrid->U[k][j][i].M2) + SQR(pGrid->U[k][j][i].M3)) / pGrid->U[k][j][i].d;
      }
    }
  }

  /* Set resistivity */
#ifdef RESISTIVITY
  eta_Ohm = eta_ohm_origin;
  Q_AD = 0.0;
  Q_Hall = 0.0;
  d_ind = 0.0;
#endif

  /* Set thermal conduction coefficient */
#ifdef THERMAL_CONDUCTION
  kappa_aniso = 1.0;
  kappa_iso = 0;
#endif

  /* Set viscosity */
#ifdef VISCOSITY
  nud = 2.21 * 1.0e-16 * (pow(Tchar, 2.5) / LnA); /*kg m^-1 s^-1*/
  nu0 = nud / Rhochar;                            /*[m^2 s^-1]*/
  nu_iso = nu0 / (Lchar * Vchar);
#endif

  /* Set optical thin radiation cooling and corona heating function
   CoolingFunc = cool_heat_corona;*/

  /* Set boundary Vcharlue functions */
  int bcix1_mode, bcox1_mode, bcix2_mode, bcox2_mode;
  bcix1_mode = par_getd("problem", "bcix1_mode");
  bcox1_mode = par_getd("problem", "bcox1_mode");
  bcix2_mode = par_getd("problem", "bcix2_mode");
  bcox2_mode = par_getd("problem", "bcox2_mode");

  /* (a) Left boundary condition */
  if (bcix1_mode == 0)
  {
    // apply the default BC setting
  }
  else if (bcix1_mode == -1)
  {
    bvals_mhd_fun(pDomain, left_x1, symmbc_ix1);
  }
  else if (bcix1_mode == 1)
  {
    bvals_mhd_fun(pDomain, left_x1, openbc_ix1);
  }

  /* (b) Right boundary condition */
  if (bcox1_mode == 0)
  {
    // apply the default BC setting
  }
  else if (bcox1_mode == -1)
  {
    bvals_mhd_fun(pDomain, right_x1, symmbc_ox1);
  }
  else if (bcox1_mode == 1)
  {
    bvals_mhd_fun(pDomain, right_x1, openbc_ox1);
  }

  /* (c) Bottom boundary condition */
  if (bcix2_mode == 0)
  {
    // apply the default BC setting
  }
  else if (bcix2_mode == 3)
  {
    bvals_mhd_fun(pDomain, left_x2, linetiedbc_ix2);
  }

  /* (d) Top boundary condition */
  if (bcox2_mode == 0)
  {
    // apply the default BC setting
  }
  else if (bcox2_mode == 1)
  {
    bvals_mhd_fun(pDomain, right_x2, openbc_ox2);
  }

  /* Allocate B1_pre and B2_pre */
  int n1 = ie - is + 1 + 2 * nghost;
  int n2 = je - js + 1 + 2 * nghost;
  int n3 = ke - ks + 1;
  if ((d_new = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for d_new \n");
  }
  if ((m1_new = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for m1_new \n");
  }
  if ((m2_new = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for m2_new \n");
  }
  if ((m3_new = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for m3_new \n");
  }

  if ((pB1pt = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for pB1pt \n");
  }
  if ((pB2pt = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for pB2pt \n");
  }

  if ((B1pre = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for B1pre \n");
  }
  if ((B2pre = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for B2pre \n");
  }

  /* Initialize B1_pre and B2_pre */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        B1pre[k][j][i] = pGrid->U[k][j][i].B1c;
        B2pre[k][j][i] = pGrid->U[k][j][i].B2c;
      }
    }
  }
}

/*==============================================================================
 * PROBLEM USER FUNCTIONS:
 * problem_write_restart() - writes problem-specific user data to restart files
 * problem_read_restart()  - reads problem-specific user data from restart files
 * get_usr_expr()          - sets pointer to expression for special output data
 * get_usr_out_fun()       - returns a user defined output function pointer
 * get_usr_par_prop()      - returns a user defined particle selection function
 * Userwork_in_loop        - problem specific work IN     main loop
 * Userwork_after_loop     - problem specific work AFTER  main loop
 *----------------------------------------------------------------------------*/

void problem_write_restart(MeshS *pM, FILE *fp)
{
  return;
}

void problem_read_restart(MeshS *pM, FILE *fp)
{
  return;
}

#ifdef MHD
/*! \fn static Real current(const GridS *pG, const int i, const int j, const
 *         int k)
 *  \brief computes x3-component of current
 */
static Real current(const GridS *pG, const int i, const int j, const int k)
{
  return ((pG->B2i[k][j][i] - pG->B2i[k][j][i - 1]) / pG->dx1 -
          (pG->B1i[k][j][i] - pG->B1i[k][j - 1][i]) / pG->dx2);
}

/*! \fn static Real divB(const GridS *pG, const int i, const int j, const int k)
 *  \brief  calculates div(B) */
static Real divB(const GridS *pG, const int i, const int j, const int k)
{
  Real qa;
  if (pG->Nx[2] > 1)
  {
    qa = (pG->B1i[k][j][i + 1] - pG->B1i[k][j][i]) / pG->dx1 +
         (pG->B2i[k][j + 1][i] - pG->B2i[k][j][i]) / pG->dx2 +
         (pG->B3i[k + 1][j][i] - pG->B3i[k][j][i]) / pG->dx3;
  }
  else
  {
    qa = (pG->B1i[k][j][i + 1] - pG->B1i[k][j][i]) / pG->dx1 +
         (pG->B2i[k][j + 1][i] - pG->B2i[k][j][i]) / pG->dx2;
  }
  return qa;
}

/*! \fn static Real bz(const GridS *pG, const int i, const int j, const int k)
 *  \brief return bz in 2.5D simulations*/
static Real bz(const GridS *pG, const int i, const int j, const int k)
{
  return pG->U[k][j][i].B3c;
}

/*! \fn static Real ohmiceta(const GridS *pG, const int i, const int j, const int k)
 *  \brief return  ohmiceta*/
static Real ohmiceta(const GridS *pG, const int i, const int j, const int k)
{
  return pG->eta_Ohm[k][j][i];
}

/*! \fn static Real b1i(const GridS *pG, const int i, const int j, const int k)
 *  \brief return  ohmiceta*/
static Real b1i(const GridS *pG, const int i, const int j, const int k)
{
  return pG->B1i[k][j][i];
}

/*! \fn static Real b2i(const GridS *pG, const int i, const int j, const int k)
 *  \brief return  ohmiceta*/
static Real b2i(const GridS *pG, const int i, const int j, const int k)
{
  return pG->B2i[k][j][i];
}

/*! \fn static Real b3i(const GridS *pG, const int i, const int j, const int k)
 *  \brief return  ohmiceta*/
static Real b3i(const GridS *pG, const int i, const int j, const int k)
{
  return pG->B3i[k][j][i];
}

static Real pb1_pt(const GridS *pG, const int i, const int j, const int k)
{
  return pB1pt[k][j][i];
}

static Real pb2_pt(const GridS *pG, const int i, const int j, const int k)
{
  return pB2pt[k][j][i];
}

#endif

ConsFun_t get_usr_expr(const char *expr)
{
#ifdef MHD
  if (strcmp(expr, "Current") == 0)
    return current;
  else if (strcmp(expr, "DivB") == 0)
    return divB;
  else if (strcmp(expr, "B3") == 0)
    return bz;
  else if (strcmp(expr, "Ohmiceta") == 0)
    return ohmiceta;
  else if (strcmp(expr, "B1i") == 0)
    return b1i;
  else if (strcmp(expr, "B2i") == 0)
    return b2i;
  else if (strcmp(expr, "B3i") == 0)
    return b3i;
  else if (strcmp(expr, "pB1pt") == 0)
    return pb1_pt;
  else if (strcmp(expr, "pB2pt") == 0)
    return pb2_pt;
#endif
  return NULL;
}

VOutFun_t get_usr_out_fun(const char *name)
{
  return NULL;
}

#ifdef RESISTIVITY

void get_eta_user(GridS *pG, int i, int j, int k,
                  Real *eta_O, Real *eta_H, Real *eta_A)
{
  /* eta */
  *eta_O = eta_ohm_origin;
  *eta_H = 0.0;
  *eta_A = 0.0;

  /* Initial increased eta  = eta_Ohm + eta_pert*function */
  if (pG->time <= eta_pert_time)
  {
    *eta_O = eta_ohm_origin + eta_pert * ((eta_pert_time - pG->time) / eta_pert_time);
  }

  return;
}
#endif

void Userwork_in_loop(MeshS *pM)
{
  GridS *pG = pM->Domain[0][0].Grid;
  /* Calculate eta_x and eta_y for checking the numerical diffusion */
  int is = pG->is, ie = pG->ie;
  int js = pG->js, je = pG->je;
  int ks = pG->ks, ke = pG->ke;
  int i, j, k;
  int ic, jc;
  Real x1, x2, x3;

  /* Introduce artificial dissipation */
  Real m1dx, m1dy, m1dz, m2dx, m2dy, m2dz;
  Real deltm1, deltm2, deltm3;
  Real diss_num, diss_nmax;
  Real ddx, ddt, dx2, dy2;
  Real vx, vy, vz;
  if (pG->dx1 >= pG->dx2)
  {
    ddx = pG->dx2;
  }
  else
  {
    ddx = pG->dx1;
  }
  ddt = pG->dt;
  dx2 = pG->dx1 * pG->dx1;
  dy2 = pG->dx2 * pG->dx2;
  diss_nmax = factor_numdiss;

  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost + 1; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        cc_pos(pG, i, j, k, &x1, &x2, &x3);

        if ((x2 <= height_numdiss) && (diss_nmax > 0.0))
        {
          diss_num = diss_nmax;

          m1dx = (pG->U[k][j][i + 1].M1 - 2.0 * pG->U[k][j][i].M1 + pG->U[k][j][i - 1].M1) / dx2;
          m1dy = (pG->U[k][j + 1][i].M1 - 2.0 * pG->U[k][j][i].M2 + pG->U[k][j - 1][i].M1) / dy2;
          m1dz = 0;

          m2dx = (pG->U[k][j][i + 1].M2 - 2.0 * pG->U[k][j][i].M2 + pG->U[k][j][i - 1].M2) / dx2;
          m2dy = (pG->U[k][j + 1][i].M2 - 2.0 * pG->U[k][j][i].M2 + pG->U[k][j - 1][i].M2) / dy2;
          m2dz = 0;

          deltm1 = -ddt * diss_num * (m1dx + m1dy + m1dz);
          deltm2 = -ddt * diss_num * (m2dx + m2dy + m2dz);
          deltm3 = 0;

          m1_new[k][j][i] = pG->U[k][j][i].M1 + deltm1;
          m2_new[k][j][i] = pG->U[k][j][i].M2 + deltm2;
          m3_new[k][j][i] = pG->U[k][j][i].M3 + deltm3;
        }
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost + 1; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        cc_pos(pG, i, j, k, &x1, &x2, &x3);

        if ((x2 <= height_numdiss) && (diss_nmax > 0.0))
        {
          pG->U[k][j][i].M1 = m1_new[k][j][i];
          pG->U[k][j][i].M2 = m2_new[k][j][i];
          pG->U[k][j][i].M3 = m3_new[k][j][i];
        }
      }
    }
  }

  /* Output dBx/dt and dBy/dt */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        /* Compute pBpt */
        pB1pt[k][j][i] = (pG->U[k][j][i].B1c - B1pre[k][j][i]) / pG->dt;
        pB2pt[k][j][i] = (pG->U[k][j][i].B2c - B2pre[k][j][i]) / pG->dt;

        /* reset B_pre for the next step */
        B1pre[k][j][i] = pG->U[k][j][i].B1c;
        B2pre[k][j][i] = pG->U[k][j][i].B2c;
      }
    }
  }

  return;
}

void Userwork_after_loop(MeshS *pM)
{
  return;
}

/*=========================== PRIVcharTE FUNCTIONS ==============================*/
/*---------------------------------------------------------------------------*/
/*  \funtion of Bx for initial conditions */
static Real bxini(const Real x1, const Real x2)
{
  /* Arti-parallel magnetic field */
  Real bx0 = 0.0;
  /* Add perturbation */
  Real pix, piy, bx1;

  pix = pi * x1 / lx_psipert;
  piy = 2.0 * pi * (x2 - yc_psipert) / ly_psipert;
  bx1 = (2.0 * pi / ly_psipert) * psi_pert * cos(pix) * sin(piy);
  bx0 = bx0 + bx1;
  return bx0;
}

/*----------------------------------------------------------------------------*/
/*  \funtion of By for initial conditions */
static Real byini(const Real x1, const Real x2)
{
  Real by0;
  /* case 1: Sine type sheet */
  if (cs_mode == 1)
  {
    if (x1 > cs_width)
      by0 = b0;
    if (x1 <= cs_width && x1 >= -cs_width)
    {
      by0 = sin(PI * x1 / (2.0 * cs_width));
    }
    if (x1 < -cs_width)
      by0 = -b0;
  }

  /* case 2: Harris sheet */
  if (cs_mode == 2)
  {
    by0 = b0 * tanh(x1 / cs_width);
  }

  /* case 3: Ideal sheet */
  if (cs_mode == 3)
  {
    if (x1 <= 0.0)
    {
      by0 = -b0;
    }
    else
    {
      by0 = b0;
    }
  }

  /* Add perturbation */
  Real pix, piy, by1;

  pix = pi * x1 / lx_psipert;
  piy = 2.0 * pi * (x2 - yc_psipert) / ly_psipert;
  by1 = (-pi / lx_psipert) * psi_pert * b0 * sin(pix) * cos(piy);
  by0 = by0 + by1;
  return by0;
}

/*---------------------------------------------------------------------------*/
/*  \funtion of Bz for initial conditions */
static Real bzini(const Real x1, const Real x2)
{
  Real bx0, by0, bz0, bzsq;
  Real pmag_max;
  pmag_max = 0.5 * b0 * b0 * (1.0 + psi_pert * pi / lx_psipert) * (1.0 + psi_pert * pi / lx_psipert);
  bx0 = bxini(x1, x2);
  by0 = byini(x1, x2);
  bzsq = 2.0 * pmag_max - bx0 * bx0 - by0 * by0;
  bz0 = sqrt(bzsq);
  bz0 = fact_bz * bz0;
  return bz0;
}

/*---------------------------------------------------------------------------*/
/*  \function presini */
static Real presini(const Real x1, const Real x2)
{
  Real bx, by, bz, p0, pB;
  Real pmag_max;
  pmag_max = 0.5 * b0 * b0 * (1.0 + psi_pert * pi / lx_psipert) * (1.0 + psi_pert * pi / lx_psipert);
  bx = bxini(x1, x2);
  by = byini(x1, x2);
  bz = bzini(x1, x2);
  pB = 0.5 * (bx * bx + by * by + bz * bz);
  p0 = (beta0 * pmag_max + pmag_max) - pB;
  return p0;
}

/*----------------------------------------------------------------------------*/
/*! \fn void linetiedbc_ix2(GridS *pGrid)
 *  \brief Sets boundary condition at the bottom.
 */
/*  ix2, line-tied, bottom */
void linetiedbc_ix2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
  int kl, ku;
  Real x1, x2, x3;
  int jc, j2;
  Real pjc, pjs, bxji, byji, bzji;
  Real density_ini;
  Real pbypx;
  Real d_temp[2 * nghost + (ie - is) + 1];

  if (pGrid->Nx[2] > 1)
  {
    kl = pGrid->ks - nghost;
    ku = pGrid->ke + nghost;
  }
  else
  {
    kl = pGrid->ks;
    ku = pGrid->ke;
  }

  /* All Variables */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i] = pGrid->U[k][js][i];
      }
    }
  }

  /* Bottom: B2i is not set at j=js-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        cc_pos(pGrid, i, js - j, k, &x1, &x2, &x3);
        pGrid->B2i[k][js - j][i] = byini(x1, x2);
      }
    }
  }

  /* Bottom: B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - (nghost - 1); i <= ie + nghost; i++)
      {
        cc_pos(pGrid, i, js - j, k, &x1, &x2, &x3);
        pbypx = (byini(x1, x2) - byini(x1 - pGrid->dx1, x2)) / pGrid->dx1;
        pGrid->B1i[k][js - j][i] = pGrid->B1i[k][js - j + 1][i] - pbypx * (pGrid->dx2);
      }
    }
  }

  /* Bottom: B3i */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B3i[k][js - j][i] = pGrid->B3i[k][js][i];
      }
    }
  }

  /* (1) Density */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        cc_pos(pGrid, i, js - j, k, &x1, &x2, &x3);
        pjs = (Gamma_1) * (pGrid->U[k][js][i].E - 0.5 * (SQR(pGrid->U[k][js][i].B1c) + SQR(pGrid->U[k][js][i].B2c) + SQR(pGrid->U[k][js][i].B3c)) - 0.5 * (SQR(pGrid->U[k][js][i].M1) + SQR(pGrid->U[k][js][i].M2) + SQR(pGrid->U[k][js][i].M3)) / pGrid->U[k][js][i].d);
        d_temp[i] = pjs / (beta0 / 2.0);
        pGrid->U[k][js - j][i].d = d_temp[i];
      }
    }
  }

  /* (2-4) Momentum & Energy */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {

        pGrid->U[k][js - j][i].M1 = 0;
        pGrid->U[k][js - j][i].M2 = 0;
        pGrid->U[k][js - j][i].M3 = 0;

        pGrid->U[k][js - j][i].E = pGrid->U[k][js][i].E - 0.5 * (SQR(pGrid->U[k][js][i].M1) + SQR(pGrid->U[k][js][i].M2) + SQR(pGrid->U[k][js][i].M3)) / pGrid->U[k][js][i].d;
      }
    }
  }

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void openbc_ox2(GridS *pGrid)
 *  \brief open boundary conditions, Outer x2 boundary (bc_ox2=2) */
/*----------------------------------------------------------------------------*/
/*  ox2, top boundary */
void openbc_ox2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ku; /* k-upper */
#endif
  Real gamma;
  int jc;

  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][je + j][i] = pGrid->U[k][je][i];
      }
    }
  }

#ifdef MHD
  /* B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B1i[k][je + j][i] = 0.0;
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][je + j][i].E = pGrid->U[k][je + j][i].E - 0.5 * (SQR(pGrid->U[k][je + j][i].B1c));
        pGrid->U[k][je + j][i].B1c = 0.0;
      }
    }
  }

  /* j=je+1 is not a boundary condition for the interface field B2i */
  for (k = ks; k <= ke; k++)
  {
    for (j = 2; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B2i[k][je + j][i] = pGrid->B2i[k][je][i];
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B3i[k][je + j][i] = pGrid->B3i[k][je][i];
      }
    }
  }
#endif /* MHD */
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void openbc_ix1(GridS *pGrid)
 *  \brief open boundary condition, Inner x1 boundary (bc_ix1=2)              */
/*----------------------------------------------------------------------------*/
/*  ix1, Left boundary */
void openbc_ix1(GridS *pGrid)
{
  int is = pGrid->is;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;

#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif
  int ic;

  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][is - i] = pGrid->U[k][j][is];
      }
    }
  }

#ifdef MHD
  /* B2i */
  if (pGrid->Nx[1] > 1)
    ju = je + 1;
  else
    ju = je;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= ju; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B2i[k][j][is - i] = pGrid->B2i[k][j][is];
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        ic = is - i;
        pGrid->U[k][j][ic].B2c = 0.5 * (pGrid->B2i[k][j][ic] + pGrid->B2i[k][j + 1][ic]);
      }
    }
  }

  /* B1i is not set at i=is-nghost */
  Real db2, dx1odx2;
  dx1odx2 = pGrid->dx1 / pGrid->dx2;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost - 1; i++)
      {
        /*pGrid->B1i[k][j][is-i] = pGrid->B1i[k][j][is];*/
        ic = is - i;
        db2 = pGrid->B2i[k][j + 1][ic] - pGrid->B2i[k][j][ic];
        pGrid->B1i[k][j][ic] = pGrid->B1i[k][j][ic + 1] + dx1odx2 * db2;
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost - 1; i++)
      {
        ic = is - i;
        pGrid->U[k][j][ic].B1c = 0.5 * (pGrid->B1i[k][j][ic] + pGrid->B1i[k][j][ic + 1]);
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B3i[k][j][is - i] = pGrid->B3i[k][j][is];
      }
    }
  }
  for (k = ks; k <= ku; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][is - i].B3c = pGrid->B3i[k][j][is - i];
      }
    }
  }
#endif /* MHD */

  /* Density and Pressure */
  Real pismi;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost - 1; i++)
      {
        ic = is - i;
        pismi = Gamma_1 * (pGrid->U[k][j][is].E - 0.5 * (SQR(pGrid->U[k][j][is].B1c) + SQR(pGrid->U[k][j][is].B2c) + SQR(pGrid->U[k][j][is].B3c)) - 0.5 * (SQR(pGrid->U[k][j][is].M1) + SQR(pGrid->U[k][j][is].M2) + SQR(pGrid->U[k][j][is].M3)) / pGrid->U[k][j][is].d);

        pGrid->U[k][j][ic].E = pismi / Gamma_1 + 0.5 * (SQR(pGrid->U[k][j][ic].B1c) + SQR(pGrid->U[k][j][ic].B2c) + SQR(pGrid->U[k][j][ic].B3c)) + 0.5 * (SQR(pGrid->U[k][j][ic].M1) + SQR(pGrid->U[k][j][ic].M2) + SQR(pGrid->U[k][j][ic].M3)) / pGrid->U[k][j][ic].d;
      }
    }
  }
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void openbc_ox1(GridS *pGrid)
 *  \brief openbc boundary conditions, Outer x1 boundary (bc_ox1=2) */
/*----------------------------------------------------------------------------*/
/* Right open boundary */
void openbc_ox1(GridS *pGrid)
{
  int ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;

#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif
  int ic;
  Real x1, x2, x3;

  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][ie + i] = pGrid->U[k][j][ie];
      }
    }
  }

#ifdef MHD
  /* B2i */
  if (pGrid->Nx[1] > 1)
    ju = je + 1;
  else
    ju = je;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= ju; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B2i[k][j][ie + i] = pGrid->B2i[k][j][ie];
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        ic = ie + i;
        pGrid->U[k][j][ic].B2c =
            0.5 * (pGrid->B2i[k][j][ic] + pGrid->B2i[k][j + 1][ic]);
      }
    }
  }

  /* i=ie+1 is not a boundary condition for the interface field B1i */
  Real dx1odx2, db2;
  dx1odx2 = pGrid->dx1 / pGrid->dx2;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = ie + 2; i <= ie + nghost; i++)
      {
        /*pGrid->B1i[k][j][ie+i] = pGrid->B1i[k][j][ie];*/
        db2 = pGrid->B2i[k][j + 1][i - 1] - pGrid->B2i[k][j][i - 1];
        pGrid->B1i[k][j][i] = pGrid->B1i[k][j][i - 1] - dx1odx2 * db2;
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost - 1; i++)
      {
        pGrid->U[k][j][ie + i].B1c =
            0.5 * (pGrid->B1i[k][j][ie + i] + pGrid->B1i[k][j][ie + i + 1]);
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B3i[k][j][ie + i] = pGrid->B3i[k][j][ie];
      }
    }
  }
  for (k = ks; k <= ku; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][ie + i].B3c = pGrid->B3i[k][j][ie + i];
      }
    }
  }

#endif /* MHD */
  /* Density and Pressure */
  Real piepi;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = ie + 1; i <= ie + nghost - 1; i++)
      {

        piepi =
            Gamma_1 *
            (pGrid->U[k][j][ie].E -
             0.5 * (SQR(pGrid->U[k][j][ie].B1c) + SQR(pGrid->U[k][j][ie].B2c) +
                    SQR(pGrid->U[k][j][ie].B3c)) -
             0.5 *
                 (SQR(pGrid->U[k][j][ie].M1) + SQR(pGrid->U[k][j][ie].M2) +
                  SQR(pGrid->U[k][j][ie].M3)) /
                 pGrid->U[k][j][ie].d);

        pGrid->U[k][j][i].E =
            piepi / (Gamma_1) +
            0.5 * (SQR(pGrid->U[k][j][i].B1c) + SQR(pGrid->U[k][j][i].B2c) +
                   SQR(pGrid->U[k][j][i].B3c)) +
            0.5 *
                (SQR(pGrid->U[k][j][i].M1) + SQR(pGrid->U[k][j][i].M2) +
                 SQR(pGrid->U[k][j][i].M3)) /
                pGrid->U[k][j][i].d;
      }
    }
  }
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void symmbc_ix1(GridS *pGrid)
 *  \brief Left, sysmmetry boundary condition, Inner x1 boundary (bc_ix1=2) */

static void symmbc_ix1(GridS *pGrid)
{
  int is = pGrid->is;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][is - i] = pGrid->U[k][j][is + i - 1];
        /* monmentum */
        pGrid->U[k][j][is - i].M1 = -pGrid->U[k][j][is + i - 1].M1;
        /* Magnetic */
        pGrid->U[k][j][is - i].B2c = -pGrid->U[k][j][is + i - 1].B2c;
      }
    }
  }

#ifdef MHD
  /* B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost - 1; i++)
      {
        pGrid->B1i[k][j][is - i] = pGrid->B1i[k][j][is + i];
      }
    }
  }

  if (pGrid->Nx[1] > 1)
    ju = je + 1;
  else
    ju = je;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= ju; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B2i[k][j][is - i] = -pGrid->B2i[k][j][is + i - 1];
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B3i[k][j][is - i] = pGrid->B3i[k][j][is + i - 1];
      }
    }
  }
#endif /* MHD */

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void symmbc_ox1(GridS *pGrid)
 *  \brief Right, symmetry boundary conditions, Outer x1 boundary (bc_ox1=2) */
/*----------------------------------------------------------------------------*/
/* Right boundary at y = 0, symmetry condition */

void symmbc_ox1(GridS *pGrid)
{
  int ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;

#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][ie + i] = pGrid->U[k][j][ie - i + 1];
        /* monmentum */
        pGrid->U[k][j][ie + i].M1 = -pGrid->U[k][j][ie - i + 1].M1;
        /* Magnetic */
        pGrid->U[k][j][ie + i].B2c = -pGrid->U[k][j][ie - i + 1].B2c;
      }
    }
  }

#ifdef MHD
  /* B2i */
  if (pGrid->Nx[1] > 1)
    ju = je + 1;
  else
    ju = je;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= ju; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B2i[k][j][ie + i] = -pGrid->B2i[k][j][ie - i + 1];
      }
    }
  }

  /* i=ie+1 is not a boundary condition for the interface field B1i */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 2; i <= nghost; i++)
      {
        pGrid->B1i[k][j][ie + i] = pGrid->B1i[k][j][ie - i + 2];
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B3i[k][j][ie + i] = pGrid->B3i[k][j][ie - i + 1];
      }
    }
  }
#endif /* MHD */
  return;
}
