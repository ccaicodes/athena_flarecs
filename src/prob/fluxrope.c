#include "copyright.h"
/*============================================================================*/
/*! \file fluxrope.c
 *  \brief  Problem generator for Fluxrope.
 *
 * PURPOSE: flux rope.
 *
 * REFERENCE: Wang, Shen, & Lin, ApJ 2009.
 *
 * Update:
 *  2018-03-30
 *    Starting version.
 */

/*============================================================================*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defs.h"
#include "athena.h"
#include "globals.h"
#include "prototypes.h"
#include "./prob/simpson_integ.c"

/*==============================================================================
 * PRIVATE FUNCTION PROTOTYPES:
 * void flarecs_linetied() - sets BCs on R-x2 boundary
 *============================================================================*/
static Real presini(const Real x1, const Real x2);
static Real densini(const Real x1, const Real x2);
static Real teini(const Real x1, const Real x2);

static Real presini_isote(const Real x2, const Real y0,
                          const Real p0, const Real te0);
Real presini_integral(Real x2);
Real densini_integral(Real x2);

static Real gsun_static(const Real x1, const Real x2, const Real x3);
static Real gsun_y(const Real x2);

static void openbc_ox2(GridS *pGrid);
static void linetiedbc_ix2(GridS *pGrid);

Real my_integ_py(Real y);

/* functions for compute flux */
static Real func_bphi(const Real r);
static Real func_rjphi(const Real r);
static Real func_uphi(const Real r);

static Real func_bmx(const Real x1, const Real x2);
static Real func_bmy(const Real x1, const Real x2);
static Real func_bmz(const Real x1, const Real x2);
static Real func_uphi_xy(const Real x, const Real y);

Real my_inter_pphi(Real r);
Real my_inter_bx(Real y);
Real my_inter_by(Real x);
Real x_current, y_current, dx_grid, dy_grid, dz_grid;

/*============================================================================*/
/* Normalization parameters */
Real Lchar, Bchar, Nechar, Rhochar, Pchar, Timechar, Tchar, Vchar;
Real Gsun_nondim;

/* a. input parameters: pressure and temperature in chromosphere and corona */
Real Te_corona = 1.0e6, Te_photos = 1.0e4;

/* b. input parameters: height and width for the TR region */
Real Height_TR = 3.0e6, Width_TR = 1.0e6;

/* c. The height for define the pgas_c in corona */
Real pgas_c = 0.05, posi_c = 1.0;

/* Constant parameters */
Real Kb = 1.38e-23, Mp = 1.6726e-27, Mu0 = 1.25663706144e-6, LnA = 30.0;
Real gravitational_const = 6.672e-11; /* (N M^2 kg^-2)*/
Real solarmass = 1.99e+30;            /* (kg) */
Real solarradius = 6.96e+8;           /* (m) */

/* Initial conditions */
Real ***pgasini, ***rhoini, ***tempini;

/* Flux parameters */
Real fr_h, fr_hb, fr_ri, fr_del, fr_rja, fr_rmom;
Real fr_xc; // the center of flux rope.

/*=========================== PUBLIC FUNCTIONS ===============================*/
/*----------------------------------------------------------------------------*/
/* problem:  */

void problem(DomainS *pDomain)
{
  GridS *pGrid = (pDomain->Grid);
  int i, is = pGrid->is, ie = pGrid->ie;
  int j, js = pGrid->js, je = pGrid->je;
  int k, ks = pGrid->ks, ke = pGrid->ke;
  int n1 = ie - is + 1 + 2 * nghost;
  int n2 = je - js + 1 + 2 * nghost;
  int n3 = ke - ks + 1 + 2 * nghost;

  Real x1, x2, x3;
  Real x1c, x2c, x3c;
  Real x1f, x2f, x3f;
  Real x1s, x2s, x3s;
  Real r;
  Real ***bx, ***by, ***bz, ***az;

  /* Inilize pgasini, rhoini, and tempini */
  if ((pgasini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for pgasini \n");
  }
  if ((rhoini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for rhoini \n");
  }
  if ((tempini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for tempini \n");
  }

  /* Termal conduction & viscosity */
  Real k_spitzer, kappa_v;
  Real nud, nu0;

  /* Magnitude of perturbations */
  Real pi;
  Real vin_turb;
  Real Lx, Ly, delta_bx, delta_by;
  Real pos_x_turb, pos_y_turb, hwd_x_turb, hwd_y_turb;

  /* Dence bottom boundary */
  Real htr, wtr, chr_rho, cor_rho;

  /* Read input parameters */
  Lchar = par_getd("problem", "Lchar");
  Bchar = par_getd("problem", "Bchar");
  Nechar = par_getd("problem", "Nechar");

  Pchar = Bchar * Bchar / Mu0;
  Rhochar = Nechar * Mp;
  Tchar = Pchar / (Nechar * 2. * Kb); /*total number is 2*ne for full ionization plasma */
  Vchar = Bchar / sqrt(Mu0 * Rhochar);
  Timechar = Lchar / Vchar;

  /* Print*/
  int myid;
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  if (myid == 0)
  {
    printf("Bchar=%10.3e (T)\n", Bchar);
    printf("nechar=%10.3e (cm^-3), Rhochar=%10.3e (kg/m^3)\n",
           Nechar / 1.0e6, Rhochar);
    printf("Pchar=%10.3e (Pa)\n", Pchar);
    printf("Tchar=%10.3e (K)\n", Tchar);
    printf("Vchar=%10.3e (km/s)\n", Vchar / 1.0e3);
    printf("Lchar=%10.3e (km)\n", Lchar / 1.0e3);
    printf("Timechar=%10.3e (s)\n", Timechar);
  }

  /* Initialise fluxrope */
  fr_h = par_getd("problem", "h");
  fr_hb = par_getd("problem", "hb");
  fr_ri = par_getd("problem", "ri");
  fr_del = par_getd("problem", "del");
  fr_rmom = par_getd("problem", "rmom");
  fr_rja = par_getd("problem", "rja");

  // reset fr_h to the corner of cell
  k = (int)(fr_h / pGrid->dx2);
  fr_h = (k + 0.5) * pGrid->dx2;
  fr_xc = 0.5 * pGrid->dx1;
  //printf("fr_xc=%.14f, fr_h=%.14f\n", fr_xc, fr_h);

  if ((bx = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector bx\n");
  }
  if ((by = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector by\n");
  }
  if ((bz = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector bz\n");
  }
  if ((az = (Real ***)calloc_3d_array(n3 + 1, n2 + 1, n1 + 1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector az\n");
  }

  /* Compute initial magnetic vector potential: az */
  Real azax, az0 = 0.0, x00 = 5.0, y00 = 5.0;
  dx_grid = pGrid->dx1;
  dy_grid = pGrid->dx2;
  dz_grid = pGrid->dx3;
  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost; j <= je + nghost + 1; j++)
    {
      for (i = is - nghost; i <= ie + nghost + 1; i++)
      {
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
        x1f = x1c - 0.5 * dx_grid;
        x2f = x2c - 0.5 * dy_grid;
        x3f = x3c - 0.5 * dz_grid;

        if (i == ie + nghost + 1)
        {
          cc_pos(pGrid, ie + nghost, j, k, &x1c, &x2c, &x3c);
          x1f = x1c + 0.5 * dx_grid;
        }
        if (j == je + nghost + 1)
        {
          cc_pos(pGrid, i, je + nghost, k, &x1c, &x2c, &x3c);
          x2f = x2c + 0.5 * dy_grid;
        }

        /* First integrate along x- */
        y_current = y00;
        azax = az0 + adaptiveSimpsons(my_inter_by, x00, x1f, 1.0e-9, 1000);
        /* Then integrate along y- */
        x_current = x1f;
        azini[k][j][i] = azax + adaptiveSimpsons(my_inter_bx, y00, x2f, 1.0e-9, 1000);
      }
    }
  }

  /* Compute initial pressure, temperature and density */
  Real pgas0, rho0, te0;
  Real pa, pb, ta, tb, rhoa, rhob;
  Real ya, yb, dx2half = 0.5 * pGrid->dx2;
  Real pcell, dcell;
  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost; j <= je + nghost; j++)
    {

      /* get pgas0 and te0 along y- in the background (no flux) */
      cc_pos(pGrid, is, j, k, &x1, &x2, &x3);
      te0 = teini(x1, x2);

      if (te0 >= Te_corona / Tchar)
      {
        pgas0 = presini_isote(x2, posi_c, pgas_c, te0);
        rho0 = pgas0 / te0;
      }
      else
      {
        ya = x2 - dx2half;
        yb = x2 + dx2half;
        pgas0 = presini_integral(x2);
      }

      for (i = is - nghost; i <= ie + nghost; i++)
      {
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
        pgasini[k][j][i] = pgas0 - func_uphi_xy(x1c, x2c);
        tempini[k][j][i] = te0;
        rhoini[k][j][i] = pgasini[k][j][i] / te0;
      }
    }
  }

  /* Calculate bx, by */
  for (k = ks; k <= ke + 1; k++)
  {
    for (j = js; j <= je + 1; j++)
    {
      for (i = is; i <= ie + 1; i++)
      {
        bx[k][j][i] = (azini[k][j + 1][i] - azini[k][j][i]) / dy_grid;
        by[k][j][i] = -(azini[k][j][i + 1] - azini[k][j][i]) / dx_grid;
        bz[k][j][i] = 0.0;
      }
    }
  }

  /* All variables */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        /* axis */
        cc_pos(pGrid, i, j, k, &x1, &x2, &x3);

        /* density */
        pGrid->U[k][j][i].d = rhoini[k][j][i];

        /* monentum */
        pGrid->U[k][j][i].M1 = 0.0;
        pGrid->U[k][j][i].M2 = 0.0;
        pGrid->U[k][j][i].M3 = 0.0;

        /* magnetic field */
        pGrid->B1i[k][j][i] = bx[k][j][i];
        pGrid->B2i[k][j][i] = by[k][j][i];
        pGrid->B3i[k][j][i] = bz[k][j][i];

        /* initial setting for magnetic field on the boundary */
        if (i == ie && ie > is)
          pGrid->B1i[k][j][i + 1] = bx[k][j][i + 1];
        if (j == je && je > js)
          pGrid->B2i[k][j + 1][i] = by[k][j + 1][i];
        if (k == ke && ke > ks)
          pGrid->B3i[k + 1][j][i] = bz[k + 1][j][i];
      }
    }
  }

  /* cell-center magnetic field */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        pGrid->U[k][j][i].B1c = 0.5 * (pGrid->B1i[k][j][i] +
                                       pGrid->B1i[k][j][i + 1]);
        pGrid->U[k][j][i].B2c = 0.5 * (pGrid->B2i[k][j][i] +
                                       pGrid->B2i[k][j + 1][i]);
        pGrid->U[k][j][i].B3c = pGrid->B3i[k][j][i];
      }
    }
  }

  /* total energy*/
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {

        pGrid->U[k][j][i].E = pgasini[k][j][i] / (Gamma_1) + 0.5 * (SQR(pGrid->U[k][j][i].B1c) + SQR(pGrid->U[k][j][i].B2c) + SQR(pGrid->U[k][j][i].B3c)) + 0.5 * (SQR(pGrid->U[k][j][i].M1) + SQR(pGrid->U[k][j][i].M2) + SQR(pGrid->U[k][j][i].M3)) / pGrid->U[k][j][i].d;
      }
    }
  }

  /* Set resistivity */
#ifdef RESISTIVITY
  eta_Ohm = par_getd("problem", "eta_Ohm");
  Q_AD = par_getd("problem", "Q_AD");
  Q_Hall = 0.0;
  d_ind = 0.0;
#endif

  /* Set thermel conduction coefficient */

  /* Set viscosity */

  /* Enroll gravitational potential to give acceleration in y-direction for 2D */
  StaticGravPot = gsun_static;

  /* Set optical thin radiation cooling and corona heating function */

  /* Set boundary value functions */
  /* (a) left-open */

  /* (b) Right-open */

  /* (c) Top-open */
  bvals_mhd_fun(pDomain, right_x2, openbc_ox2);

  /* (d) Bottom-line-tied */
  bvals_mhd_fun(pDomain, left_x2, linetiedbc_ix2);
}

/*==============================================================================
 * PROBLEM USER FUNCTIONS:
 * problem_write_restart() - writes problem-specific user data to restart files
 * problem_read_restart()  - reads problem-specific user data from restart files
 * get_usr_expr()          - sets pointer to expression for special output data
 * get_usr_out_fun()       - returns a user defined output function pointer
 * get_usr_par_prop()      - returns a user defined particle selection function
 * Userwork_in_loop        - problem specific work IN     main loop
 * Userwork_after_loop     - problem specific work AFTER  main loop
 *----------------------------------------------------------------------------*/

void problem_write_restart(MeshS *pM, FILE *fp)
{
  return;
}

void problem_read_restart(MeshS *pM, FILE *fp)
{
  return;
}

ConsFun_t get_usr_expr(const char *expr)
{
  return NULL;
}

VOutFun_t get_usr_out_fun(const char *name)
{
  return NULL;
}

#ifdef RESISTIVITY

void get_eta_user(GridS *pG, int i, int j, int k,
                  Real *eta_O, Real *eta_H, Real *eta_A)
{
  return;
}
#endif

void Userwork_in_loop(MeshS *pM)
{
  GridS *pG = pM->Domain[0][0].Grid;
  int is = pG->is, ie = pG->ie;
  int js = pG->js, je = pG->je;
  int ks = pG->ks, ke = pG->ke;
  int i, j, k;
  Real x1, x2, x3;
  /*  Pressure floor */
  Real dens_floor = 1.0;
  Real pres_floor = 0.5;
  Real pres_c, dpres;
  Real msqr, bsqr;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        msqr = SQR(pG->U[k][j][i].M1) + SQR(pG->U[k][j][i].M2) + SQR(pG->U[k][j][i].M3);
        bsqr = SQR(pG->U[k][j][i].B1c) + SQR(pG->U[k][j][i].B2c) + SQR(pG->U[k][j][i].B3c);
        pres_c = Gamma_1 * (pG->U[k][j][i].E - 0.5 * msqr / pG->U[k][j][i].d - 0.5 * bsqr);

        pG->U[k][j][i].d = MAX(pG->U[k][j][i].d, dens_floor);
        pres_c = MAX(pres_c, pres_floor);

        pG->U[k][j][i].E = pres_c / Gamma_1 + 0.5 * msqr / pG->U[k][j][i].d + 0.5 * bsqr;
      }
    }
  }
  return;
}

void Userwork_after_loop(MeshS *pM)
{
  return;
}

/*=========================== PRIVATE FUNCTIONS ==============================*/
/*----------------------------------------------------------------------------*/
/*  \fn Initial Temperature */
static Real teini(const Real x1, const Real x2)
{
  /* Non-dimensional variables */
  Real techr, tecor, h, w;
  Real t1, t2, te;

  /* The following calculation is in non-dimensional forms */
  techr = Te_photos / Tchar;
  tecor = Te_corona / Tchar;
  h = Height_TR / Lchar;
  w = Width_TR / Lchar;

  /* Temperature */
  t1 = 0.5 * (tecor - techr);
  t2 = 0.5 * (tecor + techr);
  te = t1 * tanh((x2 - h) / w) + t2;

  return te;
}

/*----------------------------------------------------------------------------*/
/*! \fn void linetiedbc_ix2(GridS *pGrid)
 *  \brief Sets boundary condition at the bottom.
 */
/*  ix2, line-tied, bottom */
/* Update:
 *   2018-03-06: 
 *   Check the reflection on the bottom: m2 = m2 in ghost zone.*/
void linetiedbc_ix2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ku; /* k-upper */
#endif

  /* Local variables */
  Real x1, x2, x3;
  Real pjs, pjsmj, Tjsmj, yjs, yjsmj;
  Real pbypx, pgas;

  Real te_js, p_js, eb_js, ek_js;
  Real te_mj, p_mj, eb_mj, ek_mj;
  Real y_js, y_mj;
  int j_mj;

  /* Set all variables in ghost zone */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i] = pGrid->U[k][js][i];
        pGrid->U[k][js - j][i].M1 = 0.0;
        pGrid->U[k][js - j][i].M2 = 0.0;
        pGrid->U[k][js - j][i].M3 = 0.0;
      }
    }
  }

#ifdef MHD
  /* B2i*/
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B2i[k][js - j][i] = by[k][js - j][i];
      }
    }
  }
  /* Bottom: B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - (nghost - 1); i <= ie + nghost; i++)
      {
        pGrid->B1i[k][js - j][i] = pGrid->B1i[k][js][i];
      }
    }
  }
  /* Bottom: B3i */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B3i[k][js - j][i] = pGrid->B3i[k][js][i];
      }
    }
  }

#endif /* MHD */

  /* Pressure and Total Energy */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        j_mj = js - j;
        p_mj = pgasini[k][j_mj][i];
        pGrid->U[k][j_mj][i].d = rhoini[k][j_mj][i];

        eb_mj = 0.5 * (SQR(pGrid->U[k][j_mj][i].B1c) + SQR(pGrid->U[k][j_mj][i].B2c) + SQR(pGrid->U[k][j_mj][i].B3c));
        ek_mj = 0.5 * (SQR(pGrid->U[k][j_mj][i].M1) + SQR(pGrid->U[k][j_mj][i].M2) + SQR(pGrid->U[k][j_mj][i].M3)) / pGrid->U[k][j_mj][i].d;

        pGrid->U[k][j_mj][i].E = p_mj / Gamma_1 + eb_mj + ek_mj;
      }
    }
  }
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void openbc_ox2(GridS *pGrid)
 *  \brief open boundary conditions, Outer x2 boundary (bc_ox2=2) */
/*----------------------------------------------------------------------------*/
/*  ox2, top boundary */
void openbc_ox2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ku; /* k-upper */
#endif
  Real x1, x2, x3;

  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][je + j][i] = pGrid->U[k][je - j + 1][i];
        pGrid->U[k][je + j][i].B1c = -pGrid->U[k][je - j + 1][i].B1c;
      }
    }
  }

#ifdef MHD
  /* B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - (nghost - 1); i <= ie + nghost; i++)
      {
        pGrid->B1i[k][je + j][i] = -pGrid->B1i[k][je - j + 1][i];
      }
    }
  }

  /* B2i: j=je+1 is not a boundary condition for the interface field B2i */
  for (k = ks; k <= ke; k++)
  {
    for (j = 2; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B2i[k][je + j][i] = pGrid->B2i[k][je - j + 2][i];
      }
    }
  }

  /* B3i: */
  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B3i[k][je + j][i] = pGrid->B3i[k][je - j + 1][i];
      }
    }
  }
#endif
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static Real gsun_static(const Real x1, const Real x2, const Real x3)
 *  \brief Gravitational potential; g = 0.1
 */
static Real gsun_static(const Real x1, const Real x2, const Real x3)
{
  Real gp;
  Real safety = 1.0;
  /* gravity potential */
  gp = -(gravitational_const * solarmass) / (x2 * Lchar + solarradius);
  /* Nondimensional gp */
  gp = gp * Rhochar / Pchar;
  return gp;
}

/*----------------------------------------------------------------------------*/
/*! \fn static Real gsun_r(const Real x2)
 *  \brief Nondimensional Gravitaty at height x2
 */
static Real gsun_y(const Real x2)
{
  Real g, gnondim;
  Real r;
  r = solarradius + x2 * Lchar;                    /* (m) */
  g = (gravitational_const * solarmass) / (r * r); /* (m s^-2)*/
  gnondim = g * Rhochar * Lchar / Pchar;
  return gnondim;
}

/*-----------------------------------------------------------------------------
 * \fn Real my_integ_py                                  
 * \brief Integration function to get integ(gsun(y)/T(y))dy
 * ---------------------------------------------------------------------------*/
Real my_integ_py(Real y)
{
  return gsun_y(y) / teini(0.0, y);
}

/*----------------------------------------------------------------------------*/
/*  \fn gas pressure assuming constant temperature */
static Real presini_isote(const Real x2, const Real y0,
                          const Real p0, const Real te0)
{
  Real pgas, gm, r, r0;
  gm = gravitational_const * solarmass * Rhochar / (Lchar * Pchar);
  r = solarradius / Lchar + x2;
  r0 = solarradius / Lchar + y0;
  pgas = p0 * exp(gm / te0 * (1.0 / r - 1.0 / r0));
  return pgas;
}

/*----------------------------------------------------------------------------*/
/*  \fn gas pressure performing numerical integration */
Real presini_integral(Real x2)
{
  Real pgas;
  Real Iout_simp;

  /* Simpson's rule */
  Iout_simp = -adaptiveSimpsons(my_integ_py, posi_c, x2, 1.0e-10, 10000);

  pgas = pgas_c * exp(Iout_simp);
  return pgas;
}

/*----------------------------------------------------------------------------*/
/*  \fn density performing numerical integration */
Real densini_integral(Real x2)
{
  Real rho;
  rho = presini_integral(x2) / teini(0.0, x2);
  return rho;
}

/* ============================================================================
 * Functions for fluxrope
 * ===========================================================================*/
/*----------------------------------------------------------------------------*/
static Real func_bmx(const Real x1, const Real x2)
{
  /*c model field x-component */
  Real rs, rm, rb;
  Real r1, r2;
  Real bmx;

  r1 = fr_ri - 0.5 * fr_del;
  r2 = fr_ri + 0.5 * fr_del;

  rs = sqrt(pow(x1 - fr_xc, 2) + (x2 - fr_h) * (x2 - fr_h));
  rm = sqrt(pow(x1 - fr_xc, 2) + (x2 + fr_h) * (x2 + fr_h));
  rb = sqrt(pow(x1 - fr_xc, 2) + (x2 + fr_hb) * (x2 + fr_hb));

  if (rs > 0.0)
  {
    /* quadrupole */
    bmx = func_bphi(rs) * (x2 - fr_h) / rs - func_bphi(rm) * (x2 + fr_h) / rm - func_bphi(r2) * fr_rmom * fr_hb * r2 * (x2 + fr_hb) * (3.0 * pow(x1 - fr_xc, 2) - pow((x2 + fr_hb), 2)) / pow(rb, 6);
    /* dipole case */
    bmx = func_bphi(rs) * (x2 - fr_h) / rs - func_bphi(rm) * (x2 + fr_h) / rm - func_bphi(r2) * fr_rmom * fr_hb * r2 * (pow(x1 - fr_xc, 2) - (x2 + fr_hb) * (x2 + fr_hb)) / pow(rb, 4);
  }
  else
  {
    bmx = 0.0;
  }
  return bmx;
}

/*----------------------------------------------------------------------------*/
static Real func_bmy(const Real x1, const Real x2)
{
  /*  model field z-component */
  Real rs, rm, rb;
  Real r1, r2;
  Real bmy;

  r1 = fr_ri - 0.5 * fr_del;
  r2 = fr_ri + 0.5 * fr_del;

  rs = sqrt(pow(x1 - fr_xc, 2) + (x2 - fr_h) * (x2 - fr_h));
  rm = sqrt(pow(x1 - fr_xc, 2) + (x2 + fr_h) * (x2 + fr_h));
  rb = sqrt(pow(x1 - fr_xc, 2) + (x2 + fr_hb) * (x2 + fr_hb));

  if (rs > 0.0)
  {
    /* quadrupole case */
    bmy = -func_bphi(rs) * (x1 - fr_xc) / rs + func_bphi(rm) * (x1 - fr_xc) / rm - func_bphi(r2) * fr_rmom * fr_hb * r2 * (x1 - fr_xc) * (-pow(x1 - fr_xc, 2) + 3.0 * pow((x2 + fr_hb), 2)) / pow(rb, 6);
    /* dipole case */
    bmy = -func_bphi(rs) * (x1 - fr_xc) / rs + func_bphi(rm) * (x1 - fr_xc) / rm - func_bphi(r2) * fr_hb * fr_rmom * r2 * 2.0 * (x1 - fr_xc) * (x2 + fr_hb) / pow(rb, 4);
  }
  else
  {
    bmy = 0.0;
  }
  return bmy;
}

/* ---------------------------------------------------------------------------*/
static Real func_bmz(const Real x1, const Real x2)
{
  Real bmz, rc;
  Real r = sqrt(pow(x1 - fr_xc, 2) + pow(x2 - fr_h, 2));
  Real r_minimum = fabs(fr_xc);
  rc = MAX(r, r_minimum);
  bmz = sqrt(2.0 * fabs(func_uphi(rc)));
  return bmz;
}

/*----------------------------------------------------------------------------*/
static Real func_bphi(const Real r)
{
  /* cylindrical field function */
  Real riq, delq, piq, t1, t2, t3, bphi;
  Real pi = 3.14159265358979;
  Real ro, r1, r2;

  r1 = fr_ri - 0.5 * fr_del;
  r2 = fr_ri + 0.5 * fr_del;

  riq = fr_ri * fr_ri;
  delq = fr_del * fr_del;
  piq = pi * pi;

  if (r <= r1)
  {
    bphi = -0.5 * fr_rja * r;
  }
  else if (r <= r2)
  {
    t1 = 0.5 * r1 * r1 - delq / piq + 0.5 * r * r;
    t2 = (fr_del * r / pi) * sin((pi / fr_del) * (r - r1));
    t3 = (delq / piq) * cos((pi / fr_del) * (r - r1));
    bphi = -0.5 * fr_rja * (t1 + t2 + t3) / r;
  }
  else
  {
    bphi = -0.5 * fr_rja * (riq + 0.25 * delq - 2. * delq / piq) / r;
  }
  return bphi;
}

/*----------------------------------------------------------------------------*/
static Real func_rjphi(const Real r)
{
  /*  current density */
  Real pi = 3.14159265358979;
  Real ro, r1, r2;
  Real rjphi;

  r1 = fr_ri - 0.5 * fr_del;
  r2 = fr_ri + 0.5 * fr_del;

  if (r <= r1)
  {
    rjphi = 1.0 * fr_rja;
  }
  else if (r <= r2)
  {
    rjphi = 0.5 * fr_rja * (cos((pi / fr_del) * (r - r1)) + 1.);
  }
  else
  {
    rjphi = 0.;
  }
  return rjphi;
}

/*----------------------------------------------------------------------------*/
static Real func_uphi_xy(const Real x, const Real y)
{
  Real r = sqrt(pow(x - fr_xc, 2) + pow(y - fr_h, 2));
  return func_uphi(r);
}

/*----------------------------------------------------------------------------*/
static Real func_uphi(const Real r)
{
  Real uphi;
  uphi = adaptiveSimpsons(my_inter_pphi, r, 1.0, 1.0e-7, 1000);
  return uphi;
}

/* ----------------------------------------------------------------------------
 * for integration of Pphi
 * ---------------------------------------------------------------------------*/
Real my_inter_pphi(Real r)
{
  return func_rjphi(r) * func_bphi(r);
}

/* ----------------------------------------------------------------------------
 * for integration of bx(x0, y)
 * ---------------------------------------------------------------------------*/
Real my_inter_bx(Real y)
{
  Real x0 = x_current;
  return func_bmx(x0, y);
}

/* ----------------------------------------------------------------------------
 * for integration of -by(x, y0)
 * ---------------------------------------------------------------------------*/
Real my_inter_by(Real x)
{
  Real y0 = y_current;
  return -func_bmy(x, y0);
}