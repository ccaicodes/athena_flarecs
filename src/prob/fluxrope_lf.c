#include "copyright.h"
/*============================================================================*/
/*! \file fluxrope_it.c
 *  \brief  Problem generator for Fluxrope.
 *
 * PURPOSE: Test the initial fluxrope.
 *
 * REFERENCE: Lin & Forbes 2000.
 *
 * Update:
 *  2019-02-09
 *    Starting version.
 *  2019-02-16
 *    Include gravity.
 */

/*============================================================================*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defs.h"
#include "athena.h"
#include "globals.h"
#include "prototypes.h"

/*==============================================================================
 * PRIVATE FUNCTION PROTOTYPES:
 * void flarecs_linetied() - sets BCs on R-x2 boundary
 *============================================================================*/
static Real func_teini(const Real x1, const Real x2);

/* functions for compute flux */
static Real func_bphi(const Real r);
static Real func_rjphi(const Real r);
static Real func_rjphi_bg(const Real r);
static Real func_rjphi_rope(const Real r);

static Real func_uphi_bg(const Real r);
static Real func_uphi_rope(const Real r);

static Real func_bmx(const Real x, const Real y);
static Real func_bmy(const Real x, const Real y);
static Real func_azbg(const Real x, const Real y);
static Real func_azini(const Real x, const Real y);

/* boundary */
static void openbc_ix1(GridS *pGrid);
static void openbc_ox1(GridS *pGrid);
static void symmbc_ox1(GridS *pGrid);
static void openbc_ix2(GridS *pGrid);
static void openbc_ox2(GridS *pGrid);
static void linetiedbc_ix2(GridS *pGrid);
static Real func_pbypxini(Real x, Real y);
static Real func_2nd_extrap(const Real x1, const Real x2, const Real x3,
                            const Real f1, const Real f2, const Real f3,
                            const Real yc);

/* Functions about gravity */
static Real gsun_static(const Real x1, const Real x2, const Real x3);
static Real gsun_y(const Real x2);
static Real func_py_integral(Real x2,
                             const Real pgas_sta,
                             const Real posi_sta);
static Real func_presini_isote(const Real x2, const Real y0,
                               const Real p0, const Real te0);

/* Function for integration */
Real func_integ_bx(Real y, const Real xfix);
Real func_integ_by(Real x, const Real yfix);
Real func_integ_py(Real y, Real xfix);
Real func_integ_px_bg(Real x, Real yfix);
Real func_integ_px_rope(Real x, Real yfix);
Real func_integ_pphi_bg(Real r, const Real phi);
Real func_integ_pphi_rope(Real r, const Real phi);

static Real func_py_integral(Real x2, const Real pgas_sta, const Real posi_sta);
static Real func_px_bg_integral(const Real x1, const Real x2);
static Real func_px_rope_integral(const Real x1, const Real x2);

double adaptiveSimpsons(double (*f)(double, double), // ptr to function
                        double param,
                        double a, double b, // interval [a,b]
                        double epsilon,     // error tolerance
                        int maxRecursionDepth);
double adaptiveSimpsonsAux(double (*f)(double, double),
                           double param,
                           double a, double b,
                           double epsilon,
                           double S,
                           double fa, double fb, double fc, int bottom);

/*============================================================================*/
/* Normalization parameters */
Real Lchar, Bchar, Nechar, Rhochar, Pchar, Timechar, Tchar, Vchar;
Real Gsun_nondim;

/* a. input parameters: pressure and temperature in chromosphere and corona */
Real Te_corona = 2.0e6, Te_photos = 5500.0; // (K)

/* b. input parameters: height and width for the TR region */
Real Height_TR = 5.0e6, Width_TR = 0.5e6; // (m)

/* c. The height for define the pgas_0 in corona */
Real posi_c = 1.0;

/* Constant parameters */
Real Kb = 1.38e-23, Mp = 1.6726e-27, Mu0 = 1.25663706144e-6, LnA = 30.0;
Real gravitational_const = 6.672e-11; /* (N M^2 kg^-2)*/
Real solarmass = 1.99e+30;            /* (kg) */
Real solarradius = 6.96e+8;           /* (m) */

/* Initial conditions */
Real ***pgasini, ***rhoini, ***tempini, ***az;
Real ***bx, ***by, ***bz;
Real ***bxvz_pre, ***byvz_pre, ***bxi_est, ***byi_est;

/* Grid parameters */
Real dx_grid, dy_grid, dz_grid;

/* Flux rope parameters */
Real fr_h, fr_d, fr_ri, fr_del, fr_rja, fr_sigma;
Real fr_h_time;
Real fr_mode;
Real I_source; // Total current
Real scale_ambient;
Real gauss_c1, gauss_c2; // Two parameters for setting guassian J(r)

/* Background sources */
Real lambada, I_ratio;
Real bg_ri, bg_rja;
Real source_xs, source_xe;
int sw_bz;

/* Define the temperature range in computing kappa(T) */
Real temp_low, temp_high;

/* Artificialy reset cooling and conduction in lower temperature region */
Real Tc_low;
int sw_lowt_cool; // use for decreasing the cooling and increacing thermal
                  // conduction once tempreture is lower than Tc_low to keep
                  // Kappa(T)*Q(T) constant. (Reeves et al.2010 ApJ)
int sw_explicit_conduct_dt;

/* Reset Output dt = dt_reset_out after time_reset_out */
Real dt_reset_out = 0.05, time_reset_out = 100.0;

Real eta_input;
Real time_eta_plus, jz_anomal_limit; // Set the additional eta at the beginning.

/*=========================== PUBLIC FUNCTIONS ===============================*/
/*----------------------------------------------------------------------------*/
/* problem:  */

void problem(DomainS *pDomain)
{
  GridS *pGrid = (pDomain->Grid);
  int i, is = pGrid->is, ie = pGrid->ie;
  int j, js = pGrid->js, je = pGrid->je;
  int k, ks = pGrid->ks, ke = pGrid->ke;
  int n1 = ie - is + 1 + 2 * nghost;
  int n2 = je - js + 1 + 2 * nghost;
  int n3 = ke - ks + 1 + 2 * nghost;

  Real x1, x2, x3;
  Real x1c, x2c, x3c;
  Real x1f, x2f, x3f;
  Real x1s, x2s, x3s;
  Real r;
  Real ***pc, ***pc_rope, ***pc_bg;
  dx_grid = pGrid->dx1;
  dy_grid = pGrid->dx2;
  dz_grid = pGrid->dx3;

  /* Inilize pgasini, rhoini, and tempini */
  if ((pgasini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for pgasini \n");
  }
  if ((rhoini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for rhoini \n");
  }
  if ((tempini = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for tempini \n");
  }

  if ((bx = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector bx\n");
  }
  if ((by = (Real ***)calloc_3d_array(n3, n2, n1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector by\n");
  }
  if ((bz = (Real ***)calloc_3d_array(n3, n2 + 2, n1 + 2, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector bz\n");
  }
  if ((az = (Real ***)calloc_3d_array(n3 + 1, n2 + 1, n1 + 1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector az\n");
  }
  if ((pc = (Real ***)calloc_3d_array(n3 + 1, n2 + 1, n1 + 1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector pc\n");
  }
  if ((pc_rope = (Real ***)calloc_3d_array(n3 + 1, n2 + 1, n1 + 1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector pc\n");
  }
  if ((pc_bg = (Real ***)calloc_3d_array(n3 + 1, n2 + 1, n1 + 1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector pc\n");
  }
  if ((bxvz_pre = (Real ***)calloc_3d_array(n3, n2 + 1, n1 + 1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector bxvz_pre\n");
  }
  if ((byvz_pre = (Real ***)calloc_3d_array(n3, n2 + 1, n1 + 1, sizeof(Real))) == NULL)
  {
    ath_error("[field_loop]: Error allocating memory for vector byvz_pre\n");
  }

  if ((bxi_est = (Real ***)calloc_3d_array(n3, n2 + 1, n1 + 1, sizeof(Real))) == NULL)
  {
    ath_error("[User_in_loop]: Error allocating memory for vector bxi_est\n");
  }
  if ((byi_est = (Real ***)calloc_3d_array(n3, n2 + 1, n1 + 1, sizeof(Real))) == NULL)
  {
    ath_error("[User_in_loop]: Error allocating memory for vector byi_est\n");
  }

  /* Termal conduction & viscosity */
  Real k_spitzer, kappa_v;
  Real nud, nu0;

  /* Dence bottom boundary */
  Real htr, wtr, chr_rho, cor_rho;

  /* Read input parameters */
  Lchar = par_getd("problem", "Lchar");
  Bchar = par_getd("problem", "Bchar");
  Nechar = par_getd("problem", "Nechar");

  Pchar = Bchar * Bchar / Mu0;
  Rhochar = Nechar * Mp;
  Tchar = Pchar / (Nechar * 2. * Kb); /* Total number is 2*ne for full
                                         ionization plasma */
  Vchar = Bchar / sqrt(Mu0 * Rhochar);
  Timechar = Lchar / Vchar;

  /* Print*/
  int myid;
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  if (myid == 0)
  {
    printf("nechar=%10.3e (cm^-3), Rhochar=%10.3e (kg/m^3)\n",
           Nechar / 1.0e6, Rhochar);
    printf("Pchar=%10.3e (Pa)\n", Pchar);
    printf("Tchar=%10.3e (K)\n", Tchar);
    printf("Vchar=%10.3e (km/s)\n", Vchar / 1.0e3);
    printf("Lchar=%10.3e (km)\n", Lchar / 1.0e3);
    printf("Bchar=%10.3e (T)\n", Bchar);
    printf("Timechar=%10.3e (s)\n", Timechar);
  }

  /* Read temperature floor for kappa calculations */
  temp_low = par_getd("problem", "Temperature_low");
  temp_high = par_getd("problem", "Temperature_high");

  /* Read parameters for cooling */
  Tc_low = par_getd("problem", "Tc_low");
  sw_lowt_cool = par_getd("problem", "sw_lowt_cool");
  sw_explicit_conduct_dt = par_getd("problem", "sw_explicit_conduct_dt");

  /* Switch Bz */
  sw_bz = par_getd("problem", "sw_bz");

  /* Initialize fluxrope */
  fr_h = par_getd("problem", "h");
  fr_d = par_getd("problem", "d");
  fr_ri = par_getd("problem", "ri");
  fr_del = par_getd("problem", "del");
  fr_rja = par_getd("problem", "rja");
  fr_mode = par_getd("problem", "mode");

  lambada = par_getd("problem", "bg_lambada");
  I_ratio = par_getd("problem", "bg_I_ratio");
  bg_rja = par_getd("problem", "bg_rja");
  bg_ri = par_getd("problem", "bg_ri");
  source_xs = par_getd("problem", "source_xs");
  source_xe = par_getd("problem", "source_xe");

  scale_ambient = par_getd("problem", "scale_ambient");
  gauss_c1 = fr_ri * (2. / 4.29193);
  gauss_c2 = bg_ri * (2. / 4.29193);

  /* Switch Anomalous resistivity */
  time_eta_plus = par_getd("problem", "time_eta_plus");
  jz_anomal_limit = par_getd("problem", "jz_anomal_limit");

  // Reset the center of the fluxrope at the grid
  int j_center;
  j_center = (int)(fr_h / pGrid->dx2);
  fr_h = (j_center)*pGrid->dx2;
  // Initilize the fr_h_time
  fr_h_time = fr_h;

  /* Total current inside the fluxrope I and souce current */
  Real I;
  if (fr_mode == 1)
  {
    I = PI * fr_rja * (SQR(fr_ri) + 0.25 * SQR(fr_del) - 2. * SQR(fr_del / PI));
  }
  else
  {
    I = fr_rja * 2.0 * PI * SQR(gauss_c1) * (1.0 - exp(-0.5 * SQR(10.0 / gauss_c1)));
  }
  I_source = I / I_ratio; // Background total current: I_source

  /* Pressure */
  Real rho0 = 1.0 * scale_ambient, t0 = Te_corona / Tchar, pgas0;
  pgas0 = rho0 * t0;
  if (myid == 0)
  {
    printf("rho0 = %10.3e, pgas0 = %10.3e, t0 = %10.3e\n", rho0, pgas0, t0);
  }

  /* Calculate bx, by */
  for (k = ks; k <= ke + 1; k++)
  {
    for (j = js - nghost; j <= je + nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
        bx[k][j][i] = func_bmx(x1c - 0.5 * dx_grid, x2c);
        by[k][j][i] = func_bmy(x1c, x2c - 0.5 * dy_grid);
        bz[k][j][i] = 0;
      }
    }
  }

  /* Recalculate Az */
  for (k = ks; k <= ke; k++)
  {
    az[k][js - nghost][is - nghost] = 0;
    for (j = js - nghost + 1; j <= je + nghost + 1; j++)
    {
      az[k][j][is - nghost] = az[k][j - 1][is - nghost] + bx[k][j - 1][is - nghost] * dy_grid;
    }
    for (j = js - nghost; j <= je + nghost + 1; j++)
    {
      for (i = is - nghost + 1; i <= ie + nghost + 1; i++)
      {
        az[k][j][i] = az[k][j][i - 1] - by[k][j][i - 1] * dx_grid;
      }
    }
  }
  /*for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost; j <= je + nghost + 1; j++)
    {
      for (i = is - nghost; i <= ie + nghost + 1; i++)
      {
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
        az[k][j][i] = func_azini(x1c-0.5*pGrid->dx1, x2c-0.5*pGrid->dx2);
      }
    }
  }*/

  /* Calculate bxi, byi from Az */
  for (k = ks; k <= ke + 1; k++)
  {
    for (j = js - nghost; j <= je + nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        bx[k][j][i] = (az[k][j + 1][i] - az[k][j][i]) / dy_grid;
        by[k][j][i] = -(az[k][j][i + 1] - az[k][j][i]) / dx_grid;
        bz[k][j][i] = 0;
      }
    }
  }

  /* Compute initial pressure, temperature and density */
  // (b) Pc
  /*
  Real byavg;
  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost + 1; j <= je + nghost; j++)
    {
      pc[k][j][is - nghost] = 0;
      pc[k][j][is - nghost + 1] = 0;
      pc_rope[k][j][is - nghost] = 0;
      pc_rope[k][j][is - nghost + 1] = 0;
      pc_bg[k][j][is - nghost] = 0;
      pc_bg[k][j][is - nghost + 1] = 0;
      for (i = is - nghost + 2; i <= ie + nghost; i++)
      {
        byavg = 0.5 * (by[k][j][i - 2] + by[k][j][i - 1]);
        cc_pos(pGrid, i - 1, j, k, &x1c, &x2c, &x3c);
        r = sqrt(pow(x1c - 0.5 * dx_grid, 2) + pow(x2c - 0.5 * dy_grid - fr_h, 2));
        // jz from model
        pc[k][j][i] = pc[k][j][i - 2] - func_rjphi(r) * byavg * (2.0 * dx_grid);
        // jz from numerical diff
        //pc[k][j][i] = pc[k][j][i - 2] - jz[k][j][i-1] * byavg * (2.0 * dx_grid);

        // Seperate Pc into two parts
        pc_bg[k][j][i] = pc_bg[k][j][i - 2] - func_rjphi_bg(r) * byavg * (2.0 * dx_grid);
        pc_rope[k][j][i] = pc_rope[k][j][i - 2] - func_rjphi_rope(r) * byavg * (2.0 * dx_grid);
      }
    }
  }
  */
  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost; j <= je + nghost + 1; j++)
    {
      for (i = is - nghost; i <= ie + nghost + 1; i++)
      {
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
        r = sqrt(SQR(x1c - 0.5 * pGrid->dx1) + SQR(x2c - 0.5 * pGrid->dx2 - fr_h));
        pc_bg[k][j][i] = fabs(func_uphi_bg(r));
        pc_rope[k][j][i] = fabs(func_uphi_rope(r));
      }
    }
  }
  // (c) average pressure
  Real peq_avg, peq_avg_bg, peq_avg_rope;
  Real dxh = 0.5 * pGrid->dx1, dyh = 0.5 * pGrid->dx2;
  Real beta_pre, pmag;
  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost; j <= je + nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
        peq_avg = 0.25 * (pc[k][j][i] + pc[k][j][i + 1] + pc[k][j + 1][i] + pc[k][j + 1][i + 1]);
        peq_avg_bg = 0.25 * (pc_bg[k][j][i] + pc_bg[k][j][i + 1] + pc_bg[k][j + 1][i] + pc_bg[k][j + 1][i + 1]);
        peq_avg_rope = 0.25 * (pc_rope[k][j][i] + pc_rope[k][j][i + 1] + pc_rope[k][j + 1][i] + pc_rope[k][j + 1][i + 1]);

        // Update Bz or Gas pressure ;
        // case A: no Bz
        if (sw_bz == 0)
        {
          bz[k][j][i] = 0.0;
          pgasini[k][j][i] = func_py_integral(x2c, pgas0, posi_c) + fabs(peq_avg);
        }
        else if (sw_bz == 1)
        {
          // case B: with Bz
          bz[k][j][i] = sqrt(2.0 * fabs(peq_avg_rope));
          pgasini[k][j][i] = func_py_integral(x2c, pgas0, posi_c) + fabs(peq_avg_bg);
        }
        else
        {
          // Others
          ath_error("[Initial condition]: Error Bz\n");
        }

        tempini[k][j][i] = func_teini(x1c, x2c);
        // Isothermal
        rhoini[k][j][i] = pgasini[k][j][i] / tempini[k][j][i];
        // Adiabatic
        //Real rho_temp = (func_py_integral(x2c, pgas0, posi_c) / tempini[k][j][i]);
        //rhoini[k][j][i] = rho_temp*pow(pgasini[k][j][i]/rho_temp, 0.6);
      }
    }
  }

  /* All variables */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        /* density */
        pGrid->U[k][j][i].d = rhoini[k][j][i];

        /* monentum */
        pGrid->U[k][j][i].M1 = 0.0;
        pGrid->U[k][j][i].M2 = 0.0;
        pGrid->U[k][j][i].M3 = 0.0;

        /* magnetic field */
        pGrid->B1i[k][j][i] = bx[k][j][i];
        pGrid->B2i[k][j][i] = by[k][j][i];
        pGrid->B3i[k][j][i] = bz[k][j][i];

        /* initial setting for magnetic field on the boundary */
        if (i == ie && ie > is)
          pGrid->B1i[k][j][i + 1] = bx[k][j][i + 1];
        if (j == je && je > js)
          pGrid->B2i[k][j + 1][i] = by[k][j + 1][i];
        if (k == ke && ke > ks)
          pGrid->B3i[k + 1][j][i] = bz[k + 1][j][i];
      }
    }
  }

  /* Cell-center magnetic field */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        pGrid->U[k][j][i].B1c = 0.5 * (pGrid->B1i[k][j][i] +
                                       pGrid->B1i[k][j][i + 1]);
        pGrid->U[k][j][i].B2c = 0.5 * (pGrid->B2i[k][j][i] +
                                       pGrid->B2i[k][j + 1][i]);
        pGrid->U[k][j][i].B3c = pGrid->B3i[k][j][i];
      }
    }
  }

  /* total energy*/
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = is; i <= ie; i++)
      {
        pGrid->U[k][j][i].E = pgasini[k][j][i] / (Gamma_1) + 0.5 * (SQR(pGrid->U[k][j][i].B1c) + SQR(pGrid->U[k][j][i].B2c) + SQR(pGrid->U[k][j][i].B3c)) + 0.5 * (SQR(pGrid->U[k][j][i].M1) + SQR(pGrid->U[k][j][i].M2) + SQR(pGrid->U[k][j][i].M3)) / pGrid->U[k][j][i].d;
      }
    }
  }

  /* Initialise VzBx-VxBz & VzBy-VyBz */
  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost; j <= je + nghost + 1; j++)
    {
      for (i = is - nghost; i <= ie + nghost + 1; i++)
      {
        bxvz_pre[k][j][i] = 0;
        byvz_pre[k][j][i] = 0;
      }
    }
  }

  /* Set resistivity */
#ifdef RESISTIVITY
  eta_Ohm = par_getd("problem", "eta_Ohm");
  eta_input = par_getd("problem", "eta_Ohm");
  Q_AD = par_getd("problem", "Q_AD");
  Q_Hall = 0.0;
  d_ind = 0.0;
#endif

  /* Set thermel conduction coefficient */
#ifdef THERMAL_CONDUCTION
  kappa_aniso = 1.0;
  kappa_iso = 0;
#endif

  /* Set viscosity */
#ifdef VISCOSITY
  // nu0 = 3.4e15 cm^2 s^-1,
  //nu_iso = 3.4e15*Timechar/(Lchar*1.0e2)/(Lchar*1.0e2);
  //printf("Non-dimensional nu_iso=%10.3e\n", nu_iso);
  //nu_aniso = 0.0;
#endif

  /* Enroll gravitational potential to give acceleration in y-direction for2D*/
  StaticGravPot = gsun_static;

  /* Set optical thin radiation cooling and corona heating function */

  /* Set boundary value functions: Order for updating boundary conditions must
      always be x1-x2-x3 in order to
      fill the corner cells properly*/

  /* (a) left-open */
  bvals_mhd_fun(pDomain, left_x1, openbc_ix1);

  /* (b) Right-open */
  bvals_mhd_fun(pDomain, right_x1, symmbc_ox1);

  /* (c) Botom-open */
  bvals_mhd_fun(pDomain, left_x2, linetiedbc_ix2);

  /* (d) Top-open */
  bvals_mhd_fun(pDomain, right_x2, openbc_ox2);

  /* Release arrays */
  if (pc != NULL)
    free_3d_array(pc);
  if (pc_bg != NULL)
    free_3d_array(pc_bg);
  if (pc_rope != NULL)
    free_3d_array(pc_rope);
}

/*==============================================================================
 * PROBLEM USER FUNCTIONS:
 * problem_write_restart() - writes problem-specific user data to restart files
 * problem_read_restart()  - reads problem-specific user data from restart files
 * get_usr_expr()          - sets pointer to expression for special output data
 * get_usr_out_fun()       - returns a user defined output function pointer
 * get_usr_par_prop()      - returns a user defined particle selection function
 * Userwork_in_loop        - problem specific work IN     main loop
 * Userwork_after_loop     - problem specific work AFTER  main loop
 *----------------------------------------------------------------------------*/

void problem_write_restart(MeshS *pM, FILE *fp)
{
  return;
}

void problem_read_restart(MeshS *pM, FILE *fp)
{
  return;
}

static Real out_az(const GridS *pG, const int i, const int j, const int k)
{
  return az[k][j][i];
}

static Real out_accy(const GridS *pG, const int i, const int j, const int k)
{
  int is = pG->is, ie = pG->ie;
  int js = pG->js, je = pG->je;
  Real x1c, x2c, x3c;
  Real jz, rho_avg, bx_avg, fg;
  if ((i > is - nghost + 1) && (i < ie + nghost - 1) && (j > js - nghost + 1) && (j < je + nghost - 1))
  {
    jz = ((pG->B2i[k][j][i] - pG->B2i[k][j][i - 1]) / pG->dx1 -
          (pG->B1i[k][j][i] - pG->B1i[k][j - 1][i]) / pG->dx2);
    bx_avg = 0.5 * (pG->B1i[k][j][i] + pG->B1i[k][j - 1][i]);
    rho_avg = 0.25 * (pG->U[k][j][i].d + pG->U[k][j][i - 1].d + pG->U[k][j - 1][i].d + pG->U[k][j - 1][i - 1].d);
    cc_pos(pG, i, j, k, &x1c, &x2c, &x3c);
    fg = gsun_y(x2c);
  }
  else
  {
    jz = 0;
    bx_avg = 0;
    rho_avg = pG->U[k][j][i].d;
    fg = 0;
  }
  return jz * bx_avg / rho_avg + fg;
}

static Real out_currentdensity(const GridS *pG, const int i, const int j, const int k)
{
  Real jz;
  int is = pG->is, ie = pG->ie;
  int js = pG->js, je = pG->je;
  if ((i > is - nghost + 1) && (i < ie + nghost - 1) && (j > js - nghost + 1) && (j < je + nghost - 1))
  {
    jz = ((pG->B2i[k][j][i] - pG->B2i[k][j][i - 1]) / pG->dx1 -
          (pG->B1i[k][j][i] - pG->B1i[k][j - 1][i]) / pG->dx2);
  }
  else
  {
    jz = 0;
  }
  return jz;
}

static Real out_ohmiceta(const GridS *pG, const int i, const int j, const int k)
{
  Real eta;
  eta = 0.0;
#ifdef RESISTIVITY
  eta = pG->eta_Ohm[k][j][i];
#endif
  return eta;
}

ConsFun_t get_usr_expr(const char *expr)
{
  if (strcmp(expr, "Az") == 0)
    return out_az;
  if (strcmp(expr, "Jz") == 0)
    return out_currentdensity;
  if (strcmp(expr, "accy") == 0)
    return out_accy;
  if (strcmp(expr, "eta") == 0)
    return out_ohmiceta;
  return NULL;
}

VOutFun_t get_usr_out_fun(const char *name)
{
  return NULL;
}

#ifdef RESISTIVITY

void get_eta_user(GridS *pG, int i, int j, int k,
                  Real *eta_O, Real *eta_H, Real *eta_A)
{
  int is = pG->is, ie = pG->ie;
  int js = pG->js, je = pG->je;
  int ks = pG->ks, ke = pG->ke;
  Real x1c, x2c, x3c;
  cc_pos(pG, i, j, k, &x1c, &x2c, &x3c);

  // 1. Get the local current density Jz
  Real jz;
  if ((i >= is - nghost + 2) && (i <= ie + nghost - 2) && (j >= js - nghost + 2) && (j <= je + nghost - 2))
  {
    jz = (pG->B2i[k][j][i] - pG->B2i[k][j][i - 1]) / pG->dx1 - (pG->B1i[k][j][i] - pG->B1i[k][j - 1][i]) / pG->dx2;
  }
  else
  {
    jz = 0;
  }

  // 2. Estimate the anomalous resistivity scale once Jz > Jz_threshold
  Real time_factor = MAX(0, 1.0 - pG->time / time_eta_plus);
  Real anomalous_scale;
  if (fabs(jz) >= jz_anomal_limit)
  {
    anomalous_scale = 1.0 + time_factor * SQR(fabs(jz)) / SQR(jz_anomal_limit);
  }
  else
  {
    anomalous_scale = 1.0;
  }

  // 3. No Anomalous resistivity inside the fluxrope center
  // 3.1: artificially set r_radius
  Real r_width = 0.075;
  Real edge = 0.005;
  Real r = sqrt(pow(x1c, 2) + pow(x2c - fr_h_time, 2));
  Real ratio_fr = 0.5 * tanh((r - r_width) / edge) + 0.5;
  anomalous_scale = anomalous_scale * ratio_fr;

  // 3.2: No anomalous inside the fluxrop: depends on Bz
  //anomalous_scale = anomalous_scale * MAX(1.0 - 10.0 * fabs(pG->B3i[k][j][i]), 0.0);

  // 4. No Anomalous resistivity at the lower boundary
  Real h = Height_TR / Lchar, w = Width_TR / Lchar;
  Real ratio_bot = 0.5 * tanh((x2c - h) / w) + 0.5;
  anomalous_scale = anomalous_scale * ratio_bot;

  // 5. Update resistivity
  *eta_O = eta_input * anomalous_scale;

  return;
}
#endif

void Userwork_in_loop(MeshS *pM)
{
  GridS *pG = pM->Domain[0][0].Grid;
  int is = pG->is, ie = pG->ie;
  int js = pG->js, je = pG->je;
  int ks = pG->ks, ke = pG->ke;
  int i, j, k;
  Real x1c, x2c, x3c;

  /* Vz
  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost; j <= je + nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pG->U[k][j][i].E = pG->U[k][j][i].E - SQR(pG->U[k][j][i].M3) / pG->U[k][j][i].d;
        pG->U[k][j][i].M3 = 0;
      }
    }
  }
  */

  /* (3) Obtain the time-dependent fr_h_time 
  // (3.1) Get the fr_h_time in each node including unvalid values
  // Use Bx
  float fr_h_time_temp;
  fr_h_time_temp = -1.0;
  if (pG->MaxX[0] == 0)
  {
    for (k = ks; k <= ke; k++)
    {
      j = je;
      while ((pG->B1i[k][j][ie + 1] < 0.0) && (j >= js))
      {
        j = j - 1;
      }
      if (pG->B1i[k][j][ie + 1] > 0.0)
      {
        cc_pos(pG, ie + 1, j, k, &x1c, &x2c, &x3c);
        fr_h_time_temp = x2c + 0.5 * pG->dx2;
      }
    }
  }

  // (3.2) fr_h_time_temp and find the valid fr_h_time
  int myid, nprocs, j_node;
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  float *fr_h_time_buf = NULL;
  float fr_h_time_valid;
  fr_h_time_buf = (float *)malloc(sizeof(float) * nprocs);

  MPI_Gather(&fr_h_time_temp, 1, MPI_FLOAT, fr_h_time_buf, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
  if (myid == 0)
  {
    float fr_h_time_max;
    fr_h_time_max = 0.0;
    for (j_node = 0; j_node <= nprocs - 1; j_node++)
    {
      if (fr_h_time_buf[j_node] >= fr_h_time_max)
      {
        fr_h_time_max = fr_h_time_buf[j_node];
      }
    }
    for (j_node = 0; j_node <= nprocs - 1; j_node++)
    {
      fr_h_time_buf[j_node] = fr_h_time_max;
    }
  }
  MPI_Scatter(fr_h_time_buf, 1, MPI_FLOAT, &fr_h_time_temp, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
  free(fr_h_time_buf);
  */

  // (3.1b) Find the fluxrope center using Bz
  float bzc, bzmax = 0.0, fr_h_time_temp = 0.0; // save |Bz|max and its location in y-
  for (k = ks; k <= ke; k++)
  {
    for (j = js - nghost; j <= je + nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        bzc = fabs(pG->U[k][j][i].B3c);
        if (bzc > bzmax)
        {
          cc_pos(pG, i, j, k, &x1c, &x2c, &x3c);
          bzmax = bzc;
          fr_h_time_temp = x2c;
        }
      }
    }
  }

  // (3.2b)
  int myid, nprocs, j_node;
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  float *fr_h_time_buf = NULL, *bzmax_buf = NULL;
  fr_h_time_buf = (float *)malloc(sizeof(float) * nprocs);
  bzmax_buf = (float *)malloc(sizeof(float) * nprocs);

  MPI_Gather(&fr_h_time_temp, 1, MPI_FLOAT, fr_h_time_buf, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
  MPI_Gather(&bzmax, 1, MPI_FLOAT, bzmax_buf, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);

  if (myid == 0)
  {
    float bzmax_all = 0;
    for (j_node = 0; j_node <= nprocs - 1; j_node++)
    {
      if (bzmax_buf[j_node] >= bzmax_all)
      {
        bzmax_all = bzmax_buf[j_node];
        fr_h_time = fr_h_time_buf[j_node];
      }
    }
    for (j_node = 0; j_node <= nprocs - 1; j_node++)
    {
      fr_h_time_buf[j_node] = fr_h_time;
    }
  }
  MPI_Scatter(fr_h_time_buf, 1, MPI_FLOAT, &fr_h_time_temp, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
  free(fr_h_time_buf);
  free(bzmax_buf);

  // (3.3) Update fr_h_time
  fr_h_time = fr_h_time_temp;
  if (myid == 0)
  {
    printf("fluxrope_height=%f\n", fr_h_time);
  }

  return;
}

void Userwork_after_loop(MeshS *pM)
{
  return;
}

/*=========================== PRIVATE FUNCTIONS ==============================*/
/* ============================================================================
 * Functions for fluxrope
 * ===========================================================================*/
/*----------------------------------------------------------------------------*/
static Real func_bmx(const Real x, const Real y)
{
  /*c model field x-component */
  Real rs, rm;
  Real bmx, bmx_bg;
  rs = sqrt(pow(x, 2) + pow(y - fr_h, 2));
  rm = sqrt(pow(x, 2) + pow(y + fr_h, 2));

  // flux rope (and its mirros)
  bmx = 0.0;
  if (rs > 0.0)
  {
    bmx = +func_bphi(rs) * (y - fr_h) / rs - func_bphi(rm) * (y + fr_h) / rm;
  }
  else
  {
    bmx = 0.0;
  }

  // background
  Real yc = y + fr_d;
  /* Single source
   bmx_bg = -0.25*I_source*((lambada - x)*(yc*yc + SQR(lambada + x))
                   + (lambada + x)*(yc*yc + SQR(lambada - x)))
    /(PI*(yc*yc + SQR(lambada - x))*(yc*yc + SQR(lambada + x)));
  */
  /* Multiple sources */
  Real ddy = 1.0e-11;
  bmx_bg = (func_azbg(x, yc + ddy) - func_azbg(x, yc - ddy)) / (2.0 * ddy);

  return bmx + bmx_bg;
}

/*----------------------------------------------------------------------------*/
static Real func_bmy(const Real x, const Real y)
{
  /*  model field y-component */
  Real rs, rm;
  Real bmy, bmy_bg;
  rs = sqrt(pow(x, 2) + pow(y - fr_h, 2));
  rm = sqrt(pow(x, 2) + pow(y + fr_h, 2));

  // fluxrope
  bmy = 0;
  if (rs > 0.0)
  {
    bmy = -func_bphi(rs) * x / rs + func_bphi(rm) * x / rm;
  }
  else
  {
    bmy = 0;
  }

  // background
  Real yc = y + fr_d;
  /* Single source
   bmy_bg = -0.25*I_source*yc*(SQR(lambada - x) - SQR(lambada + x))
    /(PI*(yc*yc + SQR(lambada - x))*(yc*yc + SQR(lambada + x)));
  */
  /* Multiple sources */
  Real ddx = 1.0e-11;
  bmy_bg = -(func_azbg(x + ddx, yc) - func_azbg(x - ddx, yc)) / (2.0 * ddx);

  return bmy + bmy_bg;
}

/*----------------------------------------------------------------------------*/
static Real func_azbg(const Real x, const Real y)
{
  /* Single source
  Real u = (x+lambada)/y;
  Real v = (x-lambada)/y;
  Real abg = I_source*(atan(u) - atan(v));
  */

  /* Multiple sources */
  int np = 200; // number of sources
  int i;
  Real abg = 0;
  Real I_source_sep = I_source / np;
  for (i = 0; i < np; i++)
  {
    Real lambada_i = (source_xe - source_xs) / np * i + source_xs;
    Real u = (x + lambada_i) / y;
    Real v = (x - lambada_i) / y;
    abg = abg + I_source_sep * (0.25 / PI) * (atan(u) - atan(v));
  }
  return abg;
}

/*----------------------------------------------------------------------------*/
static Real func_azini(Real x, Real y)
{
  Real Ixx, Iyy, az_out;
  Real az0 = 0, x0 = -2.0, y0 = fr_h;
  // Integrate (y=y0 -> y) at x = x0 in the y-direction
  Iyy = adaptiveSimpsons(func_integ_bx,
                         x0,
                         y0, y, 1.0e-5, 1000);

  // Integrate (x=x0 -> x) at y = y in the x-direction
  Ixx = adaptiveSimpsons(func_integ_by,
                         y,
                         x0, x, 1.0e-5, 1000);

  az_out = Ixx + Iyy;
  return az_out;
}

/*----------------------------------------------------------------------------*/
static Real func_bphi(const Real r)
{
  Real bphi, bphi_rope;
  if (fr_mode == 1)
  {
    /* Cylindrical field function */
    Real riq, delq, piq, t1, t2, t3, bphi;
    Real ro, r1, r2;
    r1 = fr_ri - 0.5 * fr_del;
    r2 = fr_ri + 0.5 * fr_del;
    riq = fr_ri * fr_ri;
    delq = fr_del * fr_del;
    piq = PI * PI;
    if (r <= r1)
    {
      bphi_rope = -0.5 * fr_rja * r;
    }
    else if (r <= r2)
    {
      t1 = 0.5 * r1 * r1 - delq / piq + 0.5 * r * r;
      t2 = (fr_del * r / PI) * sin((PI / fr_del) * (r - r1));
      t3 = (delq / piq) * cos((PI / fr_del) * (r - r1));
      bphi_rope = -0.5 * fr_rja * (t1 + t2 + t3) / r;
    }
    else
    {
      bphi_rope = -0.5 * fr_rja * (riq + 0.25 * delq - 2. * delq / piq) / r;
    }
  }
  else
  {
    /* Gaussian distribtion */
    Real I_rope = fr_rja * 2.0 * PI * SQR(gauss_c1) * (1.0 - exp(-0.5 * SQR(r / gauss_c1)));
    if (r > 0.0)
    {
      bphi_rope = -I_rope / (2.0 * PI * r);
    }
    else
    {
      bphi_rope = 0.0;
    }
  }

  /* bphi_bg */
  Real bphi_bg;
  Real Ibg = bg_rja * 2.0 * PI * SQR(gauss_c2) * (1.0 - exp(-0.5 * SQR(r / gauss_c2)));
  if (r > 0.0)
  {
    bphi_bg = -Ibg / (2.0 * PI * r);
  }
  else
  {
    bphi_bg = 0.0;
  }

  /* Total bphi */
  bphi = bphi_rope + bphi_bg;
  return bphi;
}

/*----------------------------------------------------------------------------*/
static Real func_rjphi(const Real r)
{
  return func_rjphi_rope(r) + func_rjphi_bg(r);
}

static Real func_rjphi_rope(const Real r)
{
  Real rjphi_rope;
  if (fr_mode == 1)
  {
    /* Case 1: Cylindrical rope */
    Real r1, r2;
    r1 = fr_ri - 0.5 * fr_del;
    r2 = fr_ri + 0.5 * fr_del;
    if (r <= r1)
    {
      rjphi_rope = fr_rja;
    }
    else if (r <= r2)
    {
      rjphi_rope = 0.5 * fr_rja * (cos((PI / fr_del) * (r - r1)) + 1.);
    }
    else
    {
      rjphi_rope = 0.0;
    }
  }
  else
  {
    /* Case 2: Gaussion distribution */
    rjphi_rope = fr_rja * exp(-0.5 * pow((r / gauss_c1), 2));
  }
  return rjphi_rope;
}

static Real func_rjphi_bg(const Real r)
{
  return bg_rja * exp(-0.5 * pow((r / gauss_c2), 2));
}

/*----------------------------------------------------------------------------*/
static Real func_teini(const Real x, const Real y)
{
  /* Temperature inside the fluxrope */
  Real pi = 3.14159265358979;
  Real r = sqrt(pow(x, 2) + pow(y - fr_h, 2));
  Real Te, t1, t2;

  /* Temperature distribution in y-direction: in non-dimensional forms */
  Real h = Height_TR / Lchar;
  Real w = Width_TR / Lchar;
  Real tecor = Te_corona / Tchar, techr = Te_photos / Tchar;
  t1 = 0.5 * (tecor - techr);
  t2 = 0.5 * (tecor + techr);
  Te = t1 * tanh((y - h) / w) + t2;

  return Te;
}

/*----------------------------------------------------------------------------*/
/*  \fn gas pressure assuming constant temperature */
static Real func_presini_isote(const Real x2, const Real y0,
                               const Real p0, const Real te0)
{
  Real pgas, gm, r, r0;

  gm = gravitational_const * solarmass * Rhochar / (Lchar * Pchar);
  r = solarradius / Lchar + x2;
  r0 = solarradius / Lchar + y0;
  pgas = p0 * exp(gm / te0 * (1.0 / r - 1.0 / r0));

  return pgas;
}

/* ----------------------------------------------------------------------------
 * For integration
 * ---------------------------------------------------------------------------*/
Real my_integ_pphi(Real r)
{
  return func_rjphi(r) * func_bphi(r);
}

/*----------------------------------------------------------------------------*/
/*! \fn static Real gsun_static(const Real x1, const Real x2, const Real x3)
 *  \brief Gravitational potential; g = 0.1
 */
static Real gsun_static(const Real x1, const Real x2, const Real x3)
{
  Real gp, gpnondim;
  Real safety = 1.0;
  /* gravity potential (m^2/s^2) */
  gp = -(gravitational_const * solarmass) / (x2 * Lchar + solarradius);
  /* Non-dimensional gp */
  gpnondim = gp * Rhochar / Pchar;
  //gpnondim = gp / pow(Vchar, 2);
  return gpnondim;
}

/*----------------------------------------------------------------------------*/
/*! \fn static Real gsun_r(const Real x2)
 *  \brief Non-dimensional Gravity at height x2
 */
static Real gsun_y(const Real x2)
{
  Real g, gnondim;
  Real r;
  r = solarradius + x2 * Lchar;                    /* (m) */
  g = (gravitational_const * solarmass) / (r * r); /* (m s^-2)*/
  gnondim = g * Rhochar * Lchar / Pchar;
  //gnondim = g * Lchar / pow(Vchar, 2);
  return gnondim;
}

/*----------------------------------------------------------------------------*/
/*  \fn gas pressure performing numerical integration */
static Real func_py_integral(Real x2,
                             const Real pgas_sta, const Real posi_sta)
{
  Real pgas;
  Real Iout_simp;

  Iout_simp = adaptiveSimpsons(func_integ_py,
                               0.0,
                               posi_sta, x2, 1.0e-5, 1000);
  pgas = pgas_sta * exp(Iout_simp);

  return pgas;
}

static Real func_uphi_rope(const Real r)
{
  Real uphi;
  Real rend = 10.0;
  if (r <= rend)
  {
    uphi = adaptiveSimpsons(func_integ_pphi_rope,
                            0,
                            r, rend, 1.0e-5, 1000);
  }
  else
  {
    uphi = 0;
  }
  return uphi;
}

static Real func_uphi_bg(const Real r)
{
  Real uphi;
  Real rend = 10.0;
  if (r <= rend)
  {
    uphi = adaptiveSimpsons(func_integ_pphi_bg,
                            0,
                            r, rend, 1.0e-5, 1000);
  }
  else
  {
    uphi = 0;
  }
  return uphi;
}

Real func_integ_bx(Real y, const Real xfix)
{
  return func_bmx(xfix, y);
}

Real func_integ_by(Real x, const Real yfix)
{
  return -func_bmy(x, yfix);
}

Real func_integ_py(Real y, Real xfix)
{
  return -gsun_y(y) / func_teini(0.0, y);
}

Real func_integ_px_bg(Real x, Real yfix)
{
  Real r = sqrt(SQR(x) + SQR(yfix - fr_h));
  return -func_bmy(x, yfix) * func_rjphi_bg(r);
  //return 0;
}

Real func_integ_px_rope(Real x, Real yfix)
{
  Real r = sqrt(SQR(x) + SQR(yfix - fr_h));
  return -func_bmy(x, yfix) * func_rjphi_rope(r);
  //return 0;
}

Real func_integ_pphi_rope(Real r, const Real phi)
{
  return func_rjphi_rope(r) * func_bphi(r);
}

Real func_integ_pphi_bg(Real r, const Real phi)
{
  return func_rjphi_bg(r) * func_bphi(r);
}

//==============================================================================
// Adaptive Simpson's Rule
//==============================================================================
double adaptiveSimpsons(double (*f)(double, double), // ptr to function
                        double param,
                        double a, double b, // interval [a,b]
                        double epsilon,     // error tolerance
                        int maxRecursionDepth)
{ // recursion cap
  double c = (a + b) / 2, h = b - a;
  double fa = f(a, param), fb = f(b, param), fc = f(c, param);
  double S = (h / 6) * (fa + 4 * fc + fb);
  return adaptiveSimpsonsAux(f,
                             param,
                             a, b, epsilon, S, fa, fb, fc, maxRecursionDepth);
}

//==============================================================================
// Recursive auxiliary function for adaptiveSimpsons() function below
//==============================================================================
double adaptiveSimpsonsAux(double (*f)(double, double),
                           double param,
                           double a, double b,
                           double epsilon,
                           double S,
                           double fa, double fb, double fc, int bottom)
{

  double c = (a + b) / 2, h = b - a;
  double d = (a + c) / 2, e = (c + b) / 2;
  double fd = f(d, param), fe = f(e, param);
  double Sleft = (h / 12) * (fa + 4 * fd + fc);
  double Sright = (h / 12) * (fc + 4 * fe + fb);
  double S2 = Sleft + Sright;
  if (bottom <= 0 || fabs(S2 - S) <= 15 * epsilon) // magic 15 comes
    // from error analysis
    return S2 + (S2 - S) / 15;
  return adaptiveSimpsonsAux(f,
                             param,
                             a, c, epsilon / 2, Sleft, fa, fc, fd, bottom - 1) +
         adaptiveSimpsonsAux(f,
                             param,
                             c, b, epsilon / 2, Sright, fc, fb, fe, bottom - 1);
}

/*----------------------------------------------------------------------------*/
/*! \fn static void openbc_ix1(GridS *pGrid)
 *  \brief open boundary condition, Inner x1 boundary (bc_ix1=2) */
static void openbc_ix1(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif
  Real x1c, x2c, x3c;

  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][is - i] = pGrid->U[k][j][is];
        pGrid->U[k][j][is - i].M1 = -fabs(pGrid->U[k][j][is].M1);
      }
    }
  }

#ifdef MHD
  /* Az */
  for (k = ks; k <= ke; k++)
  {
    // the is column
    az[k][js][is] = 0;
    for (j = js + 1; j <= je + 1; j++)
    {
      az[k][j][is] = az[k][j - 1][is] + pGrid->B1i[k][j - 1][is] * pGrid->dx2;
    }
    // is + 1 -> is + 2 column
    for (j = js; j <= je + 1; j++)
    {
      for (i = is + 1; i <= is + 2; i++)
      {
        az[k][j][i] = az[k][j][i - 1] - pGrid->B2i[k][j][i - 1] * pGrid->dx1;
      }
    }

    // In ghost zone
    /*
    // case 0: zero order extrapolation
    for (j = js; j <= je + 1; j++)
    {
      for (i = is - 1; i >= is - nghost; i--)
      {
        az[k][j][i] = az[k][j][i + 1];
      }
    }
    */
    // case 1: linear extrapolation
    for (j = js; j <= je + 1; j++)
    {
      for (i = is - 1; i >= is - nghost; i--)
      {
        az[k][j][i] = 2.0 * az[k][j][i + 1] - az[k][j][i + 2];
      }
    }
    /*
    // case 2: 2nd order extrapolation ...
    for (j = js; j <= je + 1; j++)
    {
      for (i = is - 1; i >= is - nghost; i--)
      {
        cc_pos(pGrid, is, j, k, &x1c, &x2c, &x3c);
        Real x_1 = x1c - 0.5 * pGrid->dx1;
        Real x_2 = x_1 + pGrid->dx1;
        Real x_3 = x_2 + pGrid->dx1;
        cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
        Real x_j = x1c - 0.5 * pGrid->dx1;
        az[k][j][i] = func_2nd_extrap(x_1, x_2, x_3,
                                      az[k][j][is], az[k][j][is + 1], az[k][j][is + 2],
                                      x_j);
      }
    }
    */
  }

  /* B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B1i[k][j][is - i] = (az[k][j + 1][is - i] - az[k][j][is - i]) / pGrid->dx2;
      }
    }
  }

  /* B2i: */
  if (pGrid->Nx[1] > 1)
    ju = je + 1;
  else
    ju = je;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= ju; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B2i[k][j][is - i] = -(az[k][j][is - i + 1] - az[k][j][is - i]) / pGrid->dx1;
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B3i[k][j][is - i] = pGrid->B3i[k][j][is];
      }
    }
  }
  /* B1c, 2c, 3c */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][is - i].B1c = 0.5 * (pGrid->B1i[k][j][is - i] + pGrid->B1i[k][j][is - i + 1]);
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][is - i].B2c = 0.5 * (pGrid->B2i[k][j][is - i] + pGrid->B2i[k][j + 1][is - i]);
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][is - i].B3c = pGrid->B3i[k][j][is - i];
      }
    }
  }
#endif /* MHD */

  /* Pressure and energy */
  int isp, ic;
  Real eb_isp, ek_isp, p_isp_ogm;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        isp = is;
        eb_isp = 0.5 * (SQR(pGrid->U[k][j][isp].B1c) + SQR(pGrid->U[k][j][isp].B2c) + SQR(pGrid->U[k][j][isp].B3c));
        ek_isp = 0.5 * (SQR(pGrid->U[k][j][isp].M1) + SQR(pGrid->U[k][j][isp].M2) + SQR(pGrid->U[k][j][isp].M3)) / pGrid->U[k][j][isp].d;
        p_isp_ogm = pGrid->U[k][j][isp].E - eb_isp - ek_isp;

        ic = is - i;
        pGrid->U[k][j][ic].E = p_isp_ogm + 0.5 * (SQR(pGrid->U[k][j][ic].B1c) + SQR(pGrid->U[k][j][ic].B2c) + SQR(pGrid->U[k][j][ic].B3c)) + 0.5 * (SQR(pGrid->U[k][j][ic].M1) + SQR(pGrid->U[k][j][ic].M2) + SQR(pGrid->U[k][j][ic].M3)) / pGrid->U[k][j][ic].d;
      }
    }
  }

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void openbc_ix2(GridS *pGrid)
 *  \brief openbc boundary conditions, Inner x2 boundary (bc_ix2=2) */
static void openbc_ix2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ku; /* k-upper */
#endif
  int jc;

  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i] = pGrid->U[k][js][i];
      }
    }
  }

#ifdef MHD
  /* B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      jc = js - j;
      for (i = is - (nghost - 1); i <= ie + nghost; i++)
      {
        pGrid->B1i[k][js - j][i] = 2.0 * pGrid->B1i[k][js - j + 1][i] - pGrid->B1i[k][js - j + 2][i];
      }
    }
  }

  /* B2i is not set at j=js-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost - 1; j++)
    {
      jc = js - j;
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B2i[k][js - j][i] = pGrid->B2i[k][js - j + 1][i] + (pGrid->dx2 / pGrid->dx1) * (pGrid->B1i[k][js - j][i + 1] - pGrid->B1i[k][js - j][i]);
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B3i[k][js - j][i] = 2.0 * pGrid->B3i[k][js - j + 1][i] - pGrid->B3i[k][js - j + 2][i];
      }
    }
  }

  /* B1c, B2c, B3c */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - (nghost - 1); i <= ie + (nghost - 1); i++)
      {
        pGrid->U[k][js - j][i].B1c = 0.5 * (pGrid->B1i[k][js - j][i] + pGrid->B1i[k][js - j][i + 1]);
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost - 1; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i].B2c = 0.5 * (pGrid->B2i[k][js - j][i] + pGrid->B2i[k][js - j + 1][i]);
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i].B3c = pGrid->B3i[k][js - j][i];
      }
    }
  }
#endif /* MHD */

  /* Pressure and energy */
  Real eb_js, ek_js, pjs;
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        eb_js = 0.5 * (SQR(pGrid->U[k][js][i].B1c) + SQR(pGrid->U[k][js][i].B2c) + SQR(pGrid->U[k][js][i].B3c));
        ek_js = 0.5 * (SQR(pGrid->U[k][js][i].M1) + SQR(pGrid->U[k][js][i].M2) + SQR(pGrid->U[k][js][i].M3)) / pGrid->U[k][js][i].d;
        pjs = Gamma_1 * (pGrid->U[k][js][i].E - eb_js - ek_js);

        jc = js - j;
        pGrid->U[k][jc][i].E = pjs / (Gamma_1) + 0.5 * (SQR(pGrid->U[k][jc][i].B1c) + SQR(pGrid->U[k][jc][i].B2c) + SQR(pGrid->U[k][jc][i].B3c)) + 0.5 * (SQR(pGrid->U[k][jc][i].M1) + SQR(pGrid->U[k][jc][i].M2) + SQR(pGrid->U[k][jc][i].M3)) / pGrid->U[k][jc][i].d;
      }
    }
  }
  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void linetiedbc_ix2(GridS *pGrid)
 *  \brief Sets boundary condition at the bottom.
 */
/*  ix2, line-tied, bottom */
void linetiedbc_ix2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
  Real x1c, x2c, x3c;
  Real x1f, x2f;

#ifdef MHD
  int ku; /* k-upper */
#endif

  /* Set all variables in ghost zone */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i] = pGrid->U[k][js][i];
        pGrid->U[k][js - j][i].M1 = 0.0;
        pGrid->U[k][js - j][i].M2 = 0.0;
        pGrid->U[k][js - j][i].M3 = 0.0;
      }
    }
  }

#ifdef MHD
  /* B2i is not set at j=js-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B2i[k][js - j][i] = by[k][js - j][i];
      }
    }
  }
  /* B1i is not set at i=is-nghost */
  Real pbypx;
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - (nghost - 1); i <= ie + nghost; i++)
      {
        //cc_pos(pGrid, i, js - j + 1, k, &x1c, &x2c, &x3c);
        //x1f = x1c - 0.5 * pGrid->dx1;
        //x2f = x2c - 0.5 * pGrid->dx2;
        //pbypx = func_pbypxini(x1f, x2f);
        pbypx = (pGrid->B2i[k][js - j + 1][i] - pGrid->B2i[k][js - j + 1][i - 1]) / pGrid->dx1;
        pGrid->B1i[k][js - j][i] = pGrid->B1i[k][js - j + 1][i] - pbypx * pGrid->dx2;
      }
    }
  }
  /* B3i */
  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B3i[k][js - j][i] = pGrid->B3i[k][js][i];
      }
    }
  }

  /* B1c, 2c, 3c */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost - 1; i++)
      {
        pGrid->U[k][js - j][i].B1c = 0.5 * (pGrid->B1i[k][js - j][i] + pGrid->B1i[k][js - j][i + 1]);
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i].B2c = 0.5 * (pGrid->B2i[k][js - j][i] + pGrid->B2i[k][js - j + 1][i]);
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][js - j][i].B3c = pGrid->B3i[k][js - j][i];
      }
    }
  }
#endif /* MHD */

  /* Pressure and energy */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        /* fixed boundary with gravity
        int j_mj = js - j;
        Real p_mj = pgasini[k][j_mj][i];
        Real eb_mj = 0.5 * (SQR(pGrid->U[k][j_mj][i].B1c) + SQR(pGrid->U[k][j_mj][i].B2c) + SQR(pGrid->U[k][j_mj][i].B3c));
        pGrid->U[k][j_mj][i].E = p_mj / Gamma_1 + eb_mj;
        */

        // Depends on pressure
        Real eb_js = 0.5 * (SQR(pGrid->U[k][js][i].B1c) + SQR(pGrid->U[k][js][i].B2c) + SQR(pGrid->U[k][js][i].B3c));
        Real ek_js = 0.5 * (SQR(pGrid->U[k][js][i].M1) + SQR(pGrid->U[k][js][i].M2) + SQR(pGrid->U[k][js][i].M3)) / pGrid->U[k][js][i].d;
        Real p_js = Gamma_1 * (pGrid->U[k][js][i].E - eb_js - ek_js);

        int jc = js - j;
        Real T_js = p_js / pGrid->U[k][js][i].d;
        Real yjs, yjc;
        cc_pos(pGrid, i, js, k, &x1c, &yjs, &x3c);
        cc_pos(pGrid, i, jc, k, &x1c, &yjc, &x3c);
        Real p_jc = func_presini_isote(yjc, yjs, p_js, T_js);
        pGrid->U[k][jc][i].d = p_jc / T_js;
        pGrid->U[k][jc][i].E = p_jc / Gamma_1 + 0.5 * (SQR(pGrid->U[k][jc][i].B1c) + SQR(pGrid->U[k][jc][i].B2c) + SQR(pGrid->U[k][jc][i].B3c));
      }
    }
  }
  return;
}

static Real func_pbypxini(Real x, Real y)
{
  Real pbypx;
  pbypx = (func_bmy(x + 1.0e-9, y) - func_bmy(x - 1.0e-9, y)) / 2.0e-9;
  return pbypx;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void openbc_ox1(GridS *pGrid)
 *  \brief open boundary conditions, Outer x1 boundary (bc_ox1=2) */
static void openbc_ox1(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][ie + i] = pGrid->U[k][j][ie];
      }
    }
  }

#ifdef MHD
  /* B2i */
  if (pGrid->Nx[1] > 1)
    ju = je + 1;
  else
    ju = je;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= ju; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B2i[k][j][ie + i] = 2.0 * pGrid->B2i[k][j][ie + i - 1] - pGrid->B2i[k][j][ie + i - 2];
      }
    }
  }

  /* B1i: i=ie+1 is not a boundary condition for the interface field B1i */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 2; i <= nghost; i++)
      {
        pGrid->B1i[k][j][ie + i] = -(pGrid->dx1 / pGrid->dx2) * (pGrid->B2i[k][j + 1][ie + i - 1] - pGrid->B2i[k][j][ie + i - 1]) + pGrid->B1i[k][j][ie + i - 1];
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B3i[k][j][ie + i] = 2.0 * pGrid->B3i[k][j][ie + i - 1] - pGrid->B3i[k][j][ie + i - 2];
      }
    }
  }
  /* B1c, 2c, 3c */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost - 1; i++)
      {
        pGrid->U[k][j][ie + i].B1c = 0.5 * (pGrid->B1i[k][j][ie + i] + pGrid->B1i[k][j][ie + i + 1]);
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][ie + i].B2c = 0.5 * (pGrid->B2i[k][j][ie + i] + pGrid->B2i[k][j + 1][ie + i]);
      }
    }
  }
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][ie + i].B3c = pGrid->U[k][j][ie].B3c;
      }
    }
  }
#endif /* MHD */

  /* Pressure and energy */
  int ic;
  Real eb_ic, ek_ic, pic;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {

        ic = ie;
        eb_ic = 0.5 * (SQR(pGrid->U[k][j][ic].B1c) + SQR(pGrid->U[k][j][ic].B2c) + SQR(pGrid->U[k][j][ic].B3c));
        ek_ic = 0.5 * (SQR(pGrid->U[k][j][ic].M1) + SQR(pGrid->U[k][j][ic].M2) + SQR(pGrid->U[k][j][ic].M3)) / pGrid->U[k][j][ic].d;
        pic = Gamma_1 * (pGrid->U[k][j][ic].E - eb_ic - ek_ic);

        ic = ie + i;
        pGrid->U[k][j][ic].E = pic / (Gamma_1) + 0.5 * (SQR(pGrid->U[k][j][ic].B1c) + SQR(pGrid->U[k][j][ic].B2c) + SQR(pGrid->U[k][j][ic].B3c)) + 0.5 * (SQR(pGrid->U[k][j][ic].M1) + SQR(pGrid->U[k][j][ic].M2) + SQR(pGrid->U[k][j][ic].M3)) / pGrid->U[k][j][ic].d;
      }
    }
  }

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void symmbc_ox1(GridS *pGrid)
 *  \brief OUTFLOW boundary conditions, Outer x1 boundary (bc_ox1=2) */
static void symmbc_ox1(GridS *pGrid)
{
  int ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ju, ku; /* j-upper, k-upper */
#endif

  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->U[k][j][ie + i] = pGrid->U[k][j][ie - i + 1];
        pGrid->U[k][j][ie + i].B2c = -pGrid->U[k][j][ie - i + 1].B2c;
        pGrid->U[k][j][ie + i].M1 = -pGrid->U[k][j][ie - i + 1].M1;
      }
    }
  }

#ifdef MHD
  /* i=ie+1 is not a boundary condition for the interface field B1i */
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 2; i <= nghost; i++)
      {
        pGrid->B1i[k][j][ie + i] = pGrid->B1i[k][j][ie - i + 2];
      }
    }
  }

  if (pGrid->Nx[1] > 1)
    ju = je + 1;
  else
    ju = je;
  for (k = ks; k <= ke; k++)
  {
    for (j = js; j <= ju; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B2i[k][j][ie + i] = -pGrid->B2i[k][j][ie - i + 1];
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = js; j <= je; j++)
    {
      for (i = 1; i <= nghost; i++)
      {
        pGrid->B3i[k][j][ie + i] = pGrid->B3i[k][j][ie - i + 1];
      }
    }
  }
#endif /* MHD */

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static void outflow_ox2(GridS *pGrid)
 *  \brief OpenBC_ox2 boundary conditions, Outer x2 boundary (bc_ox2=2) */
static void openbc_ox2(GridS *pGrid)
{
  int is = pGrid->is, ie = pGrid->ie;
  int js = pGrid->js, je = pGrid->je;
  int ks = pGrid->ks, ke = pGrid->ke;
  int i, j, k;
#ifdef MHD
  int ku; /* k-upper */
#endif
  int jc;
  Real x1c, x2c, x3c;

  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->U[k][je + j][i] = pGrid->U[k][je][i];
        pGrid->U[k][je + j][i].M2 = fabs(pGrid->U[k][je][i].M2);
      }
    }
  }

#ifdef MHD
  /* Az */
  for (k = ks; k <= ke; k++)
  {
    // je + 1 row
    az[k][je + 1][is] = 0;
    for (i = is + 1; i <= ie + nghost + 1; i++)
    {
      az[k][je + 1][i] = az[k][je + 1][i - 1] - pGrid->B2i[k][je + 1][i - 1] * pGrid->dx1;
    }
    for (i = is - 1; i >= is - nghost; i--)
    {
      az[k][je + 1][i] = az[k][je + 1][i + 1] + pGrid->B2i[k][je + 1][i] * pGrid->dx1;
    }
    // je -> je-1 rows
    for (j = je; j >= je - 1; j--)
    {
      az[k][j][is] = az[k][j + 1][is] - pGrid->B1i[k][j][is] * pGrid->dx2;
      for (i = is + 1; i <= ie + nghost + 1; i++)
      {
        az[k][j][i] = az[k][j][i - 1] - pGrid->B2i[k][j][i - 1] * pGrid->dx1;
      }
      for (i = is - 1; i >= is - nghost; i--)
      {
        az[k][j][i] = az[k][j][i + 1] + pGrid->B2i[k][j][i] * pGrid->dx1;
      }
    }
    // In ghost zone
    // case 0: zero order extrapolation
    for (j = je + 2; j <= je + nghost + 1; j++)
    {
      for (i = is - nghost; i <= ie + nghost + 1; i++)
      {
        az[k][j][i] = az[k][j - 1][i];
      }
    }
    /*
    // case 1: linear extrapolation
    for (j = je + 2; j <= je + nghost + 1; j++)
    {
      for (i = is - nghost; i <= ie + nghost + 1; i++)
      {
        az[k][j][i] = 2.0 * az[k][j - 1][i] - az[k][j - 2][i];
      }
    }
    // case 2: 2nd order extrapolation ...
    for (j = je + 2; j <= je + nghost + 1; j++)
    {
      cc_pos(pGrid, i, je + 1, k, &x1c, &x2c, &x3c);
      Real y_1 = x2c - 0.5 * pGrid->dx2;
      Real y_2 = y_1 - pGrid->dx2;
      Real y_3 = y_2 - pGrid->dx2;
      cc_pos(pGrid, i, j, k, &x1c, &x2c, &x3c);
      Real y_j = x2c - 0.5 * pGrid->dx2;
      for (i = is - nghost; i <= ie + nghost + 1; i++)
      {
        az[k][j][i] = func_2nd_extrap(y_1, y_2, y_3,
                                      az[k][je + 1][i], az[k][je][i], az[k][je - 1][i],
                                      y_j);
      }
    }
    */
  }

  /* B1i is not set at i=is-nghost */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      jc = je + j;
      for (i = is - (nghost - 1); i <= ie + nghost; i++)
      {
        pGrid->B1i[k][jc][i] = (az[k][jc + 1][i] - az[k][jc][i]) / pGrid->dx2;
      }
    }
  }

  /* j=je+1 is not a boundary condition for the interface field B2i */
  for (k = ks; k <= ke; k++)
  {
    for (j = 2; j <= nghost; j++)
    {
      jc = je + j;
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B2i[k][jc][i] = -(az[k][jc][i + 1] - az[k][jc][i]) / pGrid->dx1;
      }
    }
  }

  if (pGrid->Nx[2] > 1)
    ku = ke + 1;
  else
    ku = ke;
  for (k = ks; k <= ku; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        pGrid->B3i[k][je + j][i] = pGrid->B3i[k][je][i];
      }
    }
  }
  /* B1c, B2c, and B3c */
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      jc = je + j;
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        Real bxc = (az[k][jc + 1][i] - az[k][jc][i]) / pGrid->dx2;
        Real bxcp = (az[k][jc + 1][i + 1] - az[k][jc][i + 1]) / pGrid->dx2;
        pGrid->U[k][jc][i].B1c = 0.5 * (bxc + bxcp);

        Real byc = -(az[k][jc][i + 1] - az[k][jc][i]) / pGrid->dx1;
        Real bycp = -(az[k][jc + 1][i + 1] - az[k][jc + 1][i]) / pGrid->dx1;
        pGrid->U[k][jc][i].B2c = 0.5 * (byc + bycp);

        pGrid->U[k][jc][i].B3c = pGrid->B3i[k][je + j][i];
      }
    }
  }

#endif /* MHD */

  /* Pressure and energy  */
  Real eb_je, ek_je, pje;
  Real yje, yjc;
  for (k = ks; k <= ke; k++)
  {
    for (j = 1; j <= nghost; j++)
    {
      for (i = is - nghost; i <= ie + nghost; i++)
      {
        /* Case 1: */
        eb_je = 0.5 * (SQR(pGrid->U[k][je][i].B1c) + SQR(pGrid->U[k][je][i].B2c) + SQR(pGrid->U[k][je][i].B3c));
        ek_je = 0.5 * (SQR(pGrid->U[k][je][i].M1) + SQR(pGrid->U[k][je][i].M2) + SQR(pGrid->U[k][je][i].M3)) / pGrid->U[k][je][i].d;
        pje = Gamma_1 * (pGrid->U[k][je][i].E - eb_je - ek_je);

        jc = je + j;
        Real Tje = pje / pGrid->U[k][je][i].d;
        cc_pos(pGrid, i, je, k, &x1c, &yje, &x3c);
        cc_pos(pGrid, i, jc, k, &x1c, &yjc, &x3c);
        Real pjc = func_presini_isote(yjc, yje, pje, Tje);

        /* Case 2 
        int jc = je + j, jcm = jc - 1;
        Real eb_jcm = 0.5 * (SQR(pGrid->U[k][jcm][i].B1c) + SQR(pGrid->U[k][jcm][i].B2c) + SQR(pGrid->U[k][jcm][i].B3c));
        Real ek_jcm = 0.5 * (SQR(pGrid->U[k][jcm][i].M1) + SQR(pGrid->U[k][jcm][i].M2) + SQR(pGrid->U[k][jcm][i].M3)) / pGrid->U[k][jcm][i].d;
        Real pjcm = Gamma_1 * (pGrid->U[k][jcm][i].E - eb_jcm - ek_jcm);

        cc_pos(pGrid, i, jc, k, &x1c, &yjc, &x3c);
        Real rho_c = 0.5 * (pGrid->U[k][jc][i].d + pGrid->U[k][jcm][i].d);
        Real fg = gsun_y(yjc - 0.5 * pGrid->dx2) * rho_c;
        Real pjc = pjcm - fg * pGrid->dx2;
        */

        pGrid->U[k][jc][i].E = pjc / (Gamma_1) + 0.5 * (SQR(pGrid->U[k][jc][i].B1c) + SQR(pGrid->U[k][jc][i].B2c) + SQR(pGrid->U[k][jc][i].B3c)) + 0.5 * (SQR(pGrid->U[k][jc][i].M1) + SQR(pGrid->U[k][jc][i].M2) + SQR(pGrid->U[k][jc][i].M3)) / pGrid->U[k][jc][i].d;
      }
    }
  }
  return;
}

/*---------------------------------------------------------------------------*/
static Real func_2nd_extrap(const Real x1, const Real x2, const Real x3,
                            const Real f1, const Real f2, const Real f3,
                            const Real yc)
{
  Real x1q = SQR(x1), x2q = SQR(x2), x3q = SQR(x3);
  Real a = (f1 * (x2 - x3) - f2 * (x1 - x3) + f3 * (x1 - x2)) / (x1q * x2 - x1q * x3 - x1 * x2q + x1 * x3q + x2q * x3 - x2 * x3q);
  Real b = (-f1 * (x2q - x3q) + f2 * (x1q - x3q) - f3 * (x1q - x2q)) / (x1q * x2 - x1q * x3 - x1 * x2q + x1 * x3q + x2q * x3 - x2 * x3q);
  Real c = (f1 * x2 * x3 * (x2 - x3) - f2 * x1 * x3 * (x1 - x3) + f3 * x1 * x2 * (x1 - x2)) / (x1q * x2 - x1q * x3 - x1 * x2q + x1 * x3q + x2q * x3 - x2 * x3q);
  Real fc = a * SQR(yc) + b * yc + c;
  return fc;
}
