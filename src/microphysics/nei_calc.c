#include "../copyright.h"
/*============================================================================*/
/*! \file nei_calc.c
 *  \brief Non-equilibrium calculation routines.
 *
 * PURPOSE: NEI calcualtions in Athena code.
 *
 * REFERENCE: Shen, Raymond & Murphy, A&C, 2015.
 *
 * Update:
 *  2016-11-09:
 *  Initialization.
 *  2016-11-23
 *  Bus fixed in nei_two_step: unit of dt_c in advance_eigenmethod.
 *  2018-01-18
 *  Use Passive Scalars and perform Operator split NEI, 1st order.
 */

/*============================================================================*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../defs.h"
#include "../athena.h"
#include "../globals.h"
#include "../prototypes.h"

/*==============================================================================
 * PRIVATE VARIALBES AND FUNCTION PROTOTYPES:
 *============================================================================*/
/* NEI calc elements list */
#define natommax 31
int natom_numb, natom_list[natommax];

/* eigentables */
struct eigen_strc {
  int nte;
  int natom;
  double *telist;
  double *eqis;
  double *evals;
  double *evect;
  double *evect_invers;
};
struct eigen_strc etb[28];


/* ion_mesh files */
Real nei_dt_out;
Real time_out = 0.0;

/* Ion functions */
void read_eigentb();

void nei_init(GridS *pGrid);
void nei_two_step(GridS *pG);
void nei_stop(GridS *pGrid);

void advance_eigenmethod(GridS *pG, const int natom, const int index_sta_s);
void ionic_bc(const int is, const int ie, 
    const int js, const int je, 
    const int ks, const int ke);

int fd_teindex(const int n, const double x0, double *xarr);
Real minmod_slope(const Real aim1, const Real ai, const Real aip1, const Real dx);
Real mclimiter(const Real aim1, const Real ai, const Real aip1, const Real dx);
Real delta_slope(const Real aim1, const Real ai, const Real aip1);
/*==============================================================================
 * Characteristic values for Ionization Calc
 * Notice: The ionization rate coefficient is in cm^-3 s^1. Then ne_char is in
 * cm^-3 in here, which is different with the following MKI unit.             */
Real te_char;    /* unit: K    */
Real ne_char;    /* unit: cm^-3*/
Real time_char;  /* unit: s    */

/*=========================== PUBLIC FUNCTIONS ===============================*/
/*----------------------------------------------------------------------------*/
void nei_init(GridS *pGrid)
{
  int i, is = pGrid->is, ie = pGrid->ie;
  int j, js = pGrid->js, je = pGrid->je;
  int k, ks = pGrid->ks, ke = pGrid->ke;
  int il, iu, jl, ju, kl, ku;

  /* set calculaton box including ghost zone */
  if (pGrid->Nx[2]>1){
    kl = ks - nghost;
    ku = ke + nghost;
  } else {
    kl = ks;
    ku = ke;
  }
  jl = js - nghost;
  ju = je + nghost;
  il = is - nghost;
  iu = ie + nghost;

  /* gas */
  Real pgas, te_c;

  /* Initialize ion structures*/
  int n1 = ie-is+1+2*nghost;
  int n2 = je-js+1+2*nghost;
  int n3 = ke-ks+1+2*nghost;
  int ng = n1*n2*n3;
  int ion;
  int nte;
  int index;
  int ielem;
  int iatom, natom, nstat;
  int issta;
  int kk;
  int nsmax = 27;
  int nssave, issave;
  Real conce_c, conce[nsmax];
  Real sum;

  /*--------------------------------------------------------------------------*/
  /* Define characteristic values                                             */
  /*--------------------------------------------------------------------------*/
  /* Normalization parameters for the MHD part*/
  Real Lchar, Bchar, Nechar, Rhochar, Pchar, Timechar, Tchar, Vchar;

  /* Constant parameters */
  Real Kb = 1.38e-23, Mp = 1.6726e-27, Mu0 = 1.25663706144e-6, LnA = 30.0;
  Real G=6.672e-11, Msun=1.989e30, Rsun=6.693e8; /*in unit m^3kg^-1s^-2       */ 

  /* Read input parameters */
  Lchar = par_getd("problem", "Lchar");
  Bchar = par_getd("problem", "Bchar");
  Nechar = par_getd("problem", "Nechar");

  Pchar = Bchar*Bchar/Mu0;
  Rhochar = Nechar*Mp;
  Tchar = Pchar/(Nechar*2.*Kb); /*total number is 2*ne for full ionization plasma */
  Vchar = Bchar/sqrt(Mu0*Rhochar);
  Timechar = Lchar/Vchar;

  te_char = Tchar;         /* Unit: K     */
  ne_char = Nechar*1.0e-6; /* Unit: cm^-3 */
  time_char = Timechar;    /* Unit: s     */

  /*--------------------------------------------------------------------------*/
  /* Read eigentables                                                         */
  /*--------------------------------------------------------------------------*/
  read_eigentb();
  printf("Read_eigentable: done.\n");
 
  /*--------------------------------------------------------------------------*/
  /* Initial ionization fractions in EI                                       */
  /*--------------------------------------------------------------------------*/
  /* Read atomic information from input file */
  char* str_natomlist;
  const char delim[1] = ",";
  char *token;
  int iion, nstat_numb;
  str_natomlist = par_gets("NEI", "natom_list");
  
  /* Get atomic index to be computed */
  iatom = 0;
  token = strtok(str_natomlist, delim);
  natom_list[iatom] = atoi(token);
  iatom = iatom + 1;
  while( token != NULL ) {
    token = strtok(NULL, delim);
    if (token != NULL) {
      natom_list[iatom] = atoi(token);
      iatom = iatom + 1;
    }
  }
  
  natom_numb = iatom;
  nstat_numb = 0;
  for (iatom=0; iatom<natom_numb; iatom++) {
    nstat = natom_list[iatom] + 1;
    nstat_numb = nstat_numb + nstat;
    printf("natom = %d, %d, %d\n", iatom, natom_list[iatom], nstat);
  }
  
  /* Check configuration and input */
  if (nstat_numb != NSCALARS) {
    printf("NSCALARS=%d, and nstat_numb=%d\n", NSCALARS, nstat_numb);
    ath_error("[NEI Initial Step]: Conflict of NEI states between Configure(NSCALARS) and Input(natom_list).\n");
  }
  
  /* Set the initial condition at each cell for all ions */
  for(k = kl; k <= ku; k++) {
    for(j = jl; j <= ju; j++) {
      for(i = il; i <= iu; i++) {

        /* Get Temperature (K) */
#ifdef MHD
        pgas = (Gamma_1)*(pGrid->U[k][j][i].E
                          - 0.5*(SQR(pGrid->U[k][j][i].B1c)
                                 + SQR(pGrid->U[k][j][i].B2c)
                                 + SQR(pGrid->U[k][j][i].B3c))
                          - 0.5*(SQR(pGrid->U[k][j][i].M1)
                                 + SQR(pGrid->U[k][j][i].M2)
                                 + SQR(pGrid->U[k][j][i].M3))
                          /pGrid->U[k][j][i].d);
#else
        pgas = (Gamma_1)*(pGrid->U[k][j][i].E
                          - 0.5*(SQR(pGrid->U[k][j][i].M1)
                                 + SQR(pGrid->U[k][j][i].M2)
                                 + SQR(pGrid->U[k][j][i].M3))
                          /pGrid->U[k][j][i].d);
#endif

        te_c = (pgas/pGrid->U[k][j][i].d)*te_char;

        /* Temperature index */
        nte = etb[0].nte;
        index = fd_teindex(nte, te_c, etb[0].telist);
        
        /* 2018 new version */
        iion = 0;
        for (ielem=0; ielem<natom_numb; ielem++) {
          natom = natom_list[ielem];
          iatom = natom - 1;
          for (ion = 0; ion <= natom; ion++) {
            kk = ion*nte + index;
            conce_c = etb[iatom].eqis[kk];
            pGrid->U[k][j][i].s[iion] = conce_c*(pGrid->U[k][j][i].d);
            iion = iion + 1;
          }
        }
      } /* End ion_mesh grid loop here: (i,j,k)*/
    }
  }

  /* Check the initialization process
     for(k = kl; k <= ku; k++) {
     for(j = jl; j <= ju; j++) {
     for(i = il; i <= iu; i++) {
     for (ielem = 0; ielem <= elem_numb-1; ielem++) {
     natom = elem_natom[ielem];
     sum = 0.0;
     for (ion = 0; ion <= natom; ion++) {
     printf("[%d, %d, %d], [natom=%d], ion_mesh[%d]=%f\n", k, j, i, natom, ion,
     ion_mesh[k][j][i].atomid[ielem].conce[ion]);
     sum = sum + ion_mesh[k][j][i].atomid[ielem].conce[ion];
     }
     printf(".......................sum = %9.7f\n", sum);
     }
     }
     }
     }  */
  printf("Initialization ionic charge states: done.\n");
  return;
}

/*----------------------------------------------------------------------------*/
void nei_stop(GridS *pG)
{
  /* Free ionbc buffer */

  /* Free ...*/
  return;
}


/*----------------------------------------------------------------------------*/
void nei_two_step(GridS *pG)
{
  int is = pG->is, ie = pG->ie;
  int js = pG->js, je = pG->je;
  int ks = pG->ks, ke = pG->ke;

  int n1 = ie-is+1+2*nghost;
  int n2 = je-js+1+2*nghost;
  int n3 = ke-ks+1;
  int ng = n1*n2*n3;
  int i, j, k, kk;

  int il, iu, jl, ju, kl, ku;

  /* set calculaton box including ghost zone */
  if (pG->Nx[2]>1){
    kl = ks - nghost;
    ku = ke + nghost;
  } else {
    kl = ks;
    ku = ke;
  }
  jl = js - nghost;
  ju = je + nghost;
  il = is - nghost;
  iu = ie + nghost;

  /* atomic */
  int ielem; /* element index */
  int ista, natom;
  
  /*--------------------------------------------------------------------------*/
  /* (1) Advance one step using Eigenvalue methos                             */
  /*--------------------------------------------------------------------------*/
  ista = 0;
  for (ielem=0; ielem<natom_numb; ielem++) {
    /* The current natom */
    natom = natom_list[ielem];
    
    //printf("natom=%d, ista=%d\n", natom, ista);
    advance_eigenmethod(pG, natom, ista);
    
    /* For the next element */
    ista = ista + natom + 1;
    
  }
  
  /* apply ionic boundary conditions */
  //ionic_bc(is, ie, js, je, ks, ke);

  /*---------------------------------------------------------------------------
   * (2) Update ionization states in ghost zone by MPI                        
   * ------------------------------------------------------------------------*/
  //sendrecv_ionbc(pG);

  /*---------------------------------------------------------------------------
   * (3) Advance dt for advection part
   *-------------------------------------------------------------------------*/
  //advance_order_3rd_split(pG);
  //advance_order_2nd_split(pG);

  /*---------------------------------------------------------------------------
   * (4) Update nei_m123 & nei_d to the current time 
   * ------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * (5) Output ion_mesh to files 
   * ------------------------------------------------------------------------*/
  return;
}

/*----------------------------------------------------------------------------*/
void read_eigentb()
{
  FILE *fp;
  char filename[300];
  char* nei_datapath;

  int i,j,k,kk;
  int iatom;
  /* local temperary variables */
  int nte_c, natom_c;
  double temp;

  /* Get NEI table path */
  nei_datapath = par_gets("NEI", "nei_datapath");
  
  /* Enter the main loop */
  for (iatom = 0; iatom <28; iatom++) {

    /* open file*/
    sprintf(filename, "%s%d.dat", nei_datapath,iatom+1);

    fp = fopen(filename, "r");

    /* read nte*/
    fscanf(fp, "%d", &nte_c);
    etb[iatom].nte = nte_c;

    /* read natom */
    fscanf(fp, "%d", &natom_c);
    etb[iatom].natom = natom_c;

    /* read te_arr */
    etb[iatom].telist = (double*)calloc(nte_c, sizeof(double));
    for (j = 0; j < nte_c; j++) {
      fscanf(fp, "%lf", &temp);
      etb[iatom].telist[j] = temp;
    }

    /* read eqistate */
    etb[iatom].eqis = (double*)calloc((natom_c+1)*nte_c, sizeof(double));
    for (i = 0; i < natom_c+1; i++) {
      for (j = 0; j < nte_c; j++) {
        fscanf(fp, "%lf", &temp);
        kk = i*nte_c + j;
        etb[iatom].eqis[kk] = temp;
      }
    }

    /* read eigenvalues */
    etb[iatom].evals = (double*)calloc((natom_c+1)*nte_c, sizeof(double));
    for (i = 0; i < natom_c+1; i++) {
      for (j = 0; j < nte_c; j++) {
        fscanf(fp, "%lf", &temp);
        kk = i*nte_c + j;
        etb[iatom].evals[kk] = temp;
      }
    }

    /* read eigenvector */
    etb[iatom].evect = (double*)calloc((natom_c+1)*(natom_c+1)*nte_c, sizeof(double));
    for (i = 0; i < natom_c+1; i++) {
      for (j = 0; j < natom_c+1; j++) {
        for (k = 0; k < nte_c; k++) {
          fscanf(fp, "%lf", &temp);
          kk = i*nte_c*(natom_c+1) + j*nte_c + k;
          etb[iatom].evect[kk] = temp;
        }
      }
    }

    /* read eigenvector_invers */
    etb[iatom].evect_invers = (double*)calloc((natom_c+1)*(natom_c+1)*nte_c, sizeof(double));
    for (i = 0; i < natom_c+1; i++) {
      for (j = 0; j < natom_c+1; j++) {
        for (k = 0; k < nte_c; k++) {
          fscanf(fp, "%lf", &temp);
          kk = i*nte_c*(natom_c+1) + j*nte_c + k;
          etb[iatom].evect_invers[kk] = temp;
        }
      }
    }

    fclose(fp);
  }
  return;
}

/*------------------------------------------------------------------------------
 * fd_teindex                                                                   
 *----------------------------------------------------------------------------*/
int fd_teindex(const int n, const double x0, double *xarr)
{
  int index, index_out;
  int i;
  Real dx;
  Real dx0 = 1.0e+20;

  for (i = 0; i < n; i++) {
    dx = fabs(x0 - xarr[i]);
    if (dx <= dx0) {
      dx0 = dx;
      index = i;
    }
  }
  return index;
}

/*------------------------------------------------------------------------------
 * functions                                                                    
 *----------------------------------------------------------------------------*/
void advance_eigenmethod(GridS *pG, const int natom, const int index_sta_s)
{
  /* Two input parameters in here:
   (1) natom -- the atomic number for the current element
   (2) index_sta_s -- the begining index of the current element located in U.s[*]
   list.*/
  
  /* Grid size */
  int i, j, k;
  int is = pG->is, ie = pG->ie;
  int js = pG->js, je = pG->je;
  int ks = pG->ks, ke = pG->ke;
  
  /* Ionic index */
  int nstat = natom +1;
  int iatom = natom -1;
  
  /* Eigenmatrix */
  int nsmax = nstat;
  Real evals[nsmax];
  Real evect[nsmax][nsmax];
  Real evect_invers[nsmax][nsmax];
  Real diagona_vec[nsmax][nsmax];
  Real matrix_1[nsmax][nsmax];
  Real matrix_2[nsmax][nsmax];
  Real conce_pre[nstat], conce_new[nstat], conce_sav[nstat];
  
  int ion, jon, kon;
  int index_te, nte;
  int kk;
  Real sum, d_sum;
  
  int n_eff;
  Real d_conce, conce_max;
  
  /* MHD variables */
  Real ek, eb;
  Real pgas, ne_c, te_c, dt_c;
  dt_c = pG->dt*time_char;
  
  /* enter grids loop */
  for(k = ks; k <= ke; k++) {
    for(j = js; j <= je; j++) {
      for(i = is; i <= ie; i++) {
        
        /* get temperature, density from mesh */
        ek = 0.5*(SQR(pG->U[k][j][i].M1)
                  + SQR(pG->U[k][j][i].M2)
                  + SQR(pG->U[k][j][i].M3))/(pG->U[k][j][i].d);
#ifdef MHD
        eb = 0.5*(SQR(pG->U[k][j][i].B1c)
                  + SQR(pG->U[k][j][i].B2c)
                  + SQR(pG->U[k][j][i].B3c));
        pgas = Gamma_1*(pG->U[k][j][i].E-eb-ek);
        
#else
        pgas = Gamma_1*(pG->U[k][j][i].E-ek);
#endif
        
        /* in eigenmethod, all variables are SI physical unit
         * : te_c (K),  ne_c (cm^-3), and dt_c (s) */
        te_c = (pgas/pG->U[k][j][i].d)*te_char;
        ne_c = (pG->U[k][j][i].d)*ne_char;
        
        /* get conce from ion_mesh */
        for (ion = 0; ion <= natom; ion++) {
          conce_pre[ion] = pG->U[k][j][i].s[ion+index_sta_s]/(pG->U[k][j][i].d);
        }
        
        /* check negative fractions
        for (ion = 0; ion <= natom; ion++) {
          if (conce_pre[ion] <-1.0e-7) {
            printf("Read negative conce[%d]=%9.7f\n", ion, conce_pre[ion]);
          }
        }*/
        
        /* Get eigentalbe info */
        nte = etb[0].nte;
        
        /* Calculate Te index in eigentables */
        index_te = 0;
        while (etb[0].telist[index_te] <= te_c) {
          index_te++;
        }
        if ((etb[0].telist[index_te]-te_c) <= (te_c-etb[0].telist[index_te-1])) {
          index_te = index_te;
        } else {
          index_te = index_te-1;
        }
        
        /* Load eigentable pieces */
        for (ion = 0; ion < nstat; ion++) {
          kk = ion*nte + index_te;
          evals[ion] = etb[iatom].evals[kk];
        }
        
        for (ion = 0; ion < nstat; ion++) {
          for (jon = 0; jon < nstat; jon++) {
            kk = ion*nte*(nstat) + jon*nte + index_te;
            evect[ion][jon] = etb[iatom].evect[kk];
            evect_invers[ion][jon] = etb[iatom].evect_invers[kk];
          }
        }
        
        /* temperary diagonamatrix */
        for (ion = 0; ion < nstat; ion++) {
          for (jon = 0; jon < nstat; jon++) {
            diagona_vec[ion][jon] = 0.0;
          }
        }
        for (ion = 0; ion < nstat; ion++) {
          diagona_vec[ion][ion] = exp(evals[ion]*dt_c*ne_c);
        }
        
        /* matrix operations*/
        /* (a) get matrix_1: */
        for (ion = 0; ion < nstat; ion++) {
          for (jon = 0; jon < nstat; jon++) {
            sum  = 0.0;
            for (kon = 0; kon < nstat; kon++) {
              sum = sum + evect[kon][jon]*diagona_vec[ion][kon];
            }
            matrix_1[ion][jon] = sum;
          }
        }
        
        /* (b) get matrix_2: */
        for (ion = 0; ion < nstat; ion++) {
          for (jon = 0; jon < nstat; jon++) {
            sum  = 0.0;
            for (kon = 0; kon < nstat; kon++) {
              sum = sum + matrix_1[kon][jon]*evect_invers[ion][kon];
            }
            matrix_2[ion][jon] = sum;
          }
        }
        
        /* (c) get ion fractions */
        for (jon = 0; jon < nstat; jon++) {
          sum = 0.0;
          for (kon = 0; kon < nstat; kon++) {
            sum = sum + matrix_2[kon][jon]*conce_pre[kon];
          }
          conce_new[jon] = sum;
        }
        
        /* (e) negative check */
        for (ion = 0; ion <= natom; ion++) {
          if(conce_new[ion] < 0.0){
            conce_sav[ion] = +0.0;
          } else {
            conce_sav[ion] = conce_new[ion];
          }
        }
        sum = 0.0;
        Real conce_max = 0.0;
        int if_max;
        for (ion = 0; ion <= natom; ion++) {
          sum = sum + conce_sav[ion];
          if (conce_sav[ion] >= conce_max) {
            if_max = ion;
            conce_max = conce_sav[ion];
          }
        }
        d_sum = 1.0 - sum;
        conce_sav[if_max] = conce_sav[if_max] + d_sum;
        
        /* Ensure the total fraction is equal to 1 */
        sum = 0.0;
        for (ion = 0; ion <= natom; ion++) {
          sum = sum + conce_sav[ion];
        }
        d_sum = 1.0 - sum;
        if (fabs(d_sum) >= 1.0e-6) {
          for (ion = 0; ion <= natom; ion++) {
            printf(" Natom[%d].ion[%d] = %.7e", natom, ion, conce_pre[ion]);
          }
          printf(" total fraction = %.7f\n", sum);
          ath_error("[NEI Error]: Total ion fraction is less than One .\n");
        }
        
        /* (f) save ion fractions into ion_mesh */
        for (ion = 0; ion <= natom; ion++) {
          pG->U[k][j][i].s[ion+index_sta_s] = (double)conce_sav[ion]*pG->U[k][j][i].d;
        }
        
        /* clean */
        for (ion = 0; ion <= natom; ion++) {
          conce_new[ion] = 0.0;
        }
        
      } /* loop i */
    } /* loop j */
  } /* loop k */
  return;
}

/*------------------------------------------------------------------------------
 * advance_order_1st
 *----------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
 * advance_order_2nd_split                                                    
 *----------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
 * advance_order_2nd_unsplit                                                    
 *----------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
 * advance_order_3rd_split                                                    
 *----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
 * sendrecv_ionbc                                                
 *----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
 * ionic_bc
 * 2016-10-24:
 *  2D cases.                                                  
 *----------------------------------------------------------------------------*/

Real minmod_slope(const Real aim1, const Real ai, const Real aip1, const Real dx)
{
  Real minmod;
  Real a, b, a_abs, b_abs;
  a = (ai-aim1)/dx;
  b = (aip1-ai)/dx;
  a_abs = fabs(a);
  b_abs = fabs(b);
  if (a_abs < b_abs && a*b > 0.0) {
    minmod = a;
  } else if (b_abs < a_abs && a*b > 0.0) {
    minmod = b;
  } else {
    minmod = 0.0;
  }
  return minmod;
}

Real mclimiter(const Real aim1, const Real ai, const Real aip1, const Real dx)
{
  Real paipx;
  Real ksi;
  Real minvalue, signvalue;
  Real p1, p2, p3, diff;
  p1 = fabs((aip1 - aim1)/(2.0*dx));
  p2 = 2.0*fabs((aip1-ai)/dx);
  p3 = 2.0*fabs((ai-aim1)/dx);
  // minvalue
  if (p1 <= p2) {
    minvalue = p1;
  } else {
    minvalue = p2;
  }
  if (p3 <= minvalue) {
    minvalue = p3;
  }
  // signvalue
  diff = aip1-aim1;
  if (diff < 0.0) {
    signvalue = -1.0;
  } else if (diff == 0.0) {
    signvalue = 0.0;
  } else {
    signvalue = 1.0;
  }
  // return partial a partial x
  ksi = (aip1 - ai)*(ai - aim1);
  if (ksi > 0.0) {
    paipx = minvalue*signvalue;
  } else {
    paipx = 0.0;
  }
  return paipx;
}

Real delta_slope(const Real aim1, const Real ai, const Real aip1)
{
  Real delm;
  Real delc, dell, delr;
  Real mindel, signvalue;
  // delta ajc ...
  delc = (aip1 - aim1)/2.0;
  delr = aip1-ai;
  dell = ai-aim1;
  
  if ((aip1-ai)*(ai-aim1) > 0.0) {
    // minvalue
    mindel = MIN(fabs(delc), fabs(2.0*dell));
    mindel = MIN(mindel, fabs(2.0*delr));
    
    // sgn
    if (delc < 0.0) {
      signvalue = -1.0;
    } else if (delc == 0.0) {
      signvalue = 0.0;
    } else {
      signvalue = 1.0;
    }
    
    delm = mindel*signvalue;
  } else {
    delm = 0.0;
  }
  return delm;
}

