#include "../copyright.h"
/*============================================================================*/
/*! \file conduction.c
 *  \brief Adds explicit thermal conduction term to the energy equation,
 *      dE/dt = Div(Q)
 *
 *   where
 *    - Q = kappa_iso Grad(T) + kappa_aniso([b Dot Grad(T)]b) = heat flux
 *    - T = (P/d)*(mbar/k_B) = temperature
 *    - b = magnetic field unit vector
 *
 *   Here
 *    - kappa_iso   is the   isotropic coefficient of thermal diffusion
 *    - kappa_aniso is the anisotropic coefficient of thermal diffusion
 *
 * Note the kappa's are DIFFUSIVITIES, not CONDUCTIVITIES.  Also note this
 * version uses "dimensionless units" in that the factor (mbar/k_B) is not
 * included in calculating the temperature (instead, T=P/d is used).  For cgs
 * units, kappa must be entered in units of [cm^2/s], and the heat fluxes would
 * need to be multiplied by (k_B/mbar).
 *
 * The heat flux Q is calculated by calls to HeatFlux_* functions.
 *
 * CONTAINS PUBLIC FUNCTIONS:
 * - conduction() - updates energy equation with thermal conduction
 * - conduction_init() - allocates memory needed
 * - conduction_destruct() - frees memory used
 *
 * Update:
 *   2016-09-12: Add super time-stepping in here according to Meyer et al.
 *   2012, MN. The stages for perfroming RKL2 is aways larger than 3, which
 *   should be changed in following modifications. By now, it works on 2D
 *   cases. (Chengcai Shen)
 *
 *   2016-09-13: Add parallel calculations. However, it is not the asynchronous
 *   MPICH mode, the efficience is then still low.
 *   In the current test, we only include Kappa parallel in here.
 *    (Chengcai Shen) 
 *   2016-10-03: Bug fixed and use Te in boundaries for MPI messages.
 *    (Chengcai Shen)
 *   2016-10-11: Increase Kappa when T is lower than Tc(Reeves et al. 2010, ApJ)
 *    (Chengcai Shen)
 *   2016-10-12: Bug fixed in increasing kappa according inputing 'sw_cooling'
 *    (Chengcai Shen)
 *   2016-10-19:
 *    Improve MPI parallel caculations in 2D and 3D cases. (Chengcai Shen)
 *   2016-10-28:
 *    Improve MPI efficiency by reranging the order of messages.
 *    (Chengcai Shen)
 *   2016-11-01:
 *    Improve MPI. (Chengcai Shen) 
 *   2018-03-07
 *    Reset the explicit time-step.
 *   2018-05-28
 *    Add <stdlib.h>.
 *    */

/*============================================================================*/

#include <math.h>
#include <float.h>
#include <stdlib.h>
#include "../defs.h"
#include "../athena.h"
#include "../globals.h"
#include "prototypes.h"
#include "../prototypes.h"

#ifdef THERMAL_CONDUCTION

#ifdef BAROTROPIC
#error : Thermal conduction requires an adiabatic EOS
#endif

/* Arrays for the temperature and heat fluxes */
static Real ***Temp=NULL;
static Real3Vect ***Q=NULL;

/*==============================================================================
 * PRIVATE FUNCTION PROTOTYPES:
 *   HeatFlux_iso   - computes   isotropic heat flux
 *   HeatFlux_aniso - computes anisotropic heat flux
 *============================================================================*/

void HeatFlux_iso(DomainS *pD);
void HeatFlux_aniso(DomainS *pD, const int is, const int iem1, 
		                 const int js, const int jem1,
				 const int ks, const int kem1);

/*kappa iso and kappa_aniso depend on temperature*/
extern Real Kappa_iso_dependTemp(const Real T);
extern Real Kappa_aniso_dependTemp(const Real T);

static Real limiter2(const Real A, const Real B);
static Real limiter4(const Real A, const Real B, const Real C, const Real D);
static Real vanleer (const Real A, const Real B);
static Real minmod  (const Real A, const Real B);

/*=========================== PUBLIC FUNCTIONS ===============================*/
/*----------------------------------------------------------------------------*/
/*! \fn void conduction(DomainS *pD)
 *  \brief Explicit thermal conduction
 */
void conduction(DomainS *pD)
{
  GridS *pG = (pD->Grid);
  int i, is = pG->is, ie = pG->ie;
  int j, jl, ju, js = pG->js, je = pG->je;
  int k, kl, ku, ks = pG->ks, ke = pG->ke;
  int ii, jj, kk;
#ifdef STS
  Real my_dt = STS_dt;
#else
  Real my_dt = pG->dt;
#endif
  Real dtodx1=my_dt/pG->dx1, dtodx2=0.0, dtodx3=0.0;

  /* variables for super time-stepping: RKL2 (Meyer et al, 2012, MN) */
  int n1 = ie-is+1 + 2*nghost;
  int n2 = je-js+1 + 2*nghost;
  int n3 = ke-ks+1 + 2*nghost;
  if(ke == ks) n3 = 1; 

  /* Time-step part*/
  int j_cyc, s_cyc;
  Real qa;
  Real dt_parab, dt_temp, mindx, Temax;
  Real kd_arr[n3][n2][n1];
  Real kd, kdx, kdy;
  Real y0[n3][n2][n1], yj[n3][n2][n1], yjm1[n3][n2][n1], yjm2[n3][n2][n1];
  Real lclass_0[n3][n2][n1], lclass_jm1[n3][n2][n1];
  Real aj, ajm1, bj, bjm1, bjm2;
  Real muj, nuj, mujbar, mu1bar, gammajbar;
  Real sum_s;

  /* MPI send & recv */
  MPI_Request reqs[4];
  MPI_Status stats[4];
  int myid, nprocs;
  int prev_x, next_x,
      prev_y, next_y,
      prev_z, next_z;
  int tag1=1, tag2=2;
  int nxio = ie-is+1, nyio = je-js+1, nzio = ke-ks+1;
  int nxsq = nyio*nzio,
      nysq = nxio*nzio,
      nzsq = nxio*nyio;
  Real te_ix[nxsq], te_ox[nxsq], te_ixg[nxsq], te_oxg[nxsq];
  Real te_iy[nysq], te_oy[nysq], te_iyg[nysq], te_oyg[nysq];
  Real te_iz[nzsq], te_oz[nzsq], te_izg[nzsq], te_ozg[nzsq];

  /* MPI cost test*/
  Real mpit0, mpit1, mpicost;
  mpicost = 0.0;

  /* Get MPI info */
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  /* Read parameters */
  //sw_cooling = par_getd("problem", "sw_cooling");

  /* Check the mpi index:
  printf("myid=%d, lx=%d, rx=%d, ly=%d, ry=%d, lz=%d, rz=%d\n",
      myid, 
      pG->lx1_id, pG->rx1_id, pG->lx2_id, pG->rx2_id, pG->lx3_id, pG->rx3_id);*/

  /* array ranges in 1D, 2D, and 3D */
  if (pG->Nx[1] > 1){
    jl = js - 1;
    ju = je + 1;
    dtodx2 = my_dt/pG->dx2;
  } else {
    jl = js;
    ju = je;
  }
  if (pG->Nx[2] > 1){
    kl = ks - 1;
    ku = ke + 1;
    dtodx3 = my_dt/pG->dx3;
  } else {
    kl = ks;
    ku = ke;
  }

  /* Intital super time-stepping */
  /* E0 at the beginning */
  for (k=kl; k<=ku; k++) {
    for (j=jl; j<=ju; j++) {
      for (i=is-1; i<=ie+1; i++) {
        y0[k][j][i] = pG->U[k][j][i].E;
      }
    }
  }
  /* Time-step in explicit scheme */
  dt_parab = my_dt;
  mindx = pG->dx1;
  if (mindx >= pG->dx2) {
    mindx = pG->dx2;
  }
  if (mindx >= pG->dx3 && pG->Nx[2]>1) {
    mindx = pG->dx3;
  }
  qa = (mindx*mindx)/4.0;
  if (pG->Nx[1] > 1) qa = (mindx*mindx)/8.0;
  if (pG->Nx[2] > 1) qa = (mindx*mindx)/6.0;

  Temax = 0.0;
  for (k=kl; k<=ku; k++) {
    for (j=jl; j<=ju; j++) {
      for (i=is-1; i<=ie+1; i++) {
        Temp[k][j][i] = (Gamma_1/pG->U[k][j][i].d)*(y0[k][j][i]
            - (0.5/pG->U[k][j][i].d)*
            (SQR(pG->U[k][j][i].M1)
             +SQR(pG->U[k][j][i].M2)
             +SQR(pG->U[k][j][i].M3))
            - (0.5)*(SQR(pG->U[k][j][i].B1c)
              +SQR(pG->U[k][j][i].B2c)
              +SQR(pG->U[k][j][i].B3c)));

        if(Temax <= Temp[k][j][i]) Temax = Temp[k][j][i];
        kd_arr[k][j][i] = Kappa_aniso_dependTemp(Temax);
      }
    }
  }

  // Get dt_parab according to the maximum temperature.
  //dt_parab = qa/Kappa_aniso_dependTemp(Temax);
  
  /* Get average kappa in both x- and y- directions. */
  dt_parab = 1.0;
  for (k=kl; k<=ku; k++) {
    for (j=jl; j<=ju-1; j++) {
      for (i=is; i<=pG->ie-1; i++) {
        kdx = 2.0*(kd_arr[k][j][i+1]*kd_arr[k][j][i])
          /(kd_arr[k][j][i+1] + kd_arr[k][j][i]);
        kdy = 2.0*(kd_arr[k][j+1][i]*kd_arr[k][j][i])
          /(kd_arr[k][j+1][i] + kd_arr[k][j][i]);
        kd = MAX(kdx, kdy);
        dt_parab = MIN(dt_parab, (qa/kd));
      }
    }
  }

  /* Compute super time-stepping stages */
  s_cyc = (int)(pow((4.*my_dt/dt_parab + 2.0),0.5));
  sum_s = (double)s_cyc*(double)(s_cyc+1) -2.0;
  if (sum_s/4.0 <= my_dt/dt_parab) {
    s_cyc = s_cyc + 2;
  }
  if (s_cyc <= 3){
    s_cyc = 3;
  }
  /*printf("id=%d, dt_p=%f, s=%d\n", myid, dt_parab, s_cyc);*/

  /* Get the minimum s_cyc on all parallel domains */
  int *nscyc_buf = NULL;
  nscyc_buf = (int *)malloc(sizeof(int) * nprocs);

  MPI_Gather(&s_cyc, 1, MPI_INT, nscyc_buf, 1, MPI_INT, 0, MPI_COMM_WORLD);
  if (myid == 0) {
    s_cyc = 3;
    for (j_cyc=0; j_cyc<=nprocs-1; j_cyc++){
      if (s_cyc <= nscyc_buf[j_cyc]) {
        s_cyc = nscyc_buf[j_cyc];
      }
    }
    for (j_cyc=0; j_cyc<=nprocs-1; j_cyc++){
      nscyc_buf[j_cyc] = s_cyc;
    }
  }
  MPI_Scatter(nscyc_buf, 1, MPI_INT, &s_cyc, 1, MPI_INT, 0, MPI_COMM_WORLD);
  free(nscyc_buf);

  /* Compute the operator_0: Lclass_0*/
  HeatFlux_aniso(pD, is, ie, js, je, ks, ke);

  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie; i++) {
        lclass_0[k][j][i] = (Q[k][j][i+1].x1 - Q[k][j][i].x1)/pG->dx1;
      }
    }
  }
  if (pG->Nx[1] > 1){
    for (k=ks; k<=ke; k++) {
      for (j=js; j<=je; j++) {
        for (i=is; i<=ie; i++) {
          lclass_0[k][j][i] = lclass_0[k][j][i]+(Q[k][j+1][i].x2 - Q[k][j][i].x2)/pG->dx2;
        }
      }
    }
  }
  if (pG->Nx[2] > 1){
    for (k=ks; k<=ke; k++) {
      for (j=js; j<=je; j++) {
        for (i=is; i<=ie; i++) {
          lclass_0[k][j][i] = lclass_0[k][j][i]+(Q[k+1][j][i].x3 - Q[k][j][i].x3)/pG->dx3;
        }
      }
    }
  }

  /* Prapare for the cycle of super time-stepping */
  bjm1 = 1./3.;
  bjm2 = 1./3.;
  ajm1 = 1.0 - bjm1;
  sum_s = (double)s_cyc*(double)(s_cyc+1) -2.0;
  mu1bar = 4.0/(3.0*sum_s);

  for (k=kl; k<=ku; k++) {
    for (j=jl; j<=ju; j++) {
      for (i=is-1; i<=ie+1; i++) {
        yjm2[k][j][i] = y0[k][j][i];
        yjm1[k][j][i] = yjm2[k][j][i] + mu1bar*my_dt*lclass_0[k][j][i];
      }
    }
  }

  /* Start sub_cycle: j_cyc */
  for (j_cyc = 2; j_cyc <= s_cyc; j_cyc++){

    /* (is_cyc: a) parameters */
    bj = ((double)j_cyc*(double)j_cyc + (double)j_cyc -2.)/(2.*j_cyc*(j_cyc+1.0));
    aj = 1.- bj;
    muj    = (2.*j_cyc-1)/((double)j_cyc)*(bj/bjm1);
    nuj    = -(j_cyc-1.0)/((double)j_cyc)*(bj/bjm2);
    mujbar = 4.0*(2.*j_cyc-1.0)/(j_cyc*sum_s)*(bj/bjm1);
    gammajbar = -1.0*ajm1*mujbar;


    /* (is_cyc: b)Compute the operator_j-1: Lclass_jm1*/
    /* (b-0) Clean Q */
    for (k=kl; k<=ku; k++) {
      for (j=jl; j<=ju; j++) {
        for (i=is-1; i<=ie+1; i++) {
          Q[k][j][i].x1 = 0.0;
          Q[k][j][i].x2 = 0.0;
          Q[k][j][i].x3 = 0.0;
        }
      }
    }

    /*(b-1) Temperature from yjm1 */
    for (k=ks; k<=ke; k++) {
      for (j=js; j<=je; j++) {
        for (i=is; i<=ie; i++) {
          Temp[k][j][i] = (Gamma_1/pG->U[k][j][i].d)*(yjm1[k][j][i]
              - (0.5/pG->U[k][j][i].d)*
              (SQR(pG->U[k][j][i].M1)
               +SQR(pG->U[k][j][i].M2)
               +SQR(pG->U[k][j][i].M3))
              - (0.5)*(SQR(pG->U[k][j][i].B1c)
                +SQR(pG->U[k][j][i].B2c)
                +SQR(pG->U[k][j][i].B3c)));
        }
      }
    }
    /* at boundary: te to be setted from simple extropolation, 
     * The values will be updated using MPI messages if they are inner points.*/
    for (k=kl; k<=ku; k++) {
      for (j=js; j<=je; j++) {
        Temp[k][j][is-1] = Temp[k][j][is];
        Temp[k][j][ie+1] = Temp[k][j][ie];
      }
    }
    for (k=kl; k<=ku; k++) {
      for (i=is-1; i<=ie+1; i++) {
        Temp[k][js-1][i] = Temp[k][js][i];
        Temp[k][je+1][i] = Temp[k][je][i];
      }
    }
    if (pG->Nx[2] > 1) {
      for (j=js-1; j<=je+1; j++) {
        for (i=is-1; i<=ie+1; i++) {
          Temp[ks-1][j][i] = Temp[ks][j][i];
          Temp[ke+1][j][i] = Temp[ke][j][i];
        }
      }
    }

    /* Print MPI cost
    mpit0 = MPI_Wtime(); */

    /*(b-2a.x) x-direction: buffer te_ix, te_ox */
    prev_x = pG->lx1_id;
    next_x = pG->rx1_id;
    if (prev_x < 0)  prev_x = MPI_PROC_NULL;
    if (next_x < 0)  next_x = MPI_PROC_NULL;
    /* define te_ix and te_ox*/
    for (k=ks; k<=ke; k++) {
      for (j=js; j<=je; j++) {
        ii = (j-js) + nyio*(k-ks);
        te_ix[ii] = Temp[k][j][is];
        te_ox[ii] = Temp[k][j][ie];
      }
    }
    MPI_Irecv(&te_ixg, nxsq, MPI_DOUBLE, prev_x, tag1, MPI_COMM_WORLD, &reqs[0]);
    MPI_Irecv(&te_oxg, nxsq, MPI_DOUBLE, next_x, tag2, MPI_COMM_WORLD, &reqs[1]);
    MPI_Isend(&te_ix, nxsq, MPI_DOUBLE, prev_x, tag2, MPI_COMM_WORLD, &reqs[2]);
    MPI_Isend(&te_ox, nxsq, MPI_DOUBLE, next_x, tag1, MPI_COMM_WORLD, &reqs[3]);
    MPI_Waitall(4, reqs, stats);

    if (prev_x >= 0) {
      for (k=ks; k<=ke; k++) {
        for (j=js; j<=je; j++) {
          ii = (j-js) + nyio*(k-ks);
          Temp[k][j][is-1] = te_ixg[ii];
        }
      }
    }
    if (next_x >= 0) {
      for (k=ks; k<=ke; k++) {
        for (j=js; j<=je; j++) {
          ii = (j-js) + nyio*(k-ks);
          Temp[k][j][ie+1] = te_oxg[ii];
        }
      }
    }

    /*(b-2a.y) y-direction: buffer te_iy, te_oy */
    prev_y = pG->lx2_id;
    next_y = pG->rx2_id;
    if (prev_y < 0)  prev_y = MPI_PROC_NULL;
    if (next_y < 0)  next_y = MPI_PROC_NULL;
    /* te_iy and te_oy*/
    for (k=ks; k<=ke; k++) {
      for (i=is; i<=ie; i++) {
        jj = (i-is) + nxio*(k-ks);
        te_iy[jj] = Temp[k][js][i];
        te_oy[jj] = Temp[k][je][i];
      }
    }
    MPI_Irecv(&te_iyg, nysq, MPI_DOUBLE, prev_y, tag1, MPI_COMM_WORLD, &reqs[0]);
    MPI_Irecv(&te_oyg, nysq, MPI_DOUBLE, next_y, tag2, MPI_COMM_WORLD, &reqs[1]);
    MPI_Isend(&te_iy, nysq, MPI_DOUBLE, prev_y, tag2, MPI_COMM_WORLD, &reqs[2]);
    MPI_Isend(&te_oy, nysq, MPI_DOUBLE, next_y, tag1, MPI_COMM_WORLD, &reqs[3]);
    MPI_Waitall(4, reqs, stats);

    if (prev_y >= 0) {
      for (k=ks; k<=ke; k++) {
        for (i=is; i<=ie; i++) {
          jj = (i-is) + nxio*(k-ks);
          Temp[k][js-1][i] = te_iyg[jj];
        }
      }
    }
    if (next_y >= 0) {
      for (k=ks; k<=ke; k++) {
        for (i=is; i<=ie; i++) {
          jj = (i-is) + nxio*(k-ks);
          Temp[k][je+1][i] = te_oyg[jj];
        }
      }
    }

    /*(b-2a.z) z-direction: buffer te_iz, te_oz */
    if (pG->Nx[2] > 1) {
      prev_z = pG->lx3_id;
      next_z = pG->rx3_id;
      if (prev_z < 0)  prev_z = MPI_PROC_NULL;
      if (next_z < 0)  next_z = MPI_PROC_NULL;
      /* define te_iz and te_oz*/
      for (j=js; j<=je; j++) {
        for (i=is; i<=ie; i++) {
          kk = (i-is) + nxio*(j-js);
          te_iz[kk] = Temp[ks][j][i];
          te_oz[kk] = Temp[ke][j][i];
        }
      }
      MPI_Irecv(&te_izg, nzsq, MPI_DOUBLE, prev_z, tag1, MPI_COMM_WORLD, &reqs[0]);
      MPI_Irecv(&te_ozg, nzsq, MPI_DOUBLE, next_z, tag2, MPI_COMM_WORLD, &reqs[1]);
      MPI_Isend(&te_iz, nzsq, MPI_DOUBLE, prev_z, tag2, MPI_COMM_WORLD, &reqs[2]);
      MPI_Isend(&te_oz, nzsq, MPI_DOUBLE, next_z, tag1, MPI_COMM_WORLD, &reqs[3]);
      MPI_Waitall(4, reqs, stats);
      if (pG->Nx[2] > 1) {
        if (prev_z >= 0) {
          for (j=js; j<=je; j++) {
            for (i=is; i<=ie; i++) {
              kk = (i-is) + nxio*(j-js);
              Temp[ks-1][j][i] = te_izg[kk];
            }
          }
        }
        if (next_z >= 0) {
          for (j=js; j<=je; j++) {
            for (i=is; i<=ie; i++) {
              kk = (i-is) + nxio*(j-js);
              Temp[ke+1][j][i] = te_ozg[kk];
            }
          }
        }
      }
    }

    /* Print MPI cost 
    mpit1 = MPI_Wtime();
    mpicost = mpicost + (mpit1-mpit0); */

    /* (b-2c) compute heat-flux in inner regions */
    HeatFlux_aniso(pD, is, ie, js, je, ks, ke);

    /* (b-3) Operator for yj-1: lclass_jm1[][][] */
    for (k=ks; k<=ke; k++) {
      for (j=js; j<=je; j++) {
        for (i=is; i<=ie; i++) {
          lclass_jm1[k][j][i] = (Q[k][j][i+1].x1 - Q[k][j][i].x1)/pG->dx1;
        }
      }
    }
    if (pG->Nx[1] > 1){
      for (k=ks; k<=ke; k++) {
        for (j=js; j<=je; j++) {
          for (i=is; i<=ie; i++) {
            lclass_jm1[k][j][i] = lclass_jm1[k][j][i]
              +(Q[k][j+1][i].x2 - Q[k][j][i].x2)/pG->dx2;
          }
        }
      }
    }
    if (pG->Nx[2] > 1){
      for (k=ks; k<=ke; k++) {
        for (j=js; j<=je; j++) {
          for (i=is; i<=ie; i++) {
            lclass_jm1[k][j][i] = lclass_jm1[k][j][i]
              +(Q[k+1][j][i].x3 - Q[k][j][i].x3)/pG->dx3;
          }
        }
      }
    }

    /* (is_cyc: c) Computer yj */
    for (k=ks; k<=ke; k++) {
      for (j=js; j<=je; j++) {
        for (i=is; i<=ie; i++) {
          yj[k][j][i] = muj*yjm1[k][j][i]
            + nuj*yjm2[k][j][i] +
            + (1.-muj-nuj)*y0[k][j][i]
            + mujbar*my_dt*lclass_jm1[k][j][i]
            + gammajbar*my_dt*lclass_0[k][j][i];

        }
      }
    }

    /* (is_cyc: d) for the next step */
    ajm1 = aj;
    bjm2 = bjm1;
    bjm1 = bj;
    for (k=ks; k<=ke; k++) {
      for (j=js; j<=je; j++) {
        for (i=is; i<=ie; i++) {
          yjm2[k][j][i] = yjm1[k][j][i];
          yjm1[k][j][i] = yj[k][j][i];
        }
      }
    }

  } /*end sub_cycle in here */

  /* Update results into pGrid */
  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie; i++) {
        pG->U[k][j][i].E = yj[k][j][i];
      }
    }
  }

  /* Print MPI cost 
  if(myid == 0) printf("....Conduction_MPI total cost=%10.3e (s), scyc=%d\n", mpicost, s_cyc);
  */
  return;
}


/*----------------------------------------------------------------------------*/
/*! \fn void HeatFlux_iso(DomainS *pD)
 *  \brief Calculate heat fluxes with isotropic conduction
 */

void HeatFlux_iso(DomainS *pD)
{
  GridS *pG = (pD->Grid);
  int i, is = pG->is, ie = pG->ie;
  int j, js = pG->js, je = pG->je;
  int k, ks = pG->ks, ke = pG->ke;
  Real kd;
  Real kappa_c, kappa_p;
  Real t_avg, b_avg, d_avg;

  /* Add heat fluxes in 1-direction */

  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie+1; i++) {
        kappa_c = Kappa_iso_dependTemp(Temp[k][j][i]);
        kappa_p = Kappa_iso_dependTemp(Temp[k][j][i-1]);
        kd = 2.0*kappa_c*kappa_p/(kappa_c + kappa_p);
        Q[k][j][i].x1 = kd*(Temp[k][j][i] - Temp[k][j][i-1])/pG->dx1;
      }
    }
  }

  /* Add heat fluxes in 2-direction */

  if (pG->Nx[1] > 1) {
    for (k=ks; k<=ke; k++) {
      for (j=js; j<=je+1; j++) {
        for (i=is; i<=ie; i++) {
          kappa_c = Kappa_iso_dependTemp(Temp[k][j][i]);
          kappa_p = Kappa_iso_dependTemp(Temp[k][j-1][i]);
          kd = 2.0*kappa_c*kappa_p/(kappa_c + kappa_p);
          Q[k][j][i].x2 = kd*(Temp[k][j][i] - Temp[k][j-1][i])/pG->dx2;
        }
      }
    }
  }

  /* Add heat fluxes in 3-direction */

  if (pG->Nx[2] > 1) {
    for (k=ks; k<=ke+1; k++) {
      for (j=js; j<=je; j++) {
        for (i=is; i<=ie; i++) {
          kappa_c = Kappa_iso_dependTemp(Temp[k][j][i]);
          kappa_p = Kappa_iso_dependTemp(Temp[k-1][j][i]);
          kd = 2.0*kappa_c*kappa_p/(kappa_c + kappa_p);
          Q[k][j][i].x3 = kd*(Temp[k][j][i] - Temp[k-1][j][i])/pG->dx3;
        }
      }
    }
  }

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn void HeatFlux_aniso(DomainS *pD)
 *  \brief Calculate heat fluxes with anisotropic conduction
 */
void HeatFlux_aniso(DomainS *pD, const int is, const int iem1, 
    const int js, const int jem1,
    const int ks, const int kem1)
{
  GridS *pG = (pD->Grid);
  /*int i, is = pG->is, ie = pG->ie;
    int j, js = pG->js, je = pG->je;
    int k, ks = pG->ks, ke = pG->ke;*/

  /* Chose the computation ranges from intent parameters */
  int i, ie=iem1;
  int j, je=jem1;
  int k, ke=kem1;

  Real Bx,By,Bz,B02,dTc,dTl,dTr,lim_slope,dTdx,dTdy,dTdz,bDotGradT,kd;
  /* Temperature dependent kappa: kbar */
  Real kappa_c, kappa_p, kbar;
#ifdef MHD
  if (pD->Nx[1] == 1) return;  /* problem must be at least 2D */

  /* Compute heat fluxes in 1-direction  --------------------------------------*/

  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie+1; i++) {

        /* Monotonized temperature difference dT/dy */
        dTdy = limiter4(Temp[k][j+1][i  ] - Temp[k][j  ][i  ],
            Temp[k][j  ][i  ] - Temp[k][j-1][i  ],
            Temp[k][j+1][i-1] - Temp[k][j  ][i-1],
            Temp[k][j  ][i-1] - Temp[k][j-1][i-1]);
        dTdy /= pG->dx2;

        /* Monotonized temperature difference dT/dz, 3D problem ONLY */
        if (pD->Nx[2] > 1) {
          dTdz = limiter4(Temp[k+1][j][i  ] - Temp[k  ][j][i  ],
              Temp[k  ][j][i  ] - Temp[k-1][j][i  ],
              Temp[k+1][j][i-1] - Temp[k  ][j][i-1],
              Temp[k  ][j][i-1] - Temp[k-1][j][i-1]);
          dTdz /= pG->dx3;
        }

        /* Add flux at x1-interface, 2D PROBLEM */

        if (pD->Nx[2] == 1) {
          By = 0.5*(pG->U[k][j][i-1].B2c + pG->U[k][j][i].B2c);
          B02 = SQR(pG->B1i[k][j][i]) + SQR(By);
          B02 = MAX(B02,TINY_NUMBER); /* limit in case B=0 */
          bDotGradT = pG->B1i[k][j][i]*(Temp[k][j][i]-Temp[k][j][i-1])/pG->dx1
            + By*dTdy;
          /*kd = kappa_aniso*0.5*(pG->U[k][j][i].d + pG->U[k][j][i-1].d);*/
          kappa_c = Kappa_aniso_dependTemp(Temp[k][j][i]);
          kappa_p = Kappa_aniso_dependTemp(Temp[k][j][i-1]);
          kbar = 2.0*kappa_c*kappa_p/(kappa_c + kappa_p);

          Q[k][j][i].x1 = kbar*(pG->B1i[k][j][i]*bDotGradT)/B02;

          /* Add flux at x1-interface, 3D PROBLEM */

        } else {
          By = 0.5*(pG->U[k][j][i-1].B2c + pG->U[k][j][i].B2c);
          Bz = 0.5*(pG->U[k][j][i-1].B3c + pG->U[k][j][i].B3c);
          B02 = SQR(pG->B1i[k][j][i]) + SQR(By) + SQR(Bz);
          B02 = MAX(B02,TINY_NUMBER); /* limit in case B=0 */
          bDotGradT = pG->B1i[k][j][i]*(Temp[k][j][i]-Temp[k][j][i-1])/pG->dx1
            + By*dTdy + Bz*dTdz;
          /*kd = kappa_aniso*0.5*(pG->U[k][j][i].d + pG->U[k][j][i-1].d);*/
          kappa_c = Kappa_aniso_dependTemp(Temp[k][j][i]);
          kappa_p = Kappa_aniso_dependTemp(Temp[k][j][i-1]);
          kbar = 2.0*kappa_c*kappa_p/(kappa_c + kappa_p);

          Q[k][j][i].x1 = kbar*(pG->B1i[k][j][i]*bDotGradT)/B02;
        }
      }
    }
  }

  /* Compute heat fluxes in 2-direction  --------------------------------------*/

  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je+1; j++) {
      for (i=is; i<=ie; i++) {

        /* Monotonized temperature difference dT/dx */
        dTdx = limiter4(Temp[k][j  ][i+1] - Temp[k][j  ][i  ],
            Temp[k][j  ][i  ] - Temp[k][j  ][i-1],
            Temp[k][j-1][i+1] - Temp[k][j-1][i  ],
            Temp[k][j-1][i  ] - Temp[k][j-1][i-1]);
        dTdx /= pG->dx1;

        /* Monotonized temperature difference dT/dz, 3D problem ONLY */
        if (pD->Nx[2] > 1) {
          dTdz = limiter4(Temp[k+1][j  ][i] - Temp[k  ][j  ][i],
              Temp[k  ][j  ][i] - Temp[k-1][j  ][i],
              Temp[k+1][j-1][i] - Temp[k  ][j-1][i],
              Temp[k  ][j-1][i] - Temp[k-1][j-1][i]);
          dTdz /= pG->dx3;
        }

        /* Add flux at x2-interface, 2D PROBLEM */

        if (pD->Nx[2] == 1) {
          Bx = 0.5*(pG->U[k][j-1][i].B1c + pG->U[k][j][i].B1c);
          B02 = SQR(Bx) + SQR(pG->B2i[k][j][i]);
          B02 = MAX(B02,TINY_NUMBER); /* limit in case B=0 */

          bDotGradT = pG->B2i[k][j][i]*(Temp[k][j][i]-Temp[k][j-1][i])/pG->dx2
            + Bx*dTdx;
          /*kd = kappa_aniso*0.5*(pG->U[k][j][i].d + pG->U[k][j-1][i].d);*/
          kappa_c = Kappa_aniso_dependTemp(Temp[k][j][i]);
          kappa_p = Kappa_aniso_dependTemp(Temp[k][j-1][i]);
          kbar = 2.0*kappa_c*kappa_p/(kappa_c + kappa_p);
          Q[k][j][i].x2 = kbar*(pG->B2i[k][j][i]*bDotGradT)/B02;

          /* Add flux at x2-interface, 3D PROBLEM */

        } else {
          Bx = 0.5*(pG->U[k][j-1][i].B1c + pG->U[k][j][i].B1c);
          Bz = 0.5*(pG->U[k][j-1][i].B3c + pG->U[k][j][i].B3c);
          B02 = SQR(Bx) + SQR(pG->B2i[k][j][i]) + SQR(Bz);
          B02 = MAX(B02,TINY_NUMBER); /* limit in case B=0 */
          bDotGradT = pG->B2i[k][j][i]*(Temp[k][j][i]-Temp[k][j-1][i])/pG->dx2
            + Bx*dTdx + Bz*dTdz;
          /*kd = kappa_aniso*0.5*(pG->U[k][j][i].d + pG->U[k][j-1][i].d);*/
          kappa_c = Kappa_aniso_dependTemp(Temp[k][j][i]);
          kappa_p = Kappa_aniso_dependTemp(Temp[k][j-1][i]);
          kbar = 2.0*kappa_c*kappa_p/(kappa_c + kappa_p);
          Q[k][j][i].x2 = kbar*(pG->B2i[k][j][i]*bDotGradT)/B02;
        }
      }
    }
  }

  /* Compute heat fluxes in 3-direction, 3D problem ONLY  ---------------------*/

  if (pD->Nx[2] > 1) {
    for (k=ks; k<=ke+1; k++) {
      for (j=js; j<=je; j++) {
        for (i=is; i<=ie; i++) {

          /* Monotonized temperature difference dT/dx */
          dTdx = limiter4(Temp[k  ][j][i+1] - Temp[k  ][j][i  ],
              Temp[k  ][j][i  ] - Temp[k  ][j][i-1],
              Temp[k-1][j][i+1] - Temp[k-1][j][i  ],
              Temp[k-1][j][i  ] - Temp[k-1][j][i-1]);
          dTdx /= pG->dx1;

          /* Monotonized temperature difference dT/dy */
          dTdy = limiter4(Temp[k  ][j+1][i] - Temp[k  ][j  ][i],
              Temp[k  ][j  ][i] - Temp[k  ][j-1][i],
              Temp[k-1][j+1][i] - Temp[k-1][j  ][i],
              Temp[k-1][j  ][i] - Temp[k-1][j-1][i]);
          dTdy /= pG->dx2;

          /* Add flux at x3-interface, 3D PROBLEM */

          Bx = 0.5*(pG->U[k-1][j][i].B1c + pG->U[k][j][i].B1c);
          By = 0.5*(pG->U[k-1][j][i].B2c + pG->U[k][j][i].B2c);
          B02 = SQR(Bx) + SQR(By) + SQR(pG->B3i[k][j][i]);
          B02 = MAX(B02,TINY_NUMBER); /* limit in case B=0 */
          bDotGradT = pG->B3i[k][j][i]*(Temp[k][j][i]-Temp[k-1][j][i])/pG->dx3
            + Bx*dTdx + By*dTdy;
          /*kd = kappa_aniso*0.5*(pG->U[k][j][i].d + pG->U[k-1][j][i].d);*/
          kappa_c = Kappa_aniso_dependTemp(Temp[k][j][i]);
          kappa_p = Kappa_aniso_dependTemp(Temp[k-1][j][i]);
          kbar = 2.0*kappa_c*kappa_p/(kappa_c + kappa_p);
          Q[k][j][i].x3 = kbar*(pG->B3i[k][j][i]*bDotGradT)/B02;
        }
      }
    }
  }
#endif /* MHD */

  return;
}


/*----------------------------------------------------------------------------*/
/*! \fn void Kappa_aniso_dependTemp(const Real T)
 *  \brief Calculate thermal conduction coeffetions
 */
extern Real Kappa_aniso_dependTemp(const Real T)
{
  Real kappa_spitzer, kappa_parallel;

  /* Dependent characteristic values */
  extern Real Tchar, Pchar, Lchar, Vchar;

  /* Define the temperature ranges */
  extern Real temp_low, temp_high;
  extern Real Tc_low;
  extern int sw_lowt_cool;

  /* Constant */
  Real lnA = 30.0;

  /* Dimensional Te*/
  Real Te;
  Te = T*Tchar;
  if (Te < temp_low) Te = temp_low;
  if (Te > temp_high) Te = temp_high;

  /* T is for dimensionless temperature, and kappa is for dimensionless Kappa */
  kappa_spitzer = 1.84e-10*(pow(Te, 5./2.)/lnA);
  kappa_parallel = kappa_spitzer*Tchar/(Pchar*Lchar*Vchar);

  /* Increase Kappa once T <= Tc_low in solar transition region */
  Real factor;
  if ((sw_lowt_cool == 1) & (Te <= Tc_low) ){
    factor = (Tc_low/Te)*(Tc_low/Te)*(Tc_low/Te);
    kappa_parallel = kappa_parallel*factor;
  }

  return kappa_parallel;
}

/*----------------------------------------------------------------------------*/
/*! \fn void Kappa_iso_dependTemp(const Real T, const Real B, const Real rho)
 *  \brief Calculate thermal conduction coeffetions
 */
extern Real Kappa_iso_dependTemp(const Real T)
{
  Real kappa_vert;
  kappa_vert = 1.0e-5*Kappa_aniso_dependTemp(T);

  /* when B vanish, set bnew > 0 
  bnew = B;
  if(bnew <= 1.0e-06){
    bnew = 1.0e-06;
  }
  vert_coeff = 8.04*pow(lnA/1.66057, 2.0)*1.0e+21
    *pow((rho*rhochar), 2.0)/(pow(Te, 3.0) *pow(bnew*bchar, 2.0));
  if(vert_coeff >= 0.999){
    vert_coeff = 0.999;
  }
  kappa_vert = vert_coeff*kappa_spitzer*tchar/(pchar*lchar*va);*/

  return kappa_vert;
}

/*----------------------------------------------------------------------------*/
/* limiter2 and limiter4: call slope limiters to preserve monotonicity
*/

static Real limiter2(const Real A, const Real B)
{
  /* van Leer slope limiter */
  return vanleer(A,B);

  /* monotonized central (MC) limiter */
  /* return minmod(2.0*minmod(A,B),0.5*(A+B)); */
}

static Real limiter4(const Real A, const Real B, const Real C, const Real D)
{
  return limiter2(limiter2(A,B),limiter2(C,D));
}

/*----------------------------------------------------------------------------*/
/* vanleer: van Leer slope limiter
*/

static Real vanleer(const Real A, const Real B)
{
  if (A*B > 0) {
    return 2.0*A*B/(A+B);
  } else {
    return 0.0;
  }
}

/*----------------------------------------------------------------------------*/
/* minmod: minmod slope limiter
*/

static Real minmod(const Real A, const Real B)
{
  if (A*B > 0) {
    if (A > 0) {
      return MIN(A,B);
    } else {
      return MAX(A,B);
    }
  } else {
    return 0.0;
  }
}

/*----------------------------------------------------------------------------*/
/*! \fn void conduction_init(MeshS *pM)
 *  \brief Allocate temporary arrays
 */

void conduction_init(MeshS *pM)
{
  int nl,nd,size1=1,size2=1,size3=1,Nx1,Nx2,Nx3;

  /* Cycle over all Grids on this processor to find maximum Nx1, Nx2, Nx3 */
  for (nl=0; nl<(pM->NLevels); nl++){
    for (nd=0; nd<(pM->DomainsPerLevel[nl]); nd++){
      if (pM->Domain[nl][nd].Grid != NULL) {
        if (pM->Domain[nl][nd].Grid->Nx[0] > size1){
          size1 = pM->Domain[nl][nd].Grid->Nx[0];
        }
        if (pM->Domain[nl][nd].Grid->Nx[1] > size2){
          size2 = pM->Domain[nl][nd].Grid->Nx[1];
        }
        if (pM->Domain[nl][nd].Grid->Nx[2] > size3){
          size3 = pM->Domain[nl][nd].Grid->Nx[2];
        }
      }
    }
  }

  Nx1 = size1 + 2*nghost;

  if (pM->Nx[1] > 1){
    Nx2 = size2 + 2*nghost;
  } else {
    Nx2 = size2;
  }

  if (pM->Nx[2] > 1){
    Nx3 = size3 + 2*nghost;
  } else {
    Nx3 = size3;
  }
  if ((Temp = (Real***)calloc_3d_array(Nx3,Nx2,Nx1, sizeof(Real))) == NULL)
    goto on_error;
  if ((Q = (Real3Vect***)calloc_3d_array(Nx3,Nx2,Nx1,sizeof(Real3Vect)))==NULL)
    goto on_error;

  return;

on_error:
  conduction_destruct();
  ath_error("[conduct_init]: malloc returned a NULL pointer\n");
}

/*----------------------------------------------------------------------------*/
/*! \fn void conduction_destruct(void)
 *  \brief Free temporary arrays
 */

void conduction_destruct(void)
{
  if (Temp != NULL) free_3d_array(Temp);
  if (Q != NULL) free_3d_array(Q);
  return;
}
#endif /* THERMAL_CONDUCTION */

