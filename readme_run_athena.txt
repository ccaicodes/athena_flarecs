#－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
Name: 
   云台超算运行Athena的简要说明
Author:
   Chengcai Shen & Jun Lin
Update:
   2014-09-17
   2014-10-07

#－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
#------------------------------------------------------------------
1. Athena的目录结构。
#------------------------------------------------------------------
在athena4.2根目录下，有几个目录需要注意：
(1)src/probs/
里边有文件flarecs.c，是我们针对耀斑电流片模拟设置的初始条件和边界条件；
(2)cai_probs/
里边有针对flarecs.c的输入文件athinput.flarecs；也有合并mpi并行计算结果的bash脚本join_onetime.sh和join_vtk_onescence.sh；里边也有提交athena任务的PBS脚本模版和提交合并数据的PBS脚本模版；
(3)bin/
包含每次编译Athena后的可执行文件；

#-------------------------------------------------------------------
2. 编译
#-------------------------------------------------------------------
在athena4.2的根目录下(...***.../athena4.2/)进行编译，执行以下命令:
make clean
./configure --with-problem=flarecs --with-gas=mhd --enable-resistivity --enable-mpi
make all
编译成功后会在...../athena4.2/bin/目录下生成可执行文件athena

说明：因为云台超算里有NAS存储，因此源文件的修改时间会不同，导致make all不能正确识别文件的修改顺序而提示警告。因此每次编译前简单的执行一次make clean，则可以无视该警告.

#-------------------------------------------------------------------
3. 提交athena计算任务
#-------------------------------------------------------------------
一般我会单独建一个目录执行每一次运算，比如新建目录'run_20140916a'，然后将两个文件拷贝到该目录下:athena_mpi_flarecs.pbs和athinput.flarecs。
然后在run_20140916a/目录下执行如下命令提交任务：
qsub :athena_mpi_flarecs.pbs

说明:
(1)athinput.flarecs文件中，我们设置计算的相关参数，例如:
Nx1, Nx2, Nx3是计算网格格点数；
NGrid_x1, NGrid_x2, NGrid_x3设置并行计算单元；
x1min,x1max,x2min,x2max,x3min,x3max设置计算域的大小，沿x-,y-,z-的无量纲的范围；
以及其它参数gamma, beta, eta, 扰动幅度等；

(2)athena_mpi_flarecs.pbs用于提交任务，有三个重要的地方：
athena 可执行文件的位置；
athena 输入文件的位置；
并行计算节点必须和athinput.flarecs中的NGrid_x1, NGrid_x2, NGrid_x3相匹配;

#--------------------------------------------------------------------
4. 合并数据
#--------------------------------------------------------------------
因为mpi并行计算输出多个data文件，所以我们可以使用如下两种脚本来合并vtk格式的数据：
(1)合并一个时刻的数据: join_onetime.sh
执行时有三个参数需要指定：datapath, nodes, timeindex, 以空格分割。例如要合并64个并行节点的计算在time＝35.0的结果，则执行:
./join_onetime.sh ...path to .../run_20140916a/ 64 0350
会在join_onetime.sh的当前目录生成一个合并后的.vtk文件；

(2)合并所有时刻的数据: join_vtk_onescence.sh和PSB脚本joinvtk_job.pbs
在joinvtk_job.pbs中相应地设置好路径，并行节点数，可执行脚本join_vtk_onescence.sh的位置，然后提交该合并任务:
qsub joinvtk_job.pbs
计算结束后会在run_20140916a/目录下生产多个.vtk文件，并在文件名中标示出时间序号。

说明：
因为合并多个文件需要的计算时间较长，必须用PBS提交串行任务，不能直接运行join_vtk_onescence.sh脚本，否则会拖累超算的登录服务器。


