pro plot_saved_var2d,path=path

  ; check input parameters
  if(not keyword_set(path))then begin
    print, 'Error: invailied data path in plot_saved_var2d.'
    stop
  endif

  ; (0-2) figure
  set_plot,'ps'
  psfile = path+'ps_var.ps'
  device,xsize=8.9*2.0/2.54,ysize=8.9*1.5/2.54,/inches,/color,file=psfile
  !p.charsize=1.0
  !p.charthick=1.0
  !p.thick=1.0

  files = file_search(path,'*.sav')
  nfiles = n_elements(files)

  ; chose a index of files
  for ifile = 0, nfiles-1 do begin
    restore, files[ifile]

    ; 2d plane
    a = az_calc(x1, x2, b1, b2)
    ; page 1: rho and p
    plot_rho= contour_2d(d, x1, x2, nlev=20, color_index=5,$
      xtext='!3x', ytext='y', text='Density, t='+string(time,'(f6.2)'), $
      posi=[0.1,0.1,0.48,0.9], /fillc, /colorbar)
    plot_p= contour_2d(p, x1, x2, nlev=20, color_index=33,$
      xtext='!3x', ytext='y', text='Pressure', $
      posi=[0.52,0.1,0.9,0.9], /fillc, /colorbar, /noera)
    plot_a = contour_2d(a, x1, x2, nlev=40, color_index=0,$
      vmax_input = 1.0, vmin_input=0.0,$
      posi=[0.52,0.1,0.9,0.9], /noera)

    ; page 2: vx and vy
    plot_vx= contour_2d(v1, x1, x2, nlev=20, color_index=70,$
      xtext='!3x', ytext='y', text='Vx, t='+string(time,'(f6.2)'), $
      posi=[0.1,0.1,0.48,0.9], /fillc, /colorbar)
    plot_vy= contour_2d(v2, x1, x2, nlev=20, color_index=70,$
      xtext='!3x', ytext='y', text='Vy', $
      posi=[0.52,0.1,0.9,0.9], /fillc, /colorbar, /noera)
    plot_a = contour_2d(a, x1, x2, nlev=40, color_index=0,$
      vmax_input = 1.0, vmin_input=0.0,$
      posi=[0.52,0.1,0.9,0.9], /noera)  

    ; page 3: bx and by
    plot_vx= contour_2d(b1, x1, x2, nlev=15, color_index=0,$
      /sysm, xtext='!3x', ytext='y', text='Bx, t='+string(time,'(f6.2)'), $
      posi=[0.1,0.1,0.48,0.9], /fillc, /colorbar)
    plot_vy= contour_2d(b2, x1, x2, nlev=15, color_index=0,$
      /sysm, xtext='!3x', ytext='y', text='By', $
      posi=[0.52,0.1,0.9,0.9], /fillc, /colorbar, /noera)
    plot_a = contour_2d(a, x1, x2, nlev=40, color_index=0,$
      vmax_input = 1.0, vmin_input=0.0,$
      posi=[0.52,0.1,0.9,0.9], /noera)

    ; page 4; Te
    te = p/d
    plot_vy= contour_2d(te, x1, x2, nlev=15, color_index=39,$
      xtext='!3x', ytext='y', text='Te, t='+string(time,'(f6.2)'), $
      vmax_input = 5.0*0.05, vmin_input=0.02, $
      posi=[0.52,0.1,0.9,0.9], /fillc, /colorbar)
    plot_a = contour_2d(a, x1, x2, nlev=40, color_index=0,$
      vmax_input = 1.0, vmin_input=0.0,$
      posi=[0.52,0.1,0.9,0.9], /noera)
  endfor
  ;----------------------------------------------------------------------------
  ; stop in here
  ;----------------------------------------------------------------------------
  print,'normal stop'
  device,/close
  if(!version.os_family eq 'unix')then begin
    set_plot,'x'
  endif else begin
    set_plot,'win'
  endelse
end
