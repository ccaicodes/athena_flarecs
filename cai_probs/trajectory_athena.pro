;+
;
; NAME:
;   tracjectory_athena
;
; PURPOSE:
;
;   Trace a streamline using step-doubling methods
;
; CALLING SEQUENCE:
;    advance_1st_eluar
;    advance_stepdoubling
;
; INPUTS:
;
;    ps0: initial position to be calculated
;    time0: initial time
;    direct_time: tracing direction.
;    1:
;    -1:
;
; KEYWORD PARAMETERS:
;
;
;
; OUTPUTS:
;    The postion of all points to be traced.
;
;
; MODIFICATION HISTORY:
;
;-

pro trajectory_athena,pos_start,index,time_direction,path_vsav
  common velocity, vx_t0, vy_t0, vx_t1, vy_t1, x, y, tsta, tend
  ;
  ; parameter
  ;
  error_limit = 1.0e-4 ; (dimensionless)
  ;
  ; restore velocity fields
  ;
  vfiles = file_Search(path_vsav, 'velocity*.sav')
  nvfiles = n_elements(vfiles)
  
  ; initial postion
  np = n_elements(pos_start[*,0])
  
  
  ind_sta = index
  if(time_direction eq 1)then ind_end = nvfiles-1
  if(time_direction eq -1)then ind_end = 0
  ;
  ; define output files
  ;
  position_new = pos_start
  
  for index = ind_sta, ind_end, time_direction do begin
    ;
    ; get vx_t0 and vx_t1 depending on t
    ;
    index_0 = ind_sta
    index_1 = ind_sta + time_direction
    
    restore,vfiles(index_0)
    vx_t0 = v1
    vy_t0 = v2
    tsta    = time
    restore,vfiles(index_1)
    vx_t1 = v1
    vy_t1 = v2
    tend    = time
    x     = x1
    y     = x2
    
    interval_t = abs(tend - tsta)
    dgrid = abs(x1(1)-x1(0))
    
    ;
    ; enter loop for each point to be calculated
    ;
    for ip = 0, np-1 do begin
      
      x0 = position_new[ip,0]
      y0 = position_new[ip,1]
      t0 = position_new[ip,2]
      pos0 = [x0, y0, t0]
      advanced_t = 0.0
      while (advanced_t lt interval_t) do begin
        ;
        ; estimate time-step at time: t0
        ;
        vc = func_v_interpol(pos0)
        vmax = sqrt(vc[0]*vc[0] + vc[1]*vc[1])
        dt = 0.1*dgrid/vmax
        dt_last = interval_t - advanced_t
        if(dt ge dt_last)then dt = dt_last
        ;
        ; call step-doublling routine
        ;
        pos0 = [x0, y0, t0]
        pos1 = advance_stepdoubling(pos0, error_limit, dt, time_direction)
        ;
        ; prepare for the next cycle
        ;
        pos0 = pos1
        advanced_t = advanced_t + dt

      endwhile
      ;
      ; save into array
      ;
      for iterm = 0,2 do begin
        position_new[ip, iterm] = pos1[iterm]
      endfor
      velocity_new = position_new
      vc = func_v_interpol(pos1)
      for iterm = 0,1 do begin
        velocity_new[ip, iterm] = vc[iterm]
      endfor
      ;
      ; print screen
      ;
      print, index, ip
    endfor ; end for ip cycle
    
    ;
    ; write results at time-nodes
    ;
    save, position_new, velocity_new, filename=path_vsav+'traj_pos_'+string(index+1, '(I04)')+'.sav'
    index_0 = index_0 + time_direction
    index_0 = index_0 + time_direction
  endfor
  
end

;******************************************************************************
; advance_stepdoubling
;******************************************************************************
function advance_stepdoubling, pos0, error_limit, dt, time_direction
  common velocity, vx_t0, vy_t0, vx_t1, vy_t1, x, y, tsta, tend
  
  ; initial
  swstep2 = 1
  while (swstep2 eq 1) do begin
    ;  assume two step (0.5dt + 0.5dt)
    posha = advance_1st_eluar(pos0,  0.5*dt*time_direction)
    poshb = advance_1st_eluar(posha, 0.5*dt*time_direction)
    
    ;  assume one step (1 dt)
    pos1 = advance_1st_eluar(pos0, dt*time_direction)
    
    ;  Check the difference
    dd = fltarr(2)
    for i = 0, 1 do begin
      dd(i) = abs(pos1(i) - poshb(i))/abs(pos1(i) - pos0(i))
    endfor
    ddmin = min(dd)
    
    if (ddmin le error_limit)then begin
      swstep2 = 0
    endif else begin
      swstep2 = 1
      dt = 0.5d0 * dt
    endelse
  endwhile
  ;
  ;  return ps1 and dt
  ;
  return, pos1
end

function advance_1st_eluar, pos0, dt
  common velocity, vx_t0, vy_t0, vx_t1, vy_t1, x, y, tsta, tend
  
  vc = func_v_interpol(pos0)
  px1 = pos0(0) + dt * vc(0)
  py1 = pos0(1) + dt * vc(1)
  time1 = pos0(2) + dt
  
  return, [px1, py1, time1]
end

;******************************************************************************
; func_v_interpol
;******************************************************************************
function func_v_interpol, pos
  common velocity, vx_t0, vy_t0, vx_t1, vy_t1, x, y, tsta, tend
  xi = pos[0]
  yi = pos[1]
  ti = pos[2]
  
  nx = n_elements(x)
  ny = n_elements(y)
  
  if(x(0) le x(nx-1))then begin
    res = where(x ge xi)
  endif else begin
    res = where(x le xi)
  endelse
  i1 = res[0]-1
  i2 = res[0]
  
  if(y(0) le y(ny-1))then begin
    res = where(y ge yi)
  endif else begin
    res = where(y le yi)
  endelse
  j1 = res[0]-1
  j2 = res[0]
  
  v = func_bilinear(xi, yi, x[i1:i2], y[j1:j2], vx_t0[i1:i2,j1:j2])
  vxi0 = v[0]
  v = func_bilinear(xi, yi, x[i1:i2], y[j1:j2], vx_t1[i1:i2,j1:j2])
  vxi1 = v[0]
  vxi  = (vxi1 - vxi0)*(ti - tsta)/(tend - tsta) + vxi0
  v = func_bilinear(xi, yi, x[i1:i2], y[j1:j2], vy_t0[i1:i2,j1:j2])
  vyi0 = v[0]
  v = func_bilinear(xi, yi, x[i1:i2], y[j1:j2], vy_t1[i1:i2,j1:j2])
  vyi1 = v[0]
  vyi  = (vyi1 - vyi0)*(ti - tsta)/(tend - tsta) + vyi0
  
  return, [vxi, vyi]
end

FUNCTION func_bilinear,px,py,x,y,var
  vi1c = (var[0,1] - var[0,0])*(py - y[0])/(y[1]-y[0]) + var[0,0]
  vi2c = (var[1,1] - var[1,0])*(py - y[0])/(y[1]-y[0]) + var[1,0]
  vc = (vi2c - vi1c)*(px - x[0])/(x[1] - x[0]) + vi1c
  RETURN, vc
END