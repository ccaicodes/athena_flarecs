pro read_plot_athena
  COMMON SHARE1,nx,ny,nz,nvar,nscalars
  COMMON SHARE2,x,y,z
  COMMON SHARE3,time,dt,gamm1,isocs
  COMMON SHARE4,d,e,p,vx,vy,vz,bx,by,bz,s,phi
  common share5,dx,dy,dz,x0,y0,z0
  COMMON SHARE6,bchar,dchar,pchar,tchar,lchar,vchar,timechar
  path = '/home/chshen/Works/workspace/computational/Athena/run_athena/run_athena_20160130c_islington/vtk/'
  time_start = 0.0  ; dimensionless time
  time_dt    = 0.01  ; dimensionless time
  
  ; (0-2) figure
  set_plot,'ps'
  psfile = path+'div_v.ps'
  device,xsize=xsz,ysize=ysz,/inches,/color,file=psfile
  !p.charsize=1.0
  !p.charthick=1.0
  !p.thick=1.0
  ;!p.MULTI=[0,nfile,1]
  loadct,33
  
  ; (1) set mhd_parameters
  B0 = 40.0 ; (Gauss)
  T0 = 2.0e+06 ; (K)
  L0 = 1.0e+4  ; (km)
  mhd_parameters, B0, T0, L0
  
  files = file_search(path,'*.vtk')
  nfiles = n_elements(files)
  for ifile = 0, nfiles-1 do begin
    filename=files[ifile]
    readvtk_mhd,filename,2./3.
    
    print,ifile
    ;--------------------------------------------------------------------------
    ; get x, y
    ;--------------------------------------------------------------------------
    x = fltarr(nx)
    y = fltarr(ny)
    for i = 0, nx-1 do begin
      x(i) = i*dx + x0
    endfor
    for j = 0, ny-1 do begin
      y(j) = j*dy + y0
    endfor
    
    ;--------------------------------------------------------------------------
    ; calculate magnetic vector potential: az
    ;--------------------------------------------------------------------------
    a = fltarr(nx, ny)
    a(nx-1,ny-1) = 0.0
    for mz = 1,ny-1 do begin
      kz = ny-1-mz
      a(nx-1,kz) = a(nx-1,kz+1) + 0.5*(bx(nx-1,kz)+bx(nx-1,kz+1))*(y(kz)-y(kz+1))
    endfor
    for mz = 0,ny-1 do begin
      kz = ny-1-mz
      for mx = 0,nx-1-1 do begin
        kx = nx-1-mx
        a(kx-1,kz) = a(kx,kz) - 0.5*(by(kx,kz)+by(kx-1,kz))*(x(kx-1)-x(kx))
      endfor
    endfor
    
    ; (1) div v
    divv = fltarr(nx,ny)
    for i = 1, nx - 2 do begin
      dx = x(i+1)-x(i-1)
      for j = 1, ny - 2 do begin
        dz = y(j+1)-y(j-1)
        dv1 = vx(i+1,j) - vx(i-1,j)
        dv2 = vy(i,j+1) - vy(i,j-1)
        divv[i,j] = dv1/dx + dv2/dz
      endfor
    endfor
    ; apply boundary
    divv[*,0]    = divv[*,1]
    divv[*,ny-1] = divv[*,ny-2]
    divv[0,*]    = divv[1,*]
    divv[nx-1,*] = divv[nx-2,*]
    
    ; (2) curl v
    curlv = fltarr(nx,ny)
    for i = 1, nx - 2 do begin
      dx = x(i+1)-x(i-1)
      for j = 1, ny - 2 do begin
        dz = y(j+1)-y(j-1)
        dv1 = vx(i,j+1) - vx(i,j-1)
        dv2 = vy(i+1,j) - vy(i-1,j)
        curlv[i,j] = dv1/dz + dv2/dx
      endfor
    endfor
    ; apply boundary
    curlv[*,0]    = curlv[*,1]
    curlv[*,ny-1] = curlv[*,ny-2]
    curlv[0,*]    = curlv[1,*]
    curlv[nx-1,*] = curlv[nx-2,*]
    
    ; (3) div m_ms
    mu0 = 4*!PI*1.0e-7
    m_ms_x = fltarr(nx, ny)
    m_ms_y = fltarr(nx, ny)
    mach   = fltarr(nx, ny)
    v_alfven = fltarr(nx ,ny)
    v_cs   = fltarr(nx, ny)
    v_ms   = fltarr(nx, ny)
    for i = 0, nx-1 do begin
      for j = 0, ny-1 do begin
        v_alfven(i,j) = bchar*SQRT(bx(i,j)^2+by(i,j)^2)/SQRT(mu0*dchar*d(i,j))
        v_cs(i,j) = SQRT(pchar*p(i,j)/(dchar*d(i,j)))
        v_ms(i,j) = SQRT((v_alfven(i,j)^2) + (v_cs(i,j)^2))
        m_ms_x(i,j) = vx(i,j)*vchar/v_ms(i,j)
        m_ms_y(i,j) = vy(i,j)*vchar/v_ms(i,j)
        mach(i,j) = (vchar*SQRT(vx(i,j)^2 + vy(i,j)^2))/v_ms(i,j)
      endfor
    endfor
    
    divmms = fltarr(nx,ny)
    for i = 1, nx - 2 do begin
      dx = (x(i+1)-x(i-1))
      for j = 1, ny - 2 do begin
        dz = (y(j+1)-y(j-1))
        dv1 = m_ms_x(i+1,j) - m_ms_x(i-1,j)
        dv2 = m_ms_y(i,j+1) - m_ms_y(i,j-1)
        divmms[i,j] = dv1/dx + dv2/dz
      endfor
    endfor
    ; apply boundary
    divmms[*,0]    = divmms[*,1]
    divmms[*,ny-1] = divmms[*,ny-2]
    divmms[0,*]    = divmms[1,*]
    divmms[nx-1,*] = divmms[nx-2,*]
    
    ;--------------------------------------------------------------------------
    ; save variables into file
    ;--------------------------------------------------------------------------
    time = timechar*(time_start + ifile*time_dt)
    v1 = vx*vchar
    v2 = vy*vchar
    x1 = x*Lchar
    x2 = y*Lchar
    d  = d*dchar
    p  = p*pchar
    b1 = bx*bchar
    b2 = by*bchar

    ;save, time, x1, x2, v1, v2, d, p, b1, b2, a, divv, divmms, curlv, $
    ;  filename=path+'velocity_'+string(ifile,'(I04)')+'.sav' 
    
    ;--------------------------------------------------------------------------
    ; Test plot
    ;--------------------------------------------------------------------------
    var = p
    varmax = max(var)
    varmin = min(var)
    nlev = 50
    lev = dindgen(nlev)*(varmax-varmin)/double(nlev-1)+varmin
    cgloadct, 33
    contour, var, x, y, Levels=lev, YTitle='!8x!3', XTitle='!8y!3',$
      title='Time='+string(time, '(F8.1)')+'(s)',$
      position=[0.1,0.1,0.9,0.80],/iso,/fill
    contour, a, x, y, nLevels=40,position=[0.1,0.1,0.9,0.80],/iso,/noerase
    cgColorbar, YTICKS=0, Ystyle=4, Range=[varmin, varmax],title='!3Pressure', $
      position=[0.1,0.9,0.65,0.95],/top
      
  endfor
  ;
  print,'normal stop'
  device,/close
  if(!version.os_family eq 'unix')then begin
    set_plot,'x'
  endif else begin
    set_plot,'win'
  endelse
end

;------------------------------------------------------------------------------
; Input arguments:
;    B0, mangetic field (unit: Gauss)
;    T0, temperature (unit: K)
;    L0, Length (unit: km)
PRO mhd_parameters, B0, T0, L0
  COMMON SHARE6,bchar,dchar,pchar,tchar,lchar,vchar,timechar
  ; constant
  mu0 = 4*!PI*1.0e-7
  KB = 1.38e-23 ;(J K^-1)

  ; magnetic, to Tesla
  B0 = B0*1.0e-4 ;(Tesla)
  ; magnetic pressure
  pmag = 0.5*B0*B0/mu0 ;(Pa)
  ; pressure
  p0 = B0*B0/mu0
  ; plasma beta
  beta0 = 0.1 ;(Pa)
  ; gas pressure
  pgas = beta0*pmag ;(Pa)
  ; temperture
  ; input parameter ; (K)
  ; density
  n0 = pgas/(KB*T0) ;(m^-3)
  rho0 = n0*1.6726e-27 ;(kg m^-3)
  ; Alfven speed
  va = B0/SQRT(mu0*rho0)
  ; length, to m
  L0 = L0*1.0e+03 ;(m)
  ; time scale
  time0 = L0/va ;(s)
  
  ; return
  bchar = B0
  dchar = rho0
  pchar = p0
  tchar = T0
  lchar = L0
  vchar = va
  timechar = time0
END

;------------------------------------------------------------------------------
; Procedure READVTK: Reads ATHENA VTK files
;
; Chengcai:
;    read mhd and primary pressure.
;
PRO readvtk_mhd,filename,pfact
  COMMON SHARE1,nx,ny,nz,nvar,nscalars
  COMMON SHARE2,x,y,z
  COMMON SHARE3,time,dt,gamm1,isocs
  COMMON SHARE4,d,e,p,vx,vy,vz,bx,by,bz,s,phi
  common share5,dx,dy,dz,x0,y0,z0
  ; -----------------------------------------
  ; pfact = gamm1 for adiabatic
  ; pfact = isocs for isothermal
  ; leave out parameter to use existing value
  ; -----------------------------------------
  ;
  ; Read header information, which is assumed
  ; to be (roughly) in the following form:
  ;
  ; # vtk DataFile Version 3.0
  ; Really cool Athena data at time = 0.000000e+00
  ; BINARY
  ; DATASET STRUCTURED_POINTS
  ; DIMENSIONS 257 257 513
  ; ORIGIN -5.000000e-02 -5.000000e-02 -1.000000e-01
  ; SPACING 3.906250e-04 3.906250e-04 3.906250e-04
  ; CELL_DATA 33554432
  ; SCALARS density float
  ; LOOKUP_TABLE default
  ; (array of dim[nx,ny,nz])
  ; VECTORS velocity float
  ; (array of dim[3,nx,ny,nz])
  ; SCALARS total_energy float
  ; LOOKUP_TABLE default
  ; (array of dim[nx,ny,nz])
  ; VECTORS cell-centered-B float
  ; (array of dim[3,nx,ny,nz])
  ;
  ; There are differences in the VTK files output from
  ; different versions of Athena and also join_vtk_dump!
  
  string = ' '
  string_array=STRARR(8)
  ndata = LONARR(3)
  
  openr,1,filename
  ; read line 1 (do nothing)
  readf,1,string
  readf,1,string
  string_array=STRSPLIT(string,' ',count=cnt,/EXTRACT)
  if (cnt eq 8) then begin
    reads,string_array[7],time
    print,"Time:", time
  end
  ; read lines 3,4 (do nothing)
  readf,1,string
  readf,1,string
  ; read line 5, get dimensions
  readf,1,string
  string_array=STRSPLIT(string,' ',/EXTRACT)
  reads,string_array[1],nxs
  reads,string_array[2],nys
  reads,string_array[3],nzs
  nx = LONG(nxs)-1
  ny = LONG(nys)-1
  nz = LONG(nzs)-1
  if (ny eq 0) then ny=1
  if (nz eq 0) then nz=1
  print,"nx,ny,nz:",nx,ny,nz
  ; read line 6, get origin
  readf,1,string
  string_array=STRSPLIT(string,' ',/EXTRACT)
  reads,string_array[1],x0
  reads,string_array[2],y0
  reads,string_array[3],z0
  print,"x0,y0,z0:",x0,y0,z0
  ; read line 7, get grid spacing
  readf,1,string
  string_array=STRSPLIT(string,' ',/EXTRACT)
  reads,string_array[1],dx
  reads,string_array[2],dy
  reads,string_array[3],dz
  print,"dx,dy,dz:",dx,dy,dz
  ; read line 8, do nothing
  readf,1,string
  
  
  nvar = 0
  isothermal = 1
  mhd = 0
  while (not eof(1)) do begin
    readf,1,string
    block = matchsechead(string)
    if ((block le 0) and (not eof(1))) then begin
      readf,1,string
      block = matchsechead(string)
    end
    if ((block le 0) and (not eof(1))) then begin
      print,"Unexpected file format"
      stop
    end else begin
      print,string
      if (block lt 20) then begin
        ; SCALARS block
        if (block eq 11) then begin
          readscalarblock,1,d
          nvar = nvar + 1
        end else if (block eq 12) then begin
          readscalarblock,1,e
          isothermal = 0
          nvar = nvar + 1
        end else if (block eq 13) then begin
          readscalarblock,1,p
          isothermal = 0
          nvar = nvar + 1
        end else begin
          readscalarblock,1
          print,"Data from unrecognized SCALARS block not stored."
        end
      end else if (block lt 30) then begin
        ; VECTORS block
        if (block eq 21) then begin
          readvectblock,1,vx,vy,vz
          nvar = nvar + 3
        end else if (block eq 22) then begin
          readvectblock,1,vx,vy,vz
          nvar = nvar + 3
          vx = vx/d
          vy = vy/d
          vz = vz/d
        end else if (block eq 23) then begin
          readvectblock,1,bx,by,bz
          mhd = 1
          nvar = nvar + 3
        end else begin
          readvectblock,1
          print,"Data from unrecognized VECTORS block not stored."
        end
      end else begin
        print,"Unrecognized block type!"
        stop
      end
    end
  endwhile
  
  close,1
  
  if (isothermal eq 0) then begin
    if (mhd eq 0) then begin
      print,"Assuming adiabatic hydro"
    end else begin
      print,"Assuming adiabatic MHD"
    end
  end else begin
    if (mhd eq 0) then begin
      print,"Assuming isothermal hydro"
    end else begin
      print,"Assuming isothermal MHD"
    end
  end
  
  if (isothermal eq 0) then begin
    if (N_Elements(pfact) eq 1) then gamm1 = pfact
    if (N_Elements(gamm1) eq 0) then begin
      print,"Pressure not set because gamm1 undefined!"
    end else begin
      print,"Pressure set assuming gamm1=",gamm1
      if (mhd eq 0) then begin
        p=gamm1*(e-0.5*d*(vx^2+vy^2+vz^2))
      end else begin
        ;        p=gamm1*(e-0.5*d*(vx^2+vy^2+vz^2)-0.5*(bx^2+by^2+bz^2))
      end
    end
  end else begin
    if (N_Elements(pfact) eq 1) then isocs = pfact
    if (N_Elements(isocs) eq 0) then begin
      print,"Pressure not set because isocs undefined!"
    end else begin
      print,"Pressure set assuming isocs=",isocs
      p=isocs*isocs*d
    end
  end
  
END
;------------------------------------------------------------------------------
FUNCTION matchsechead,string

  if (string eq "SCALARS density float") then begin
    return,11
  end else if (string eq "VECTORS velocity float") then begin
    return,21
  end else if (string eq "VECTORS momentum float") then begin
    return,22
  end else if (string eq "SCALARS total_energy float") then begin
    return,12
  end else if (string eq "SCALARS pressure float") then begin
    return,13
  end else if (string eq "VECTORS cell_centered_B float") then begin
    return,23
  end else if (strpos(string, "SCALARS") ne -1) then begin
    return,10
  end else if (strpos(string, "VECTORS") ne -1) then begin
    return,20
  end else if (string eq "") then begin
    return,0
  end else begin
    return,-1
  end
  
END
;------------------------------------------------------------------------------
PRO readvectblock,fp,vectx,vecty,vectz
  COMMON SHARE1,nx,ny,nz,nvar,nscalars
  ; VECTORS block
  
  var=fltarr(3,nx,ny,nz)
  readu,fp,var
  var=SWAP_ENDIAN(temporary(var),/SWAP_IF_LITTLE_ENDIAN)
  
  vectx=temporary(reform(var[0,*,*,*]))
  vecty=temporary(reform(var[1,*,*,*]))
  vectz=temporary(reform(var[2,*,*,*]))
  
END
;------------------------------------------------------------------------------
PRO readscalarblock,fp,var
  COMMON SHARE1,nx,ny,nz,nvar,nscalars
  ; SCALARS block
  
  string = ' '
  readf,fp,string ; "LOOKUP_TABLE default"
  var=fltarr(nx,ny,nz)
  readu,fp,var
  var=SWAP_ENDIAN(temporary(var),/SWAP_IF_LITTLE_ENDIAN)
  
END
;------------------------------------------------------------------------------
