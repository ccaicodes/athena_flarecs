pro plot_saved_var3d,path=path
  ;path = '/home/chshen/Works/workspace/computational/Athena/run_athena/run_athena_20160209b/vtk/'

  ; (0-2) figure
  set_plot,'ps'
  psfile = path+'ps_var.ps'
  device,xsize=8.9*2.0/2.54,ysize=8.9*1.5/2.54,/inches,/color,file=psfile
  !p.charsize=1.0
  !p.charthick=1.0
  !p.thick=1.0

  files = file_search(path,'*.sav')
  nfiles = n_elements(files)

  ; chose a index of files
  for ifile = 0, nfiles-1 do begin
    restore, files[ifile]

    ; 2d plane
    iz = n3/2
    
    plot_rho= contour_2d(d[*,*,iz], x1, x2, nlev=20, color=1,$
      xtext='x', ytext='y', text='Density', posi=[0.1,0.1,0.48,0.9])
    plot_p= contour_2d(p[*,*,iz], x1, x2, nlev=20, color=33,$
      xtext='x', ytext='y', text='Pressure', posi=[0.52,0.1,0.9,0.9], /noera)

    plot_vx= contour_2d(v1[*,*,iz], x1, x2, nlev=20, color=70,$
      xtext='x', ytext='y', text='Vx', posi=[0.1,0.1,0.48,0.9])
    plot_vy= contour_2d(v2[*,*,iz], x1, x2, nlev=20, color=70,$
      xtext='x', ytext='y', text='Vy', posi=[0.52,0.1,0.9,0.9], /noera)
      
    plot_vx= contour_2d(b1[*,*,iz], x1, x2, nlev=20, color=33,$
      /sysm, xtext='x', ytext='y', text='Magnetic Bx', posi=[0.1,0.1,0.48,0.9])
    plot_vy= contour_2d(b2[*,*,iz], x1, x2, nlev=20, color=33,$
      /sysm, xtext='x', ytext='y', text='Magnetic By', posi=[0.52,0.1,0.9,0.9], /noera)
     
    ; return az on the x-y plane
    for icut = 0, 4 do begin
      iz = icut*(n3-1)/4
      az = az_calc(x1, x2, b1[*,*,iz], b2[*,*,iz])
      contour, az, x1, x2, nlevels=25, title='az at '+string(x3[iz],'(f7.4)')
    endfor
    
    ; 2d plane on the x-z plane
    iy = 0
    b12d = fltarr(n1, n3)
    b22d = fltarr(n1, n3)
    b32d = fltarr(n1, n3)
    b12d[0:n1-1,0:n3-1] = b1[0:n1-1,iy,0:n3-1]
    b22d[0:n1-1,0:n3-1] = b2[0:n1-1,iy,0:n3-1]
    b32d[0:n1-1,0:n3-1] = b3[0:n1-1,iy,0:n3-1]
    plot_vx= contour_2d(b12d, x1, x3, nlev=20, color=33,$
      /sysm, xtext='x', ytext='z', text='Magnetic Bx', posi=[0.1,0.1,0.33,0.9])
    plot_vy= contour_2d(b22d, x1, x3, nlev=20, color=33,$
      /sysm, xtext='x', ytext='z', text='Magnetic By', posi=[0.4,0.1,0.63,0.9], /noera)
    plot_vy= contour_2d(b32d, x1, x3, nlev=20, color=33,$
      /sysm, xtext='x', ytext='z', text='Magnetic Bz', posi=[0.7,0.1,0.93,0.9], /noera)
  endfor
  ;----------------------------------------------------------------------------
  ; stop in here
  ;----------------------------------------------------------------------------
  print,'normal stop'
  device,/close
  if(!version.os_family eq 'unix')then begin
    set_plot,'x'
  endif else begin
    set_plot,'win'
  endelse
end
