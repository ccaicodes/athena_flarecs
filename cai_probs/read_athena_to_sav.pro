pro read_athena_to_sav, path=path,time_sta=time_sta, time_dt=time_dt
  COMMON SHARE1,nx,ny,nz,nvar,nscalars
  COMMON SHARE2,x,y,z
  COMMON SHARE3,time,dt,gamm1,isocs
  COMMON SHARE4,d,e,p,vx,vy,vz,bx,by,bz,s,phi
  common share5,dx,dy,dz,x0,y0,z0
  COMMON SHARE6,bchar,dchar,pchar,tchar,lchar,vchar,timechar
  ;path = '/home/chshen/Works/workspace/computational/Athena/run_20141211a/vtk/'
  
  if(keyword_set(time_sta))then begin
    time_start = time_sta
  endif else begin
    time_start = 0.0  ; dimensionless time
  endelse
  if(keyword_set(time_dt))then begin
    time_dt = time_dt
  endif else begin
    time_dt    = 0.5  ; dimensionless time
  endelse

  ; (0-2) figure
  
  ; (1) set mhd_parameters
  B0 = 40.0 ; (Gauss)
  T0 = 2.0e+06 ; (K)
  L0 = 1.0e+4  ; (km)
  mhd_parameters, B0, T0, L0
  
  files = file_search(path,'*.vtk')
  nfiles = n_elements(files)
  for ifile = 0, nfiles-1 do begin
    filename=files[ifile]
    readvtk_mhd,filename,2./3.
    
    print,ifile
    ;--------------------------------------------------------------------------
    ; get x, y
    ;--------------------------------------------------------------------------
    x = fltarr(nx)
    y = fltarr(ny)
    z = fltarr(nz)
    for i = 0, nx-1 do begin
      x(i) = i*dx + x0
    endfor
    for j = 0, ny-1 do begin
      y(j) = j*dy + y0
    endfor
    for k = 0, nz-1 do begin
      z(k) = k*dz + z0
    endfor
    ;--------------------------------------------------------------------------
    ; save variables into file
    ;--------------------------------------------------------------------------
    time = (time_start + ifile*time_dt)
    v1 = vx;*vchar
    v2 = vy;*vchar
    v3 = vz;*vchar
    x1 = x;*Lchar
    x2 = y;*Lchar
    x3 = z;*Lchar
    d  = d;*dchar
    p  = p;*pchar
    b1 = bx;*bchar
    b2 = by;*bchar
    b3 = bz;*bchar
    n1 = nx
    n2 = ny
    n3 = nz

    save, time, n1, n2, n3, x1, x2, x3, v1, v2, v3, d, p, b1, b2, b3, $
      filename=path+'var_'+string(ifile,'(I04)')+'.sav' 
      
  endfor
  ;
  print,'normal stop'
end

;------------------------------------------------------------------------------
; Input arguments:
;    B0, mangetic field (unit: Gauss)
;    T0, temperature (unit: K)
;    L0, Length (unit: km)
PRO mhd_parameters, B0, T0, L0
  COMMON SHARE6,bchar,dchar,pchar,tchar,lchar,vchar,timechar
  ; constant
  mu0 = 4*!PI*1.0e-7
  KB = 1.38e-23 ;(J K^-1)

  ; magnetic, to Tesla
  B0 = B0*1.0e-4 ;(Tesla)
  ; magnetic pressure
  pmag = 0.5*B0*B0/mu0 ;(Pa)
  ; pressure
  p0 = B0*B0/mu0
  ; plasma beta
  beta0 = 0.1 ;(Pa)
  ; gas pressure
  pgas = beta0*pmag ;(Pa)
  ; temperture
  ; input parameter ; (K)
  ; density
  n0 = pgas/(KB*T0) ;(m^-3)
  rho0 = n0*1.6726e-27 ;(kg m^-3)
  ; Alfven speed
  va = B0/SQRT(mu0*rho0)
  ; length, to m
  L0 = L0*1.0e+03 ;(m)
  ; time scale
  time0 = L0/va ;(s)
  
  ; return
  bchar = B0
  dchar = rho0
  pchar = p0
  tchar = T0
  lchar = L0
  vchar = va
  timechar = time0
END

;------------------------------------------------------------------------------
; Procedure READVTK: Reads ATHENA VTK files
;
; Chengcai:
;    read mhd and primary pressure.
;
PRO readvtk_mhd,filename,pfact
  COMMON SHARE1,nx,ny,nz,nvar,nscalars
  COMMON SHARE2,x,y,z
  COMMON SHARE3,time,dt,gamm1,isocs
  COMMON SHARE4,d,e,p,vx,vy,vz,bx,by,bz,s,phi
  common share5,dx,dy,dz,x0,y0,z0
  ; -----------------------------------------
  ; pfact = gamm1 for adiabatic
  ; pfact = isocs for isothermal
  ; leave out parameter to use existing value
  ; -----------------------------------------
  ;
  ; Read header information, which is assumed
  ; to be (roughly) in the following form:
  ;
  ; # vtk DataFile Version 3.0
  ; Really cool Athena data at time = 0.000000e+00
  ; BINARY
  ; DATASET STRUCTURED_POINTS
  ; DIMENSIONS 257 257 513
  ; ORIGIN -5.000000e-02 -5.000000e-02 -1.000000e-01
  ; SPACING 3.906250e-04 3.906250e-04 3.906250e-04
  ; CELL_DATA 33554432
  ; SCALARS density float
  ; LOOKUP_TABLE default
  ; (array of dim[nx,ny,nz])
  ; VECTORS velocity float
  ; (array of dim[3,nx,ny,nz])
  ; SCALARS total_energy float
  ; LOOKUP_TABLE default
  ; (array of dim[nx,ny,nz])
  ; VECTORS cell-centered-B float
  ; (array of dim[3,nx,ny,nz])
  ;
  ; There are differences in the VTK files output from
  ; different versions of Athena and also join_vtk_dump!
  
  string = ' '
  string_array=STRARR(8)
  ndata = LONARR(3)
  
  openr,1,filename
  ; read line 1 (do nothing)
  readf,1,string
  readf,1,string
  string_array=STRSPLIT(string,' ',count=cnt,/EXTRACT)
  if (cnt eq 8) then begin
    reads,string_array[7],time
    print,"Time:", time
  end
  ; read lines 3,4 (do nothing)
  readf,1,string
  readf,1,string
  ; read line 5, get dimensions
  readf,1,string
  string_array=STRSPLIT(string,' ',/EXTRACT)
  reads,string_array[1],nxs
  reads,string_array[2],nys
  reads,string_array[3],nzs
  nx = LONG(nxs)-1
  ny = LONG(nys)-1
  nz = LONG(nzs)-1
  if (ny eq 0) then ny=1
  if (nz eq 0) then nz=1
  print,"nx,ny,nz:",nx,ny,nz
  ; read line 6, get origin
  readf,1,string
  string_array=STRSPLIT(string,' ',/EXTRACT)
  reads,string_array[1],x0
  reads,string_array[2],y0
  reads,string_array[3],z0
  print,"x0,y0,z0:",x0,y0,z0
  ; read line 7, get grid spacing
  readf,1,string
  string_array=STRSPLIT(string,' ',/EXTRACT)
  reads,string_array[1],dx
  reads,string_array[2],dy
  reads,string_array[3],dz
  print,"dx,dy,dz:",dx,dy,dz
  ; read line 8, do nothing
  readf,1,string
  
  
  nvar = 0
  isothermal = 1
  mhd = 0
  while (not eof(1)) do begin
    readf,1,string
    block = matchsechead(string)
    if ((block le 0) and (not eof(1))) then begin
      readf,1,string
      block = matchsechead(string)
    end
    if ((block le 0) and (not eof(1))) then begin
      print,"Unexpected file format"
      stop
    end else begin
      print,string
      if (block lt 20) then begin
        ; SCALARS block
        if (block eq 11) then begin
          readscalarblock,1,d
          nvar = nvar + 1
        end else if (block eq 12) then begin
          readscalarblock,1,e
          isothermal = 0
          nvar = nvar + 1
        end else if (block eq 13) then begin
          readscalarblock,1,p
          isothermal = 0
          nvar = nvar + 1
        end else begin
          readscalarblock,1
          print,"Data from unrecognized SCALARS block not stored."
        end
      end else if (block lt 30) then begin
        ; VECTORS block
        if (block eq 21) then begin
          readvectblock,1,vx,vy,vz
          nvar = nvar + 3
        end else if (block eq 22) then begin
          readvectblock,1,vx,vy,vz
          nvar = nvar + 3
          vx = vx/d
          vy = vy/d
          vz = vz/d
        end else if (block eq 23) then begin
          readvectblock,1,bx,by,bz
          mhd = 1
          nvar = nvar + 3
        end else begin
          readvectblock,1
          print,"Data from unrecognized VECTORS block not stored."
        end
      end else begin
        print,"Unrecognized block type!"
        stop
      end
    end
  endwhile
  
  close,1
  
  if (isothermal eq 0) then begin
    if (mhd eq 0) then begin
      print,"Assuming adiabatic hydro"
    end else begin
      print,"Assuming adiabatic MHD"
    end
  end else begin
    if (mhd eq 0) then begin
      print,"Assuming isothermal hydro"
    end else begin
      print,"Assuming isothermal MHD"
    end
  end
  
  if (isothermal eq 0) then begin
    if (N_Elements(pfact) eq 1) then gamm1 = pfact
    if (N_Elements(gamm1) eq 0) then begin
      print,"Pressure not set because gamm1 undefined!"
    end else begin
      print,"Pressure set assuming gamm1=",gamm1
      if (mhd eq 0) then begin
        p=gamm1*(e-0.5*d*(vx^2+vy^2+vz^2))
      end else begin
        ;        p=gamm1*(e-0.5*d*(vx^2+vy^2+vz^2)-0.5*(bx^2+by^2+bz^2))
      end
    end
  end else begin
    if (N_Elements(pfact) eq 1) then isocs = pfact
    if (N_Elements(isocs) eq 0) then begin
      print,"Pressure not set because isocs undefined!"
    end else begin
      print,"Pressure set assuming isocs=",isocs
      p=isocs*isocs*d
    end
  end
  
END
;------------------------------------------------------------------------------
FUNCTION matchsechead,string

  if (string eq "SCALARS density float") then begin
    return,11
  end else if (string eq "VECTORS velocity float") then begin
    return,21
  end else if (string eq "VECTORS momentum float") then begin
    return,22
  end else if (string eq "SCALARS total_energy float") then begin
    return,12
  end else if (string eq "SCALARS pressure float") then begin
    return,13
  end else if (string eq "VECTORS cell_centered_B float") then begin
    return,23
  end else if (strpos(string, "SCALARS") ne -1) then begin
    return,10
  end else if (strpos(string, "VECTORS") ne -1) then begin
    return,20
  end else if (string eq "") then begin
    return,0
  end else begin
    return,-1
  end
  
END
;------------------------------------------------------------------------------
PRO readvectblock,fp,vectx,vecty,vectz
  COMMON SHARE1,nx,ny,nz,nvar,nscalars
  ; VECTORS block
  
  var=fltarr(3,nx,ny,nz)
  readu,fp,var
  var=SWAP_ENDIAN(temporary(var),/SWAP_IF_LITTLE_ENDIAN)
  
  vectx=temporary(reform(var[0,*,*,*]))
  vecty=temporary(reform(var[1,*,*,*]))
  vectz=temporary(reform(var[2,*,*,*]))
  
END
;------------------------------------------------------------------------------
PRO readscalarblock,fp,var
  COMMON SHARE1,nx,ny,nz,nvar,nscalars
  ; SCALARS block
  
  string = ' '
  readf,fp,string ; "LOOKUP_TABLE default"
  var=fltarr(nx,ny,nz)
  readu,fp,var
  var=SWAP_ENDIAN(temporary(var),/SWAP_IF_LITTLE_ENDIAN)
  
END
;------------------------------------------------------------------------------
