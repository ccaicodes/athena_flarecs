pro plot_divv_streamline

  path = '/Users/ccai/Works/MHD_simulations/run_athena/run_20150322c_t50_dt005/vtk/'
  
  ; (0-2) figure
  set_plot,'ps'
  psfile = path+'div_v.ps'
  device,xsize=xsz,ysize=ysz,/inches,/color,file=psfile
  !p.charsize=1.0
  !p.charthick=1.0
  !p.thick=1.0
  ;!p.MULTI=[0,nfile,1]
  loadct,33
  
  files = file_search(path,'velocity_*.sav')
  nfiles = n_elements(files)
  
  ; chose a index of files
  ifile = 43
  restore, files[ifile]
  ;save, time, x1, x2, v1, v2, filename=path+'velocity_'+string(ifile,'(I04)')+'.sav'
  x = x1
  y = x2
  vx = v1
  vy = v2
  nx = n_elements(x)
  ny = n_elements(y)
  
  ;----------------------------------------------------------------------------
  ; (1) div v
  ;----------------------------------------------------------------------------
  divv = dblarr(nx,ny)
  for i = 1, nx - 2 do begin
    dx = x(i+1)-x(i-1)
    for j = 1, ny - 2 do begin
      dz = y(j+1)-y(j-1)
      dv1 = vx(i+1,j) - vx(i-1,j)
      dv2 = vy(i,j+1) - vy(i,j-1)
      divv[i,j] = dv1/dx + dv2/dz
    endfor
  endfor
  ; apply boundary
  divv[*,0]    = divv[*,1]
  divv[*,ny-1] = divv[*,ny-2]
  divv[0,*]    = divv[1,*]
  divv[nx-1,*] = divv[nx-2,*]
  
  ;----------------------------------------------------------------------------
  ; (2) curl v
  ;----------------------------------------------------------------------------
  curlv = dblarr(nx,ny)
  for i = 1, nx - 2 do begin
    dx = x(i+1)-x(i-1)
    for j = 1, ny - 2 do begin
      dz = y(j+1)-y(j-1)
      dv1 = vx(i,j+1) - vx(i,j-1)
      dv2 = vy(i+1,j) - vy(i-1,j)
      curlv[i,j] = dv1/dz + dv2/dx
    endfor
  endfor
  ; apply boundary
  curlv[*,0]    = curlv[*,1]
  curlv[*,ny-1] = curlv[*,ny-2]
  curlv[0,*]    = curlv[1,*]
  curlv[nx-1,*] = curlv[nx-2,*]
  
  ; plot
  contour, vy, x, y, /iso, /fill, nlevels=40
  contour, abs(divv), x, y, /iso, /fill, nlevels=40
  contour, curlv, x, y, /iso, /fill, nlevels=40
  
  ;var = abs(divv)
  ;varmax = max(var)
  ;varmin = min(var)
  ;nlev = 50
  ;lev = dindgen(nlev)*(varmax-varmin)/double(nlev-1)+varmin
  ;contour, var, x, y, Levels=lev, YTitle='!8x!3', XTitle='!8y!3',/fill,$
  ;  position=[0.1,0.1,0.9,0.85],xrange=[-0.35,0.35],yrange=[0.0,0.7],/iso
  
  
  ;----------------------------------------------------------------------------
  ; (3) trace streamlines
  ;----------------------------------------------------------------------------
  res = min(divv, location)
  ind = ARRAY_INDICES(divv, location)
  ix0 = ind(0)
  iy0 = ind(1)
  np = 10
  pos_start = fltarr(np,3)
  for ip = 0, 9 do begin
    pos_start[ip,0] = x[ix0 + ip*2]
    pos_start[ip,1] = y[iy0 + ip*2]
    pos_start[ip,2] = time
  endfor
  
  ; call trace routines
  direction = 1
  call_procedure, 'TRAJECTORY_ATHENA', pos_start,ifile,direction,path
  
  ;----------------------------------------------------------------------------
  ; (4) plot trajectory
  ;----------------------------------------------------------------------------
  files = file_search(path, 'traj_pos_*.sav')
  nfiles = n_elements(files)
  streamlines = fltarr(nfiles, 3, np)
    
  for ifile = 0, nfiles-1 do begin
    restore, files[ifile]
    ; plot
    for ip = 0,np-1 do begin
      xi = position_new[ip, 0]
      yi = position_new[ip, 1]
      plots, xi, yi, psym = 1, color=255*ip/(np-1)
      print, ip, xi, yi
      streamlines[ifile, 0, ip] = xi
      streamlines[ifile, 1, ip] = yi
      streamlines[ifile, 2, ip] = position_new[ip, 2]
      print, position_new[ip, 0:1], velocity_new[ip, 0:1]
    endfor
  endfor
  
  
  ;----------------------------------------------------------------------------
  ; stop in here
  ;----------------------------------------------------------------------------
  print,'normal stop'
  device,/close
  if(!version.os_family eq 'unix')then begin
    set_plot,'x'
  endif else begin
    set_plot,'win'
  endelse
end
