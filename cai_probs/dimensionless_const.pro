pro dimensionless_const, te=te, beta0=beta0,b0=b0,l0=l0, lnA=lnA
  ; constant
  mu0 = 4*!PI*1.0e-7
  KB = 1.38e-23 ;(J K^-1)
    
  ; magnetic
  if(not keyword_set(b0))then begin
    b0 = 10.0*1.0e-4 ;(T)
  endif

  ; magnetic pressure
  pmag = 0.5*b0*b0/mu0 ;(Pa)

  ; pressure (not gas pressure)
  p0 = b0*b0/mu0

  ; plasma beta
  if(not keyword_set(beta0))then begin
    beta0 = 0.1
  endif

  ; gas pressure
  pgas = beta0*pmag ;(Pa)

  ; temperture
  if(not keyword_set(t0))then begin
    t0 = 1.0e+06 ; (K)
  endif

  ; density
  n0 = pgas/(KB*t0) ;(m^-3)
  rho0 = n0*1.6726e-27 ;(kg m^-3)

  ; Alfven speed
  va = b0/sqrt(mu0*rho0)
  ; sound speed
  cs = sqrt(pgas/rho0)
  
  ; length
  if(not keyword_set(l0))then begin
    l0 = 1.0e+08 ;(m)
  endif

  ; time scale
  time0 = l0/va ;(s)

  ; initial maximum themal conductivity
  if(not keyword_set(lnA))then begin
    lnA = 30.0
  endif
  k_spitzer = 1.84e-10/lnA*(t0^2.5)
  kappa_p_nodimen = k_spitzer*t0/(p0*l0*va) 
  kappa_v = (8.04*(lnA^2)*(1.66057^2)*1.0e+21)*(rho0^2/t0^3/b0^2)*k_spitzer
  kappa_v_nodimen = kappa_v*t0/(p0*l0*va)

  ; Three time scale:
  ;(1) alfven-crossing time scale
  tau_a = l0/va
  ;(2) sound-crossing time scale
  tau_s = l0/cs
  ;(3) heat conduction time scale
  tau_c = l0*l0*(n0*kb/k_spitzer)


  print,'---------------------------------------------------'
  print,'B0=',b0, string(' (T)')
  print,'T0=',t0, string(' (K)')
  print,'rho0=', rho0, string(' (kg m^-3)'), n0, string('(m^-3)')
  print,'P0=', p0, string(' (Pa)')
  print,'Beta0=',beta0
  print,'L0=',l0,string('(m)')
  print,'Tau=',time0,string('(s)')
  print,'---------------------------------------------------'

  print,'pmag=',pmag, string(' (Pa)')
  print,'pgas=',pgas,   string(' (Pa)')
  print,'va=',va/1000.0, string(' (km/s)')
  print,'cs=',cs/1000.0, string(' (km/s)')
  print,'---------------------------------------------------'
  
  print,'non-dimensional kappa:'
  print,'lnA = ', lnA
  print,'kappa_spitzer = ', k_spitzer, '(W m^-1 K^-1)'
  print,'kappa_v       = ', kappa_v, '(W m^-1 K^-1)'
  print,'kappa_p_nodimen = ',kappa_p_nodimen
  print,'kappa_v_nodimen = ',kappa_v_nodimen

  print,'---------------------------------------------------'
  print,'Time scale:'
  print,'Alfven-crossing time:',tau_a, '(s)'
  print,'Sound-crossing time:',tau_s,'(s)'
  print,'Heat conduction time:',tau_c,'(s)'
  print,'---------------------------------------------------'
end
